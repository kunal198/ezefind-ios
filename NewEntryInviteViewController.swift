//
//  NewEntryInviteViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/15/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import AddressBook




//http://stackoverflow.com/questions/24752627/accessing-ios-address-book-with-swift-array-count-of-zero




class NewEntryInviteViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{
    
    var finalDictionaryResult = NSMutableDictionary()
    var finalArrayResult = NSMutableArray()
    var ary_ContactListSectionTitles = NSArray()
    var sortedKeys = [String]()
    
    var contactNumberSelected = String()
    
    var joinedStrings = String()
    
    var indexxPath = NSIndexPath()
    
     var contactNameArrayPhone = [String]()
    
    
    var searchTxt = ""
    let prods = ["abc", "terra", "ar", "fogo"]
    
    
    var searchResults:AnyObject? = [AnyObject]()
    
    
    var isFiltered : Bool!
    
    var array: AnyObject? = [AnyObject]()
    var contactName_Array = [String]()
    var namesArray:[String] = [String]()
    
    @IBOutlet weak var inviteBtnSelection: UIButton!
    
    
    @IBOutlet weak var searchResultTableView: UITableView!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    var strArray = NSMutableArray()
    var ContactNumberStringArray = [String]()

    
    var newArray =  NSMutableArray()

    var sectionAtIndex = String()
    var firstCharacterStr = String()
    
    var contactList: NSArray = NSArray()
    var contactDetails = Dictionary<String,String>()
    
    var strContactNumber = NSMutableArray()
    var arrayBool = [Bool]()
    var packageId = Int()
    
    var contacts = [Dictionary<String,String>]()
    
    var addressBook: ABAddressBookRef?
    
    @IBOutlet var tableView: UITableView!
    
    var contactNameArray = NSMutableArray()
    var contactNumberArray = NSMutableArray()
    
    
    @IBOutlet weak var searchTableView: UITableView!
    
    var sections = NSMutableArray()
    
     var name = ""
     var phone = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        isFiltered = false
        
        //getAddressBookNames()
        
        sections = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
       
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        
        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
        {
            NSLog("requesting access...")
            var emptyDictionary: CFDictionaryRef?
            
            var addressBook = !(ABAddressBookCreateWithOptions(emptyDictionary, nil) != nil)
            
            
            ABAddressBookRequestAccessWithCompletion(addressBook)
            {
                (granted: Bool, error: CFError!) in
                dispatch_async(dispatch_get_main_queue()) {
                    
                    if !granted
                    {
                        println("Just denied")
                        
                        var refreshAlert = UIAlertController(title: "Provide Access", message: "Provide eventnode with access to your contacts. Go to Settings > Privacy > Contacts in iOS8 and enable eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                            
                        }))
                        
                        self.presentViewController(refreshAlert, animated: true, completion: nil)
                        
                    }
                    else
                    {
                        
                        
                        
                    }
                }
            }
            
            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted)
        {
            NSLog("access denied")
            
            var refreshAlert = UIAlertController(title: "Provide Access", message: "Provide eventnode with access to your contacts. Go to Settings > Privacy > Contacts in iOS8 and enable eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Authorized)
        {
            processContactNames()
        }
    }
    
    
    
    
    
    func getAddressBookNames()
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
        {
            NSLog("requesting access...")
            var emptyDictionary: CFDictionaryRef?
            var addressBook = !(ABAddressBookCreateWithOptions(emptyDictionary, nil) != nil)
            ABAddressBookRequestAccessWithCompletion(addressBook,{success, error in
                if success
                {
                    self.getContactNames();
                }
                else
                {
                    NSLog("unable to request access")
                }
            })
        }
        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted)
        {
            NSLog("access denied")
        }
        else if (authorizationStatus == ABAuthorizationStatus.Authorized)
        {
            NSLog("access granted")
            getContactNames()
        }
    }
    
    
    
    
    
    
    func getContactNames()
    {
        var errorRef: Unmanaged<CFError>?
        var addressBook: ABAddressBookRef? = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        
       contactList = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue()
        
        println("number of contacts: \(contactList.count)")
        
        for record:ABRecordRef in contactList
        {
            var contactName: String = ABRecordCopyCompositeName(record).takeRetainedValue() as NSString as String

        }
    }

    
    
    
    
    
    
    
    
    
    func processContactNames()
    {
        var errorRef: Unmanaged<CFError>?
        
        var addressBook: ABAddressBookRef? = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        
        contactList = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue()
        
        println("records in the array \(contactList.count)")
        

        for record:ABRecordRef in contactList
        {
        
          var contactDetails: NSDictionary! = processAddressbookRecord(record)
            
        }
        
    
        
        if let people = ABAddressBookCopyArrayOfAllPeople(addressBook)?.takeRetainedValue() as? NSArray
        {
            
            for person in people
            {
                
                if let cname = ABRecordCopyCompositeName(person )?.takeRetainedValue() as? String
                {
                    name = cname
                    
                }
                
                
                let numbers:ABMultiValue = ABRecordCopyValue(
                    person, kABPersonPhoneProperty).takeRetainedValue()
                
                for ix in 0 ..< ABMultiValueGetCount(numbers)
                {
                    //let label = ABMultiValueCopyLabelAtIndex(numbers,ix).takeRetainedValue() as String
                    let value = ABMultiValueCopyValueAtIndex(numbers,ix).takeRetainedValue() as! String
                    
                    phone = value
                   
                    contactDetails["name"] = name
                    contactDetails["phone"] = phone
                    
                    
                    var dictionary = NSMutableDictionary()
                    dictionary.setValue(name, forKey: "name")
                    dictionary.setValue(phone, forKey: "phone")
                    strArray.addObject(dictionary)
                    
                    
            }
            
        }
            
       for var i = 0 ; i < sections.count ; i++
       {
            sectionAtIndex = (self.sections[i] as? String)!

          // println("section value = \(sectionAtIndex)")
        
         finalArrayResult = []
        
        
          for var j = 0 ; j < strArray.count ; j++
          {
            
            let nameStr:String = strArray[j]["name"] as! String
            
           // println("name is = \(nameStr)")
            
            var firstChar = Array(nameStr)[0]
            
           // println("first character = \(firstChar)")
            
           firstCharacterStr = toString(firstChar)
            
            // println("first String from character = \(firstCharacterStr)")
            
             
            if sectionAtIndex == firstCharacterStr
            {
               
                let nameStr:String = strArray[j]["name"] as! String
                
               // println("nameStr = \(nameStr)")
            
                //contacts.addObject(nameStr)
                
               // contacts.append(strArray[j] as! Dictionary<String, String>)
                
                finalArrayResult.addObject(strArray[j])
                
                tableView.reloadData()
                
            }
            
            
            
          }
        
        
//        println("final array result = \(finalArrayResult)")
//        println("section at index = \(sectionAtIndex)")
        
       finalDictionaryResult .setValue(finalArrayResult, forKey: sectionAtIndex)
            
//       println("final dictionary = \(finalDictionaryResult)")
        
       sortedKeys = (finalDictionaryResult.allKeys as! [String]).sorted(<)
        
        
       }
            
            for var i = 0 ; i < strArray.count ; i++
            {
                arrayBool.append(false)
                
            }

         tableView.reloadData()
        
//         println("contacts sorted are = \(contacts)")
//         println("str array = \(strArray)")
        
        namesArray = strArray.valueForKey("name") as! [String]
            
//        println("names array = \(namesArray)")
//        println("all keys = \(ary_ContactListSectionTitles)")
//        println("sortedKeys array = \(sortedKeys)"
        
      }
        
        
    }
    
    
    
    
    func processAddressbookRecord(addressBookRecord: ABRecordRef)->NSDictionary
    {
        
        if var contactName: String = ABRecordCopyCompositeName(addressBookRecord)?.takeRetainedValue() as? String
        {
            
            
            var contactDetails: NSDictionary! = ["contactName":contactName, "contactNumber":""]
            
            return contactDetails
        }
        else
        {
            var emailAddresses: Array<String>!
            emailAddresses = []
            
            
            var contactDetails: NSDictionary! = ["contactName":"", "contactNumber":""]
            
            
            return contactDetails
        }
        
    }
    
    
    
    
    func extractABAddressBookRef(abRef: Unmanaged<ABAddressBookRef>!) -> ABAddressBookRef?
    {
        
        if let ab = abRef
        {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeUnretainedValue()
        }
        
        return nil
    }
    

    
    
    //MARK:- TextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        //println("search array = \(namesArray)")
     

//        searchResults = namesArray.filter({(coisas:String) -> Bool in
//            let stringMatch = coisas.rangeOfString(self.searchTextField.text)
//            return stringMatch != nil
//            
//        })
        

        textField.resignFirstResponder()
        
        return true
    }
    
    
    
    
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        
        if searchTextField.text != ""
        {
            searchTxt = searchTextField.text
            
            println(searchTxt)
            
            
            //            searchResults = namesArray.filter({(produtos:String) -> Bool in
            //
            //                let nameMatch = produtos.rangeOfString(self.searchTxt, options: NSStringCompareOptions.CaseInsensitiveSearch)
            //
            //                println(nameMatch)
            //
            //                return nameMatch != nil})
            
            isFiltered = true
            
            
            let resultPredicate = NSPredicate(format: "name contains[c] %@", searchTxt)
            searchResults = strArray.filteredArrayUsingPredicate(resultPredicate)
            
            
            let testCompositeArray:NSMutableArray = NSMutableArray()
            
           // println("search resultss = \(searchResults)")
            
            
            for var i = 0 ; i < searchResults?.count ; i++
            {
                testCompositeArray.addObject(searchResults![i])
                
            }
            
            //println("search for convert resultss = \(testCompositeArray)")
            
            
            for var i = 0 ; i < sections.count ; i++
            {
                sectionAtIndex = (self.sections[i] as? String)!
                
                finalArrayResult = []
                
                for var j = 0 ; j < testCompositeArray.count ; j++
                {
                    
                    let nameStr:String = testCompositeArray[j]["name"] as! String
                    
                    var firstChar = Array(nameStr)[0]
                    
                    firstCharacterStr = toString(firstChar)
                    
                    
                    if sectionAtIndex == firstCharacterStr
                    {
                        
                        let nameStr:String = testCompositeArray[j]["name"] as! String
                        
                        finalArrayResult.addObject(testCompositeArray[j])
                        
                        tableView.reloadData()
                        
                    }
                    
                    
                    
                }
                
                
                finalDictionaryResult .setValue(finalArrayResult, forKey: sectionAtIndex)
                
                sortedKeys = (finalDictionaryResult.allKeys as! [String]).sorted(<)
                
                
            }
            
            for var i = 0 ; i < strArray.count ; i++
            {
                arrayBool.append(false)
                
            }
            
            tableView.reloadData()
            
        }
        else
        {
            for var i = 0 ; i < sections.count ; i++
            {
                sectionAtIndex = (self.sections[i] as? String)!
                
                finalArrayResult = []
            
                for var j = 0 ; j < strArray.count ; j++
                {
                    
                    let nameStr:String = strArray[j]["name"] as! String
                    
                    var firstChar = Array(nameStr)[0]
                    
                    firstCharacterStr = toString(firstChar)
                    
                    if sectionAtIndex == firstCharacterStr
                    {
                        
                        let nameStr:String = strArray[j]["name"] as! String
                        
                        finalArrayResult.addObject(strArray[j])
                        
                        tableView.reloadData()
                        
                    }
                    
                    
                    
                }
                
                
                finalDictionaryResult .setValue(finalArrayResult, forKey: sectionAtIndex)
                
                sortedKeys = (finalDictionaryResult.allKeys as! [String]).sorted(<)
                
                
            }
            
            for var i = 0 ; i < strArray.count ; i++
            {
                arrayBool.append(false)
                
            }
            
            tableView.reloadData()
        }

    }
    
    


    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    

    
     //MARK:- TableView Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {

        array = finalDictionaryResult.valueForKey(sections[section] as! String)
            
        return array!.count
       
    }
    
    
   
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var inviteCell:NewEntryInviteTableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as? NewEntryInviteTableViewCell
        
        
        inviteCell?.radioBtn.setBackgroundImage(UIImage(named: "ic_radiocheck.png"), forState: UIControlState.Selected)
        
        inviteCell?.radioBtn.setBackgroundImage(UIImage(named: "ic_radiouncheck.png"), forState: UIControlState.Normal)
        
            inviteCell!.radioBtn.tag = indexPath.row
            
            
            if arrayBool[indexPath.row] == true
            {
                
            }
            else
            {
                
            }
        
        
            var contactNumber = String()
        
        
            var sectionTitle: String = sections[indexPath.section] as! String
            contactName_Array = (finalDictionaryResult[sectionTitle]!.valueForKey("name") as? [String])!
            var contactName: String = contactName_Array[indexPath.row] as! String
            inviteCell!.contactNameLbl.text = contactName
            
            
            var sectionTitlePhone: String = sections[indexPath.section] as! String
            var contactNameArrayPhone: [AnyObject] = (finalDictionaryResult[sectionTitlePhone]!.valueForKey("phone") as? [AnyObject])!
            contactNumber = contactNameArrayPhone[indexPath.row] as! String
            
            inviteCell!.contactNumberLbl.text = contactNumber
        
        
        //println("count = \(strContactNumber)")
        
            if strContactNumber.containsObject(contactNumber)
            {
                inviteCell?.radioBtn.selected = true
            }
            else
            {
                inviteCell?.radioBtn.selected = false

            }
        
            
            return inviteCell!

 
    }
    
    
    
   
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        var sectionTitlePhone: String = sections[indexPath.section] as! String
        
        contactNameArrayPhone = (finalDictionaryResult[sectionTitlePhone]!.valueForKey("phone") as? [String])!
        
        contactNumberSelected = contactNameArrayPhone[indexPath.row] as! String
        
        println("contact number = \(contactNumberSelected)")

    }

    
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
           // println("final dicct = \(finalDictionaryResult)")
            return finalDictionaryResult.count
    }
    
    
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [AnyObject]!
    {

        return self.sections as [AnyObject]

    }
    
    
    func tableView(tableView: UITableView,
        sectionForSectionIndexTitle title: String,
        atIndex index: Int) -> Int
    {
            
            return index
    }
    
    func tableView(tableView: UITableView,
        titleForHeaderInSection section: Int) -> String?
    {
            
            return self.sections[section] as? String
    }
  
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 40
    }
    
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView
    {
//        var header :UITableViewHeaderFooterView = UITableViewHeaderFooterView()
//        
//        header.contentView.backgroundColor = UIColor.grayColor()
        
        var HeaderView: UIView = UIView(frame: CGRectMake(0, 8, UIScreen.mainScreen().bounds.size.width, 50))
        
        HeaderView.backgroundColor = UIColor.whiteColor()

        
        
        
        var titleLineHeader: UILabel = UILabel(frame: CGRectMake(0, 0, self.view.frame.size.width, 8))
        //titleLine.textAlignment = .Left
        
         titleLineHeader.backgroundColor = UIColor(red: 52/255, green: 117/255, blue: 221/255, alpha: 1.0)
        
        //titleLine.backgroundColor = UIColor.blackColor()
        
        HeaderView.addSubview(titleLineHeader)
        
        
        
        var title: UILabel = UILabel(frame: CGRectMake(20, 18, 120, 20))
        title.textAlignment = .Left
        title.textColor = UIColor.blackColor()
        
        //var titleArray = self.sections[section] as? String
        
        title.text = self.sections[section] as? String
        
        
        HeaderView.addSubview(title)
        
        
        var countt: UILabel = UILabel(frame: CGRectMake(150, 18, 70, 20))
        countt.textAlignment = .Right
        countt.textColor = UIColor.blackColor()
       // countt.backgroundColor = UIColor.redColor()
        
        let array = finalDictionaryResult.valueForKey(sections[section] as! String)
        
        
       // var arrayCount = finalDictionaryResult.count
        
        countt.text = toString(array!.count)
        //println("count = \(countt.text)")
        
        HeaderView.addSubview(countt)
        
        
        
        var contactsLbl: UILabel = UILabel(frame: CGRectMake(220, 18, 70, 20))
        contactsLbl.textAlignment = .Right
        contactsLbl.textColor = UIColor.blackColor()
        
        //var titleArray = self.sections[section] as? String
        
        contactsLbl.text = "Contacts"
        contactsLbl.font = UIFont.systemFontOfSize(14)
        
        HeaderView.addSubview(contactsLbl)

        
        
        
        
        var titleLine: UILabel = UILabel(frame: CGRectMake(0, 42, self.view.frame.size.width, 2))
        //titleLine.textAlignment = .Left
       
       // titleLine.backgroundColor = UIColor(red: 71/255, green: 125/255, blue: 211/255, alpha: 1.0)
        
       titleLine.backgroundColor = UIColor.grayColor()

        
        //titleLine.backgroundColor = UIColor.blackColor()
        
        HeaderView.addSubview(titleLine)
        
        
        return HeaderView
    }
    
    
    
    //MARK:- Radio Button
    
    @IBAction func radioBtn(sender: AnyObject)
    {
        
//        indexxPath = NSIndexPath(forRow: sender.tag, inSection: 0)
//        
//        var cell = tableView.cellForRowAtIndexPath(indexxPath) as! NewEntryInviteTableViewCell
//        
//        println(indexxPath)
        
        
        //inviteCell?.radioBtn.selected
        
        if arrayBool[sender.tag] == false
        {
        
            sender.setBackgroundImage(UIImage(named: "ic_radiocheck.png"), forState: UIControlState.Normal)
            
            println("1")
            
            
            
            println("sender value = \(sender.tag)")
            
            strContactNumber.addObject(contactNumberSelected)
             println(strContactNumber)
            
           
            
            for var i = 0; i < strContactNumber.count; i++
            {
                
                ContactNumberStringArray.append(strContactNumber[i] as! String)
            }
            

//            let locale = NSLocale.currentLocale()
//            if let country = locale.objectForKey(NSLocaleCountryCode) as? String
//            {
//                 print(country)
//                
//                if country == "US"
//                {
//                    print("An American")
//                }
//                else
//                {
//                    print("Not an American")
//                }
//            }
            
            
            
            let lang = NSLocale.currentLocale().localeIdentifier
            
            let langId = NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode) as! String
            let countryId = NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as! String
            
            let language = "\(langId)-\(countryId)"
            
             print(language)
            
        
                

              var stringData = NSString()
                
                // 1
                let filePath = NSBundle.mainBundle().pathForResource("countries",ofType:"json")
                // 2
                var readError:NSError?
                // 3
                if let data = NSData(contentsOfFile:filePath!, options:NSDataReadingOptions.DataReadingUncached, error:&readError)
                {
                    // 4
                    stringData = NSString(data: data, encoding: NSUTF8StringEncoding)!
                    // 5
                    println("  data read: \(stringData)")
                }
                
                
            
//            let tempArray = [stringData .valueForKey("code")]
//            
//             println("tempArray: \(tempArray)")
            
            
            
            
        
            
            joinedStrings = ",".join(ContactNumberStringArray)
            
            println("joined string = \(joinedStrings)")
            
            println("joined string array = \(ContactNumberStringArray)")

        
            inviteBtnSelection.titleLabel?.textColor = UIColor.greenColor()
            
            arrayBool[sender.tag] = true
        }
        else
        {
            sender.setBackgroundImage(UIImage(named: "ic_radiouncheck.png"), forState: UIControlState.Normal)
            
            arrayBool[sender.tag] = false
            
            
            
            println("2")
            
        }

    

    
    }
    
    
    
    
    //MARK:- Navigation Barcode Btn
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(GraphViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }
    
    
    
    //MARK:- Logout Btn
    
    @IBAction func logoutBtn(sender: AnyObject)
    {
        var loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let login = loginStoryboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        
        self.navigationController?.pushViewController(login, animated: false)

        
    }

    
    
    
    //MARK:- Invite User Button
    
    @IBAction func inviteUser(sender: AnyObject)
    {
        inviteUserApi()
    }
    
    
    
    
    //MARK:- Invite User Api
    
    func inviteUserApi()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        

//        
//        strContactNumber = []
//        
//        for var i = 0 ; i < contacts.count ; i++
//        {
//            if arrayBool[i] == true
//            {
//                strContactNumber.append(contacts[i]["phone"]!)
//                println(strContactNumber)
//            }
//            
//        }
        
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&PackageId=%i&Contact=%@&Typed=%@",str,packageId,joinedStrings,strInventoryOrPackageTitle)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/invite-package"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    println("CreatePackage\(dictObj)")
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                      
                    }
                    else
                    {
                        var message = dictObj?.valueForKey("message") as! NSString
                        
                        var alert = UIAlertView(title: "Alert", message: message as String, delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
        

    }
    
    
    
}
