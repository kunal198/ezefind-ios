//
//  ReportDateViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/16/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import EventKit
import Charts

class ReportDateViewController: UIViewController,DSLCalendarViewDelegate
{

    
     var datSr_last = ""
    
    var datSr = ""
    
    var strYearMonthDay = NSString()
    
    var months:[String]!
    
    var yearMonthWeek:[String]!
    
    var yearWeekMonthUnitsSold:[Double] = []
    
      var dateFormat = NSDateFormatter()
    var dateInFormat = NSString()
    
    
     let date = NSDate()
    
     var startDate_Dragging = NSDateComponents()
     var lastDate_Dragging = NSDateComponents()
     let calendar = NSCalendar.currentCalendar()
    
    var reportDateDict:NSDictionary = NSDictionary()
    
    @IBOutlet weak var totalValueGraph_lbl: UILabel!
    @IBOutlet weak var yearBtn_lbl: UIButton!
    @IBOutlet weak var monthBtn_lbl: UIButton!
    @IBOutlet weak var weekBtn_lbl: UIButton!
    
    @IBOutlet weak var dayBtn_lbl: UIButton!
    
    @IBOutlet weak var barChartScrollView: UIScrollView!
    @IBOutlet weak var total_lbl: UILabel!
    @IBOutlet weak var scrollView_main: UIScrollView!
    @IBOutlet weak var blueView: UIView!
    @IBOutlet weak var bothBlueBtn: UIButton!
    @IBOutlet weak var bothBtn: UIButton!
    @IBOutlet weak var packageBlueBtn: UIButton!
    
    @IBOutlet weak var inventoryBlueBtn: UIButton!
    @IBOutlet weak var inventoryBtn: UIButton!
    @IBOutlet var calendarView: DSLCalendarView!
    @IBOutlet var scrollView: UIScrollView!
   
    
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var packageBtn: UIButton!
    
    @IBOutlet var dateBtn: UIButton!
    
    var shouldShowDaysOut = true
    var animationFinished = true
    
    
    @IBOutlet weak var daysLbl: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
         scrollView_main.contentSize.height  = total_lbl.frame.origin.y + total_lbl.frame.size.height+10
        
         scrollView.contentSize.height  = dateBtn.frame.origin.y + dateBtn.frame.size.height+10
        
         barChartScrollView.contentSize.width = barChartView.frame.origin.x + barChartView.frame.size.width
        
        calendarView.delegate = self
        
        strBothInventoryPackage = "package"
        
        
      //  months = ["SHIRTS", "PANTS","BAGS","BINS","SINGLE ITEM","SHIRTS", "PANTS","BAGS","BINS","SINGLE ITEM","SHIRTS", "PANTS","BAGS","BINS","SINGLE ITEM"]
        
       // let unitsSold = [1.0, 1.0, 2.5, 3.0,5.0,1.0, 1.0, 2.5, 3.0,5.0,1.0, 1.0, 2.5, 3.0,5.0]
        
        BarChart()
        
  //      setChart(months, values: unitsSold)
        
        
        
        
        
//        NSDate *today = [NSDate date];
//        NSCalendar *gregorian = [[NSCalendar alloc]
//            initWithCalendarIdentifier:NSGregorianCalendar];
//        NSDateComponents * components =
//            [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:today];
//        NSInteger day = [components day];
//        NSInteger month = [components month];
//        NSInteger year = [components year];
        
    
        println("date = \(date)")
        
        
      
        
        dateFormat.dateFormat = "dd-MM-yyyy"
        
        dateInFormat = dateFormat.stringFromDate(date)
        
        println(dateInFormat)
        
        //dateTitle.text = dateInFormat as String
        
        
        
        println("calendar = \(calendar)")
        
        
        let components = calendar.components(.CalendarUnitHour | .CalendarUnitMinute, fromDate: date)
        println("components = \(components)")
        
        
        let hour = components.hour
        println("hour = \(hour)")
        
        
        let minutes = components.minute
        println("minutes = \(minutes)")
    
        
        
       reportDateApi()
        
    }
    
    
    
    
    
    
    
    //MARK:- BarChart
    
    
    func BarChart()
    {
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.noDataTextDescription = "GIVE REASON"
        barChartView.descriptionText = ""
        
        
        barChartView.drawBarShadowEnabled = false
        barChartView.drawValueAboveBarEnabled = true
        barChartView.maxVisibleValueCount = 60
        barChartView.pinchZoomEnabled = false
        barChartView.drawGridBackgroundEnabled = false
        
        var xaxis = barChartView.xAxis as ChartXAxis
        xaxis.labelPosition = .Bottom
        xaxis.labelFont = UIFont.systemFontOfSize(10)
        
        xaxis.spaceBetweenLabels = 2
        
        
        
        
        var leftAxis = barChartView.leftAxis as ChartYAxis
        
        
        leftAxis.labelFont = UIFont.systemFontOfSize(10)
        leftAxis.labelCount = 4
        leftAxis.valueFormatter = NSNumberFormatter.alloc()
        leftAxis.valueFormatter!.maximumFractionDigits = 1
        leftAxis.valueFormatter!.negativeSuffix = " $"
        leftAxis.valueFormatter!.positiveSuffix = " $"
        leftAxis.labelPosition = .OutsideChart
        leftAxis.spaceTop = 5
        
        
        var rightAxis = barChartView.rightAxis as ChartYAxis
        rightAxis.drawGridLinesEnabled = false
        rightAxis.labelFont = UIFont.systemFontOfSize(10)
        rightAxis.labelCount = 4
        rightAxis.valueFormatter = leftAxis.valueFormatter
        rightAxis.spaceTop = 5
        
        barChartView.legend.position = .BelowChartLeft
        barChartView.legend.form = .Square
        barChartView.legend.formSize = 9.0
        barChartView.legend.font = UIFont(name: "HelveticaNeue-Light", size: 11.0)!
        barChartView.legend.xEntrySpace = 4.0
        
        
    }
    
    
    
    
    
    func setChart(dataPoints:[String], values:[Double])
    {
        
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for  i in 0..<dataPoints.count
            
        {
            let dataEntry = BarChartDataEntry(value:values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
        let chartData = BarChartData(xVals: months, dataSet: chartDataSet)
        barChartView.data = chartData
        
    }
    
    
    
    
    
    func setChartYearMonthWeek(dataPoints:[String], values:[Double])
    {
        
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for  i in 0..<dataPoints.count
            
        {
            let dataEntry = BarChartDataEntry(value:values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
        let chartData = BarChartData(xVals: yearMonthWeek, dataSet: chartDataSet)
        barChartView.data = chartData
        
    }
    
    
    
    
    
    
    
    
    //MARK:- Report Date Api
    
    func reportDateApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&main_filter=%@&Filter_by=%@&StartDate=%@",userId, strBothInventoryPackage,strYearMonthDay,dateInFormat)
        //&EndDate=%@
        
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/date"        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        println("url = \(url)")
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    
                    println("dictionary value = \(dictObj)")
                    
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        
                        alert.show()
                        
                        spinningIndicator.hide(true)
                        
                        
                    }
                    else
                    {
                        
                        var success = dictObj?.valueForKey("success") as! Bool
                        
                        if success == false
                        {
                            //var dict = dictObj?.valueForKey("success") as! NSDictionary
                            var message = dictObj!.valueForKey("message") as! String
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            
                            var totalValue = dictObj?.valueForKey("Total_Value") as! Int
                            
                            println("total value = \(totalValue)")
                            
                            
                            self.totalValueGraph_lbl.text = NSString(format: "%i", totalValue) as String
                            
                            
                            var Data = dictObj?.valueForKey("Data") as! NSDictionary
                            println("data = \(Data)")
                            
                            
                            
                            
                            self.yearMonthWeek = Data.allKeys as NSArray as! [String]
                            println(self.yearMonthWeek)
                            self.yearWeekMonthUnitsSold = Data.allValues as NSArray as! [Double]
                            println(self.yearWeekMonthUnitsSold)
                            
                            
                            
                            self.setChartYearMonthWeek(self.yearMonthWeek, values: self.yearWeekMonthUnitsSold)
                            
                            self.barChartScrollView.hidden = false
                            
                            self.total_lbl.hidden = false
                            
                            self.blueView.hidden = true
                            
                            self.daysLbl.hidden = false
                            
                            self.totalValueGraph_lbl.hidden = false
                            
                            
                            
                            
                             spinningIndicator.hide(true)
                        }
                        
                       
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }

    
    
    
    
    
    func calendarView(calendarView: DSLCalendarView!, didSelectRange range: DSLCalendarRange!)
    {
        if range != nil
        {
            println("User selected \(range.startDay) \n to \n \(range.endDay)")
            
            //reportDateApi()
        }
    }

    
    
    
    func calendarView(calendarView: DSLCalendarView!, didDragToDay day: NSDateComponents!, selectingRange range: DSLCalendarRange!) -> DSLCalendarRange!
    {
        
        
        
        startDate_Dragging = range.startDay
        
        println("start date = \(startDate_Dragging)")

        
        
//        //let date = NSDate()
//        let calendar = NSCalendar.currentCalendar()
//        let components = calendar.components(.CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: date)
//        
//        let year =  components.year
//        let month = components.month
//        let day = components.day
//        
//        println("year = \(year)")
//        println("month = \(month)")
//        println("date = \(day)")
//
        
        
        
        var Startcompo = NSDateComponents()
        Startcompo = range.startDay
        
        println("compo = \(Startcompo)")
        
        let year =  Startcompo.year
        let month = Startcompo.month
        let day = Startcompo.day
        
        println("year montha nd date = \(year)-\(month)-\(day)")
        println("month = \(month)")
        println("date = \(day)")
        
          datSr = "\(year)-\(month)-\(day)"
         println("datSr = \(datSr)")

        //println(\(year)-\(month)-\(day))
        
        println()
        
        var sDate = NSDateFormatter()
        sDate.dateStyle = NSDateFormatterStyle.ShortStyle
        
        println("sdate = \(sDate)")
        
            lastDate_Dragging = range.endDay
             println("last date = \(lastDate_Dragging)")
        
        
        
        
        var Lastcompo = NSDateComponents()
        Lastcompo = range.endDay
        
        println("compo = \(Lastcompo)")
        
        let year_last =  Lastcompo.year
        let month_last = Lastcompo.month
        let day_last = Lastcompo.day
        
        
        
        datSr_last = "\(year_last)-\(month_last)-\(day_last)"
        println("datSr = \(datSr_last)")

        
        
        var lDate = NSDateFormatter()
        
        
        println("year last = \(year_last)")
        println("month last= \(month_last)")
        println("date last = \(day_last)")
        

        
            
        return range;

    }
    
    
    
    
    
    func calendarView(calendarView: DSLCalendarView!, didChangeToVisibleMonth month: NSDateComponents!)
    {
         //NSLog(@"Now showing %@", month);
        
        println("Now showing = \(month)")
    }
    
    
    
    
    func calendarView(calendarView: DSLCalendarView!, willChangeToVisibleMonth month: NSDateComponents!, duration: NSTimeInterval)
    {
         //NSLog(@"Will show %@ in %.3f seconds", month, duration);
        
        println("Will show %@ in %.3f seconds =  \(month), \(duration)")
    }
    
    
    
    
    
    
    
    //MARK:- ReportItemFilterApi
    
    func ReportDateFilter()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&main_filter=%@&Filter_by=%@&StartDate=%@&EndDate=%@",userId, strBothInventoryPackage,strYearMonthDay,datSr,datSr_last)
        
        
        
        
       

        
        // var post = NSString(format: "UserId=%i&main_filter=%@",userId, strBothInventoryPackage)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/date"//&UserId=\(userId)&main_filter=\()"
        
        // var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/items"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    println(dictObj)
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                        
                        
                    }
                    else
                    {
                        
                        var success = dictObj?.valueForKey("success") as! Int
                        
                        if success == 0
                        {
                            var message = dictObj!.valueForKey("message") as! String
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            var  dataDic = dictObj?.valueForKey("Data") as! NSDictionary
                            
                            
                            
                            var totalValue = dictObj?.valueForKey("Total_Value") as! Int
                            
                            println("total value = \(totalValue)")
                            
                            
                            self.totalValueGraph_lbl.text = NSString(format: "%i", totalValue) as String
                            
                            
                            
                            println(dataDic.allKeys)
                            
                            if self.strYearMonthDay == "year"
                                
                            {
                                self.yearMonthWeek = dataDic.allKeys as NSArray as! [String]
                                self.yearWeekMonthUnitsSold = dataDic.allValues as NSArray as! [Double]
                                println(self.yearMonthWeek)
                            }
                            
                            
                            if self.strYearMonthDay == "month"
                            {
                                
                                
                                if self.yearMonthWeek != nil
                                {
                                    self.yearMonthWeek.removeAll()
                                }
                                
                                if self.yearWeekMonthUnitsSold != []
                                {
                                    self.yearWeekMonthUnitsSold.removeAll()
                                }
                                
                                
                                self.yearMonthWeek = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
                                println(self.yearMonthWeek)
                                
                                var jan = dataDic.valueForKey("01") as! Int
                                println(jan)
                                
                                var janDouble = Double(jan)
                                
                                
                                var feb = dataDic.valueForKey("02") as! Int
                                println(feb)
                                
                                var febDouble = Double(feb)
                                
                                var mar = dataDic.valueForKey("03") as! Int
                                println(mar)
                                
                                var marDouble = Double(mar)
                                
                                
                                var apr = dataDic.valueForKey("04") as! Int
                                println(apr)
                                
                                var aprDouble = Double(apr)
                                
                                
                                var may = dataDic.valueForKey("05") as! Int
                                println(may)
                                
                                var mayDouble = Double(may)
                                
                                var june = dataDic.valueForKey("06") as! Int
                                println(june)
                                
                                var juneDouble = Double(june)
                                
                                var july = dataDic.valueForKey("07") as! Int
                                println(july)
                                
                                var julyDouble = Double(july)
                                
                                
                                var aug = dataDic.valueForKey("08") as! Int
                                println(aug)
                                
                                var augDouble = Double(aug)
                                
                                var sep = dataDic.valueForKey("09") as! Int
                                println(sep)
                                
                                var sepDouble = Double(sep)
                                
                                
                                var oct = dataDic.valueForKey("10") as! Int
                                println(oct)
                                
                                var octDouble = Double(oct)
                                
                                var nov = dataDic.valueForKey("11") as! Int
                                println(nov)
                                
                                var novDouble = Double(nov)
                                
                                var dec = dataDic.valueForKey("12") as! Int
                                println(dec)
                                
                                var decDouble = Double(dec)
                                
                                
                                
                                
                                self.yearWeekMonthUnitsSold = [janDouble,febDouble,marDouble,aprDouble,mayDouble,juneDouble,julyDouble,augDouble,sepDouble,octDouble,novDouble,decDouble]
                                
                            }
                            
                            
                            
                            
                            if self.strYearMonthDay == "day"
                            {
                                if self.yearMonthWeek != nil
                                {
                                    self.yearMonthWeek.removeAll()
                                }
                                
                                if self.yearWeekMonthUnitsSold != []
                                {
                                    self.yearWeekMonthUnitsSold.removeAll()
                                }
                                
                                //                            self.yearMonthWeek = ["Mon","Tue","Wed","Thur","Fri","Sat","Sun"]
                                //
                                //                            println(self.yearMonthWeek)
                                
                                
                                
                                //                            for item in dataDic.allKeys
                                //                            {
                                //                                self.yearMonthWeek.append((item as! NSString) as String)
                                //
                                //                                println("all keys = \(self.yearMonthWeek)")
                                //                            }
                                
                                
                                
                                
                                self.yearMonthWeek = dataDic.allKeys as NSArray as! [String]
                                println("all keys = \(self.yearMonthWeek)")
                                
                                
                                
                                self.yearWeekMonthUnitsSold = dataDic.allValues as NSArray as! [Double]
                                
                            }
                            
                            
                            
                            
                            println(self.yearWeekMonthUnitsSold)
                            
                            self.setChartYearMonthWeek(self.yearMonthWeek, values: self.yearWeekMonthUnitsSold)
                            
                            spinningIndicator.hide(true)
                        }
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
        
    }
    
    

    
    
    
    
    
  
    @IBAction func dayBtn(sender: AnyObject)
    {

        strYearMonthDay = "day"
        
        ReportDateFilter()
        
    }
    
    
    
    @IBAction func weekBtn(sender: AnyObject)
    {
         strYearMonthDay = "day"
        ReportDateFilter()
    }
    
    
    
    
    
    @IBAction func monthBtn(sender: AnyObject)
    {
         strYearMonthDay = "month"
        ReportDateFilter()
    }
    
    
    
    
    @IBAction func yearBtn(sender: AnyObject)
    {
        strYearMonthDay = "year"
        ReportDateFilter()
    }
    
    
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    
    
    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
        
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
        
    }
    
    
    
    
    //MARK:- Back Button
    
    @IBAction func backBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphTrackViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
    }
    
    //MARK:- Report's Buttons
    
    @IBAction func allBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportAll  = reportStoryboard.instantiateViewControllerWithIdentifier("reportAll") as! ReportAllViewController
        
        self.navigationController?.pushViewController(reportAll, animated: false)
        
        
        
        
    }
    @IBAction func itemsBtn(sender: AnyObject)
    {
        
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportItem  = reportStoryboard.instantiateViewControllerWithIdentifier("reportItem") as! ReportItemViewController
        
        self.navigationController?.pushViewController(reportItem, animated: false)
        
        
        
        
    }
    
    
    @IBAction func membersBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportMember  = reportStoryboard.instantiateViewControllerWithIdentifier("reportMember") as! ReportMemberViewController
        
        self.navigationController?.pushViewController(reportMember, animated: false)
        
        
        
    }
    
    
    @IBAction func categoryBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportCategory  = reportStoryboard.instantiateViewControllerWithIdentifier("reportCategory") as! ReportCategoryViewController
        
        self.navigationController?.pushViewController(reportCategory, animated: false)
        
        
        
    }
    
    @IBAction func ValueBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportValue  = reportStoryboard.instantiateViewControllerWithIdentifier("reportValue") as! ReportValueViewController
        
        self.navigationController?.pushViewController(reportValue, animated: false)
        
        
    }
    
    @IBAction func locationBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportLocation  = reportStoryboard.instantiateViewControllerWithIdentifier("reportLocation") as! ReportLocaionViewController
        
        self.navigationController?.pushViewController(reportLocation, animated: false)
        
    }
    
    
    @IBAction func filterBtn(sender: AnyObject)
    {
        blueView.hidden = false
    }
    
    
    @IBAction func filterRefresh_Btn(sender: AnyObject)
    {
        reportDateApi()
        
        barChartScrollView.hidden = false
        
        total_lbl.hidden = false
        
        blueView.hidden = true
        
        self.daysLbl.hidden = false
        
        totalValueGraph_lbl.hidden = false
    }
    
    
    @IBAction func cross_filterBtn(sender: AnyObject)
    {
         blueView.hidden = true
    }
    
    
    @IBAction func packageBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "package"
        
        
        //barChartScrollView.hidden = false
        
        packageBtn.hidden = true
        packageBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBtn.hidden = false
    }
    
    
    @IBAction func packageBlueBtn(sender: AnyObject)
    {
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true

    }
    
    
    @IBAction func inventoryBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "inventory"
        
       // barChartScrollView.hidden = false
        
        inventoryBlueBtn.hidden = false
        inventoryBtn.hidden = true
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        bothBtn.hidden = false

    }
    
    
    @IBAction func inventoryBlueBtn(sender: AnyObject)
    {
        inventoryBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
 
    }
    
    @IBAction func bothBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "both"
        
        //barChartScrollView.hidden = false
        
        bothBtn.hidden = true
        bothBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBtn.hidden = false
 
    }
    
    @IBAction func bothBlueBtn(sender: AnyObject)
    {
        bothBtn.hidden = false
        bothBlueBtn.hidden = true
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
    }
    
}


    

   


