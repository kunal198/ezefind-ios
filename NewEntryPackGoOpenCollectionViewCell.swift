//
//  NewEntryPackGoOpenCollectionViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 10/9/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class NewEntryPackGoOpenCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var typeLbl: UILabel!
    
    @IBOutlet var dateLbl: UILabel!
    
    @IBOutlet var itemCountLbl: UILabel!
    
    @IBOutlet var cellAddBtn: UIButton!
}
