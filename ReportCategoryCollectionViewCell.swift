//
//  ReportCategoryCollectionViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 12/19/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ReportCategoryCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var shirtsName_lbl: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    
    @IBOutlet weak var filterButton_cell: UIButton!
    
}
