//
//  ReportLocaionViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/16/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import Charts

class ReportLocaionViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate
{

    
    var collectionViewHeight = Float()
    
    var unitsSold:[Double] = []
    
    var bagDouble = Double()
    var bagStr = NSString()
    
    @IBOutlet weak var innerFilterView: UIView!
    
    var months:[String]!
    
    var yearMonthWeek:[String]!
    
    var yearWeekMonthUnitsSold:[String] = []
    
    var arrayDoubleType:[Double] = []
    
    
    var reportLocationDict:NSDictionary = NSDictionary()
    
    
    var reportLocationArray = NSMutableArray()
    
    
    
    @IBOutlet weak var yearBtn: UIButton!
    
    @IBOutlet weak var yearBlueBtn: UIButton!
    
    
    @IBOutlet weak var monthBtn: UIButton!
    
    
    @IBOutlet weak var monthBlueBtn: UIButton!
    
    @IBOutlet weak var weekBtn: UIButton!
    
    @IBOutlet weak var weekBlueBtn: UIButton!
    
    
    @IBOutlet weak var totalCategory_lbl: UILabel!
    @IBOutlet weak var totalShirts_lbl: UILabel!
    @IBOutlet weak var collectionView_location: UICollectionView!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var totalLbl: UILabel!
    @IBOutlet var barScrollView: UIScrollView!
    @IBOutlet var locationScrollView: UIScrollView!
    @IBOutlet var barChartView: BarChartView!
    
    
    @IBOutlet var dateBtn: UIButton!
    
    @IBOutlet var packageBlueBtn: UIButton!
    @IBOutlet var inventoryBlueBtn: UIButton!
    @IBOutlet var bothBlueBtn: UIButton!
    @IBOutlet var bothBtn: UIButton!
    @IBOutlet var inventoryBtn: UIButton!
    @IBOutlet var packageBtn: UIButton!
    @IBOutlet var blueView: UIView!

    
    @IBOutlet weak var outerFilterView: UIView!
    
    
  
    
    @IBOutlet weak var wrapperViewTOtal: UIView!
 
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        scrollView.contentSize.height  = dateBtn.frame.origin.y + dateBtn.frame.size.height+10
        
        locationScrollView.contentSize.height  = wrapperViewTOtal.frame.origin.y + wrapperViewTOtal.frame.size.height+50
        
        
        barScrollView.contentSize.width = barChartView.frame.origin.x + barChartView.frame.size.width
        
//        barChartView.noDataText = "You need to provide data for the chart."
//        barChartView.noDataTextDescription = "GIVE REASON"
//        barChartView.descriptionText = ""
//        barChartView.xAxis.labelPosition = .Bottom
//        
//        months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
//        
//        let unitsSold = [2.0, 4.0, 6.0, 3.0, 2.0, 3.0, 4.0, 5.0, 2.0, 4.0, 5.0, 4.0]
//        
        
        strBothInventoryPackage = "package"
        
        
    //    setChart(months, values: unitsSold)
        
        
        

//        self.wrapperViewTOtal.hidden = false
//        
//        self.totalShirts_lbl.hidden = true
//        self.totalCategory_lbl.hidden = true
        
        BarChart()
        
        reportLocationApi()
        
    }
    
    
    
    
    
    
    
    
    
    
    
    //MARK:- BarChart
    
    
    func BarChart()
    {
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.noDataTextDescription = "GIVE REASON"
        barChartView.descriptionText = ""
        
        
        barChartView.drawBarShadowEnabled = false
        barChartView.drawValueAboveBarEnabled = true
        barChartView.maxVisibleValueCount = 60
        barChartView.pinchZoomEnabled = false
        barChartView.drawGridBackgroundEnabled = false
        
        var xaxis = barChartView.xAxis as ChartXAxis
        xaxis.labelPosition = .Bottom
        xaxis.labelFont = UIFont.systemFontOfSize(10)
        
        xaxis.spaceBetweenLabels = 2
        
        
        
        
        var leftAxis = barChartView.leftAxis as ChartYAxis
        
        
        leftAxis.labelFont = UIFont.systemFontOfSize(10)
        leftAxis.labelCount = 4
        leftAxis.valueFormatter = NSNumberFormatter.alloc()
        leftAxis.valueFormatter!.maximumFractionDigits = 1
        leftAxis.valueFormatter!.negativeSuffix = " $"
        leftAxis.valueFormatter!.positiveSuffix = " $"
        leftAxis.labelPosition = .OutsideChart
        leftAxis.spaceTop = 5
        
        
        var rightAxis = barChartView.rightAxis as ChartYAxis
        rightAxis.drawGridLinesEnabled = false
        rightAxis.labelFont = UIFont.systemFontOfSize(10)
        rightAxis.labelCount = 4
        rightAxis.valueFormatter = leftAxis.valueFormatter
        rightAxis.spaceTop = 5
        
        barChartView.legend.position = .BelowChartLeft
        barChartView.legend.form = .Square
        barChartView.legend.formSize = 9.0
        barChartView.legend.font = UIFont(name: "HelveticaNeue-Light", size: 11.0)!
        barChartView.legend.xEntrySpace = 4.0
        
        
    }
    
    
    
    
    
    
    
    func setChart(dataPoints:[String], values:[Double])
    {
        
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for  i in 0..<dataPoints.count
            
        {
            let dataEntry = BarChartDataEntry(value:values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
        let chartData = BarChartData(xVals: months, dataSet: chartDataSet)
        barChartView.data = chartData
        
        
    }
    
    
    
    func setChartYearMonthWeek(dataPoints:[String], values:[Double])
    {
        
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for  i in 0..<dataPoints.count
            
        {
            let dataEntry = BarChartDataEntry(value:values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
        let chartData = BarChartData(xVals: yearMonthWeek, dataSet: chartDataSet)
        barChartView.data = chartData
        
        
        
        
        
    }
    
    
    

    
    
    //MARK:- Report Location Api

    func reportLocationApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&main_filter=%@",userId, strBothInventoryPackage)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/location"//&UserId=\(userId)&main_filter=\(strBothInventoryPackage)"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in

            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    println(dictObj)
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                        
                        
                    }
                    else
                    {
                        
                        
                        var success = dictObj?.valueForKey("success") as! Bool
                        
                        if success == false
                        {
                            //var dict = dictObj?.valueForKey("success") as! NSDictionary
                            var message = dictObj!.valueForKey("message") as! String
                            var alert = UIAlertView()
                            
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            
                            alert.show()
                            
                            self.totalShirts_lbl.hidden = true
                            self.totalCategory_lbl.hidden = true
                            
                            self.wrapperViewTOtal.hidden = true
                            
                            
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            
                            self.reportLocationArray = dictObj?.valueForKey("Data") as! NSMutableArray
                            self.collectionView_location.reloadData()
                            
                            println( self.reportLocationArray)
                            
                             self.wrapperViewTOtal.hidden = false
                            
                             self.totalCategory_lbl.hidden = false
                            
                            if let total_Items
                                = dictObj?.valueForKey("TotalData") as? Int
                            {
                                self.totalCategory_lbl.text = NSString(format: "%i", total_Items) as String
                                
                                println("total items are = \(self.totalCategory_lbl.text)")
                            }
                            
                            
                            
                            println(self.reportLocationArray.valueForKey("Count")!)
                            println(self.reportLocationArray.valueForKey("location")!)
                            
                            
                            
                            self.yearMonthWeek = self.reportLocationArray.valueForKey("location") as! NSArray as! [String]
                            
                            self.yearWeekMonthUnitsSold = self.reportLocationArray.valueForKey("Count") as! [String]
                            
                            
                            //
                            //                        var toPass : [String] = ["21.0","3.12","23"]
                            //                        var force = [Float]()
                            //
                            //                        for item in toPass {
                            //                            force.append((item as NSString).floatValue)
                            //                        }
                            
                            
                            
                            
                            
                            self.totalShirts_lbl.hidden = false
                            
                            
                            if let totalCategory = self.reportLocationArray.count as? Int
                            {
                                self.totalShirts_lbl.text = NSString(format: "%i", totalCategory) as String
                                
                                println("total categories are = \(self.totalShirts_lbl.text)")
                            }
                            

                            
                            
                            
                            
                            
                            for item in self.yearWeekMonthUnitsSold
                            {
                                self.arrayDoubleType.append((item as NSString).doubleValue)
                                
                                println("array double type = \(self.arrayDoubleType)")
                            }
                            
                            
                            
                            println(self.yearMonthWeek)
                            
                            println(self.yearWeekMonthUnitsSold)
                            
                            // self.unitsSold = self.yearWeekMonthUnitsSold
                            
                            //self.months = ["Bag", "Box", "Single Item", "Bin"]
                            
                            self.unitsSold = self.arrayDoubleType
                            
                            self.months = self.yearMonthWeek
                            
                            self.setChart(self.months, values: self.unitsSold)
                            
                            
                            spinningIndicator.hide(true)

                        }
                        
                        
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }

    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
        
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
        
    }
    
    //MARK:- Back Button
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphTrackViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
    }
    
    //MARK:- Report's Buttons
    
    @IBAction func allBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportAll  = reportStoryboard.instantiateViewControllerWithIdentifier("reportAll") as! ReportAllViewController
        
        self.navigationController?.pushViewController(reportAll, animated: false)
        
        
        
        
    }
    @IBAction func itemsBtn(sender: AnyObject)
    {
        
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportItem  = reportStoryboard.instantiateViewControllerWithIdentifier("reportItem") as! ReportItemViewController
        
        self.navigationController?.pushViewController(reportItem, animated: false)
        
        
        
        
    }
    
    
    @IBAction func membersBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportMember  = reportStoryboard.instantiateViewControllerWithIdentifier("reportMember") as! ReportMemberViewController
        
        self.navigationController?.pushViewController(reportMember, animated: false)
        
        
        
    }
    
    
    @IBAction func categoryBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportCategory  = reportStoryboard.instantiateViewControllerWithIdentifier("reportCategory") as! ReportCategoryViewController
        
        self.navigationController?.pushViewController(reportCategory, animated: false)
        
        
        
    }
    
    
    
    
    @IBAction func ValueBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportValue  = reportStoryboard.instantiateViewControllerWithIdentifier("reportValue") as! ReportValueViewController
        
        self.navigationController?.pushViewController(reportValue, animated: false)

        
    }
    
    
    
    
    @IBAction func dateBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportDate  = reportStoryboard.instantiateViewControllerWithIdentifier("reportDate") as! ReportDateViewController
        
        self.navigationController?.pushViewController(reportDate, animated: false)
        
    }
    
    
    
    //MARK:- CollectionView Methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return reportLocationArray.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! ReportLocationCollectionViewCell
        
       // println("count report dict = \(self.reportLocationDict.count)")
        
        
        println(reportLocationArray[indexPath.row].valueForKey("Count")!)
    
        
        
        if let location = reportLocationArray[indexPath.row].valueForKey("location") as? String
        {
            //strBagItem = location
            cell.locationName_lbl.text = location
            
        }
        
        
        
       // cell.locationValue_label.text  = reportLocationArray[indexPath.row].valueForKey("Count") as? String
        
        
        
      

        
        
        
        if let bagItemCount = reportLocationArray[indexPath.row].valueForKey("Count") as? String
        {
            cell.locationValue_label.text = bagItemCount
            
    
           
            println("location value text = \(cell.locationValue_label.text)")
            
            
            self.bagStr = NSString(format: "%@", cell.locationValue_label.text!)
            
            
            self.bagDouble = (self.bagStr).doubleValue
            
            println(self.bagDouble)
            
            
            
            // self.yearWeekMonthUnitsSold = self.reportLocationArray.valueForKey("Count") as! NSArray as! [Double]
            
            
            //self.yearWeekMonthUnitsSold.append(self.bagDouble)
            
            
            println(self.yearWeekMonthUnitsSold)

           
        }

        
        
        
        
        var collectionViewWidth = self.view.frame.size.width
        
        println("collection view width = \(collectionViewWidth)")
        
        collectionViewHeight = Float((self.reportLocationArray.count/2)*99)
        
        collectionView_location.contentSize.height = CGFloat(collectionViewHeight)
        
        println("collection view Height = \(collectionView_location.contentSize.height)")
        
        
        println(Float(self.reportLocationArray.count/2))
        
//        scrollView.contentSize.height =  CGFloat(collectionViewHeight)
//        
//        // dateBtn.frame.origin.y + dateBtn.frame.size.height+10 +
//        
//        println(scrollView.contentSize.height)
        
        if self.reportLocationArray.count%2 == 0
        {
            collectionView_location.frame = CGRectMake(0,0, self.collectionView_location.frame.size.width, collectionView_location.contentSize.height)
            
            println("collection view frame = \(collectionView_location.frame)")
            
            barScrollView.frame.origin.y = collectionView_location.frame.origin.y + collectionView_location.frame.size.height + 100
            
             println("bar scroll view y = \(barScrollView.frame.origin.y)")
            
            wrapperViewTOtal.frame.origin.y = barScrollView.frame.origin.y + barScrollView.frame.size.height + 30
             println("wrapper view y = \(wrapperViewTOtal.frame.origin.y)")
            
            locationScrollView.contentSize.height =  wrapperViewTOtal.frame.origin.y + wrapperViewTOtal.frame.size.height + 20
            
              println("location scroll view y = \(locationScrollView.contentSize.height)")
            

        }
        else
        {
           collectionView_location.frame = CGRectMake(0,0, self.collectionView_location.frame.size.width, collectionView_location.contentSize.height + 210)
            
            
            barScrollView.frame.origin.y = collectionView_location.frame.origin.y + collectionView_location.frame.size.height + 50
            
            wrapperViewTOtal.frame.origin.y = barScrollView.frame.origin.y + barScrollView.frame.size.height + 30
            
            locationScrollView.contentSize.height =  wrapperViewTOtal.frame.origin.y + wrapperViewTOtal.frame.size.height + 20
            

        }
        
        
        
      //  collectionView_location.frame = CGRectMake(0,0, self.collectionView_location.frame.size.width, collectionView_location.contentSize.height + 150)
        
        
        
        
        return cell

    }
    
    
    
    
    
    //MARK:- Filter Button
    
    @IBAction func filterBtn(sender: AnyObject)
    {
       // blueView.hidden = false
        
        blueView.hidden = false
        innerFilterView.hidden = true
        outerFilterView.hidden = false
        
    }
    
    
    @IBAction func crossBtn(sender: AnyObject)
    {
        blueView.hidden = true
        
    }
    
    @IBAction func packageBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "package"
        packageBtn.hidden = true
        packageBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBtn.hidden = false
    }
    
    
    
    @IBAction func filterRefresh_Btn(sender: AnyObject)
    {
         reportLocationApi()
        blueView.hidden = true
    }
    
    
    
    @IBAction func packageBlueBtn(sender: AnyObject)
    {
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        
    }
    
    @IBAction func inventoryBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "inventory"
        inventoryBlueBtn.hidden = false
        inventoryBtn.hidden = true
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        bothBtn.hidden = false
        
    }
    
    @IBAction func inventoryBlueBtn(sender: AnyObject)
    {
        inventoryBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
    }
    
    
    @IBAction func bothBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "both"
        bothBtn.hidden = true
        bothBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBtn.hidden = false
    }
    
    @IBAction func bothBlueBtn(sender: AnyObject)
    {
        bothBtn.hidden = false
        bothBlueBtn.hidden = true
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        
    }
    
    
    
    
    
    
    
    @IBAction func innerFilterBtn(sender: AnyObject)
    {
        blueView.hidden = false
        innerFilterView.hidden = false
        outerFilterView.hidden = true
    }
    
    
    @IBAction func innerFilterCrossBtn(sender: AnyObject)
    {
        blueView.hidden = true
        innerFilterView.hidden = true
    }
    
    
    @IBAction func innerFilterRefreshBtn(sender: AnyObject)
    {
       blueView.hidden = true
    }
    
    
    
    
    
    //MARK:- Bags,Bins,Item,Boxes Filter Button
    
    
    @IBAction func yearbtn(sender: AnyObject)
    {
        
        strYearMonthDay = "year"
        yearBtn.hidden = true
        yearBlueBtn.hidden = false
        monthBlueBtn.hidden = true
        weekBlueBtn.hidden = true
        monthBtn.hidden = false
        weekBtn.hidden = false
        
        
        
    }
    
    @IBAction func yearBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        monthBlueBtn.hidden = true
        weekBlueBtn.hidden = true
        
        
    }
    
    
    
    @IBAction func monthBtn(sender: AnyObject)
    {
        strYearMonthDay = "month"
        monthBlueBtn.hidden = false
        monthBtn.hidden = true
        weekBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        weekBtn.hidden = false
    }
    
    
    @IBAction func monthBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        monthBlueBtn.hidden = true
        monthBtn.hidden = false
        weekBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        
    }
    
    
    @IBAction func weekBtn(sender: AnyObject)
    {
        strYearMonthDay = "week"
        weekBtn.hidden = true
        weekBlueBtn.hidden = false
        monthBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        monthBtn.hidden = false
    }
    
    
    @IBAction func weekBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        weekBtn.hidden = false
        weekBlueBtn.hidden = true
        monthBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        
        
    }

    

}
