//
//  PickListViewTitleTableViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/26/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class PickListViewTitleTableViewCell: UITableViewCell {

    @IBOutlet var quantityLbl: UILabel!
    @IBOutlet var attachementLbl: UILabel!
    @IBOutlet var descriptionTxtView: UITextView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var picListTitleImage: UIImageView!
    
    @IBOutlet weak var selectEditAndDeleteBtn: UIButton!
    
    @IBOutlet weak var editListEditBtn: UIButton!
    
    @IBOutlet weak var editListDeleteBtn: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
