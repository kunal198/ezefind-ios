//
//  ReportItemCollectionViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 12/28/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ReportItemCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var wrapperBtn: UIButton!
    
    
    @IBOutlet weak var m_label: UILabel!
    
    @IBOutlet weak var eighteen_lbl: UILabel!
    
    @IBOutlet weak var image_Btn: UIButton!
    
    @IBOutlet weak var innerFilter_Btn: UIButton!
    
    
}
