//
//  EditListViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/15/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


var editBackCheck = String()

class EditListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, UIAlertViewDelegate
{

    var packageId = String()
    var packageDataId = Int()

    @IBOutlet var dateTitle: UILabel!
    @IBOutlet var greenTitleLbl: UILabel!
     var greenTitleStr = NSString()
     var indexPath = NSIndexPath()
    @IBOutlet var editItemTableView: UITableView!
    var packageDataItems = [NSDictionary]()
    
    
    var cancelBtnAlertView = UIAlertView()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let dateTitledate = NSDate()
        
        var dateFormatTitle = NSDateFormatter()
        
        dateFormatTitle.dateFormat = "dd-MM-yyyy"
        
        var dateInFormattitle:NSString = dateFormatTitle.stringFromDate(dateTitledate)
        
        println(dateInFormattitle)
        
        dateTitle.text = dateInFormattitle as String
         greenTitleLbl.text  = greenTitleStr as String
        getPackageDataItems()

       
        
    }
    
    
    
    override func viewWillAppear(animated: Bool)
    {
       
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return packageDataItems.count
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let editCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! EditListTableViewCell
        
        editCell.editListEditBtn.layer.borderColor = UIColor.lightGrayColor().CGColor
        editCell.editListDeleteBtn.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        editCell.selectEditDeleteBtn.tag = indexPath.row
        
//        if var urlString  =  packageDataItems[indexPath.row].valueForKey("Image") as? String, imageurl = NSURL(string: NSString(format: "http://beta.brstdev.com/yiiezefind%@", urlString) as String)
//        {
//            if let data = NSData(contentsOfURL: imageurl)
//            {
//                editCell.editListImageView.contentMode = UIViewContentMode.ScaleAspectFit
//                editCell.editListImageView.image = UIImage(data: data)
//            }
//        }
        
        
        
        
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
        
        var profileUrl: String = packageDataItems[indexPath.row].valueForKey("Image") as! String
        
        var url: NSURL = NSURL(string: profileUrl)!
        
        
        editCell.editListImageView.sd_setImageWithURL(url, completed: block)
        
        
        
        
        
        
        
        if var strTitle = packageDataItems[indexPath.row].valueForKey("Title") as? String
        {
            editCell.editListTitle.text = strTitle
        }
        
        if var strDesc = packageDataItems[indexPath.row].valueForKey("Description") as? String
        {
            editCell.editListTxtView.text = strDesc
        }
        
        if var strAttachment = packageDataItems[indexPath.row].valueForKey("AttachmentCount") as? Int
        {
            editCell.editListAttachmentLbl.text = toString(strAttachment)
        }
        
        if var strQuantity = packageDataItems[indexPath.row].valueForKey("Quantity") as? Int
        {
            editCell.editListQuantityLbl.text = toString(strQuantity)
        }
        
        

        
        return editCell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
       
        println(packageId)
        println(packageDataId)
        

        
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDelegate.viewInventoryStoryboard() as UIStoryboard
        
        let packGoPackages = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("packGoPackage") as! PackGoPackagesViewController
        
        packGoPackages.itemId = packageDataItems[indexPath.row].valueForKey("ItemId") as! Int
        packGoPackages.packageDataId = toString(packageDataId)
        packGoPackages.packageId = packageId
        packGoPackages.greenTitleStr = greenTitleStr
        self.navigationController?.pushViewController(packGoPackages, animated: true)

    }
    
    
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        
        editBackCheck = "editBackBtn"
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    
    @IBAction func logoutBtn(sender: AnyObject)
    {
        var loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let login = loginStoryboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        
        self.navigationController?.pushViewController(login, animated: false)
        
        
    }
    
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
    }

    
    
    @IBAction func addlistBtn(sender: AnyObject)
    {
        
//        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
//        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
//        
//        let newEntryAddList = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryAddList") as! AddListViewController
//        
//        self.navigationController?.pushViewController(newEntryAddList, animated: true)
//        
        
        
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(AddListViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                break
            }
        }


        
    }
    
    
    
    
    @IBAction func saveBtn(sender: AnyObject)
    {
        
    }
    
    
    @IBAction func cancelBtn(sender: AnyObject)
    {
        backBtnCheck = "back"
        
        cancelBtnAlertView = UIAlertView(title: "Alert", message: "Are you sure want to cancel", delegate: self, cancelButtonTitle: "NO", otherButtonTitles: "YES")
        cancelBtnAlertView.show()
    }

    
    
    
    //MARK: - AlertView delegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        
        if alertView == cancelBtnAlertView
        {
            if buttonIndex == 1
            {
                
                backBtnCheck = "back"
                
                println("back btn clicked")
                
                
                for controller in self.navigationController!.viewControllers as Array
                {
                    if controller.isKindOfClass(NewEntryGraphViewController)
                    {
                        self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                        break
                    }
                    else if controller.isKindOfClass(NewEntryPackAndGoViewController)
                    {
                        self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                        break
                    }
                    else
                    {
                        
                    }
                    
                }
                
            }
            
            
        }
    }

    
    
    
    
    
    
    
    
    
    //MARK:- Get Package Data Items
    
    func getPackageDataItems()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        
        println(packageId)
        println(packageDataId)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PackageId=%@&PackageDataId=%i",str,packageId,packageDataId)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/getpackagesdataitems")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })
                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            
                            if success
                            {
                                self.packageDataItems = dicObj?.valueForKey("message") as! [NSDictionary]
                                self.editItemTableView.reloadData()
                                spinningIndicator.hide(true)
                                
                            }
                                
                            else
                            {
                                var message = dicObj?.valueForKey("message") as! String
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                spinningIndicator.hide(true)
                                
                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        
        
        
    }

    //MARK:- TableView Button
    
    @IBAction func selectEditAndDelteBtn(sender: AnyObject)
    {
        indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        let cell = editItemTableView.cellForRowAtIndexPath(indexPath) as! EditListTableViewCell!
        cell.selectEditDeleteBtn.hidden = true
        cell.editListEditBtn.hidden = false
        cell.editListDeleteBtn.hidden = false
    }
    
    
    @IBAction func editListEditBtn(sender: AnyObject)
    {
        let cell = editItemTableView.cellForRowAtIndexPath(indexPath) as! EditListTableViewCell!
        cell.selectEditDeleteBtn.hidden = false
        cell.editListEditBtn.hidden = true
        cell.editListDeleteBtn.hidden = true


        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDelegate.viewInventoryStoryboard() as UIStoryboard
        
        let packGoPackages = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("packGoPackage") as! PackGoPackagesViewController
        
        packGoPackages.itemId = packageDataItems[indexPath.row].valueForKey("ItemId") as! Int
        packGoPackages.packageDataId = toString(packageDataId)
        packGoPackages.packageId = packageId
        packGoPackages.greenTitleStr = greenTitleStr
        self.navigationController?.pushViewController(packGoPackages, animated: true)

        
    }
    
    @IBAction func editListDeleteBtn(sender: AnyObject)
    {
        let cell = editItemTableView.cellForRowAtIndexPath(indexPath) as! EditListTableViewCell!
        cell.selectEditDeleteBtn.hidden = false
        cell.editListEditBtn.hidden = true
        cell.editListDeleteBtn.hidden = true
        

        deleteItemApi()
        packageDataItems.removeAtIndex(indexPath.row)
        editItemTableView.reloadData()
        
    }
    
    
    
    //MARK:- Delete Item Api
    
    func deleteItemApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        println(packageId)
        println(packageDataId)
        
       var itemId =  packageDataItems[indexPath.row].valueForKey("ItemId") as! Int
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PackageId=%@&PackageDataId=%i&ItemId=%i",str,packageId,packageDataId,itemId)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/delete-item")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })
                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                           
                            
                                
                          
                                var message = dicObj?.valueForKey("message") as! String
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                spinningIndicator.hide(true)
                                
                                
                          
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        

        
    }
    
}
