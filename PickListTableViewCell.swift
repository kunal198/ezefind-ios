//
//  PickListTableViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/25/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class PickListTableViewCell: UITableViewCell {

    @IBOutlet var deleteBtn: UIButton!
    @IBOutlet var countLbl: UILabel!
    @IBOutlet var startDateLbl: UILabel!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var cellbtn: UIButton!
    @IBOutlet var deleteImage: UIImageView!
    @IBOutlet var shareImage: UIImageView!
    @IBOutlet var addImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    
    

}
