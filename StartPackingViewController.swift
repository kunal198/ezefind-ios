//
//  StartPackingViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/9/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
var strInventoryOrPackageTitle = NSString()
var startPackingTitleStr = NSString()
class StartPackingViewController: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet var addressLbl: UILabel!
    @IBOutlet var nameLbl: UILabel!
    var sizeHeight = UIScreen.mainScreen().bounds.height
    var apiDateFormat = NSDateFormatter()
    var apiTimeFormat = NSDateFormatter()
    var apiDateFormatStr = NSString()
    var apiTimeFormatStr = NSString()
    
    
    @IBOutlet var addressTxtfield: UITextField!
    @IBOutlet var nameTxtfield: UITextField!
    @IBOutlet var timePickerView: UIDatePicker!
    @IBOutlet var timeView: UIView!
    @IBOutlet var dateView: UIView!
    @IBOutlet var startTimeBtn: UIButton!
    @IBOutlet var startDateBtn: UIButton!
    @IBOutlet var blackview: UIView!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var titleLbl: UILabel!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        nameTxtfield.delegate = self
        addressTxtfield.delegate = self
        
        titleLbl.text = startPackingTitleStr as String
        datePicker.setValue(UIColor.whiteColor(), forKey: "textColor")
        
        let date = NSDate()
        

        var dateFormat = NSDateFormatter()
        
        dateFormat.dateFormat = "MM-dd-yyyy"
        
        var dateInFormat:NSString = dateFormat.stringFromDate(date)
        
        println(dateInFormat)
        
        
        var timeFormat = NSDateFormatter()
        
        timeFormat.dateFormat = "hh:mm a"
        
        var timeInFormat:NSString = timeFormat.stringFromDate(date)
        
        println(timeInFormat)
        
        
        
        startDateBtn.setTitle(dateInFormat as String, forState: UIControlState.Normal)
        
        startTimeBtn.setTitle(timeInFormat as String, forState: UIControlState.Normal)
        
        
        
        apiDateFormat.dateFormat = "yyyy-MM-dd"
        
        apiDateFormatStr  = apiDateFormat.stringFromDate(date)
        
        println("apiDate\(apiDateFormatStr)")
        
        
        
        apiTimeFormat.dateFormat = "HH:mm:ss"
        
        apiTimeFormatStr  = apiTimeFormat.stringFromDate(date)
        
        println("apiTime\(apiTimeFormatStr)")
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func logoutBtn(sender: AnyObject)
    {
        var loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let login = loginStoryboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        
        self.navigationController?.pushViewController(login, animated: false)

        
    }
    
    
    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- Start New button
    
    
    @IBAction func startNewBtn(sender: AnyObject)
    {
        if nameTxtfield.text == "" || addressTxtfield.text == ""
        {
            
            var alert  = UIAlertView(title: "Alert", message: "Please enter the Title Name and Address Name", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
        }
        
        
        else
        {
            
            var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            spinningIndicator.labelText = "Loading"
            
            
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&Title=%@&Address=%@&StartDate=%@&StartTime=%@&Typed=%@",str,nameTxtfield.text,addressTxtfield.text,apiDateFormatStr,apiTimeFormatStr,strInventoryOrPackageTitle)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/createpackage"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    println("CreatePackage\(dictObj)")
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)

                    }
                    else
                    {
                        
                        var success = dictObj?.valueForKey("success") as! Int
                        
                        
                        
                        if success == 0
                        {
                            var message = dictObj?.valueForKey("message") as! String
                            
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                           var furtherDict = dictObj?.valueForKey("message") as! NSDictionary
                            
                            
                            spinningIndicator.hide(true)
                            
                            var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
                            
                            var newEntryStoryboard = appDeleagte.newEntryStoryboard() as UIStoryboard
                            
                            let newEntryGraph = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryGraph") as! NewEntryGraphViewController
                            newEntryGraph.packageId = furtherDict.valueForKey("PackageId") as! Int
                            newEntryGraph.greenTitleStr = self.addressTxtfield.text
                            println(newEntryGraph.greenTitleStr)
                            NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "bagCount")
                            NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "boxCount")
                            NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "binCount")
                            println(newEntryGraph.packageId)
                            self.navigationController?.pushViewController(newEntryGraph, animated: true)
                        }
                        
                        
                      
                        

                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
        
        

        
        
        
        }
        
        
        
        
        
    }
    
    //MARK:- Navigtion Barcode button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
    }
    
    //MARK:- Start date and time Button
    
    @IBAction func startDateBtn(sender: AnyObject)
    {
        blackview.hidden = false
        timeView.hidden = true
        dateView.hidden  = false
    }
    
    @IBAction func startTimeBtn(sender: AnyObject)
    {
        blackview.hidden = false
        timeView.hidden = false
        dateView.hidden = true
    }
    
    
    
    //MARK:- DatePicker Date button
    
    @IBAction func cancelbtn(sender: AnyObject)
    {
        blackview.hidden = true
    }
    
    
    
    @IBAction func okBtn(sender: AnyObject)
    {
        
        var dateFormat = NSDateFormatter()
        var strFormat = NSString()
        
        dateFormat.dateFormat = "MM-dd-yyyy"
        
        strFormat = dateFormat.stringFromDate(datePicker.date)
        println(strFormat)
        
        startDateBtn.titleLabel?.text = strFormat as String
        
        blackview.hidden = true
        
        apiDateFormatStr  = apiDateFormat.stringFromDate(datePicker.date)
        
        println("apiDateOkBtn\(apiDateFormatStr)")
        
    }
    
    
    
    
    //MARK:- DatePicker time button
    
    @IBAction func timeCancelBtn(sender: AnyObject)
    {
        blackview.hidden = true
    }
    
    
    
    
    @IBAction func timeOkbtn(sender: AnyObject)
    {
        var dateFormat = NSDateFormatter()
        var strFormat = NSString()
        
        dateFormat.dateFormat = "hh:mm a"
        
        strFormat = dateFormat.stringFromDate(timePickerView.date)
        println(strFormat)
        
        startTimeBtn.titleLabel?.text = strFormat as String
        
        blackview.hidden = true
        
        
        apiTimeFormatStr  = apiTimeFormat.stringFromDate(timePickerView.date)
        
        println("apiTimeOkBtn\(apiTimeFormatStr)")
        
        
    }
    
    
    
    //MARK:- Touch Method
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        nameTxtfield.resignFirstResponder()
        addressTxtfield.resignFirstResponder()
        
    }
    
    
    //MARK:- TextField Dlegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        
        
       if textField == nameTxtfield
       {
         nameLbl.hidden = true
       }
        
       else
       {
          addressLbl.hidden = true
       }
        
        
    }
    
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        if textField == nameTxtfield
        {
            if nameTxtfield.text == ""
            {
                nameLbl.hidden = false
            }
            else
            {
                nameLbl.hidden = true
            }
            
            
        }
        
        else
        {
            
            if addressTxtfield.text == ""
            {
                addressLbl.hidden = false
            }
            else
            {
                addressLbl.hidden = true
            }

            
        }
        
    }
    
    
     
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
       
        
        
        
        
        if textField == addressTxtfield
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 110
                
            }
            
            if sizeHeight == 568 || sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y -= 90
            }
            
            
        }
            
        else
        {
            view.frame.origin.y = view.frame.origin.y
        }
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if textField == addressTxtfield
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 110
                
            }
            
            if sizeHeight == 568 ||  sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y += 90
            }
            
            
            
        }
        return true
        
    }
    
    
}
