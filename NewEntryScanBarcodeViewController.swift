//
//  NewEntryScanBarcodeViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import AVFoundation


var scanOnViewInventory:String = String()

class NewEntryScanBarcodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate
{

    @IBOutlet var greenTitleLbl: UILabel!
     var greenTitleStr = NSString()
    @IBOutlet var okBtn: UIButton!
    @IBOutlet weak var messageLabel:UILabel!
   
    
    let session         : AVCaptureSession = AVCaptureSession()
    var previewLayer    : AVCaptureVideoPreviewLayer!
    var highlightView   : UIView = UIView()
    
    
    @IBOutlet var scanBtn: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        greenTitleLbl.text = greenTitleStr as String
        
    }

    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    
    
    @IBAction func okBtn(sender: AnyObject)
    {
        
        scanOnViewInventory = "viewInventoryScan"
        
        self.navigationController?.popViewControllerAnimated(false)
        
        strBagToFurtherScanBarcode = messageLabel.text!
        
        println("scan code in scan view = \(strBagToFurtherScanBarcode)")
        
        

    }
    
    
    
    
    @IBAction func scanBtn(sender: AnyObject)
    {
        

        self.highlightView.autoresizingMask =   UIViewAutoresizing.FlexibleTopMargin |
            UIViewAutoresizing.FlexibleBottomMargin |
            UIViewAutoresizing.FlexibleLeftMargin |
            UIViewAutoresizing.FlexibleRightMargin
        // Select the color you want for the completed scan reticle
        //self.highlightView.layer.borderColor = UIColor.darkTextColor().CGColor
        //self.highlightView.layer.borderWidth = 3
        // Add it to our controller's view as a subview.
        self.view.addSubview(self.highlightView)
        // For the sake of discussion this is the camera
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        // Create a nilable NSError to hand off to the next method.
        // Make sure to use the "var" keyword and not "let"
        var error : NSError? = nil
        let input : AVCaptureDeviceInput? = AVCaptureDeviceInput.deviceInputWithDevice(device, error: &error) as? AVCaptureDeviceInput
        // If our input is not nil then add it to the session, otherwise we're kind of done!
        if input != nil
        {
            session.addInput(input)
        }
        else
        {
            // This is fine for a demo, do something real with this in your app. :)
            println(error)
        }
        
        let output = AVCaptureMetadataOutput()
        
        output.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
        
        session.addOutput(output)
        
        output.metadataObjectTypes = output.availableMetadataObjectTypes
        previewLayer = AVCaptureVideoPreviewLayer.layerWithSession(session) as! AVCaptureVideoPreviewLayer
        previewLayer.frame = self.view.bounds
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.view.layer.addSublayer(previewLayer)
        
        // Start the scanner. You'll have to end it yourself later.
        session.startRunning()
        
        
        scanBtn.hidden = true
        
        okBtn.hidden = false


    }
    
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!)
    {
        var highlightViewRect = CGRectZero
        var barCodeObject : AVMetadataObject!
        var detectionString : String!
        
        
        

        
        
        let barCodeTypes = [AVMetadataObjectTypeUPCECode,
        AVMetadataObjectTypeCode39Code,
        AVMetadataObjectTypeCode39Mod43Code,
        AVMetadataObjectTypeEAN13Code,
        AVMetadataObjectTypeEAN8Code,
        AVMetadataObjectTypeCode93Code,
        AVMetadataObjectTypeCode128Code,
        AVMetadataObjectTypePDF417Code,
        AVMetadataObjectTypeQRCode,
        AVMetadataObjectTypeAztecCode
        ]
        
//        if metadataObjects == nil || metadataObjects.count == 0
//        {
//           qrCodeFrameView?.frame = CGRectZero
//           messageLabel.text = "No QR code is detected"
//           return
//        }
        
        
        
        
        
        
        
        
        
        
        
//        // The scanner is capable of capturing multiple 2-dimensional barcodes in one scan.
//        for metadata in metadataObjects
//        {
//            for barcodeType in barCodeTypes
//            {
//                if metadata.type == barcodeType
//                {
//                    barCodeObject = self.previewLayer.transformedMetadataObjectForMetadataObject(metadata as! AVMetadataMachineReadableCodeObject)
//                    
//                    highlightViewRect = barCodeObject.bounds
//                    
//                    detectionString = (metadata as! AVMetadataMachineReadableCodeObject).stringValue
//
//                    self.session.stopRunning()
//                    
//                    break
//                }
//            }
//        }
        
        
        
        //The scanner is capable of capturing multiple 2-dimensional barcodes in one scan.

        for metaData in metadataObjects
        {
            //let decodedData:AVMetadataMachineReadableCodeObject = metaData as! AVMetadataMachineReadableCodeObject
           
            
            detectionString = (metaData as! AVMetadataMachineReadableCodeObject).stringValue
        
            
            self.session.stopRunning()
            
            if detectionString == ""
            {
                println("error in detection")
            }
            
            break
            
        }
        
        
        
        
        previewLayer.hidden = true
        
        println(detectionString)
        
        messageLabel.text = detectionString as String
        
        
        //self.highlightView.frame = highlightViewRect
       // self.view.bringSubviewToFront(self.highlightView)
    }

    
    
    @IBAction func cancelbtn(sender: AnyObject)
    {
        
        scanOnViewInventory = "cancelBtn"
        
        if receiveNextBtnCheck == "receiveNext"
        {
            for controller in self.navigationController!.viewControllers as Array
            {
                if controller.isKindOfClass(ConfirmedReceivedListViewController)
                {
                    self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                    break
                }
            }

        }
        else
        {
            self.navigationController?.popViewControllerAnimated(false)
        }
       
        
        
    }
    
    

    //MARK:- Navigation Barcode Btn
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(GraphViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }
    
  

}
