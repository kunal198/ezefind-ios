//
//  UserNameViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 1/21/16.
//  Copyright (c) 2016 mrinal khullar. All rights reserved.
//

import UIKit

class UserNameViewController: UIViewController,UITextFieldDelegate
{
    
    @IBOutlet weak var userNameBtn: UIButton!
        
    @IBOutlet weak var userNameLbl: UITextField!
  
    
    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet weak var updateBtn: UIButton!
    
    var editUserName = String()
    
    

    override func viewDidLoad()
    {
        super.viewDidLoad()

        
        self.userNameLbl.delegate = self
        
        
        userNameApi()
    }
    
    
    
    
    
    
    
    //MARK:- TextField Dlegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        self.userNameLbl.resignFirstResponder()
        return true
    }
    

    
    
    
    
    
    
    //MARK:- username Api
    
    
    func userNameApi()
    {
        
        var spinningIndicator  = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i", str)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/get-username"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    println("IMember = \(dictObj)!")
                    
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        })

                        
                    }
                    else
                    {
                        var success = dictObj?.valueForKey("success") as! Int
                        
                        var userName = dictObj?.valueForKey("Data") as! String
                        
                    
                        
                        if success == 0
                        {
//                            
//                            var alert = UIAlertView(title: "", message: "No Data Found", delegate: self, cancelButtonTitle: "OK")
                            
                            self.userNameLbl.text = userName as String
                            
                            println("userName")

                            
                         //   alert.show()
                            
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            
                            println("success")
                            
                            
                            self.userNameLbl.text = userName as String
                            
                            println("userName")
                            
                            
                            spinningIndicator.hide(true)
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }
    
    

    
    
    
    
    @IBAction func updateUserNameBtn(sender: AnyObject) {
        
        self.userNameLbl.resignFirstResponder()
        
        editUserNameApi()
    }
    
    
    
    @IBAction func EditUserNameBtn(sender: AnyObject)
    {
        println("edit btn clicked")
        
        self.userNameLbl.becomeFirstResponder()
        
        editBtn.hidden = true
        updateBtn.hidden = false
        
        
        
    }
    
    
    
    
    
    
    
    
    //MARK:- Edit UserName Api
    func editUserNameApi()
    {
        
        var spinningIndicator  = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        var data = NSData()
        
        editUserName = userNameLbl.text!
        
        
        var post = NSString(format: "UserId=%i&Username=%@",str,editUserName)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/update-username"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    println("Edit user name = \(dictObj)!")
                    
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                        
                    }
                    else
                    {
                        var success = dictObj?.valueForKey("success") as! Int
                        
                        var messageUserName = dictObj?.valueForKey("message") as! String
                        
                      
                        
                        if success == 0
                        {
                          
                            var alert = UIAlertView(title: "", message: messageUserName, delegate: self, cancelButtonTitle: "OK")
    
                            
                            println("messageUserName")
                            
                            self.updateBtn.hidden = true
                            self.editBtn.hidden = false
                            
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            
                            println("success")
                            
                            
                            self.userNameLbl.text = self.editUserName as String
                            
                           // println("userName")
                            
                            
                            var alert = UIAlertView(title: "", message: messageUserName, delegate: self, cancelButtonTitle: "OK")
                            
                            
                            println("messageUserName")
                            
                            
                            alert.show()

                            self.updateBtn.hidden = true
                            self.editBtn.hidden = false
                            
                            spinningIndicator.hide(true)
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func backBtnNavBar(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    
    
    
    @IBAction func barCodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

    }
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
