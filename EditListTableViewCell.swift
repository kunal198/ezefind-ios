//
//  EditListTableViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 10/16/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class EditListTableViewCell: UITableViewCell {
    
    @IBOutlet var selectEditDeleteBtn: UIButton!
    @IBOutlet var editListDeleteBtn: UIButton!

    @IBOutlet var editListEditBtn: UIButton!
    @IBOutlet var editListQuantityLbl: UILabel!
    @IBOutlet var editListAttachmentLbl: UILabel!
    @IBOutlet var editListTxtView: UITextView!
    @IBOutlet var editListTitle: UILabel!
    @IBOutlet var editListImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
