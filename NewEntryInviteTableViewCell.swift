//
//  NewEntryInviteTableViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/15/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class NewEntryInviteTableViewCell: UITableViewCell {

    
    
    @IBOutlet var radioBtn: UIButton!
    @IBOutlet var contactNumberLbl: UILabel!
    @IBOutlet var contactNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
