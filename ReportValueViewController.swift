//
//  ReportValueViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/16/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import Charts

class ReportValueViewController: UIViewController,UITableViewDelegate
{

    var locationArray = NSMutableArray()
    var locationValuesArray = [String]()
    
    var categoryValuesArray = [String]()
    
    
    var strBagBinItem = String()
    
    var unitsSold:[Double] = []
    var bagDouble = Double()
    var bagStr = NSString()
    
    
    var months:[String]!
    
    var yearMonthWeek:[String]!
    
    var yearWeekMonthUnitsSold:[Double] = []
    
    var categoryStr = NSString()
    var categoryDouble = Double()

    var itemStr = NSString()
    var itemDouble = Double()
    
    
    
    var locationStr = NSString()
    var locationDouble = Double()
    
    
    @IBOutlet weak var blueViewInScrollView: UIView!
    
    @IBOutlet weak var weekLbl: UILabel!
    
    @IBOutlet weak var daysLbl: UILabel!
    
    var timeStr = NSString()
    var timeDouble = Double()
    
    
    @IBOutlet weak var separatorLineView: UIView!
     var reportValuedict = NSDictionary()
    
     var packageDict = NSDictionary()
     var inventoryDict = NSDictionary()
    
     var bothDict = NSDictionary()
    
    
    @IBOutlet weak var weekTimeLbl: UILabel!
    
     //var categoryDict_inventory = NSDictionary()
     //var categoryDict_package = NSDictionary()
    
    
    var categoryDict_both = NSDictionary()
    var itemDict_both = NSDictionary()
    var locationDict_both = NSDictionary()
    var timeDict_both = NSDictionary()
    

    
    var categoryDict_package = NSDictionary()
    var timeDict_package = NSDictionary()
    var itemDict_package = NSDictionary()
    var locationDict_package = NSDictionary()
    
    
    
    
    var categoryDict_inventory = NSDictionary()
    var itemDict_inventory = NSDictionary()
    var locationDict_inventory = NSDictionary()
    var timeDict_inventory = NSDictionary()
    
    
    
    @IBOutlet weak var categoryInnerFilter: UIButton!
    
     var dataDictionary = NSDictionary()
    
    
    @IBOutlet weak var locationInnerFILTER: UIButton!
   
    @IBOutlet weak var bagBlueBtn: UIButton!
    
    @IBOutlet weak var categoryView: UIView!
    
    @IBOutlet weak var locationView: UIView!
    
    @IBOutlet weak var binBtn: UIButton!
    
    @IBOutlet weak var bagBtn: UIButton!
    
    @IBOutlet weak var boxBtn: UIButton!
    
    @IBOutlet weak var binBlueBtn: UIButton!
    
    @IBOutlet weak var boxBlueBtn: UIButton!
    
    @IBOutlet weak var bagBinItemView: UIView!
    
    @IBOutlet weak var singleItemBtn: UIButton!
   
    
    @IBOutlet weak var locationTableView: UITableView!
    
    @IBOutlet weak var categoryTableView: UITableView!
    
    
    @IBOutlet weak var singleItemBlueBtn: UIButton!
    
    
    @IBOutlet weak var innerFilterView: UIView!
    
    
    @IBOutlet weak var weekBlueBtn: UIButton!
    @IBOutlet weak var weekBtn: UIButton!
    @IBOutlet weak var yearBtn: UIButton!
    
    @IBOutlet weak var yearBlueBtn: UIButton!
    
    @IBOutlet weak var monthBlueBtn: UIButton!
    @IBOutlet weak var monthBtn: UIButton!
    
    @IBOutlet weak var totalValue_lbl: UILabel!
    
    @IBOutlet weak var wholeTotal_lbl: UILabel!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var totalLbl: UILabel!
    @IBOutlet var valueScrollView: UIScrollView!
    @IBOutlet var barScrollView: UIScrollView!
    @IBOutlet var barChartView: BarChartView!
       
    @IBOutlet var packageBlueBtn: UIButton!
    @IBOutlet var inventoryBlueBtn: UIButton!
    @IBOutlet var bothBlueBtn: UIButton!
    @IBOutlet var bothBtn: UIButton!
    @IBOutlet var inventoryBtn: UIButton!
    @IBOutlet var packageBtn: UIButton!
    @IBOutlet var blueView: UIView!

    
    @IBOutlet var dateBtn: UIButton!
    
    @IBOutlet weak var timeName_lbl: UILabel!
    
    @IBOutlet weak var locationName_lbl: UILabel!
    @IBOutlet weak var totalLocation_lbl: UILabel!
    @IBOutlet weak var itemTotal_lbl: UILabel!
    
    @IBOutlet weak var itemName_lbl: UILabel!
    
    @IBOutlet weak var totalCategory_lbl: UILabel!
    @IBOutlet weak var totalTime_lbl: UILabel!
    
    
    @IBOutlet weak var categoryName_lbl: UILabel!
    
    
    @IBOutlet weak var outerFilterView: UIView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        scrollView.contentSize.height  = dateBtn.frame.origin.y + dateBtn.frame.size.height+10
        valueScrollView.contentSize.height  = totalLbl.frame.origin.y + totalLbl.frame.size.height+10
        barScrollView.contentSize.width = barChartView.frame.origin.x + barChartView.frame.size.width
        barScrollView.contentSize.height = barChartView.frame.origin.y + barChartView.frame.size.height
        
//        barChartView.noDataText = "You need to provide data for the chart."
//        barChartView.noDataTextDescription = "GIVE REASON"
//        barChartView.descriptionText = ""
//        barChartView.xAxis.labelPosition = .Bottom
//        
//        months = ["Jan", "Feb", "Mar", "Apr"]
//        let unitsSold = [8.5, 7.4, 6.0, 3.0]
//        
        
        strBothInventoryPackage = "package"
        strYearMonthDay = "year"
        strBagBinItem = "bag"
        
        
        
        self.totalLbl.hidden = true
        self.totalValue_lbl.hidden = true
        self.wholeTotal_lbl.hidden = true
        self.daysLbl.hidden = true
        self.weekLbl.hidden = true
        
        self.itemTotal_lbl.hidden = true
        self.totalLocation_lbl.hidden = true
        self.totalTime_lbl.hidden = true
        self.totalCategory_lbl.hidden = true
        self.separatorLineView.hidden = true
        
        
        
        
     //   setChart(months, values: unitsSold)

         reportValueApi()
        
    }
    
    
    
    
    
    //MARK:- BarChart
    
    
    func BarChart()
    {
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.noDataTextDescription = "GIVE REASON"
        barChartView.descriptionText = ""
        
        
        barChartView.drawBarShadowEnabled = false
        barChartView.drawValueAboveBarEnabled = true
        barChartView.maxVisibleValueCount = 60
        barChartView.pinchZoomEnabled = false
        barChartView.drawGridBackgroundEnabled = false
        
        var xaxis = barChartView.xAxis as ChartXAxis
        xaxis.labelPosition = .Bottom
        xaxis.labelFont = UIFont.systemFontOfSize(10)
        
        xaxis.spaceBetweenLabels = 2
        
        
        
        
        var leftAxis = barChartView.leftAxis as ChartYAxis
        
        
        leftAxis.labelFont = UIFont.systemFontOfSize(10)
        leftAxis.labelCount = 4
        leftAxis.valueFormatter = NSNumberFormatter.alloc()
        leftAxis.valueFormatter!.maximumFractionDigits = 1
        leftAxis.valueFormatter!.negativeSuffix = " $"
        leftAxis.valueFormatter!.positiveSuffix = " $"
        leftAxis.labelPosition = .OutsideChart
        leftAxis.spaceTop = 5
        
        
        var rightAxis = barChartView.rightAxis as ChartYAxis
        rightAxis.drawGridLinesEnabled = false
        rightAxis.labelFont = UIFont.systemFontOfSize(10)
        rightAxis.labelCount = 4
        rightAxis.valueFormatter = leftAxis.valueFormatter
        rightAxis.spaceTop = 5
        
        barChartView.legend.position = .BelowChartLeft
        barChartView.legend.form = .Square
        barChartView.legend.formSize = 9.0
        barChartView.legend.font = UIFont(name: "HelveticaNeue-Light", size: 11.0)!
        barChartView.legend.xEntrySpace = 4.0
        
        
    }
    
    
    
    
    
    
    
    func setChart(dataPoints:[String], values:[Double])
    {
        
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for  i in 0..<dataPoints.count
            
        {
            let dataEntry = BarChartDataEntry(value:values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
        let chartData = BarChartData(xVals: months, dataSet: chartDataSet)
        barChartView.data = chartData
        
        
    }
    
    
    
    func setChartYearMonthWeek(dataPoints:[String], values:[Double])
    {
        
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for  i in 0..<dataPoints.count
            
        {
            let dataEntry = BarChartDataEntry(value:values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
        let chartData = BarChartData(xVals: yearMonthWeek, dataSet: chartDataSet)
        barChartView.data = chartData
        
        
        
        
        
    }
    
    
    

    
    
    //MARK:- Report Value Api
    
    func reportValueApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&main_filter=%@",userId, strBothInventoryPackage)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/value"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    println(dictObj)
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        
                        self.totalCategory_lbl.text = ""
                        self.itemTotal_lbl.text = ""
                        self.totalLocation_lbl.text = ""
                        self.totalTime_lbl.text  = ""
                        self.wholeTotal_lbl.text  = ""
                        
                        
                        self.totalLbl.hidden = true
                        self.totalValue_lbl.hidden = true
                        self.wholeTotal_lbl.hidden = true

                        
                        
                        
                        spinningIndicator.hide(true)
                        
                        
                    }
                    else
                    {
                        
                        
                        
                        var success = dictObj?.valueForKey("success") as! Int
                        
                        if success == 0
                        {
                            //var dict = dictObj?.valueForKey("success") as! NSDictionary
                            var message = dictObj!.valueForKey("message") as! String
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            
//                             self.totalCategory_lbl.text = ""
//                             self.itemTotal_lbl.text = ""
//                            self.totalLocation_lbl.text = ""
//                            self.totalTime_lbl.text  = ""
//                             self.wholeTotal_lbl.text  = ""
                            
                            
                            self.totalLbl.hidden = true
                            self.totalValue_lbl.hidden = true
                            self.wholeTotal_lbl.hidden = true
                            self.daysLbl.hidden = true
                            self.weekLbl.hidden = true
                            
                            self.itemTotal_lbl.hidden = true
                            self.totalLocation_lbl.hidden = true
                            self.totalTime_lbl.hidden = true
                            self.totalCategory_lbl.hidden = true
                            self.separatorLineView.hidden = true
                            
                            
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            
                            self.reportValuedict = dictObj?.valueForKey("overall_data") as! NSDictionary
                            
    
                            
                            println("package selected = \(strBothInventoryPackage)")
                            
                            
                            self.totalLbl.hidden = false
                            self.totalValue_lbl.hidden = false
                            self.wholeTotal_lbl.hidden = false
                            self.daysLbl.hidden = false
                            self.weekLbl.hidden = false
                            
                            self.itemTotal_lbl.hidden = false
                            self.totalLocation_lbl.hidden = false
                            self.totalTime_lbl.hidden = false
                            self.totalCategory_lbl.hidden = false
                            
                            self.separatorLineView.hidden = false
                           
                            if strBothInventoryPackage == "both"
                            {
                                
                                self.bothDict = self.reportValuedict.valueForKey("both") as! NSDictionary
                                
                                self.categoryDict_both = self.bothDict.valueForKey("category") as! NSDictionary
                                
//                                var total = self.categoryDict_both.valueForKey("total_value") as! Int
//                                
//                                println("total = \(total)")
                                
                                
                                
                                var category_data = self.categoryDict_both.valueForKey("data") as! NSDictionary
                                
                                println("category data values are = \(category_data)")
                                
                                
                                self.categoryValuesArray = category_data.allKeys as! [String]
                                
                                println("category data values array is = \(self.categoryValuesArray)")
                                
                                
                                self.categoryTableView.reloadData()
                                

                                
                                
                                
                                
                                
                                println(self.categoryDict_both.allKeys)
                                
                                println(self.bothDict.valueForKey("total") as? String)
                                
                                
                                
                                
                                
                                if let categoryTotal = self.categoryDict_both.valueForKey("total_value") as? Int
                                {
                                    self.totalCategory_lbl.text = NSString(format: "%i", categoryTotal) as String
                                    
                                    println(self.totalCategory_lbl.text)
                                    
                                    
                                    self.categoryStr = NSString(format: "%@", self.totalCategory_lbl.text!)
                                    
                                    self.categoryDouble = (self.categoryStr).doubleValue
                                    
                                    println(self.categoryDouble)
                                    
                                    
                                }
                                
                                
                                
                                
                                self.itemDict_both = self.bothDict.valueForKey("item") as! NSDictionary
                                
                                
                                var total = self.itemDict_both.valueForKey("total_value") as! Int
                                
                                
                                if let itemTotal = self.itemDict_both.valueForKey("total_value") as? Int
                                {
                                    self.itemTotal_lbl.text = NSString(format: "%i", itemTotal) as String
                                    
                                    println(self.itemTotal_lbl.text)
                                    
                                    
                                    self.itemStr = NSString(format: "%@", self.itemTotal_lbl.text!)
                                    
                                    self.itemDouble = (self.itemStr).doubleValue
                                    
                                    println(self.itemDouble)
                                    
                                    
                                }
                                
                                
                                
                                
                                
                                
                                self.locationDict_both = self.bothDict.valueForKey("location") as! NSDictionary
                                
                                
                                
                                
                                
                                var location_data = self.locationDict_both.valueForKey("data") as! NSDictionary
                                println("location data values are = \(location_data)")
                                
                                
                                self.locationValuesArray = location_data.allKeys as! [String]
                                
                                println("location data values array is = \(self.locationValuesArray)")
                                
                                
                                self.locationTableView.reloadData()
                                

                                
                                
                                
                                var total_locaion = self.locationDict_both.valueForKey("total_value") as! Int
                                
                                if let location_total = self.locationDict_both.valueForKey("total_value") as? Int
                                {
                                    self.totalLocation_lbl.text = NSString(format: "%i", location_total) as String
                                    
                                    println(self.totalLocation_lbl.text)
                                    
                                    
                                    
                                    
                                    self.locationStr = NSString(format: "%@", self.itemTotal_lbl.text!)
                                    
                                    self.locationDouble = (self.locationStr).doubleValue
                                    
                                    println(self.locationDouble)
                                    
                                }
                                
                                
                                
                                
                                self.timeDict_both = self.bothDict.valueForKey("time") as! NSDictionary
                                
                                if let time_total = self.timeDict_both.valueForKey("total_value") as? Int
                                {
                                    self.totalTime_lbl.text = NSString(format: "%i", time_total) as String
                                    
                                    println(self.totalTime_lbl.text)
                                    
                                    
                                    self.timeStr = NSString(format: "%@", self.itemTotal_lbl.text!)
                                    
                                    self.timeDouble = (self.timeStr).doubleValue
                                    
                                    println(self.timeDouble)
                                }
                                
                                
                                
                                
                                if var total_both = self.bothDict.valueForKey("total") as? Int
                                {
                                    self.wholeTotal_lbl.text = NSString(format: "%i", total_both) as String
                                }
                                
                                
                                
                                
                                
                                
                                //  println(self.reportMemberArray.valueForKey("count"))
                                
                                
                                //  self.yearMonthWeek = self.reportMemberArray.valueForKey("name") as! NSArray as! [String]
                                
                                // self.yearWeekMonthUnitsSold = self.reportMemberArray.valueForKey("count") as! NSArray as! [Double]
                                
                                
                                //  println(self.yearMonthWeek)
                                
                                //println(self.yearWeekMonthUnitsSold)
                                
                                // self.unitsSold = self.yearWeekMonthUnitsSold
                                
                                //self.months = ["Bag", "Box", "Single Item", "Bin"]
                                
                                //self.months = self.yearMonthWeek
                                
                                
                                self.unitsSold = [self.itemDouble,self.locationDouble,self.timeDouble,self.categoryDouble]
                                
                                self.months = ["Items","Location","Time","Category"]
                                
                                self.setChart(self.months, values: self.unitsSold)
                                
                            }
                            
                            
                            
                            
                            
                            
                            
                            if strBothInventoryPackage == "inventory"
                            {
                                
                                self.inventoryDict = self.reportValuedict.valueForKey("inventory") as! NSDictionary
                                println("inventory dict = \(self.inventoryDict)")
                                

                                
                                
                                
                                self.categoryDict_inventory = self.inventoryDict.valueForKey("category") as! NSDictionary
                                
                                
                                
                                
                                var category_data = self.categoryDict_inventory.valueForKey("data") as! NSDictionary
                                
                                println("category data values are = \(category_data)")
                                
                                
                                self.categoryValuesArray = category_data.allKeys as! [String]
                                
                                println("category data values array is = \(self.categoryValuesArray)")
                                
                                
                                self.categoryTableView.reloadData()

                                
                              
                                
                                
                                //var total = self.inventoryDict.valueForKey("total_value") as! Int
                                
                                if let itemTotal = self.categoryDict_inventory .valueForKey("total_value") as? Int
                                {
                                    self.totalCategory_lbl.text = NSString(format: "%i", itemTotal) as String
                                    
                                    println(self.totalCategory_lbl.text)
                                    
                                    
                                    
                                    
                                    self.categoryStr = NSString(format: "%@", self.totalCategory_lbl.text!)
                                    
                                    self.categoryDouble = (self.categoryStr).doubleValue
                                    
                                    println(self.categoryDouble)
                                    
                                }
                                
                                
                                
                                
                                
                                
                                self.itemDict_inventory = self.inventoryDict.valueForKey("item") as! NSDictionary
                                
                                //var total = itemDict_inventory.valueForKey("total_value") as! Int
                                
                                if let categoryTotal = self.itemDict_inventory.valueForKey("total_value") as? Int
                                {
                                    self.itemTotal_lbl.text = NSString(format: "%i", categoryTotal) as String
                                    
                                    println(self.itemTotal_lbl.text)
                                    
                                    
                                    self.itemStr = NSString(format: "%@",self.itemTotal_lbl.text!)
                                    
                                    self.itemDouble = (self.itemStr).doubleValue
                                    
                                    println(self.itemDouble)
                                }
                                
                                
                                
                                
                                
                                
                                self.locationDict_inventory = self.inventoryDict.valueForKey("location") as! NSDictionary
                                
                                
                                
//                                self.locationArray = self.locationDict_inventory.valueForKey("data") as! NSMutableArray
                                
                                
                                
                                
                                println("location dictionary array = \(self.locationDict_inventory)")
                                
                                
                                
                                var location_data = self.locationDict_inventory.valueForKey("data") as! NSDictionary
                                println("location data values are = \(location_data)")
                                
                                
                                self.locationValuesArray = location_data.allKeys as! [String]
                                
                                println("location data values array is = \(self.locationValuesArray)")
                                
                                
                                self.locationTableView.reloadData()
                                
                                
                                
                                var total_locaion = self.locationDict_inventory.valueForKey("total_value") as! Int
                                
                                if let location_total = self.locationDict_inventory.valueForKey("total_value") as? Int
                                {
                                    self.totalLocation_lbl.text = NSString(format: "%i", location_total) as String
                                    
                                    println(self.totalLocation_lbl.text)
                                    
                                    
                                    
                                    self.locationStr = NSString(format: "%@",self.totalLocation_lbl.text!)
                                    
                                    self.locationDouble = (self.locationStr).doubleValue
                                    
                                    println(self.locationDouble)
                                }
                                
                                
                                
                                
                                self.timeDict_inventory = self.inventoryDict.valueForKey("time") as! NSDictionary
                                
                                if let time_total = self.timeDict_inventory.valueForKey("total_value") as? Int
                                {
                                    self.totalTime_lbl.text = NSString(format: "%i", time_total) as String
                                    
                                    println(self.totalTime_lbl.text)
                                    
                                    
                                    
                                    
                                    self.timeStr = NSString(format: "%@",self.totalTime_lbl.text!)
                                    
                                    self.timeDouble = (self.timeStr).doubleValue
                                    
                                    println(self.timeDouble)
                                }
                                
                                
                                
                                
                                if var total_inventory = self.inventoryDict.valueForKey("total") as? Int
                                {
                                    self.wholeTotal_lbl.text = NSString(format: "%i", total_inventory) as String
                                }
                                
                                
                                
                                
                                self.unitsSold = [self.itemDouble,self.locationDouble,self.timeDouble,self.categoryDouble]
                                
                                self.months = ["Items","Location","Time","Category"]
                                
                                self.setChart(self.months, values: self.unitsSold)
                                
                            }
                            
                            
                            
                            
                            if strBothInventoryPackage == "package"
                            {
                                
                                self.packageDict = self.reportValuedict.valueForKey("package") as! NSDictionary
                                
                                println("package dict = \(self.packageDict)")

                                
                                
                                self.categoryDict_package = self.packageDict.valueForKey("category") as! NSDictionary
                                
                                
                                
                                
                                var category_data = self.categoryDict_package.valueForKey("data") as! NSDictionary
                                
                                println("category data values are = \(category_data)")
                                
                                println("package allkeys = \(self.categoryDict_package.allKeys)")

                                
                                
                                self.categoryValuesArray = category_data.allKeys as! [String]
                                
                                println("category data values array is = \(self.categoryValuesArray)")
                                
                                
                                self.categoryTableView.reloadData()
                                

                                
                                
                                
                                //var total = self.inventoryDict.valueForKey("total_value") as! Int
                                
                                if let itemTotal = self.categoryDict_package .valueForKey("total_value") as? Int
                                {
                                    self.totalCategory_lbl.text = NSString(format: "%i", itemTotal) as String
                                    
                                    println(self.totalCategory_lbl.text)
                                    
                                    
                                    
                                    
                                    
                                    self.categoryStr = NSString(format: "%@", self.totalCategory_lbl.text!)
                                    
                                    self.categoryDouble = (self.categoryStr).doubleValue
                                    
                                    println(self.categoryDouble)
                                }
                                
                                
                                
                                
                                
                                
                                self.itemDict_package = self.packageDict.valueForKey("item") as! NSDictionary
                                
                                //var total = itemDict_inventory.valueForKey("total_value") as! Int
                                
                                if let categoryTotal = self.itemDict_package.valueForKey("total_value") as? Int
                                {
                                    self.itemTotal_lbl.text = NSString(format: "%i", categoryTotal) as String
                                    
                                    println(self.itemTotal_lbl.text)
                                    
                                    
                                    
                                    self.itemStr = NSString(format: "%@",  self.itemTotal_lbl.text!)
                                    
                                    self.itemDouble = (self.itemStr).doubleValue
                                    
                                    println(self.itemDouble)
                                }
                                
                                
                                
                                
                                
                                
                                self.locationDict_package = self.packageDict.valueForKey("location") as! NSDictionary
                                
                                
                                var location_data = self.locationDict_package.valueForKey("data") as! NSDictionary
                                println("location data values are = \(location_data)")
                                
                                
                                self.locationValuesArray = location_data.allKeys as! [String]
                                
                                println("location data values array is = \(self.locationValuesArray)")
                                
                                
                                self.locationTableView.reloadData()

                                
                                
                                
                                
                                var total_locaion = self.locationDict_package.valueForKey("total_value") as! Int
                                
                                if let location_total = self.locationDict_package.valueForKey("total_value") as? Int
                                {
                                    self.totalLocation_lbl.text = NSString(format: "%i", location_total) as String
                                    
                                    println(self.totalLocation_lbl.text)
                                    
                                    
                                    
                                    
                                    self.locationStr = NSString(format: "%@", self.totalLocation_lbl.text!)
                                    
                                    self.locationDouble = (self.locationStr).doubleValue
                                    
                                    println(self.locationDouble)
                                }
                                
                                
                                
                                
                                self.timeDict_package = self.packageDict.valueForKey("time") as! NSDictionary
                                
                                if let time_total = self.timeDict_package.valueForKey("total_value") as? Int
                                {
                                    self.totalTime_lbl.text = NSString(format: "%i", time_total) as String
                                    
                                    println(self.totalTime_lbl.text)
                                    
                                    self.timeStr = NSString(format: "%@",self.totalTime_lbl.text!)
                                    
                                    self.timeDouble = (self.timeStr).doubleValue
                                    
                                    println(self.timeDouble)
                                }
                                
                                
                                
                                if var total_package = self.packageDict.valueForKey("total") as? Int
                                {
                                    self.wholeTotal_lbl.text = NSString(format: "%i", total_package) as String
                                }
                                
                                
                                
                                
                                
                                self.unitsSold = [self.itemDouble,self.locationDouble,self.timeDouble,self.categoryDouble]
                                
                                self.months = ["Items","Location","Time","Category"]
                                
                                self.setChart(self.months, values: self.unitsSold)
                                
                                
                                
                            }
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            spinningIndicator.hide(true)

                        }
                        
                        
                        
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }


    
    @IBAction func itemsWrapperBtn(sender: AnyObject)
    {
        
        
        println("items btn clicked")
        
        if strBothInventoryPackage == "both"
        {
            
            if let itemTotal = self.itemDict_both.valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", itemTotal) as String
                
                //println(self.itemTotal_lbl.text)
            }

            
       }
        
        
        if strBothInventoryPackage == "inventory"
        {
            if let categoryTotal = self.itemDict_inventory.valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", categoryTotal) as String
                
                //println(self.itemTotal_lbl.text)
            }

        }
        
        
        if strBothInventoryPackage == "package"
        {
            
            if let categoryTotal = itemDict_package.valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", categoryTotal) as String
                
                //println(self.itemTotal_lbl.text)
            }

        }
        
        
    }
    
    
    
    @IBAction func locationWrapperBtn(sender: AnyObject)
    {
        if strBothInventoryPackage == "both"
        {
            if let location_total = self.locationDict_both.valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", location_total) as String
                
                //println(self.totalLocation_lbl.text)
            }

        }
        
        
        
        if strBothInventoryPackage == "inventory"
        {
            if let location_total = self.locationDict_inventory.valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", location_total) as String
                
                //println(self.totalLocation_lbl.text)
            }

        }
        
        
        if strBothInventoryPackage == "package"
        {
            if let location_total = locationDict_package.valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", location_total) as String
                
               // println(self.totalLocation_lbl.text)
            }
            
        }
        
    }
    
    
    @IBAction func timeWrapperBtn(sender: AnyObject)
    {
        if strBothInventoryPackage == "both"
        {
            if let time_total = self.timeDict_both.valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", time_total) as String
                
                // println(self.totalTime_lbl.text)
            }
 
        }
        
        
        if strBothInventoryPackage == "inventory"
        {
            if let time_total = self.timeDict_inventory.valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", time_total) as String
                
                //println(self.totalTime_lbl.text)
            }
        }
        
        
        if strBothInventoryPackage == "package"
        {
            if let time_total = timeDict_package.valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", time_total) as String
                
                //println(self.totalTime_lbl.text)
            }
            
        }
        
    }
    
    
    @IBAction func categoryWrapperBtn(sender: AnyObject)
    {
        
        if strBothInventoryPackage == "both"
        {
            
            if let categoryTotal = self.categoryDict_both.valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", categoryTotal) as String
                
                println(self.totalValue_lbl.text!)
            }

        }
        
        
        if strBothInventoryPackage == "inventory"
        {
            if let itemTotal = self.categoryDict_inventory .valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", itemTotal) as String
                
               // println(self.totalCategory_lbl.text)
            }

        }
        
        
        if strBothInventoryPackage == "package"
        {
            if let itemTotal = self.categoryDict_package .valueForKey("total_value") as? Int
            {
                self.totalValue_lbl.text = NSString(format: "%i", itemTotal) as String
                
                //println(self.totalCategory_lbl.text)
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    
    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
        
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
        
    }
    
    //MARK:- Back Button
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphTrackViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
    }
    
    //MARK:- Report's Buttons
    
    @IBAction func allBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportAll  = reportStoryboard.instantiateViewControllerWithIdentifier("reportAll") as! ReportAllViewController
        
        self.navigationController?.pushViewController(reportAll, animated: false)
        
        
        
        
    }
    @IBAction func itemsBtn(sender: AnyObject)
    {
        
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportItem  = reportStoryboard.instantiateViewControllerWithIdentifier("reportItem") as! ReportItemViewController
        
        self.navigationController?.pushViewController(reportItem, animated: false)
        
        
        
        
    }
    
    
    @IBAction func membersBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportMember  = reportStoryboard.instantiateViewControllerWithIdentifier("reportMember") as! ReportMemberViewController
        
        self.navigationController?.pushViewController(reportMember, animated: false)
        
        
        
    }
    
    
    @IBAction func categoryBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportCategory  = reportStoryboard.instantiateViewControllerWithIdentifier("reportCategory") as! ReportCategoryViewController
        
        self.navigationController?.pushViewController(reportCategory, animated: false)
        

        
    }
    
        
    @IBAction func locationBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportLocation  = reportStoryboard.instantiateViewControllerWithIdentifier("reportLocation") as! ReportLocaionViewController
        
        self.navigationController?.pushViewController(reportLocation, animated: false)
        
        
    }
    
    @IBAction func dateBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportDate  = reportStoryboard.instantiateViewControllerWithIdentifier("reportDate") as! ReportDateViewController
        
        self.navigationController?.pushViewController(reportDate, animated: false)
        
    }
    
    
    
    //MARK:- Filter Button
    @IBAction func filterBtn(sender: AnyObject)
    {
        blueView.hidden = false
        outerFilterView.hidden = false
        innerFilterView.hidden = true
        bagBinItemView.hidden = true
        
        
        locationView.hidden = true
        categoryView.hidden = true
    }
    
    
    @IBAction func crossBtn(sender: AnyObject)
    {
        blueView.hidden = true
        blueViewInScrollView.hidden = true
        
    }
    
    
    
    
    @IBAction func filterRefresh_Btn(sender: AnyObject)
    {
        reportValueApi()
        blueView.hidden = true

    }
    
    
    
    @IBAction func packageBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "package"
        packageBtn.hidden = true
        packageBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBtn.hidden = false
    }
    
    
    @IBAction func packageBlueBtn(sender: AnyObject)
    {
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        
    }
    
    @IBAction func inventoryBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "inventory"
        inventoryBlueBtn.hidden = false
        inventoryBtn.hidden = true
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        bothBtn.hidden = false
        
    }
    
    @IBAction func inventoryBlueBtn(sender: AnyObject)
    {
        inventoryBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
    }
    
    
    @IBAction func bothBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "both"
        bothBtn.hidden = true
        bothBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBtn.hidden = false
    }
    
    @IBAction func bothBlueBtn(sender: AnyObject)
    {
        bothBtn.hidden = false
        bothBlueBtn.hidden = true
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        
    }
    
    
    
    
    
    @IBAction func innerFilterBtn(sender: AnyObject)
    {
        blueView.hidden = false
        
        innerFilterView.hidden = false
        
        blueViewInScrollView.hidden = false
        
        bagBinItemView.hidden = true
        
        outerFilterView.hidden = true
        
        categoryView.hidden = true
        
        locationView.hidden = true
    }
    
    
    
    
    
    
    @IBAction func itemsInnerBtn(sender: AnyObject)
    {
        blueView.hidden = false
        innerFilterView.hidden = true
        outerFilterView.hidden = true
        
        bagBinItemView.hidden = false
        
         blueViewInScrollView.hidden = false
        
        categoryView.hidden = true
        locationView.hidden = true
        
    }
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func innerFilterCrossBtn(sender: AnyObject)
    {
        blueView.hidden = true
        innerFilterView.hidden = true
        
        blueViewInScrollView.hidden = true
    }
    
    
    @IBAction func innerFilterRefreshBtn(sender: AnyObject)
    {
        
        println("inner filter refresh btn clicked")
        
    
        timeInnerFilterApi()

    }
    
    
    
    //    value time filter
    //    http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/value-time-filter
    //    paramters
    //    UserId -> 49/51/253/389/ etc
    //    main_filter ->package/inventory/both
    //    Filter_By -> month/week/year
    

    
    //MARK:- Report Value Inner TIME Filter Api
    
    func timeInnerFilterApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&main_filter=%@&Filter_By=%@",userId, strBothInventoryPackage,strYearMonthDay)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/value-time-filter"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    println(dictObj)
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        
                        alert.show()
                        
                        self.blueView.hidden = true
                        
                        self.innerFilterView.hidden = true
                        
                        self.blueViewInScrollView.hidden = true
                        
                        spinningIndicator.hide(true)
                        
                    }
                    else
                    {
                        
            
                        var success = dictObj?.valueForKey("success") as! Int
                        
                        
                    
                        if success == 0
                        {
                            
                            var message = dictObj!.valueForKey("message") as! String
                            
            
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            
                            alert.show()
                            
                            self.totalTime_lbl.text = "0"
                            
                            
                            self.blueView.hidden = true
                            self.innerFilterView.hidden = true
                            
                            self.blueViewInScrollView.hidden = true
                           
                            spinningIndicator.hide(true)
                            
                        }
                        else
                        {
                            
                            self.reportValuedict = dictObj?.valueForKey("data") as! NSDictionary
                            
                            
                            println("report inner dictionary = \(self.reportValuedict)")
                            
                            println("package selected = \(strBothInventoryPackage)")
                            
                            
                            
                            if strBothInventoryPackage == "both"
                            {
                                
                                if var bagValue = dictObj?.valueForKey("avg") as? Int
                                {
                                    self.totalTime_lbl.text = NSString(format: "%i", bagValue) as String
                                    
                                    println("bag value = \(self.itemTotal_lbl.text)")
                                }
                                
                            }
                            else if strBothInventoryPackage == "package"
                            {
                                
                                
                                if var bagValue = dictObj?.valueForKey("avg") as? Int
                                {
                                    self.totalTime_lbl.text = NSString(format: "%i", bagValue) as String
                                    
                                    println("bag value = \(self.itemTotal_lbl.text)")
                                }
                            }
                                
                            else if strBothInventoryPackage == "inventory"
                            {
                                
                                if var bagValue = dictObj?.valueForKey("avg") as? Int
                                {
                                    self.totalTime_lbl.text = NSString(format: "%i", bagValue) as String
                                    
                                    println("bag value = \(self.itemTotal_lbl.text)")
                                }
                                
                            }
                            
                            self.blueView.hidden = true
                            self.innerFilterView.hidden = true
                            
                            self.blueViewInScrollView.hidden = true
                            
                            spinningIndicator.hide(true)
                            
                        }
                        
                        
                        
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }

    
    
    
    
    
    //MARK:- Bags,Bins,Item,Boxes Filter Button
    
    
    @IBAction func yearbtn(sender: AnyObject)
    {
        
        strYearMonthDay = "year"
    
        self.weekTimeLbl.text = "YEAR"
        
        yearBtn.hidden = true
        yearBlueBtn.hidden = false
        monthBlueBtn.hidden = true
        weekBlueBtn.hidden = true
        monthBtn.hidden = false
        weekBtn.hidden = false
        
        
        
    }
    
    @IBAction func yearBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        monthBlueBtn.hidden = true
        weekBlueBtn.hidden = true
        
        
    }
    
    
    
    @IBAction func monthBtn(sender: AnyObject)
    {
        strYearMonthDay = "month"
        
        self.weekTimeLbl.text = "MONTH"
        
        monthBlueBtn.hidden = false
        monthBtn.hidden = true
        weekBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        weekBtn.hidden = false
    }
    
    
    @IBAction func monthBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        monthBlueBtn.hidden = true
        monthBtn.hidden = false
        weekBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        
    }
    
    
    @IBAction func weekBtn(sender: AnyObject)
    {
        strYearMonthDay = "week"
        
        self.weekTimeLbl.text = "WEEK"
        
        weekBtn.hidden = true
        weekBlueBtn.hidden = false
        monthBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        monthBtn.hidden = false
    }
    
    
    @IBAction func weekBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        weekBtn.hidden = false
        weekBlueBtn.hidden = true
        monthBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        
        
    }
    
    
    
    
    
    
    
    
    @IBAction func bagBtn(sender: AnyObject)
    {
        
        strBagBinItem = "bag"
        bagBtn.hidden = true
        bagBlueBtn.hidden = false
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        binBtn.hidden = false
        boxBtn.hidden = false
        singleItemBtn.hidden = false
        
    }
    
    
    @IBAction func bagBlueBtn(sender: AnyObject)
    {
        strBagBinItem = ""
        bagBtn.hidden = false
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        
    }
    
    @IBAction func binBtn(sender: AnyObject)
    {
        
        strBagBinItem = "bin"
        binBtn.hidden = true
        binBlueBtn.hidden = false
        bagBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        bagBtn.hidden = false
        boxBtn.hidden = false
        singleItemBtn.hidden = false
        
        
        
        
        
    }
    
    @IBAction func binBlueBtn(sender: AnyObject)
    {
        
        strBagBinItem = ""
        binBtn.hidden = false
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        
        
        
        
    }
    
    @IBAction func boxBtn(sender: AnyObject)
    {
        
        strBagBinItem = "box"
        boxBtn.hidden = true
        boxBlueBtn.hidden = false
        binBlueBtn.hidden = true
        bagBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        binBtn.hidden = false
        bagBtn.hidden = false
        singleItemBtn.hidden = false
    
    }
    
    
    @IBAction func boxBlueBtn(sender: AnyObject)
    {
        
        strBagBinItem = ""
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        
        boxBtn.hidden = false
        
    }
    
    
    
    @IBAction func singleItemBtn(sender: AnyObject)
    {
        
        strBagBinItem = "singleItem"
        singleItemBtn.hidden = true
        singleItemBlueBtn.hidden = false
        boxBlueBtn.hidden = true
        binBlueBtn.hidden = true
        bagBlueBtn.hidden = true
        boxBtn.hidden = false
        binBtn.hidden = false
        bagBtn.hidden = false
        
    }
    
    
    
    @IBAction func singleItemBlueBtn(sender: AnyObject)
    {
        
        strBagBinItem = ""
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        
        singleItemBtn.hidden = false
        
    }
    
    
    
    @IBAction func crossBtn_itemFilter(sender: AnyObject)
    {
        blueView.hidden = true
        
        bagBinItemView.hidden = true
        
        blueViewInScrollView.hidden = true
    }
    
    
    
    
    
    @IBAction func BagBinFilterRefreshBtn(sender: AnyObject)
    {
        
        println("bag,bin,item refresh Btn clicked")
        
        reportValueInnerItemBagBinFilterApi()
        
        blueView.hidden = true
        
        bagBinItemView.hidden = true
    }
    

    
    
    
    //
    //    http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/value-item-filter
    //
    //    parameter ->   UserId -> 49/ etc
    //    main_filter -> package/inventory (by default both)
    //    Type => bag/bin/box/single item
    
    


    
    
    @IBAction func cancelTableView(sender: AnyObject)
    {
        blueView.hidden = true
        locationView.hidden = true
    }
    
    
    
    
    
    //MARK:- Report Value Inner Item Filter Api
    
    func reportValueInnerItemBagBinFilterApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&main_filter=%@&Type=%@",userId, strBothInventoryPackage,strBagBinItem)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/value-item-filter"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    println(dictObj)
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        self.blueViewInScrollView.hidden = true
                        spinningIndicator.hide(true)
                        
                        
                    }
                    else
                    {
                        
                        
                        
                        var success = dictObj?.valueForKey("success") as! Int
                        
                        
                        
                        
                        if success == 0
                        {
                            var message = dictObj!.valueForKey("message") as! String
                        
                            
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            
                            alert.show()
                            
                           self.itemTotal_lbl.text = ""
                            
                          self.blueViewInScrollView.hidden = true
                          spinningIndicator.hide(true)
                            
                        }
                        else
                        {
                            
                           self.reportValuedict = dictObj?.valueForKey("data") as! NSDictionary
                           
                            
                            println("report inner dictionary = \(self.reportValuedict)")
                            
                            println("package selected = \(strBothInventoryPackage)")
                            
                            
                            
                            if strBothInventoryPackage == "both"
                            {
                                
                               
//                                if self.strBagBinItem == "bag"
//                                {
                                     if var bagValue = dictObj?.valueForKey("total_value") as? Int
                                     {
                                        self.itemTotal_lbl.text = NSString(format: "%i", bagValue) as String
                                        
                                        println("bag value = \(self.itemTotal_lbl.text)")
                                     }
//                                }
//                                else if self.strBagBinItem == "box"
//                                {
//                                    
//                                }
//                                else if self.strBagBinItem == "singleItem"
//                                {
//                                    
//                                }
//                                else if self.strBagBinItem == "bin"
//                                {
//                                    
//                                }
//                                else
//                                {
//                                    
//                                    
//                                }
                            }
                            else if strBothInventoryPackage == "package"
                            {
                                
                                
                                if var bagValue = dictObj?.valueForKey("total_value") as? Int
                                {
                                    self.itemTotal_lbl.text = NSString(format: "%i", bagValue) as String
                                    
                                    println("bag value = \(self.itemTotal_lbl.text)")
                                }
                            }

                            else if strBothInventoryPackage == "inventory"
                            {
                                
                                if var bagValue = dictObj?.valueForKey("total_value") as? Int
                                {
                                    self.itemTotal_lbl.text = NSString(format: "%i", bagValue) as String
                                    
                                    println("bag value = \(self.itemTotal_lbl.text)")
                                }
                                    
                            }

                            
                 
                            self.blueViewInScrollView.hidden = true
                            
                            spinningIndicator.hide(true)
                            
                        }
                        
                        
                        
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }
    
    
    
    
    
    
    @IBAction func categoryInnerFilter(sender: AnyObject)
    {
        
        categoryInnerFilter.tag = 2
        
        blueView.hidden = false
        
        blueViewInScrollView.hidden = false
        
        locationView.hidden = true
        
        bagBinItemView.hidden = true
        
        outerFilterView.hidden = true
        
        innerFilterView.hidden = true
        
        categoryView.hidden = false
        
        //scrollView.hidden = true
        
       // categoryFilterApi()
    }
    
    
    
    
    //MARK: - CategoryTableViewMethods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        //if categoryInnerFilter.tag == 2
        if tableView == categoryTableView
        {
            println("category count = \(categoryValuesArray.count)")
            
            return categoryValuesArray.count
        }
        else //if locationInnerFILTER.tag == 5
        {
            
            println("location count = \(locationValuesArray.count)")
            return locationValuesArray.count
        }
        
    }
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var newEntryCategoryCell = tableView.dequeueReusableCellWithIdentifier("newEntryCategoryCell", forIndexPath: indexPath) as! NewEntryCategoryTableViewCell
        
        
        newEntryCategoryCell.deleteCategoryBtnLbl.hidden = true

        
        //if categoryInnerFilter.tag == 2
        if tableView == categoryTableView
        {
            newEntryCategoryCell.categoryLbl.text = categoryValuesArray[indexPath.row] as? String //.valueForKey("name") as? String
            
            
            
            
            
            //        categoryStr = (locationValuesArray[indexPath.row] as? String)!
            //
            //        println("category name = \(categoryStr)")
 
        }
        else //if locationInnerFILTER.tag == 5
        {
            newEntryCategoryCell.categoryLbl.text = locationValuesArray[indexPath.row] as? String //.valueForKey("name") as? String
            
            
        }

        
        
        return newEntryCategoryCell
        
    }
    
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
        //if categoryInnerFilter.tag == 2
        
        if tableView == categoryTableView
        {
            categoryStr = (categoryValuesArray[indexPath.row] as? String)!
            
            println("category name at didSelect = \(categoryStr)")
            
            categoryFilterApi()
        }
        else //if locationInnerFILTER.tag == 5
        {
            categoryStr = (locationValuesArray[indexPath.row] as? String)!
            
            println("category name at didSelect = \(categoryStr)")
            
            locationFilterApi()
        }

        
        blueView.hidden = true
    }
    
    

    
    @IBAction func locationInnerFilter(sender: AnyObject)
    {
        
        locationInnerFILTER.tag = 5
        
        blueView.hidden = false
        locationView.hidden = false
        
        blueViewInScrollView.hidden = false
        
        bagBinItemView.hidden = true
        
         outerFilterView.hidden = true
        
         innerFilterView.hidden = true
        
        
        categoryView.hidden = true
        
        
        //locationFilterApi()
    }

    
    
//    
//    value section filter(location api filter)
//    
//    http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/value-location-filter
//    
//    parameter ->   UserId -> 49/ etc
//    main_filter -> package/inventory (by default both)
//    Filter_By => location you select
    
    
    
    
    
    
    //MARK:- Location Filter Api
    
    func  locationFilterApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&main_filter=%@&Filter_By=%@",str,strBothInventoryPackage,categoryStr)
        
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/value-location-filter")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                             self.blueViewInScrollView.hidden = true
                            spinningIndicator.hide(true)
                        })
                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Int
                            if success == 0
                            {
                                
                                
                                var message = dicObj?.valueForKey("message") as! String
                                
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                self.blueViewInScrollView.hidden = true
                                spinningIndicator.hide(true)
                                
                                
                                
                            }
                                
                            else
                            {
                                
                                if var particularLocationValue = dicObj?.valueForKey("total_value") as? Int
                                {
                                    self.totalLocation_lbl.text = NSString(format: "%i", particularLocationValue) as String
                                    
                                    println("particularLocationValue = \(self.totalLocation_lbl.text)")
                                }

                                self.blueViewInScrollView.hidden = true
                                spinningIndicator.hide(true)
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        
        
        
        
        
    }
    
    
    
    
    
    
//    
//    value section filter(category api filter)
//    
//    http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/value-category-filter
//    
//    parameter ->   UserId -> 49/ etc
//    main_filter -> package/inventory (by default both)
//    Filter_By => location you select

    
    
    
    
    
    
    //MARK:- category Filter Api
    
    func  categoryFilterApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&main_filter=%@&Filter_By=%@",str,strBothInventoryPackage,categoryStr)
        
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/value-category-filter")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            self.blueViewInScrollView.hidden = true
                            spinningIndicator.hide(true)
                        })
                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Int
                            
                            if success == 0
                            {
                                
                                var message = dicObj?.valueForKey("message") as! String
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()

                                
                                 self.blueViewInScrollView.hidden = true
                                spinningIndicator.hide(true)
                                
                                
                            }
                                
                            else
                            {
                                
                                
                                if var particularLocationValue = dicObj?.valueForKey("total_value") as? Int
                                {
                                    self.totalCategory_lbl.text = NSString(format: "%i", particularLocationValue) as String
                                    
                                    println("particularLocationValue = \(self.totalCategory_lbl.text)")
                                }

                                
                                self.blueViewInScrollView.hidden = true
                                spinningIndicator.hide(true)
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        
        

    }
    

    



}
