//
//  ReportMemberViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/16/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import Charts

 class ReportMemberViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    @IBOutlet var packageBlueBtn: UIButton!
    @IBOutlet var inventoryBlueBtn: UIButton!
    @IBOutlet var bothBlueBtn: UIButton!
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var bothBtn: UIButton!
    @IBOutlet var inventoryBtn: UIButton!
    @IBOutlet var packageBtn: UIButton!
    @IBOutlet var blueView: UIView!
     
    @IBOutlet var graphAllBtn: UIButton!
    @IBOutlet var graphBagBtn: UIButton!
    @IBOutlet var graphBinBtn: UIButton!
    @IBOutlet var graphBoBtn: UIButton!
    @IBOutlet var graphSingleItemBtn: UIButton!
    
    
    
    
    @IBOutlet var packageInventoryView: UIView!
    @IBOutlet var bagBoxBinItemView: UIView!
    
    @IBOutlet var bagBtn: UIButton!
    @IBOutlet var bagBlueBtn: UIButton!
    @IBOutlet var binBtn: UIButton!
    @IBOutlet var binBlueBtn: UIButton!
    @IBOutlet var boxBtn: UIButton!
    @IBOutlet var boxBlueBtn: UIButton!
    @IBOutlet var singleItemBtn: UIButton!
    @IBOutlet var singleItemBlueBtn: UIButton!
    
      var indexPath = NSIndexPath()
      var strBagBinItem = String()
      var reportMemberArray = NSMutableArray()
    
    var unitsSold:[Double] = []
    var bagDouble = Double()
    var bagStr = NSString()
    

    var months:[String]!
    
    var yearMonthWeek:[String]!
    
    var yearWeekMonthUnitsSold:[Double] = []
    

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pieChartView: PieChartView!
    @IBOutlet var dateBtn: UIButton!
    
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        scrollView.contentSize.height  = dateBtn.frame.origin.y + dateBtn.frame.size.height+10
        
//        let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
//        let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0]
        
        
        // BarChart()
        
        pieChartView.descriptionText = ""
        pieChartView.drawHoleEnabled = false
        
        
        var lbl = pieChartView.legend as ChartLegend
        
        lbl.position = .RightOfChart
        lbl.xEntrySpace = 7.0
        lbl.yEntrySpace = 0.0
        lbl.yOffset = 0.0

        strBothInventoryPackage = "package"
        strBagBinItem = "bag"
        
        
    
        //setChart(months, values: unitsSold)
        
        memberGraphApi()
    }
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    func setChart(dataPoints: [String], values: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count
        {
            let dataEntry = ChartDataEntry(value: values[i], xIndex: i)
            dataEntries.append(dataEntry)
            
        }
        
        let pieChartDataSet = PieChartDataSet(yVals: dataEntries, label: "Units Sold")
        pieChartDataSet.sliceSpace = 3.0
        
        
        let pieChartData = PieChartData(xVals: dataPoints, dataSet: pieChartDataSet)
        pieChartView.data = pieChartData
        
        var colors: [UIColor] = []
        
        for i in 0..<dataPoints.count
        {
            let red = Double(arc4random_uniform(256))
            let green = Double(arc4random_uniform(256))
            let blue = Double(arc4random_uniform(256))
            
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            
            colors.append(color)
        }
        
        pieChartDataSet.colors = colors
        
        
    }
    
    
    
    
    
    
    //MARK:- BagBinItem Inner Api
    func  bagBinItemInnerApi()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
    
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&main_filter=%@&Filter_by=%@",userId, strBothInventoryPackage,strBagBinItem)
        
        
        
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/members"
        
        
        
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    println(dictObj)
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        
                        
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                        
                    }
                    else
                    {
                        
                        var success = dictObj?.valueForKey("success") as! Bool
                        
                        if success == false
                        {
                            //var dict = dictObj?.valueForKey("success") as! NSDictionary
                            var message = dictObj!.valueForKey("message") as! String
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            
                            self.reportMemberArray = dictObj?.valueForKey("Data") as! NSMutableArray
                            
                            self.collectionView.reloadData()
                            
                            spinningIndicator.hide(true)
                        }
                        
                        
                        
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }
    
    
    
    


    
    
    
    
    
     //MARK:- MemberGraph Api
    func  memberGraphApi()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
    
        
        
//        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/members"
//        
//        var url = NSURL(string: path)
//        
//        var urlRequest = NSMutableURLRequest(URL: url!)
//        
//               
//        var session  = NSURLSession.sharedSession()
//        
//        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
        
            
        
            
            var data = NSData()
            
            var post = NSString(format: "UserId=%i&main_filter=%@",userId, strBothInventoryPackage)
        
        
        
        
            println(post)
            
            data = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength = String(data.length)
            
            
            var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/members"
        
        
     
            
            var url = NSURL(string: path)
            
            var urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = data
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            var session  = NSURLSession.sharedSession()
            
            var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
                
                
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    println(dictObj)
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        
                        
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
    
                    }
                    else
                    {
                        
                        
                        
                        var success = dictObj?.valueForKey("success") as! Bool
                        
                        if success == false
                        {
                            //var dict = dictObj?.valueForKey("success") as! NSDictionary
                            var message = dictObj!.valueForKey("message") as! String
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            self.reportMemberArray = dictObj?.valueForKey("Data") as! NSMutableArray
                            self.collectionView.reloadData()
                            println( self.reportMemberArray)
                            
                            
                            self.yearMonthWeek = self.reportMemberArray.valueForKey("FirstName") as! NSArray as! [String]
                            
                            self.yearWeekMonthUnitsSold = self.reportMemberArray.valueForKey("percentage") as! NSArray as! [Double]
                            
                            
                            println(self.yearMonthWeek)
                            
                            println(self.yearWeekMonthUnitsSold)
                            
                            self.unitsSold = self.yearWeekMonthUnitsSold
                            
                            //self.months = ["Bag", "Box", "Single Item", "Bin"]
                            
                            self.months = self.yearMonthWeek
                            
                            self.setChart(self.months, values: self.unitsSold)

                            
                            spinningIndicator.hide(true)
                        }

                        
                        
                      
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }

    
    
    
    
    
    
    
    
    
    
    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
        
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
        
    }
    
    @IBAction func filterRefresh_Btn(sender: AnyObject)
    {
        //memberGraphApi()
        blueView.hidden = true
        //bagBinItemInnerApi()
    }
    
    
    @IBAction func mainFilterRefreshBtn(sender: AnyObject)
    {
        memberGraphApi()
        blueView.hidden = true
    }
    
    
    //MARK:- Back Button
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphTrackViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
    }
    
    
    
    //MARK:- Report's Buttons
    
    @IBAction func allBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportAll  = reportStoryboard.instantiateViewControllerWithIdentifier("reportAll") as! ReportAllViewController
        
        self.navigationController?.pushViewController(reportAll, animated: false)
        
    }
    
    
    
    
    @IBAction func itemsBtn(sender: AnyObject)
    {
        
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportItem  = reportStoryboard.instantiateViewControllerWithIdentifier("reportItem") as! ReportItemViewController
        
        self.navigationController?.pushViewController(reportItem, animated: false)
        
    }
    
    
    
    
    
    @IBAction func categoryBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportCategory  = reportStoryboard.instantiateViewControllerWithIdentifier("reportCategory") as! ReportCategoryViewController
        
        self.navigationController?.pushViewController(reportCategory, animated: false)
        
    }
    
    
    
    
    @IBAction func ValueBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportValue  = reportStoryboard.instantiateViewControllerWithIdentifier("reportValue") as! ReportValueViewController
        
        self.navigationController?.pushViewController(reportValue, animated: false)
        
    }
    
    
    
    
    @IBAction func locationBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportLocation  = reportStoryboard.instantiateViewControllerWithIdentifier("reportLocation") as! ReportLocaionViewController
        
        self.navigationController?.pushViewController(reportLocation, animated: false)
        
        
    }
    
    
    
    
    
    @IBAction func dateBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportDate  = reportStoryboard.instantiateViewControllerWithIdentifier("reportDate") as! ReportDateViewController
        
        self.navigationController?.pushViewController(reportDate, animated: false)
        
    }
    
    
    
    
    
    
    //MARK:- CollectionView Methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        println(reportMemberArray.count)
        return reportMemberArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        var cell:ReportMemberCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("reportMemberCell", forIndexPath: indexPath) as! ReportMemberCollectionViewCell
        
        
        cell.filterBtnCell.tag = indexPath.row
        println("cell filter value = \(cell.filterBtnCell.tag)")
        
        
        //cell.imageView.image = UIImage(named: "test_img_1.png")
        
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
//        var profileUrl: String = arrayImages[0].valueForKey("Image") as! String
//        
//        var url: NSURL = NSURL(string: profileUrl)!
        
//        cell.openImagevIew.sd_setImageWithURL(url, completed: block)
        
        
        

        var profileUrl: String = reportMemberArray[indexPath.row].valueForKey("ProfileImage") as! String
        
        var url: NSURL = NSURL(string: profileUrl)!
        
//        if let data : NSData = NSData(contentsOfURL: url)
//        {
//            //cell.imageView.image = UIImage(data: data)!
        
             cell.imageView.sd_setImageWithURL(url, completed: block)

            //imageView.image = UIImage(data: data)!
//        }
//        else
//        {
//             cell.imageView.image = UIImage(named: "test_img_1.png")
//             //cell.imageView.image = UIImage(named: "unknown.png")
//        }
//        
        
        
        
        
        
        cell.nameLbl.text = ""
        if let strName = reportMemberArray[indexPath.row].valueForKey("FirstName") as? String
        {
            
            
            cell.nameLbl.text = NSString(format: "NAME: %@", strName) as String
        }
        
        

        
        
        var strBagItem = ""
        var strBagItemCount = ""
        
        if let bagItem = reportMemberArray[indexPath.row].valueForKey("Data_Type") as? String
        {
            strBagItem = bagItem
        }
        if let bagItemCount = reportMemberArray[indexPath.row].valueForKey("Bag_count") as? Int
        {
            strBagItemCount = NSString(format: "%i", bagItemCount) as String
        }
        
        var strTogether = NSString(format: "%@ %@", strBagItemCount,strBagItem)
        cell.bagItemLbl.text = strTogether as String

        
        
        
        
        cell.itemLbl.text  = ""
        
        if let strItem  = reportMemberArray[indexPath.row].valueForKey("Item_count") as? Int
        {
            cell.itemLbl.text = NSString(format: "%i Items", strItem) as String
            
            
        }
        
        
               
        
        
        return cell
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
//        cell.filterBtnCell.tag = indexPath.row
//        println("cell filter value = \(cell.filterBtnCell.tag)")
    }
    
    

    //MARK:- Graph Button
    @IBAction func graphBagBtn(sender: AnyObject)
    {
        strBagBinItem = "bag"
        graphBagBtn.hidden = true
        graphBinBtn.hidden = false
        graphBoBtn.hidden = true
        graphAllBtn.hidden = true
        graphSingleItemBtn.hidden = true
        
        graphBagBtn.setImage(UIImage(named: "bag.png"), forState: UIControlState.Normal)
        
        bagBinItemInnerApi()
        
    }
    
    

    @IBAction func graphBinBtn(sender: AnyObject)
    {
        strBagBinItem = "bin"
        graphBagBtn.hidden = true
        graphBinBtn.hidden = true
        graphBoBtn.hidden = false
        graphAllBtn.hidden = true
        graphSingleItemBtn.hidden = true
        
        graphBinBtn.setImage(UIImage(named: "bin.png"), forState: UIControlState.Normal)
        
        bagBinItemInnerApi()
    }
    
    @IBAction func graphBoxBtn(sender: AnyObject)
    {
        strBagBinItem = "box"
        graphBagBtn.hidden = true
        graphBinBtn.hidden = true
        graphBoBtn.hidden = true
        graphAllBtn.hidden = true
        graphSingleItemBtn.hidden = false
        
        graphBoBtn.setImage(UIImage(named: "box.png"), forState: UIControlState.Normal)
        
        bagBinItemInnerApi()
        
    }
    
    
    @IBAction func graphSingleBtn(sender: AnyObject)
    {
        strBagBinItem = "singleItem"
        graphBagBtn.hidden = true
        graphBinBtn.hidden = true
        graphBoBtn.hidden = true
        graphAllBtn.hidden = false
        graphSingleItemBtn.hidden = true
        
        graphSingleItemBtn.setImage(UIImage(named: "singleitem.png"), forState: UIControlState.Normal)
        
        bagBinItemInnerApi()
    }
    
    
    @IBAction func graphAllBtn(sender: AnyObject)
    {
        strBagBinItem = "all"
        graphBagBtn.hidden = false
        graphBinBtn.hidden = true
        graphBoBtn.hidden = true
        graphAllBtn.hidden = true
        graphSingleItemBtn.hidden = true
        
         graphAllBtn.setImage(UIImage(named: "all.png"), forState: UIControlState.Normal)
        
        bagBinItemInnerApi()
    }

        
    //MARK:- Filter Button
    
    @IBAction func filterBtn(sender: AnyObject)
    {
        blueView.hidden = false
        bagBoxBinItemView.hidden = true
        packageInventoryView.hidden = false
    }
    
    
    @IBAction func crossBtn(sender: AnyObject)
    {
        blueView.hidden = true
        
    }
    
    @IBAction func packageBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "package"
        packageBtn.hidden = true
        packageBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBtn.hidden = false
       
//        packageBtn.hidden = true
//        packageBlueBtn.hidden = false
//        inventoryBlueBtn.hidden = true
//        bothBlueBtn.hidden = true
//        inventoryBtn.hidden = false
//        bothBtn.hidden = false
    }
    
    
    @IBAction func packageBlueBtn(sender: AnyObject)
    {
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        
    }
    
    @IBAction func inventoryBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "inventory"
        inventoryBlueBtn.hidden = false
        inventoryBtn.hidden = true
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        bothBtn.hidden = false
        
    }
    
    @IBAction func inventoryBlueBtn(sender: AnyObject)
    {
        inventoryBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
    }
    
    
    @IBAction func bothBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "both"
        bothBtn.hidden = true
        bothBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBtn.hidden = false
    }
    
    @IBAction func bothBlueBtn(sender: AnyObject)
    {
        bothBtn.hidden = false
        bothBlueBtn.hidden = true
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        
    }

    
     //MARK:- CollectionCell Filter Button

    @IBAction func collectionCellFilterBtn(sender: AnyObject)
    {
        blueView.hidden = false

        indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        println("indexpath = \(indexPath)")
    }
    
    
    
    
    @IBAction func bagBtn(sender: AnyObject)
    {
        strBagBinItem = "bag"
        bagBtn.hidden = true
        bagBlueBtn.hidden = false
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        binBtn.hidden = false
        boxBtn.hidden = false
        singleItemBtn.hidden = false
        
    }
    
    
    @IBAction func bagBlueBtn(sender: AnyObject)
    {
        strBagBinItem = ""
        bagBtn.hidden = false
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        
    }
    
    @IBAction func binBtn(sender: AnyObject)
    {
        strBagBinItem = "bin"
        binBtn.hidden = true
        binBlueBtn.hidden = false
        bagBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        bagBtn.hidden = false
        boxBtn.hidden = false
        singleItemBtn.hidden = false


        
        
        
    }
    
    @IBAction func binBlueBtn(sender: AnyObject)
    {
        strBagBinItem = ""
        binBtn.hidden = false
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true

        
        
        
    }
    
    @IBAction func boxBtn(sender: AnyObject)
    {
        strBagBinItem = "box"
        boxBtn.hidden = true
        boxBlueBtn.hidden = false
        binBlueBtn.hidden = true
        bagBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        binBtn.hidden = false
        bagBtn.hidden = false
        singleItemBtn.hidden = false
        
        
    }
    
    
    @IBAction func boxBlueBtn(sender: AnyObject)
    {
        strBagBinItem = ""
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true

        boxBtn.hidden = false
        
    }
    
    @IBAction func singleItemBtn(sender: AnyObject)
    {
        strBagBinItem = "singleItem"
        singleItemBtn.hidden = true
        singleItemBlueBtn.hidden = false
        boxBlueBtn.hidden = true
        binBlueBtn.hidden = true
        bagBlueBtn.hidden = true
        boxBtn.hidden = false
        binBtn.hidden = false
        bagBtn.hidden = false
        
    }
    
    @IBAction func singleItemBlueBtn(sender: AnyObject)
    {
        strBagBinItem = ""
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true

        singleItemBtn.hidden = false
        
    }
    
    
    
    
    
    
   
}
