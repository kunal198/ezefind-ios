//
//  ReportLocationCollectionViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 12/18/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ReportLocationCollectionViewCell: UICollectionViewCell
{
    
    
    @IBOutlet weak var locationValue_label: UILabel!
    @IBOutlet weak var locationName_lbl: UILabel!
}
