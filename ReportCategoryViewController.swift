//
//  ReportCategoryViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/16/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import Charts

class ReportCategoryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource
{
   // var reportMemberArray:NSDictionary = NSDictionary[String,AnyObject]()

     var reportMemberArray = NSMutableArray()
    
    
    var reportArray = NSArray()
    
    
    var reportMemberDictionary = NSDictionary()
    
    var i:Int = Int()
    
    var unitsSold:[Double] = []
    var bagDouble = Double()
    var bagStr = NSString()
    
    
    
    
    var months:[String]!
    
    var yearMonthWeek:[String]!
    
    var yearWeekMonthUnitsSold:[Double] = []
    
    
    var collectionViewHeight = Float()
    
    var componentArray: [AnyObject] = []
    var valuesArray: [AnyObject] = []
    

    @IBOutlet weak var wrapperViewTOtal: UIView!
    @IBOutlet weak var outerFilterView: UIView!
    @IBOutlet weak var innerFilterView: UIView!
    @IBOutlet weak var total_Items_lbl: UILabel!
    @IBOutlet weak var totalValue_lbl: UILabel!
    
    //let jsonResult = [Dictionary<String, AnyObject>]()
  
    @IBOutlet var scrollView: UIScrollView!
    
    
    @IBOutlet weak var pantsName2_lbl: UILabel!
    @IBOutlet weak var pantsValue2_lbl: UILabel!
    @IBOutlet weak var shirtsName2_lbl: UILabel!
    @IBOutlet weak var shirtsValue2_lbl: UILabel!
    @IBOutlet weak var shirtsValue1_lbl: UILabel!
    
    @IBOutlet weak var pantsName_lbl: UILabel!
    @IBOutlet weak var pantsValue_lbl: UILabel!
    @IBOutlet weak var shirtsName1_lbl: UILabel!
    @IBOutlet weak var categoryName_btn: UIButton!
    
    @IBOutlet var dateBtn: UIButton!
    
    @IBOutlet var packageBlueBtn: UIButton!
    @IBOutlet var inventoryBlueBtn: UIButton!
    @IBOutlet var bothBlueBtn: UIButton!
    
    @IBOutlet var bothBtn: UIButton!
    @IBOutlet var inventoryBtn: UIButton!
    @IBOutlet var packageBtn: UIButton!

    @IBOutlet weak var collectionView1: UICollectionView!
    @IBOutlet var blueView: UIView!
    
    @IBOutlet var barScrollView: UIScrollView!
    @IBOutlet var barChartView: BarChartView!
    
    @IBOutlet var itemScrollView: UIScrollView!
     @IBOutlet var totalLbl: UILabel!
    
    
    
    @IBOutlet weak var yearBtn: UIButton!
    
    @IBOutlet weak var yearBlueBtn: UIButton!
    
    @IBOutlet weak var monthBtn: UIButton!
    
    @IBOutlet weak var monthBlueBtn: UIButton!
    
    @IBOutlet weak var weekBlueBtn: UIButton!
    @IBOutlet weak var weekBtn: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        scrollView.contentSize.height  = dateBtn.frame.origin.y + dateBtn.frame.size.height+10
        
        itemScrollView.contentSize.height  = wrapperViewTOtal.frame.origin.y + wrapperViewTOtal.frame.size.height + 50
        
        barScrollView.contentSize.width = barChartView.frame.origin.x + barChartView.frame.size.width
        
        //itemScrollView.contentSize.height = 1200
        
        println("item scroll view height = \(itemScrollView.contentSize.height)")
        
//        barChartView.noDataText = "You need to provide data for the chart."
//        barChartView.noDataTextDescription = "GIVE REASON"
//        barChartView.descriptionText = ""
//        barChartView.xAxis.labelPosition = .Bottom
        
        
        
        BarChart()
        
        //months = ["SHIRTS", "PANTS"]
        
        //let unitsSold = [1.0, 1.0]
        
        
        strBothInventoryPackage = "package"
        
        
        
        self.wrapperViewTOtal.hidden = true
        
        
        //  self.collectionView1.frame = CGRectMake(0, self.collectionView1.frame.origin.y, self.collectionView1.frame.size.width, 105*(reportMemberArray.count/2));

        
        var x:CGFloat = CGFloat()
        x = 0
        
        var y:CGFloat = CGFloat()
        y = 0
        
        
        
      
        
        
        
       // setChart(months, values: unitsSold)

        reportCategoryApi()
        
    }
    
    
    @IBAction func categoryName_BtnAction(sender: AnyObject)
    {
        
    }
    
    
    
    
    
    //MARK:- BarChart
    
    
    func BarChart()
    {
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.noDataTextDescription = "GIVE REASON"
        barChartView.descriptionText = ""
        
        
        barChartView.drawBarShadowEnabled = false
        barChartView.drawValueAboveBarEnabled = true
        barChartView.maxVisibleValueCount = 60
        barChartView.pinchZoomEnabled = false
        barChartView.drawGridBackgroundEnabled = false
        
        var xaxis = barChartView.xAxis as ChartXAxis
        xaxis.labelPosition = .Bottom
        xaxis.labelFont = UIFont.systemFontOfSize(10)
        
        xaxis.spaceBetweenLabels = 2
        
        
        
        
        var leftAxis = barChartView.leftAxis as ChartYAxis
        
        
        leftAxis.labelFont = UIFont.systemFontOfSize(10)
        leftAxis.labelCount = 4
        leftAxis.valueFormatter = NSNumberFormatter.alloc()
        leftAxis.valueFormatter!.maximumFractionDigits = 1
        leftAxis.valueFormatter!.negativeSuffix = " $"
        leftAxis.valueFormatter!.positiveSuffix = " $"
        leftAxis.labelPosition = .OutsideChart
        leftAxis.spaceTop = 5
        
        
        var rightAxis = barChartView.rightAxis as ChartYAxis
        rightAxis.drawGridLinesEnabled = false
        rightAxis.labelFont = UIFont.systemFontOfSize(10)
        rightAxis.labelCount = 4
        rightAxis.valueFormatter = leftAxis.valueFormatter
        rightAxis.spaceTop = 5
        
        barChartView.legend.position = .BelowChartLeft
        barChartView.legend.form = .Square
        barChartView.legend.formSize = 9.0
        barChartView.legend.font = UIFont(name: "HelveticaNeue-Light", size: 11.0)!
        barChartView.legend.xEntrySpace = 4.0
        
        
    }
    

    
    
    
    
    
    func setChart(dataPoints:[String], values:[Double])
    {
        
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for  i in 0..<dataPoints.count
            
        {
            let dataEntry = BarChartDataEntry(value:values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
        let chartData = BarChartData(xVals: months, dataSet: chartDataSet)
        barChartView.data = chartData
        
        
    }
    

    
    func setChartYearMonthWeek(dataPoints:[String], values:[Double])
    {
        
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for  i in 0..<dataPoints.count
            
        {
            let dataEntry = BarChartDataEntry(value:values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
        let chartData = BarChartData(xVals: yearMonthWeek, dataSet: chartDataSet)
        barChartView.data = chartData
        
        
        
        
        
    }
    

    
    
    
    
    
    
    
    //MARK:- Report Category Api
    
    
    func reportCategoryApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&main_filter=%@",userId, strBothInventoryPackage)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/category"//&UserId=\(userId)&main_filter=\(strBothInventoryPackage)"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    println(dictObj!)
                    
                    
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        
                        alert.show()
                        
                        //self.wrapperViewTOtal.hidden = true
                        
                        spinningIndicator.hide(true)
                        
                        
                    }
                    else
                    {
                        
                        
                        var success = dictObj?.valueForKey("success") as! Bool
                        
                        if success == false
                        {
                            //var dict = dictObj?.valueForKey("success") as! NSDictionary
                            var message = dictObj!.valueForKey("message") as! String
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            
                            self.wrapperViewTOtal.hidden = true
                            
                            spinningIndicator.hide(true)
                        }
                        
                        else
                        {
                            
                        var total_ItemsCheck = dictObj?.valueForKey("Total_Items") as? Int
                            
                         println("total check = \(total_ItemsCheck!)")
                            
                            
                            self.wrapperViewTOtal.hidden = false
                            
                            
                            if success == true &&  total_ItemsCheck == 0
                            {
                                
                                self.reportArray = dictObj?.valueForKey("Data") as! NSArray
                                
                                self.collectionView1.reloadData()

                            }
                            else
                            {
                                
                                self.reportMemberArray = dictObj?.valueForKey("Data") as! NSMutableArray
                                
                                self.collectionView1.reloadData()
                                
                                println( self.reportMemberArray)
                                
                                if let total_Items
                                    = dictObj?.valueForKey("Total_Items") as? Int
                                {
                                    self.total_Items_lbl.text = NSString(format: "%i", total_Items) as String
                                    
                                    println("total items are = \(self.total_Items_lbl.text)")
                                }
                                
                                //                        var array = [String]()
                                //
                                //                        array = self.reportMemberArray.valueForKey("name") as String
                                
                                
                                
                                if let totalCategory = self.reportMemberArray.count as? Int
                                {
                                    self.totalValue_lbl.text = NSString(format: "%i", totalCategory) as String
                                    
                                    println("total categories are = \(self.totalValue_lbl.text)")
                                }
                                
                                
                                
                                
                                println(self.reportMemberDictionary.allKeys)
                                
                                
                                //  var array = self.reportMemberArray.valueForKey("name") as? NSMutableArray
                                
                                println(self.reportMemberArray.valueForKey("count"))
                                
                                
                                self.yearMonthWeek = self.reportMemberArray.valueForKey("name") as! NSArray as! [String]
                                
                                self.yearWeekMonthUnitsSold = self.reportMemberArray.valueForKey("count") as! NSArray as! [Double]
                                
                                
                                println(self.yearMonthWeek)
                                
                                println(self.yearWeekMonthUnitsSold)
                                
                                self.unitsSold = self.yearWeekMonthUnitsSold
                                
                                //self.months = ["Bag", "Box", "Single Item", "Bin"]
                                
                                self.months = self.yearMonthWeek
                                
                                self.setChart(self.months, values: self.unitsSold)
                                

                            }
                            
                            
                            
                            spinningIndicator.hide(true)
                        }

                        
                        
                        
                    
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }

    
    
    //MARK:- CollectionView Methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        println(reportMemberArray.count)
        return reportMemberArray.count
    }
    
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        var cell:ReportCategoryCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! ReportCategoryCollectionViewCell
        
        
        
        
        for i = 0; i < self.reportMemberDictionary.allKeys.count; i++
        {
            println("key = \(self.reportMemberDictionary.allKeys[i])")
            println("value = \(self.reportMemberDictionary.allValues[i])")
            
            
        }
       
        
    
       // cell.valueLabel.text = ""
        if let strName = reportMemberArray[indexPath.row].valueForKey("count") as? Int
        {
            
            cell.valueLabel.text = NSString(format: "%i", strName) as String
           // cell.valueLabel.text = NSString(format: "Value: %@", strName) as String
            
             println(cell.valueLabel.text)
        }
        
        
        //cell.shirtsName_lbl.text = ""
        if let category = reportMemberArray[indexPath.row].valueForKey("name") as? String
        {
            
            cell.shirtsName_lbl.text = category as String
            //cell.shirtsName_lbl.text = NSString(format: "NAME: %@", category) as String
            println(cell.shirtsName_lbl.text)
            
            
            self.bagDouble = (self.bagStr).doubleValue
            
            println(self.bagDouble)
            
        }
        
        
        
        var collectionViewWidth = self.view.frame.size.width
        
        println("collection view width = \(collectionViewWidth)")
        
        collectionViewHeight = Float((self.reportMemberArray.count/2)*99)
        
        collectionView1.contentSize.height = CGFloat(collectionViewHeight)
        
        println("collection view Height = \(collectionViewHeight)")

        
        scrollView.contentSize.height =  CGFloat(collectionViewHeight)
        
       // dateBtn.frame.origin.y + dateBtn.frame.size.height+10 +
        
        println(scrollView.contentSize.height)
        
        
        
        
        
        if self.reportMemberArray.count%2 == 0
        {
            collectionView1.frame = CGRectMake(0,0, self.collectionView1.frame.size.width, collectionView1.contentSize.height)
            
            
            barScrollView.frame.origin.y = collectionView1.frame.origin.y + collectionView1.frame.size.height + 30
            
            wrapperViewTOtal.frame.origin.y = barScrollView.frame.origin.y + barScrollView.frame.size.height + 30
            
            itemScrollView.contentSize.height =  wrapperViewTOtal.frame.origin.y + wrapperViewTOtal.frame.size.height + 20
            
            
        }
        else
        {
            collectionView1.frame = CGRectMake(0,0, self.collectionView1.frame.size.width, collectionView1.contentSize.height + 210)
            
            
            barScrollView.frame.origin.y = collectionView1.frame.origin.y + collectionView1.frame.size.height + 50
            
            wrapperViewTOtal.frame.origin.y = barScrollView.frame.origin.y + barScrollView.frame.size.height + 30
            
            itemScrollView.contentSize.height =  wrapperViewTOtal.frame.origin.y + wrapperViewTOtal.frame.size.height + 20
            
            
        }

        
        
        
//        
//        collectionView1.frame = CGRectMake(0,0, self.collectionView1.frame.size.width, collectionView1.contentSize.height)
//        
//        
//        barScrollView.frame.origin.y = collectionView1.frame.origin.y + collectionView1.frame.size.height + 20
//        
//        wrapperViewTOtal.frame.origin.y = barScrollView.frame.origin.y + barScrollView.frame.size.height + 30
//        
//        itemScrollView.contentSize.height =  wrapperViewTOtal.frame.origin.y + wrapperViewTOtal.frame.size.height + 20

        
        
        return cell
    }
    
    
    
    
    
//    func collectionView(collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize
//    {
//        var cellSize:CGSize = CGSizeMake(115, 105)
//        
//        println("cell size = \(cellSize)")
//        
//        return cellSize
//    }

    
    
    
    
    
    
    func collectionViewContentSize() -> CGSize
    {
        
        var collectionViewWidth = self.view.frame.size.width
        
        println("collection view width = \(collectionViewWidth)")
        
        var collectionViewHeight = (self.reportMemberArray.count/2)*99
        
        collectionView1.contentSize.height = CGFloat(collectionViewHeight)
        
        println("collection view Height = \(collectionView1.contentSize.height)")

        
        return CGSizeMake(collectionViewWidth, collectionView1.contentSize.height);
    }
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    
    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(GraphViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
        
    }
    
    
    
    //MARK:- Back Button
    
    @IBAction func backBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(GraphTrackViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
    }
    
    
    
    //MARK:- Report's Buttons
    
    @IBAction func allBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportAll  = reportStoryboard.instantiateViewControllerWithIdentifier("reportAll") as! ReportAllViewController
        
        self.navigationController?.pushViewController(reportAll, animated: false)
        
    }
    
    
    
    @IBAction func itemsBtn(sender: AnyObject)
    {
        
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportItem  = reportStoryboard.instantiateViewControllerWithIdentifier("reportItem") as! ReportItemViewController
        
        self.navigationController?.pushViewController(reportItem, animated: false)
        
    }
    
    
    
    
    @IBAction func membersBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportMember  = reportStoryboard.instantiateViewControllerWithIdentifier("reportMember") as! ReportMemberViewController
        
        self.navigationController?.pushViewController(reportMember, animated: false)
        
    }
    
    
   
    
    
    @IBAction func ValueBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportValue  = reportStoryboard.instantiateViewControllerWithIdentifier("reportValue") as! ReportValueViewController
        
        self.navigationController?.pushViewController(reportValue, animated: false)
        
        
    }
    
    
    
    
    @IBAction func locationBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportLocation  = reportStoryboard.instantiateViewControllerWithIdentifier("reportLocation") as! ReportLocaionViewController
        
        self.navigationController?.pushViewController(reportLocation, animated: false)
        
        
    }
    
    
    
    @IBAction func dateBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportDate  = reportStoryboard.instantiateViewControllerWithIdentifier("reportDate") as! ReportDateViewController
        
        self.navigationController?.pushViewController(reportDate, animated: false)
        
    }
    
    
    
    //MARK:- Filter Button
    
    @IBAction func filterBtn(sender: AnyObject)
    {
        blueView.hidden = false
        innerFilterView.hidden = true
        outerFilterView.hidden = false
    }
    
    
    
    @IBAction func crossBtn(sender: AnyObject)
    {
        blueView.hidden = true
    }
    
    
    
    @IBAction func innerFilterBtn(sender: AnyObject)
    {
       blueView.hidden = false
        innerFilterView.hidden = false
        outerFilterView.hidden = true
    }
    
    
    
    @IBAction func innerFilterCrossBtn(sender: AnyObject)
    {
        blueView.hidden = true
        innerFilterView.hidden = true
    }
    
    
    
    @IBAction func innerFilterRefreshBtn(sender: AnyObject)
    {
        blueView.hidden = true
    }
    
    
    
    
    
    //MARK:- Bags,Bins,Item,Boxes Filter Button
    
    
    @IBAction func yearbtn(sender: AnyObject)
    {
        
        strYearMonthDay = "year"
        yearBtn.hidden = true
        yearBlueBtn.hidden = false
        monthBlueBtn.hidden = true
        weekBlueBtn.hidden = true
        monthBtn.hidden = false
        weekBtn.hidden = false
        
    }
    
    
    
    @IBAction func yearBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        monthBlueBtn.hidden = true
        weekBlueBtn.hidden = true
        
        
    }
    
    
    
    @IBAction func monthBtn(sender: AnyObject)
    {
        strYearMonthDay = "month"
        monthBlueBtn.hidden = false
        monthBtn.hidden = true
        weekBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        weekBtn.hidden = false
    }
    
    
    @IBAction func monthBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        monthBlueBtn.hidden = true
        monthBtn.hidden = false
        weekBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        
    }
    
    
    @IBAction func weekBtn(sender: AnyObject)
    {
        strYearMonthDay = "week"
        weekBtn.hidden = true
        weekBlueBtn.hidden = false
        monthBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        monthBtn.hidden = false
    }
    
    
    @IBAction func weekBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        weekBtn.hidden = false
        weekBlueBtn.hidden = true
        monthBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        
        
    }
    
//    @IBAction func bagsFilter(sender: AnyObject)
//    {
//        strBagBinBoxSingleitem = "bag"
//        blueView.hidden = false
//        viewFilter.hidden = true
//        bagsBoxesFilterView.hidden = false
//    }
//    
//    @IBAction func boxesFilter(sender: AnyObject)
//    {
//        strBagBinBoxSingleitem = "box"
//        blueView.hidden = false
//        viewFilter.hidden = true
//        bagsBoxesFilterView.hidden = false
//        
//        
//    }
//    
//    @IBAction func binsFilter(sender: AnyObject)
//    {
//        strBagBinBoxSingleitem = "bin"
//        blueView.hidden = false
//        viewFilter.hidden = true
//        bagsBoxesFilterView.hidden = false
//        
//        
//    }
//    
//    @IBAction func  singleItemFilter(sender: AnyObject)
//    {
//        strBagBinBoxSingleitem = "single item"
//        blueView.hidden = false
//        viewFilter.hidden = true
//        bagsBoxesFilterView.hidden = false
//        
//        
//    }
//
    
    
    
    
    
    
    
    @IBAction func filterRefresh_Btn(sender: AnyObject)
    {
        reportCategoryApi()
        blueView.hidden = true
    }
    
    
    
    @IBAction func packageBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "package"
        packageBtn.hidden = true
        packageBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBtn.hidden = false
    }
    
    
    @IBAction func packageBlueBtn(sender: AnyObject)
    {
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        
    }
    
    @IBAction func inventoryBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "inventory"
        inventoryBlueBtn.hidden = false
        inventoryBtn.hidden = true
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        bothBtn.hidden = false
        
    }
    
    @IBAction func inventoryBlueBtn(sender: AnyObject)
    {
        inventoryBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
    }
    
    
    @IBAction func bothBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "both"
        bothBtn.hidden = true
        bothBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBtn.hidden = false
    }
    
    @IBAction func bothBlueBtn(sender: AnyObject)
    {
        bothBtn.hidden = false
        bothBlueBtn.hidden = true
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        
    }

}
