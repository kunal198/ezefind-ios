import UIKit
import QuartzCore

class RangeSliderTrackLayer: CALayer
{
    weak var rangeSlider: RangeSlider?
    
    override func drawInContext(ctx: CGContext!)
    {
        if let slider = rangeSlider
        {
            // Clip
            let cornerRadius = bounds.height * slider.curvaceousness / 2.0
            let path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
            CGContextAddPath(ctx, path.CGPath)
            
            // Fill the track
            CGContextSetFillColorWithColor(ctx, slider.trackTintColor.CGColor)
            CGContextAddPath(ctx, path.CGPath)
            CGContextFillPath(ctx)
            
            // Fill the highlighted range
            CGContextSetFillColorWithColor(ctx, slider.trackHighlightTintColor.CGColor)
            let lowerValuePosition = Int(slider.positionForValue(slider.lowerValue))
            
            println("lower value = \(lowerValuePosition)")
            
            var upperValuePosition = Int(slider.positionForValue(slider.upperValue))
            
            println("upper value = \(upperValuePosition)")
            
            var rect = CGRect(x: lowerValuePosition, y: 0, width: upperValuePosition - lowerValuePosition, height: 10)
            
             println("rect value = \(rect)")
            
            CGContextFillRect(ctx, rect)
        }
    }
}