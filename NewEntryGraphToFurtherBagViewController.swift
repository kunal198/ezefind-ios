//
//  NewEntryGraphToFurtherViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

var strBagToFurtherScanBarcode = NSString()
var locationNameStrGps = NSString()

class NewEntryGraphToFurtherBagViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UIPickerViewDelegate,UINavigationControllerDelegate

{
    @IBOutlet var bagTitleLbl: UILabel!
    var bagCount = Int()
    var newEntryCategoryArray = NSMutableArray()
    
    var imagePicker = UIImagePickerController()
    
    var alertViewImage = UIAlertView()
    
    
    var editUserName = String()
    
    @IBOutlet weak var closeLocationView: UIButton!
    var deleteCategoryAlertView = UIAlertView()
    
    var strCategoryId = String()
    var categoryStr = String()

    
    var backBtnAlertView = UIAlertView()
    
    @IBOutlet var dateTitle: UILabel!
    var bagGreenTitleStr = NSString()
    
    @IBOutlet var greenTitleLbl: UILabel!
    var value = Bool()
    var sizeHeight = UIScreen.mainScreen().bounds.height
    var strBag = NSString()
    @IBOutlet var confirmView: UIView!
    @IBOutlet var newEntryCategoryTableView: UITableView!
    @IBOutlet var categoryView: UIView!
    @IBOutlet var newCategoryView: UIView!
    @IBOutlet var newCategoryTextField: UITextField!
    @IBOutlet var locationFurtherSaveBtn: UIButton!
    @IBOutlet var locationAddNewLbl: UILabel!
    @IBOutlet var locationShelfLbl: UILabel!
    @IBOutlet var locationDrawLbl: UILabel!
    @IBOutlet var locationNearLbl: UILabel!
    @IBOutlet var locationPlaceLbl: UILabel!
    @IBOutlet var locationFurtherTxtField: UITextField!
    @IBOutlet var locationFurtherTitle: UITextField!
    @IBOutlet var locationFurtherView: UIView!
    @IBOutlet var locationView: UIView!
    @IBOutlet var cautionView: UIView!
    
    @IBOutlet var cautionLiquidLbl: UILabel!
    @IBOutlet var cautionHeavyLbl: UILabel!
    @IBOutlet var cautionSharpLbl: UILabel!
    @IBOutlet var cautionFragileLbl: UILabel!
    
    
    @IBOutlet var memberInfoview: UIView!
    @IBOutlet var saveNowMemberInfo: UIButton!
    @IBOutlet var memberInfoTxtField: UITextField!
    @IBOutlet var redLbl: UILabel!
    @IBOutlet var yellowLbl: UILabel!
    @IBOutlet var blueLbl: UILabel!
    @IBOutlet var purpleLbl: UILabel!
    @IBOutlet var grrenLbl: UILabel!
    @IBOutlet var addPhotoBtn: UIButton!
    
    @IBOutlet var imageViewMemberInfo: UIImageView!
    

    @IBOutlet var colorView: UIView!
    @IBOutlet var ezeBarcodeView: UIView!
    @IBOutlet var blackView: UIView!
    @IBOutlet var otherBarcodeTxtField: UITextField!
    @IBOutlet var locationTxtField: UITextField!
    @IBOutlet var cautionTxtField: UITextField!
    @IBOutlet var descripitionTxtField: UITextField!
    @IBOutlet var categoryTxtField: UITextField!
    @IBOutlet var memberTxtField: UITextField!
    @IBOutlet var colorTxtField: UITextField!
    @IBOutlet var ezeBarcodeTxtField: UITextField!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var otherBarcodeView: UIView!
    @IBOutlet var bagTxtField: UITextField!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    
        
        var savedBagCount: Int = NSUserDefaults.standardUserDefaults().valueForKey("bagCount") as! Int
        
        var incremntedValue:Int = savedBagCount + bagCount
        
        println(incremntedValue)
        
        NSUserDefaults.standardUserDefaults().setInteger(incremntedValue, forKey: "bagCount")
        
        
    
        bagTxtField.text = NSString(format: "Bag %@", toString(incremntedValue)) as String
        
        bagTitleLbl.text = bagTxtField.text
        
        let date = NSDate()
        
        var dateFormat = NSDateFormatter()
        
        dateFormat.dateFormat = "MM-dd-yyyy"
        
        var dateInFormat:NSString = dateFormat.stringFromDate(date)
        
        println(dateInFormat)
        
        dateTitle.text = dateInFormat as String
        
        greenTitleLbl.text = bagGreenTitleStr as String
        
        
        
        confirmView.layer.borderColor = UIColor(red: 139.0/255, green: 191.0/255, blue: 236.0/255, alpha: 1.5).CGColor
        
        alertViewImage.delegate  = self
        imagePicker.delegate = self
        let paddingViewBag = UIView(frame: CGRectMake(0, 0, 5, bagTxtField.frame.size.height))
        bagTxtField.leftView = paddingViewBag
        bagTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewEzeBarcode = UIView(frame: CGRectMake(0, 0, 5, ezeBarcodeTxtField.frame.size.height))
        ezeBarcodeTxtField.leftView = paddingViewEzeBarcode
        ezeBarcodeTxtField.leftViewMode = UITextFieldViewMode.Always

        let paddingViewOtherBarcode = UIView(frame: CGRectMake(0, 0, 5, otherBarcodeTxtField.frame.size.height))
        otherBarcodeTxtField.leftView = paddingViewOtherBarcode
        otherBarcodeTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewDescription = UIView(frame: CGRectMake(0, 0, 5, descripitionTxtField.frame.size.height))
        descripitionTxtField.leftView = paddingViewDescription
        descripitionTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewMemberInfo = UIView(frame: CGRectMake(0, 0, 5, memberInfoTxtField.frame.size.height))
        memberInfoTxtField.leftView = paddingViewMemberInfo
        memberInfoTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewNewCategory = UIView(frame: CGRectMake(0, 0, 5, newCategoryTextField.frame.size.height))
        newCategoryTextField.leftView = paddingViewNewCategory
        newCategoryTextField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewLocationFurtherTitle = UIView(frame: CGRectMake(0, 0, 5, locationFurtherTitle.frame.size.height))
        locationFurtherTitle.leftView = paddingViewLocationFurtherTitle
        locationFurtherTitle.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewLocationFurther = UIView(frame: CGRectMake(0, 0, 5, locationFurtherTxtField.frame.size.height))
        locationFurtherTxtField.leftView = paddingViewLocationFurther
        locationFurtherTxtField.leftViewMode = UITextFieldViewMode.Always

        


        

        
        
        
        newCategoryTextField.layer.borderColor = UIColor.grayColor().CGColor
        newCategoryTextField.layer.borderWidth = 1.0
        
        locationFurtherTitle.layer.borderColor = UIColor.grayColor().CGColor
        locationFurtherTxtField.layer.borderColor = UIColor.grayColor().CGColor
        locationFurtherSaveBtn.layer.borderColor = UIColor.whiteColor().CGColor
        
        scrollView.contentSize.height = otherBarcodeView.frame.origin.y + otherBarcodeView.frame.size.height
        bagTxtField.layer.borderColor = UIColor.grayColor().CGColor
        ezeBarcodeTxtField.layer.borderColor = UIColor.grayColor().CGColor
        colorTxtField.layer.borderColor = UIColor.grayColor().CGColor
        
        memberTxtField.layer.borderColor = UIColor.grayColor().CGColor
        categoryTxtField.layer.borderColor = UIColor.grayColor().CGColor
        descripitionTxtField.layer.borderColor = UIColor.grayColor().CGColor
        cautionTxtField.layer.borderColor = UIColor.grayColor().CGColor
        locationTxtField.layer.borderColor = UIColor.grayColor().CGColor
        otherBarcodeTxtField.layer.borderColor = UIColor.grayColor().CGColor
        addPhotoBtn.layer.borderColor = UIColor.whiteColor().CGColor
        saveNowMemberInfo.layer.borderColor = UIColor.whiteColor().CGColor
        memberInfoTxtField.layer.borderColor = UIColor.blueColor().CGColor
        imageViewMemberInfo.layer.cornerRadius = imageViewMemberInfo.frame.size.width/2
        imageViewMemberInfo.layer.masksToBounds = true
        
        
        descripitionTxtField.delegate = self
        memberInfoTxtField.delegate = self
        newCategoryTextField.delegate = self
        locationFurtherTxtField.delegate = self
        
        
        locationNameStrGps = ""
        
        
        otherBarcodeTxtField.text = ""
        
        memberUserNameInfoApi()

    }

    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    @IBOutlet weak var closeWrapperBtn: UIView!
    
    @IBAction func closeLocationView(sender: AnyObject)
    {
        self.blackView.hidden = true
        self.locationView.hidden = true
    }
    
    
    override func viewWillAppear(animated: Bool)
    {
        
        if barButtonCheck == "1"
        {
            
            self.blackView.hidden = false
            self.locationView.hidden = false
            
            
            self.categoryView.hidden = true
            self.confirmView.hidden = true
            self.locationFurtherView.hidden = true
            self.ezeBarcodeView.hidden = true
            
            //self.memberView.hidden = true
            self.cautionView.hidden = true
        }
        else
        {
            self.blackView.hidden = true
            self.locationView.hidden = true
            
            
            self.categoryView.hidden = true
            self.confirmView.hidden = true
            self.locationFurtherView.hidden = true
            self.ezeBarcodeView.hidden = true
            
            //self.memberView.hidden = true
            self.cautionView.hidden = true
        }
        
        
        if value == true
        {
        
           ezeBarcodeTxtField.text = strBagToFurtherScanBarcode as String
        }
        
        if value == false
        {
            if  backBtnCheck == "back"
            {
                self.otherBarcodeTxtField.text = ""
            }
            else
            {
               otherBarcodeTxtField.text = strBagToFurtherScanBarcode as String
 
            }
        }
        
//        locationTxtField.text = locationNameStrGps as String
//        println(locationNameStrGps)
        
        
        if locationNameCheck == "1"
        {
            locationTxtField.text = locationNameInGPS
        }
        else
        {
            locationTxtField.text = ""
        }
        
    }

    
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        
        backBtnAlertView = UIAlertView(title: "Alert", message: "Are you sure to cancel", delegate: self, cancelButtonTitle: "NO", otherButtonTitles: "YES")
        
        backBtnAlertView.show()
    }
    
    
    
    @IBAction func logoutBtn(sender: AnyObject)
    {
        var loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let login = loginStoryboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        
        self.navigationController?.pushViewController(login, animated: false)

    }
  
   
    @IBAction func otherEzeBarcodeBtn(sender: AnyObject)
    {
        value = false
        
        receiveNextBtnCheck = "ezebarcode"
        
        locationFurtherView.hidden = true
        memberInfoview.hidden = true
        colorView.hidden = true
        blackView.hidden = false
        ezeBarcodeView.hidden = false
        cautionView.hidden = true
        locationView.hidden = true
        categoryView.hidden = true
        confirmView.hidden = true
        
        closeLocationView.hidden = true
        closeWrapperBtn.hidden = true

        
    }
    
    
    
    @IBAction func ezeBacodeBtn(sender: AnyObject)
    {
        value = true
        
         receiveNextBtnCheck = "ezebarcode"
        
        locationFurtherView.hidden = true
        memberInfoview.hidden = true
        colorView.hidden = true
        blackView.hidden = false
        ezeBarcodeView.hidden = false
        cautionView.hidden = true
        locationView.hidden = true
        categoryView.hidden = true
        confirmView.hidden = true
        
        closeLocationView.hidden = true
        closeWrapperBtn.hidden = true
    }
    
    
    
    @IBAction func okBtn(sender: AnyObject)
    {
        blackView.hidden = true
        
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let newEntryScanBarcode = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryScanBarcode") as! NewEntryScanBarcodeViewController
        newEntryScanBarcode.greenTitleStr = bagGreenTitleStr
        
        self.navigationController?.pushViewController(newEntryScanBarcode, animated: false)
        
    }
    
    
    @IBAction func colorBtn(sender: AnyObject)
    {
         locationFurtherView.hidden = true
        memberInfoview.hidden = true
        blackView.hidden = false
        ezeBarcodeView.hidden = true
        colorView.hidden = false
        cautionView.hidden = true
        locationView.hidden = true
        categoryView.hidden = true
        confirmView.hidden = true
        
        closeLocationView.hidden = true
        closeWrapperBtn.hidden = true
    }
    
    
    
    @IBAction func memberBtn(sender: AnyObject)
    {
         locationFurtherView.hidden = true
         blackView.hidden = false
        memberInfoview.hidden = false
        ezeBarcodeView.hidden = true
        colorView.hidden = true
        cautionView.hidden = true
        locationView.hidden = true
        categoryView.hidden = true
        confirmView.hidden = true
        
        closeLocationView.hidden = true
        closeWrapperBtn.hidden = true
        
        memberUserNameInfoApi()
        

    }
    
    
    
    
    
    
    
    
    
    
    //MARK:- Member UserName Info Api
    func memberUserNameInfoApi()
    {
        
        var spinningIndicator  = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i",str)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/member-info"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    println("Edit user name = \(dictObj)")
                    
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                        
                    }
                    else
                    {
                         var success = dictObj?.valueForKey("success") as! Int
                    
                        
                        if success == 0
                        {

                            var alert = UIAlertView(title: "", message: "No Data Found", delegate: self, cancelButtonTitle: "OK")

                            
                           //println("messageUserName")
                            
                           
                          alert.show()
                        
                          spinningIndicator.hide(true)
                        }
                        else
                        {
//                            
//                            let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
//                                //			println(self)
//                            }
//                            
//                            
//                            var profileUrl: String = dictObj?.valueForKey("Image") as! String
//                            
//                            
//                            var url: NSURL = NSURL(string: profileUrl)!
//                            
//                            self.imageViewMemberInfo.sd_setImageWithURL(url, completed: block)
//                            
                            
                            
                            
                            
                            
                            var profileUrl: String = dictObj?.valueForKey("Image") as! String
                            
                            var url: NSURL = NSURL(string: profileUrl)!
                            
                            if let data : NSData = NSData(contentsOfURL: url)
                            {
                                self.imageViewMemberInfo.image = UIImage(data: data)!
                                self.imageViewMemberInfo.backgroundColor = UIColor.clearColor()
                                
                                //imageView.image = UIImage(data: data)!
                            }
                            
                            

                            
                            
                            
                            println("success")
                            
                             self.memberInfoTxtField.text = dictObj?.valueForKey("Username") as! String

                            
                            self.memberTxtField.text = dictObj?.valueForKey("Username") as! String
                            
                            spinningIndicator.hide(true)
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }
    
    

    
    
    
    
    

    
    
    //MARK: - CategoryTableViewMethods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return newEntryCategoryArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var newEntryCategoryCell = tableView.dequeueReusableCellWithIdentifier("newEntryCategoryCell", forIndexPath: indexPath) as! NewEntryCategoryTableViewCell
        
        newEntryCategoryCell.categoryLbl.text = newEntryCategoryArray[indexPath.row].valueForKey("name") as? String
        
        newEntryCategoryCell.deleteCategoryBtnLbl.tag = indexPath.row
        
        categoryStr = (newEntryCategoryArray[indexPath.row].valueForKey("id") as? String)!
        
        println("category id = \(categoryStr)")
        
         println("category name = \(newEntryCategoryCell.categoryLbl.text)")
        
        return newEntryCategoryCell
        
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        categoryTxtField.text = newEntryCategoryArray[indexPath.row].valueForKey("name") as! String
        
        
        
        categoryStr = (newEntryCategoryArray[indexPath.row].valueForKey("id") as? String)!
        
        println("category id at particular index = \(categoryStr)")
        
        
        blackView.hidden = true
    }
    
    

    
    
    
    
    
    
    
    
    @IBAction func cautionBtn(sender: AnyObject)
    {
         locationFurtherView.hidden = true
        blackView.hidden = false
        memberInfoview.hidden = true
        ezeBarcodeView.hidden = true
        colorView.hidden = true
        cautionView.hidden = false
        locationView.hidden = true
        categoryView.hidden = true
        confirmView.hidden = true
        
        closeLocationView.hidden = true
        closeWrapperBtn.hidden = true

    }
    
    
    
    @IBAction func locationBtn(sender: AnyObject)
        
    {
        locationFurtherView.hidden = true
        blackView.hidden = false
        memberInfoview.hidden = true
        ezeBarcodeView.hidden = true
        colorView.hidden = true
        cautionView.hidden = true
        locationView.hidden = false
        categoryView.hidden = true
        confirmView.hidden = true
        
        closeLocationView.hidden = false
        closeWrapperBtn.hidden = false
        
        
        locationFurtherTxtField.text = ""
    }
    
   
    
    @IBAction func categoryBtn(sender: AnyObject)
    {
       
        locationFurtherView.hidden = true
        blackView.hidden = false
        memberInfoview.hidden = true
        ezeBarcodeView.hidden = true
        colorView.hidden = true
        cautionView.hidden = true
        locationView.hidden = true
        categoryView.hidden = false
        confirmView.hidden = true
        
        closeLocationView.hidden = true
        closeWrapperBtn.hidden = true
        
        
        locationFurtherTxtField.text = ""
        
         getCategoryApi()

    }
    
    
    
   
    @IBAction func cancelCrossbtn(sender: AnyObject)
    {
        blackView.hidden = true
    }
    
    
    
    
    @IBAction func redBtn(sender: AnyObject)
    {
        blackView.hidden = true
        colorTxtField.text = redLbl.text
    }
    
    
    
    @IBAction func yellowBtn(sender: AnyObject)
    {
        blackView.hidden = true
        colorTxtField.text = yellowLbl.text
    }
    
    
    
    @IBAction func blueBtn(sender: AnyObject)
    {
        blackView.hidden = true
        colorTxtField.text = blueLbl.text
    }
    
    
    
    @IBAction func purpleBtn(sender: AnyObject)
    {
        blackView.hidden = true
        colorTxtField.text = purpleLbl.text
    }
    
    
    
    @IBAction func greenBtn(sender: AnyObject)
    {
        blackView.hidden = true
        colorTxtField.text = grrenLbl.text
    }
    
    
    
    
    @IBAction func memberInfoBackbtn(sender: AnyObject)
    {
        
        blackView.hidden = true
    }
    
    
    
    
    @IBAction func memberInfoSaveNowBtn(sender: AnyObject)
    {
        blackView.hidden = true
        memberTxtField.text = memberInfoTxtField.text
        
        memberUserNameInfoEditApi()
    }
    
    
    
    
    
    //MARK:- Member UserName Info Edit Api
    func memberUserNameInfoEditApi()
    {
        
        var spinningIndicator  = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        
        
        var image:UIImage = imageViewMemberInfo.image!
        var imageData = UIImagePNGRepresentation(image)
        var base64String = imageData.base64EncodedStringWithOptions(.allZeros)
        var strImage64 = base64String.stringByReplacingOccurrencesOfString("+", withString: "%2B") as String
        
        var number = UInt32()
        
        number = arc4random() % 1000000
        

        
         editUserName = self.memberInfoTxtField.text
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&ProfileImage=%@&UserName=%@",str,strImage64,editUserName)
        
        //println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/update-member-info"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    println("Edit user name profile = \(dictObj)!")
                    
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                        
                    }
                    else
                    {
                        var success = dictObj?.valueForKey("success") as! Int
                        var message = dictObj?.valueForKey("message") as! String
                        
                        if success == 0
                        {
                            
                            var alert = UIAlertView(title: "", message: "No Data Found", delegate: self, cancelButtonTitle: "OK")
                            
                            
                            //println("messageUserName")
                            
                            
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            
                            
                            
                            var alert = UIAlertView(title: "", message: message, delegate: self, cancelButtonTitle: "OK")
                            
                            
                            //println("messageUserName")
                            
                            
                            alert.show()
                            
                            spinningIndicator.hide(true)
                            
                            
                            
                            
                            
                            
//                            let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
//                                //			println(self)
//                            }
//                            
//                            
//                            var profileUrl: String = dictObj?.valueForKey("ProfileImage") as! String
//                            
//                            
//                            var url: NSURL = NSURL(string: profileUrl)!
//                            
//                            self.imageViewMemberInfo.sd_setImageWithURL(url, completed: block)
//                            
//                            
//                            self.memberInfoTxtField.text = dictObj?.valueForKey("Username") as! String
                            
                            
                            spinningIndicator.hide(true)
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }
    
    

    
    
    

    
    
    @IBAction func cautionFragileBtn(sender: AnyObject)
    {
        blackView.hidden = true
        cautionTxtField.text = cautionFragileLbl.text
    }
    
    
    
    @IBAction func cautionSharpBtn(sender: AnyObject)
    {
        blackView.hidden = true
        cautionTxtField.text = cautionSharpLbl.text
    }
    
    
    @IBAction func cautionHeavyBtn(sender: AnyObject)
    {
        blackView.hidden = true
        cautionTxtField.text = cautionHeavyLbl.text
        
    }
    
    
    @IBAction func cautionLiquidBtn(sender: AnyObject)
    {
        blackView.hidden = true
        cautionTxtField.text = cautionLiquidLbl.text
        
    }
    
    @IBAction func locationPlaceBtn(sender: AnyObject)
    {
        locationFurtherView.hidden = false
        
        locationFurtherTitle.text = ""
        
        locationFurtherTitle.text = locationPlaceLbl.text
        
    }
    
    
    @IBAction func locationShelfBtn(sender: AnyObject)
    {
        locationFurtherView.hidden = false
        
         locationFurtherTitle.text = ""
        
        locationFurtherTitle.text = locationShelfLbl.text
    }
    
    @IBAction func locationDrawBtn(sender: AnyObject)
    {
        locationFurtherView.hidden = false
        
        locationFurtherTitle.text = ""
        
        locationFurtherTitle.text = locationDrawLbl.text
    }
    
    
    
    
    @IBAction func locatiojnNearBtn(sender: AnyObject)
    {
        locationFurtherView.hidden = false
        
        locationFurtherTitle.text = ""
        
        locationFurtherTitle.text = locationNearLbl.text
    }
    
    
    
    @IBAction func locationGpsBtn(sender: AnyObject)
    {
        blackView.hidden = true
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let newEntryGpsLocation = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryGpsLocation") as! NewEntryAddGpsLocationViewController
        
        self.navigationController?.pushViewController(newEntryGpsLocation, animated: false)
        

        
    }
    
    
    @IBAction func locationAddBtn(sender: AnyObject)
    {
        locationFurtherView.hidden = false
        locationFurtherTitle.text = locationAddNewLbl.text
    }
    
    
    
    @IBAction func locationFurtherSaveBtn(sender: AnyObject)
    {
        blackView.hidden = true
        
        locationTxtField.text = locationFurtherTxtField.text
        
        locationSaveForOtherCaseApi()
    }
    
    
    
    
    
    
    
    //MARK:- CategoryAddNewBtn
    
    @IBAction func categoryAddNewBtn(sender: AnyObject)
    {
        blackView.hidden = true
        newCategoryView.hidden = false
        
    }
    
    
    //MARK:- NewCategorySaveBtn
    
    @IBAction func newCategorySaveBtn(sender: AnyObject)
    {
        createCategory()
        categoryTxtField.text = newCategoryTextField.text
        newCategoryView.hidden = true
    }
    
    
    
    
    
    
    
    
    
    //MARK:- Add List Button
    
    @IBAction func addListBtn(sender: AnyObject)
    {
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let newEntryAddList = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryAddList") as! AddListViewController
        newEntryAddList.colorStr = self.colorTxtField.text
        newEntryAddList.greenTitleStr = bagGreenTitleStr
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        newEntryAddList.packageId = userDefaults.valueForKey("packageId") as! String
        newEntryAddList.packageDataId = userDefaults.valueForKey("packageDataId") as! Int
        
        println(newEntryAddList.packageId)
        println(newEntryAddList.packageDataId)
        self.navigationController?.pushViewController(newEntryAddList, animated: true)

        
    }
    
    
    
    @IBAction func cancelBtn_tableView(sender: AnyObject)
    {
       self.categoryView.hidden = true
        self.blackView.hidden = true
        
    }
    
    
    
    
    
    //MARK:- Edit Button
    
    @IBAction func editBtn(sender: AnyObject)
    {
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let newEntryEditList = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryEditList") as! EditListViewController
        newEntryEditList.greenTitleStr = bagGreenTitleStr
        self.navigationController?.pushViewController(newEntryEditList, animated: true)
        
    }
    
    
    
    
    
    //MARK:- Save Button
    
    @IBAction func confirmSaveBtn(sender: AnyObject)
        
    {
        if memberTxtField.text == "" || locationTxtField.text == "" || categoryTxtField.text == ""
        {
            var textFieldsAlert = UIAlertView(title: "Alert", message: "Please enter fields as manadatory", delegate: self, cancelButtonTitle: "OK")
            
            textFieldsAlert.show()
        }
        else
        {
            locationFurtherView.hidden = true
            blackView.hidden = false
            memberInfoview.hidden = true
            ezeBarcodeView.hidden = true
            colorView.hidden = true
            cautionView.hidden = true
            locationView.hidden = true
            categoryView.hidden = true
            confirmView.hidden = false
            
            closeLocationView.hidden = true
            closeWrapperBtn.hidden = true
            

        }
        
    }
    
    
    
    //MARK:- Cancel Button
    
    @IBAction func cancelBtn(sender: AnyObject)
    {
        backBtnAlertView = UIAlertView(title: "Alert", message: "Are you sure to cancel", delegate: self, cancelButtonTitle: "NO", otherButtonTitles: "YES")
        
        backBtnAlertView.show()
        
    }
    
   
    
    
    
    //MARK:- Like Button
    
    @IBAction func confirmLikeBtn(sender: AnyObject)
    {
        
        
        editBackCheck = "likeBtn"
        
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"

        
        blackView.hidden = true
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        let packageId = userDefaults.valueForKey("packageId") as? String
        let packageDataId = userDefaults.valueForKey("packageDataId") as? Int
        println(packageId)
        println(packageDataId)
        
        var dataModel = NSData()
        
        
        var selectedColorNil = String()
        var selectedColor = String()
        
        selectedColorNil = self.colorTxtField.text
        
        if selectedColorNil == ""
        {
            selectedColor = "Red"
            println("not selected = \(selectedColor)")
        }
        else
        {
            selectedColor = self.colorTxtField.text
            
             println("selected = \(selectedColor)")
        }
        
        
        
        
        
        var post = NSString(format:"UserId=%i&PackageId=%@&PackageDataId=%i&MemberName=%@&Caution=%@&Color=%@&Category=%@&Description=%@&BarCode=%@&Value=%@",str,packageId!,packageDataId!,memberTxtField.text,cautionTxtField.text,selectedColor,categoryTxtField.text,descripitionTxtField.text,ezeBarcodeTxtField.text,"")
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/update-packagedata")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })

                        

                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            var message = dicObj?.valueForKey("message") as! String
                            if success
                            {
                                spinningIndicator.hide(true)
                                var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
                                var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
                                
                                let newEntryAddList = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryAddList") as! AddListViewController
                                
                                newEntryAddList.colorStr = self.colorTxtField.text
                                newEntryAddList.greenTitleStr = self.bagGreenTitleStr
                                var userDefaults = NSUserDefaults.standardUserDefaults()
                                
                                newEntryAddList.packageId = userDefaults.valueForKey("packageId") as! String
                                newEntryAddList.packageDataId = userDefaults.valueForKey("packageDataId") as! Int
                                self.navigationController?.pushViewController(newEntryAddList, animated: true)
                                

                                
                            }
                                
                            else
                            {
                                
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                spinningIndicator.hide(true)
                                
                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()

        
    }
    
    
    
    
    
    
    
    //MARK:- Dislike Button
    
    @IBAction func confirmDislikeBtn(sender: AnyObject)
    {
        blackView.hidden = true
    }
    
    
    
    
    
    
    
    //MARK:-Navigation Barcode Button
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
 
    }
    
    
    
    
    
    
    
    //MARK:- Touch Method
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        descripitionTxtField.resignFirstResponder()
        memberInfoTxtField.resignFirstResponder()
        newCategoryTextField.resignFirstResponder()
        locationFurtherTxtField.resignFirstResponder()
        
    }
    
    
    
    
    
    
    
    //MARK:- TextField Dlegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
    
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        if textField == descripitionTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 200
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y -= 150
            }
            
            
        }
            
         else if textField == memberInfoTxtField || textField == newCategoryTextField || textField == locationFurtherTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 80
                
            }
            
            if textField == memberInfoTxtField
            {
            if sizeHeight == 568
            {
                view.frame.origin.y -= 100
            }
            }


        }
            
        else
        {
            view.frame.origin.y = view.frame.origin.y
        }
        return true
    }
    
    
    
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if textField == descripitionTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 200
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y += 150
            }
            
            
            
        }
        
        
        else if textField == memberInfoTxtField || textField == newCategoryTextField || textField == locationFurtherTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 80
                
            }
            
            
            if textField == memberInfoTxtField
            {
                if sizeHeight == 568
                {
                    view.frame.origin.y += 100
                }
            }

           
            
            
        }
            
        else
        {
            view.frame.origin.y = view.frame.origin.y
        }
        return true

        
        
    }

    //MARK:- Member Add Photo Button
    

    @IBAction func addPhotoBtn(sender: AnyObject)
    {
        alertViewImage = UIAlertView(title: "Alert", message: "Choose the Source", delegate: self, cancelButtonTitle: "Camera", otherButtonTitles: "Photo Gallery")
        
        alertViewImage.show()
        
    }
    
    
    
    //MARK:- AlertView Method
    
    
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alertView == alertViewImage
        {
            if buttonIndex == 0
            {
                if (UIImagePickerController.isSourceTypeAvailable(.Camera))
                {
                    if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil
                    {
                        
                        imagePicker.sourceType = .Camera
                        //imagePicker.mediaTypes = [kUTTypeMovie as String]
                        imagePicker.allowsEditing = false
                        imagePicker.delegate = self
                        
                        presentViewController(imagePicker, animated: true, completion: nil)
                    }
                    else
                    {
                        let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
                        alertView.show()
                        
                    }
                    
                    
                }
                
            }
                
                
            else
            {
                
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)
                {
                    
                    imagePicker.delegate = self
                    
                    imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                    
                    // imagePicker.mediaTypes = [kUTTypeImage]
                    
                    imagePicker.allowsEditing = true
                    
                    self.presentViewController(imagePicker, animated: true, completion: nil)
                }
                    
                    
                else
                {
                    var alert = UIAlertView(title: "Image not Updated", message: "Please check your Internet Settings", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    alert.show()
                    
                    NSLog("failed")
                }
                
                
                
            }
            
            
        }
        
        if alertView == backBtnAlertView
        {
            if buttonIndex == 1
            {
                
                deletePackageData()
            }
            
        }
        
        
        
        
        if alertView == deleteCategoryAlertView
        {
            if buttonIndex == 1
            {
                deleteCategoryFromTableView()
            }
        }

        
        
    }
    
    
    
    
    
    
    
    
    
    //MARK: - Delete Category From TableView API()
    
    func deleteCategoryFromTableView()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        blackView.hidden = true
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println("userId in userDefaults = \(str)")
        
        
        
        //strCategoryId = newEntryCategoryArray[sender.tag].valueForKey("id") as? String
        
        println("delete category at button index = \(strCategoryId)")
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&Categoryid=%@",str,strCategoryId)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/delete-category")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })
                        
                        
                        
                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            var message = dicObj?.valueForKey("message") as! String
                            if success
                            {
                                println("success message = \(message)")
                                
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                
                                alert.show()
                                
                                self.categoryTxtField.text = ""
                                
                                self.newEntryCategoryTableView.reloadData()
                                
                                spinningIndicator.hide(true)
                            }
                                
                            else
                            {
                                
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                
                                alert.show()
                                
                                spinningIndicator.hide(true)
                                
                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        
    }
    
    
    
    

    
    
    
    
    
    
    
    
    
//    
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject])
//    {
//        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
//        {
//            
//            imageViewMemberInfo.contentMode = .ScaleAspectFit
//            imageViewMemberInfo.image = pickedImage
//            
//            self.dismissViewControllerAnimated(false, completion: nil)
//            
//            
//        }
//        
//        
//        
//    }

    
    
    
    
    // MARK: - UIImagePicker Delegate Method
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
    {
        
        imageViewMemberInfo.image = image
        imageViewMemberInfo.contentMode = .ScaleAspectFit
        //imageData64Base = convertImageToBase64(image)
        
         //picker .dismissViewControllerAnimated(true, completion: nil)
        

        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
            
            self.blackView.hidden = false
            self.memberInfoview.hidden = false
        })

    
        
    }
    
    

    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        picker .dismissViewControllerAnimated(true, completion: nil)
    }
    
    

    
    
    

    //MARK:- Get Category Api
    
    
    func  getCategoryApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"

        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
       
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i",str)
        
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/getcategory")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj!)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })

                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            if success
                            {
                                self.newEntryCategoryArray = dicObj?.valueForKey("message") as! NSMutableArray
                                
                
                                println(self.newEntryCategoryArray[0].valueForKey("name"))
                                
                
                                
                                self.newEntryCategoryTableView.reloadData()
                                
                                println(self.newEntryCategoryArray)
                                
                                spinningIndicator.hide(true)
                                
                                
                            }
                                
                            else
                            {
                                var message = dicObj?.valueForKey("message") as! String
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                spinningIndicator.hide(true)
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()

        
        
        
        
    }
    
    //MARK:- Create Category
    
    
    func createCategory()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&Category=%@",str,newCategoryTextField.text)
        
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/create-category")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                spinningIndicator.hide(true)
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                   
                    dispatch_async(dispatch_get_main_queue(), {
                        spinningIndicator.hide(true)
                    })
                   
                   

                    
                    
                    
                
                }
                
            }
        })
        task.resume()
        

    }
   //MARK:- Delete PackageData
    
    func deletePackageData()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        blackView.hidden = true
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        let packageId = userDefaults.valueForKey("packageId") as? String
        let packageDataId = userDefaults.valueForKey("packageDataId") as? Int
        println(packageId)
        println(packageDataId)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PackageId=%@&PackageDataId=%i",str,packageId!,packageDataId!)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/delete-packagedata")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })
                        
                        
                        
                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            var message = dicObj?.valueForKey("message") as! String
                            if success
                            {
                                spinningIndicator.hide(true)
                                self.navigationController?.popViewControllerAnimated(true)
                                strBagToFurtherScanBarcode = ""
                                 NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "bagCount")
                                
                            }
                                
                            else
                            {
                                
                                var alert = UIAlertView(title: "Alert", message: "Something Wrong", delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                spinningIndicator.hide(true)
                                
                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        

    }
    
    
    //MARK:- LocationSaveAPiForOThercase
    
    func locationSaveForOtherCaseApi()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        blackView.hidden = true
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        let packageId = userDefaults.valueForKey("packageId") as? String
        let packageDataId = userDefaults.valueForKey("packageDataId") as? Int
        println(packageId)
        println(packageDataId)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PackageId=%@&PackageDataId=%i&Type=%@&Value=%@",str,packageId!,packageDataId!,locationFurtherTitle.text,locationFurtherTxtField.text)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/packagedata-location")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })
                        
                        
                        
                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            
                            if success
                            {
                                spinningIndicator.hide(true)
                                
                                
                                
                                
                            }
                                
                            else
                            {
                                
                                var alert = UIAlertView(title: "Alert", message: "Something Wrong", delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                spinningIndicator.hide(true)
                                
                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        

        
    }
    
    
    
    //MARK:-Category Delete Button

    @IBAction func categoryDeleteBtn(sender: AnyObject)
    {
        println("delete btn clicked")
        
        deleteCategoryAlertView = UIAlertView(title: "Alert", message: "Are you sure to delete the category", delegate: self, cancelButtonTitle: "NO", otherButtonTitles: "YES")
        
        deleteCategoryAlertView.show()
        
        strCategoryId = (newEntryCategoryArray[sender.tag].valueForKey("id") as? String)!
        
        println("delete category on sender.tag = \(strCategoryId)")

    }
    
    
}
