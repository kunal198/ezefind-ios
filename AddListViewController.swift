//
//  AddListViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/15/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import AudioToolbox


var backBtnCheck = String()

class AddListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVAudioPlayerDelegate,AVAudioRecorderDelegate
{
    
    var soundURL: NSURL?
    var soundID:SystemSoundID = 0
    
    var audioUrl = [NSURL]()
    var conditionBoolImage = Bool()
    var backBtnBool = Bool()
    var packageId = String()
    var packageDataId = Int()
    
    var arrayImageApi = [UIImage]()
    
    var cancelBtnAlertView = UIAlertView()
    var saveBtnAlertView = UIAlertView()
    @IBOutlet var dateTitle: UILabel!
    @IBOutlet var greenTitleLbl: UILabel!
    var greenTitleStr = NSString()
    var arrayImages = [UIImage]()
    
    var recordingArray = NSMutableArray()
    
    
    
    @IBOutlet weak var blackView: UIView!
    
    
    @IBOutlet weak var ezeBarCodeView: UIView!
    
    
    var colorStr = String()
    var isPlayAudio = Bool()
    
    var recordingName = String()
    var min = Int()
    var sec = Int()
    var minSecStr = String()
    
    
    @IBOutlet var audioTimerTitleView: UIView!
    @IBOutlet var audioTitleTxtFiel: UITextField!
    @IBOutlet var slider: UISlider!
    @IBOutlet var playAudioTimerLbl: UILabel!
    @IBOutlet var recordTimerLbl: UILabel!
    @IBOutlet var audioCurrentDateLbl: UILabel!
    
    var isStopPlayer = Bool()
    @IBOutlet var deleteAudioFileBtn: UIButton!
    @IBOutlet var saveFileAudioBtn: UIButton!
    @IBOutlet var audioCrossBtn: UIButton!
    @IBOutlet var pauseAudioBtn: UIButton!
    @IBOutlet var playAudioBtn: UIButton!
    
    
    
    var audioRecord:AVAudioRecorder!
    var audioPlayer:AVAudioPlayer!
    
    
    
    var titleStr:String!
    var filePathURL:NSURL!
    var  filePath : NSURL!
    
    @IBOutlet var playBtn: UIButton!
    @IBOutlet var playActiveBtn: UIButton!
    @IBOutlet var audioView: UIView!
    @IBOutlet var addListTableView: UITableView!
    var iQuantity = Int()
    var iAttachement = Int()
    
    var packageDataItems = NSArray()
    
    @IBOutlet var attachementLbl: UILabel!
    @IBOutlet var photoVideoView: UIView!
    @IBOutlet var photoVideoAudioBarcodeView: UIView!
    @IBOutlet var quantityShowLbl: UILabel!
    @IBOutlet var quantityLbl: UILabel!
    
    @IBOutlet var quantityView: UIView!
    @IBOutlet var imageView: UIImageView!
    var sizeHeight = UIScreen.mainScreen().bounds.height
    
    var imagePickerTakePhoto = UIImagePickerController()
    var imagePickerAttachment = UIImagePickerController()
    
    var alertViewImage = UIAlertView()
    
    @IBOutlet var valueTxtField: UITextField!
    @IBOutlet var descriptionTxtField: UITextField!
    @IBOutlet var titleTxtField: UITextField!
    @IBOutlet var addMoreBtn: UIButton!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        println(backBtnBool)
        
        let dateTitledate = NSDate()
        
        var dateFormatTitle = NSDateFormatter()
        
        dateFormatTitle.dateFormat = "dd-MM-yyyy"
        
        var dateInFormattitle:NSString = dateFormatTitle.stringFromDate(dateTitledate)
        
        println(dateInFormattitle)
        
        dateTitle.text = dateInFormattitle as String
        
        let date = NSDate()
        var dateFormat = NSDateFormatter()
        
        dateFormat.dateFormat = "dd/MM/yyyy"
        
        playAudioTimerLbl.text = "00:00"
        
        recordTimerLbl.text = "00:00"
        
        var dateInFormat:NSString = dateFormat.stringFromDate(date)
        audioCurrentDateLbl.text = dateInFormat as String
        println(dateInFormat)
        
        greenTitleLbl.text  = greenTitleStr as String
        
        getPackageDataItems()
        
        iQuantity = 1
        iAttachement = 0
        
        quantityView.layer.borderColor = UIColor(red: 36.0/255, green: 193.0/255, blue: 13.0/255, alpha: 1.5).CGColor
        
        photoVideoAudioBarcodeView.layer.borderColor = UIColor(red: 36.0/255, green: 193.0/255, blue: 13.0/255, alpha: 1.5).CGColor
        
        audioView.layer.borderColor = UIColor(red: 56.0/255, green: 102.0/255, blue: 201.0/255, alpha: 1.5).CGColor
        
        
        audioTimerTitleView.layer.borderColor = UIColor(red: 56.0/255, green: 102.0/255, blue: 201.0/255, alpha: 1.5).CGColor
        
        alertViewImage.delegate  = self
        imagePickerTakePhoto.delegate = self
        imagePickerAttachment.delegate = self
        let paddingViewTitle = UIView(frame: CGRectMake(0, 0, 5, titleTxtField.frame.size.height))
        titleTxtField.leftView = paddingViewTitle
        titleTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewEzeDescription = UIView(frame: CGRectMake(0, 0, 5, descriptionTxtField.frame.size.height))
        descriptionTxtField.leftView = paddingViewEzeDescription
        descriptionTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewValue = UIView(frame: CGRectMake(0, 0, 15, valueTxtField.frame.size.height))
        valueTxtField.leftView = paddingViewValue
        valueTxtField.leftViewMode = UITextFieldViewMode.Always
        
        
        addMoreBtn.layer.borderColor = UIColor.whiteColor().CGColor
        
        
        audioTitleTxtFiel.delegate = self
        descriptionTxtField.delegate = self
        titleTxtField.delegate = self
        valueTxtField.delegate = self
    }
    
    
    
    override func viewWillAppear(animated: Bool)
    {
        
        if  editBackCheck == "editBackBtn"
        {
            getPackageDataItems()
        }
        else
        {
            //getPackageDataItems()
        }
        
    }
    
    
    
    @IBAction func okBtn_barCodeVIEW(sender: AnyObject)
    {
        blackView.hidden = true
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let newEntryScanBarcode = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryScanBarcode") as! NewEntryScanBarcodeViewController
        //newEntryScanBarcode.greenTitleStr = binBoxGreenTitleStr
        self.navigationController?.pushViewController(newEntryScanBarcode, animated: false)

    }
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return packageDataItems.count
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var addCell = tableView.dequeueReusableCellWithIdentifier("addCell", forIndexPath: indexPath) as! AddListTableViewCell
        
//        
//        if var urlString  =  packageDataItems[indexPath.row].valueForKey("Image") as? String, imageurl = NSURL(string: NSString(format: "http://beta.brstdev.com/yiiezefind%@", urlString) as String)
//        {
//            if let data = NSData(contentsOfURL: imageurl)
//            {
//                addCell.cellImageView.contentMode = UIViewContentMode.ScaleAspectFit
//                addCell.cellImageView.image = UIImage(data: data)
//            }
//        }
        
        
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
        var profileUrl: String = packageDataItems[indexPath.row].valueForKey("Image") as! String
        
        var url: NSURL = NSURL(string: profileUrl)!
        
        addCell.cellImageView.sd_setImageWithURL(url, completed: block)

        
        
        
        
        
        
        if var strTitle = packageDataItems[indexPath.row].valueForKey("Title") as? String
        {
            addCell.titleTxtField.text = strTitle
        }
        
        if var strDesc = packageDataItems[indexPath.row].valueForKey("Description") as? String
        {
            addCell.descriptionTxtField.text = strDesc
        }
        
        if var strAttachment = packageDataItems[indexPath.row].valueForKey("AttachmentCount") as? Int
        {
            addCell.attachmentLbl.text = toString(strAttachment)
        }
        
        if var strQuantity = packageDataItems[indexPath.row].valueForKey("Quantity") as? Int
        {
            addCell.quantityLbl.text = toString(strQuantity)
        }
        
        
        return addCell
    }
    
    
    
    
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        quantityView.hidden = true
    }
    
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        
        backBtnCheck = "back"
        
        if backBtnBool == true
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
        else
        {
            for controller in self.navigationController!.viewControllers as Array
            {
                if controller.isKindOfClass(NewEntryGraphToFurtherBagViewController)
                {
                    self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                    break
                }
                else if controller.isKindOfClass(NewEntryGraphBinViewController)
                {
                    self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                    break
                }
                else if controller.isKindOfClass(NewEntryGraphSingleItemViewController)
                {
                    self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                    break
                }
                else if controller.isKindOfClass(NewEntryGraphBinViewController)
                {
                    self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                    break
                }
                else
                {
                    
                }
                
            }
        }
    }
    
    
    
    
    @IBAction func logoutBtn(sender: AnyObject)
    {
        var loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let login = loginStoryboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        
        self.navigationController?.pushViewController(login, animated: false)
        
        
        
    }
    
    
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(GraphViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                
                break
            }
        }
        
    }
    
    
    
    
    @IBAction func editBtn(sender: AnyObject)
    {
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let newEntryEditList = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryEditList") as! EditListViewController
        newEntryEditList.greenTitleStr = greenTitleStr
        newEntryEditList.packageDataId = packageDataId
        newEntryEditList.packageId = packageId
        self.navigationController?.pushViewController(newEntryEditList, animated: true)
        
    }
    
    
    
    
    
    @IBAction func saveBtn(sender: AnyObject)
    {
        saveBtnAlertView = UIAlertView(title: "Alert", message: "Are you sure  for save", delegate: self, cancelButtonTitle: "NO", otherButtonTitles: "YES")
        
        saveBtnAlertView.show()
    }
    
    
    
    
    
    @IBAction func cancelBtn(sender: AnyObject)
    {
        
        backBtnCheck = "back"
        
        cancelBtnAlertView = UIAlertView(title: "Alert", message: "Are you sure want to cancel", delegate: self, cancelButtonTitle: "NO", otherButtonTitles: "YES")
        
        cancelBtnAlertView.show()
    }
    
    
    
    
    //MARK:- Touch Method
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        quantityView.hidden = true
        valueTxtField.resignFirstResponder()
        descriptionTxtField.resignFirstResponder()
        titleTxtField.resignFirstResponder()
        audioTitleTxtFiel.resignFirstResponder()
    }
    
    
    
    
    
    //MARK:- TextField Dlegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        if textField == titleTxtField || textField == descriptionTxtField || textField == valueTxtField ||  textField == audioTitleTxtFiel
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 150
                
            }
            
            if sizeHeight == 568 || sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y -= 120
            }
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad
            {
                view.frame.origin.y -= 50
            }
        }
            
        else
        {
            view.frame.origin.y = view.frame.origin.y
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if textField == titleTxtField || textField == descriptionTxtField || textField == valueTxtField ||  textField == audioTitleTxtFiel
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 150
                
            }
            
            if sizeHeight == 568 ||  sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y += 120
            }
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad
            {
                view.frame.origin.y += 50
            }
            
        }
        return true
        
    }
    
    
    //MARK:- Member Add Photo Button
    
    
    @IBAction func addPhotoBtn(sender: AnyObject)
    {
        alertViewImage = UIAlertView(title: "Alert", message: "Choose the Source", delegate: self, cancelButtonTitle: "Camera", otherButtonTitles: "Photo Gallery")
        alertViewImage.show()
        
        
    }
    
    
    
    //MARK:- AlertView Method
    
    
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alertView == alertViewImage
        {
            if buttonIndex == 0
            {
                if (UIImagePickerController.availableCaptureModesForCameraDevice(UIImagePickerControllerCameraDevice.Rear) != nil)
                {
                    imagePickerTakePhoto.allowsEditing = true
                    imagePickerTakePhoto.sourceType = UIImagePickerControllerSourceType.Camera
                    imagePickerTakePhoto.cameraCaptureMode = .Photo
                    
                    
                    presentViewController(imagePickerTakePhoto, animated: true, completion: nil)
                }
                else
                {
                    let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
                    alertView.show()
                    
                }
                
            }
            else
            {
                
                imagePickerTakePhoto.allowsEditing = true
                imagePickerTakePhoto.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                
                
                presentViewController(imagePickerTakePhoto, animated: true, completion: nil)
                
            }
            
            
            
        }
        
        if alertView == saveBtnAlertView
        {
            if buttonIndex == 1
            {
                if conditionBoolImage != true
               {
                  let alert = UIAlertView(title: "Alert", message: "Select the image", delegate: self, cancelButtonTitle: "OK")
                
                    alert.show()
                
                
               }
               else if titleTxtField.text == "" || descriptionTxtField.text == ""
               {
                   let alert = UIAlertView(title: "Alert", message: "Fill the required fields", delegate: self, cancelButtonTitle: "OK")
                   alert.show()
                   
               }
               else
               {
               
                   addDataItemsApi()
                    
                   
                    
                    let alert = UIAlertView(title: "", message: "Package saved successfully. ", delegate: self, cancelButtonTitle: "OK")
                    alert.show()

                
                    
                    titleTxtField.text = ""
                    descriptionTxtField.text = ""
                    valueTxtField.text = ""
                   // imageView.image = UIImage(named: "img_take_photo.png")
                    
                    
                    iAttachement = 0
                    attachementLbl.text = "0"
                    arrayImageApi = []
                    
                    
                    
                    
//                    for controller in self.navigationController!.viewControllers as Array
//                    {
//                        if controller.isKindOfClass(NewEntryPackAndGoViewController)
//                        {
//                            self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
//                            break
//                        }
//                    }
                    
                }
                
                
            }
            
            
            
        }
        
        if alertView == cancelBtnAlertView
        {
            if buttonIndex == 1
            {
                
                backBtnCheck = "back"
                
                println("back btn clicked")
                
                
                for controller in self.navigationController!.viewControllers as Array
                {
                    
                    
                    if controller.isKindOfClass(NewEntryGraphViewController)
                    {
                        self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                        break
                    }
                    else if controller.isKindOfClass(NewEntryPackAndGoViewController)
                    {
                        self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                        break
                    }
                    else
                    {
                        
                    }
                    
                }
                
            }
            
            
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject])
    {
        if picker == imagePickerTakePhoto
        {
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                
                imageView.contentMode = .ScaleAspectFit
                imageView.image = pickedImage
                conditionBoolImage = true
                self.dismissViewControllerAnimated(false, completion: nil)
                
                
            }
        }
        else
            
        {
            
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                
                arrayImageApi.append(pickedImage)
                
                println(arrayImageApi)
                
                self.dismissViewControllerAnimated(false, completion: nil)
                iAttachement++
                attachementLbl.text = NSString(format: "%i",iAttachement) as String
                
            }
        }
        
    }
    
    
    
    //MARK:- Attachement And Quantity Button
    
    @IBAction func attachmentBtn(sender: AnyObject)
    {
        photoVideoAudioBarcodeView.hidden = false
        
        if attachementLbl.text == "5"
        {
            var alert = UIAlertView(title: "Alert", message: "You have already insert 5 attachments", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
            photoVideoAudioBarcodeView.hidden = true
        }
    }
    
    
    
    
    
    @IBAction func quantityBtn(sender: AnyObject)
    {
        quantityView.hidden = false
    }
    
    
    
    
    //MARK:- Quantity Increment and Decrement Button
    
    @IBAction func incrementButton(sender: AnyObject)
    {
        iQuantity++
        
        quantityLbl.text = NSString(format: "%i",iQuantity) as String
        quantityShowLbl.text = quantityLbl.text
        
    }
    
    
    
    @IBAction func decrementBtn(sender: AnyObject)
    {
        iQuantity--
        
        if quantityLbl.text == "1"
        {
            iQuantity = 1
        }
        
        
        quantityLbl.text = NSString(format: "%i", iQuantity) as String
        quantityShowLbl.text = quantityLbl.text
    }
    
    
    
    //MARK:- Attachment Process
    
    @IBAction func crossBtn(sender: AnyObject)
    {
        photoVideoAudioBarcodeView.hidden = true
    }
    
    
    @IBAction func photoVideoBtn(sender: AnyObject)
    {
        photoVideoAudioBarcodeView.hidden = true
        photoVideoView.hidden = false
    }
    
    
    @IBAction func audioBtn(sender: AnyObject)
    {
        photoVideoAudioBarcodeView.hidden = true
        audioView.hidden = false
        saveFileAudioBtn.enabled = false
    }
    
    
    
    
    @IBAction func barcodeBtn(sender: AnyObject)
    {
        self.blackView.hidden = false
        self.ezeBarCodeView.hidden = false
        
        iAttachement++
        
        attachementLbl.text = NSString(format: "%i",iAttachement) as String
        
        //arrayImageApi.append(strBagToFurtherScanBarcode) as String

    }
    
    
    @IBAction func takePhotoVideo(sender: AnyObject)
    {
        photoVideoView.hidden = true
        
        if (UIImagePickerController.availableCaptureModesForCameraDevice(UIImagePickerControllerCameraDevice.Rear) != nil)
        {
            imagePickerAttachment.allowsEditing = true
            imagePickerAttachment.sourceType = UIImagePickerControllerSourceType.Camera
            imagePickerAttachment.cameraCaptureMode = .Photo
            
            
            presentViewController(imagePickerAttachment, animated: true, completion: nil)
        }
        else
        {
            let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
            alertView.show()
            
        }
        
        
        
    }
    
    
    
    @IBAction func chooseExisting(sender: AnyObject)
    {
        photoVideoView.hidden = true
        imagePickerAttachment.allowsEditing = true
        imagePickerAttachment.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        
        
        presentViewController(imagePickerAttachment, animated: true, completion: nil)
        
        
    }
    
    
    
    
    @IBAction func photoVideoCrossBtn(sender: AnyObject)
    {
        photoVideoView.hidden = true
    }
    
    
    
    
    //MARK:- Get Package Data Items
    
    func getPackageDataItems()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        
        println(packageId)
        println(packageDataId)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PackageId=%@&PackageDataId=%i",str,packageId,packageDataId)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/getpackagesdataitems")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })
                        
                    }
                    else
                    {
                        
                        
                       // spinningIndicator.hide(true)
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            
                            if success
                            {
                                self.packageDataItems = dicObj?.valueForKey("message") as! NSArray
                                self.addListTableView.reloadData()
                                
                                self.titleTxtField.text = ""
                                self.descriptionTxtField.text = ""
                                self.valueTxtField.text = ""
                                self.imageView.image = UIImage(named: "img_take_photo.png")

                                spinningIndicator.hide(true)
                                
                                
                            }
                                
                            else
                            {
                                var message = dicObj?.valueForKey("message") as! String
//                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
//                                alert.show()
                                spinningIndicator.hide(true)
                                
                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        
        
        
    }
    
    
    //MARK:- Audio Process
    
    
    @IBAction func audioCrossBtn(sender: AnyObject)
    {
        
        audioView.hidden = true
        playAudioBtn.hidden = false
        pauseAudioBtn.hidden = true
        
        if isStopPlayer == true
        {
            audioPlayer.stop()
        }
        
    }
    
    
    @IBAction func saveFileBtn(sender: AnyObject)
    {
        var alert = UIAlertView(title: "Alert", message: "Successfully Saved  the File", delegate: self, cancelButtonTitle: "OK")
        alert.show()
        audioView.hidden = true
        playAudioBtn.hidden = false
        pauseAudioBtn.hidden = true
        
        if isStopPlayer == true
        {
            audioPlayer.stop()
        }
        
        
    }
    
    @IBAction func playActiveBtn(sender: AnyObject)
    {
        
        playActiveBtn.hidden = true
        playBtn.hidden = false
        playAudioBtn.enabled = true
        saveFileAudioBtn.enabled = true
        deleteAudioFileBtn.enabled = true
        audioCrossBtn.enabled = false
        
        audioRecord.stop()
    }
    
    
    @IBAction func playBtn(sender: AnyObject)
    {
        
        playActiveBtn.hidden = false
        playBtn.hidden = true
        playAudioBtn.enabled = false
        saveFileAudioBtn.enabled = false
        deleteAudioFileBtn.enabled = false
        audioCrossBtn.enabled = false
        

        
        let dirPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as! String
        
        
        
        
        
        let currentDateTime = NSDate()
        let formatter = NSDateFormatter()
        
        formatter.dateFormat = "ddMMyyyy-HHmmss"
        
        recordingName = formatter.stringFromDate(currentDateTime)+".wav"
        println(recordingName)
        
        recordingArray.addObject(recordingName)
        
        print(recordingArray)
        
//        let files = NSFileManager().enumeratorAtPath(dirPath)
//        var myFiles:[String] = []
//        while let file: AnyObject = files?.nextObject()
//        {
//            myFiles.append(file as! String)
//            
//            println("my files = \(myFiles)")
//            //self.strFiles.text = "\(self.strFiles.text)\n\(file as String)"
//        }
        
        
        
        let pathArray = [dirPath, recordingName]
        filePath = NSURL.fileURLWithPathComponents(pathArray)!
        print(filePath)

        
        
        
        
        var recordSettings = [
            AVFormatIDKey: Int(kAudioFormatAppleIMA4),  //kAudioFormatMPEG4AAC
            AVSampleRateKey: 16000.0,
            AVNumberOfChannelsKey: 1 as NSNumber,
            AVEncoderAudioQualityKey: AVAudioQuality.High.rawValue]

        
        let session = AVAudioSession.sharedInstance()
        
        session.setCategory(AVAudioSessionCategoryPlayAndRecord, error: nil)
        
        audioRecord = AVAudioRecorder(URL: filePath, settings:nil, error: nil)
    
        
       
        
        audioRecord.delegate = self
        audioRecord.meteringEnabled = true
        
        audioRecord.prepareToRecord()
        
        audioRecord.record()
        
        

        
        isPlayAudio = true
        
        var meterTimer = NSTimer.scheduledTimerWithTimeInterval(0.1,
            target:self,
            selector:"updateAudioMeter:",
            userInfo:nil,
            repeats:true)
        
        iAttachement++
        
        attachementLbl.text = NSString(format: "%i",iAttachement) as String
        
        
    }
    
    
    func updateAudioMeter(timer:NSTimer)
    {
        
        if audioRecord.recording
        {
            min = Int(audioRecord.currentTime / 60)
            sec = Int(audioRecord.currentTime % 60)
            let minsec = String(format: "%02d:%02d", min, sec)
            recordTimerLbl.text = minsec
            audioRecord.updateMeters()
            // if you want to draw some graphics...
//            var apc0 = recorder.averagePowerForChannel(0)
//            var peak0 = recorder.peakPowerForChannel(0)
        }
    }
    
    
    
    @IBAction func deleteAudioFileBtn(sender: AnyObject)
    {
        
        if isStopPlayer == true
        {
            audioPlayer.stop()
        }
        
        
        var fileManager: NSFileManager = NSFileManager.defaultManager()
        
        var documentsPath: String = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        
        
        var isAllDelete = false
        
        let stringName : String!
        
        if recordingArray.count > 0
        {
             stringName  = recordingArray.lastObject as! String
        }
        else
        {
            
            isAllDelete = true
            stringName = ""
        }
        
        
        
        
        var filePathxsa: String = documentsPath.stringByAppendingPathComponent(stringName) as String
        
        var error: NSError?
        
        print(filePathxsa)
        
        var success:Bool  = fileManager.removeItemAtPath(filePathxsa, error: &error)
        //println(success)
        
        if isAllDelete == true
        {
            success = false
        }
        
        if success
        {
            var removeSuccessFulAlert: UIAlertView = UIAlertView(title: "Alert", message: "Successfully removed", delegate: self, cancelButtonTitle: "OK")
            removeSuccessFulAlert.show()
            
            iAttachement--
            
            println("audio deleted = \(iAttachement)")
            
            recordingArray.removeLastObject()
            
            attachementLbl.text = NSString(format: "%i",iAttachement) as String
            
            isPlayAudio = false
            
            saveFileAudioBtn.enabled = false
            
//            
//            playActiveBtn.hidden = false
//            playBtn.hidden = true
//            playAudioBtn.enabled = false
//            saveFileAudioBtn.enabled = false
//            deleteAudioFileBtn.enabled = false
//           // audioCrossBtn.enabled = false
            
            playBtn.enabled = true
            
            audioCrossBtn.enabled = true
        }
        else
        {
            
            println("audio deleted = \(iAttachement)")
            
            //NSLog("Could not delete file -:%@ ", error!.localizedDescription)
            
            var alert = UIAlertView(title: "Alert", message: "File not exist", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
        
//        playAudioBtn.hidden = false
//        pauseAudioBtn.hidden = true
    }
    
    
    
    
    @IBAction func playAudioBtn(sender: AnyObject)
    {
        
        if isPlayAudio
        {
        
            //Get a URL for the sound file
            var filePathURL = NSURL.fileURLWithPath(filePath.path!, isDirectory: false)
            
            //soundURL = NSURL(fileURLWithPath: filePathURL!)
            
            //Use audio sevices to create the sound
            AudioServicesCreateSystemSoundID(filePathURL, &soundID)
            
            //Use audio services to play the sound
            AudioServicesPlaySystemSound(soundID)
            
            
            
            
            var url = NSURL.fileURLWithPath(filePath.path!)
            
            audioPlayer = AVAudioPlayer(contentsOfURL: url, error: nil)
            
           audioPlayer.prepareToPlay()
            
           audioPlayer.delegate = self
            
           audioPlayer.volume = 50.0
            
            minSecStr = String(format: "%02d min,%02d sec", min, sec)
            println(minSecStr)
            
            playAudioTimerLbl.text = minSecStr
            
            slider.maximumValue = Float(audioPlayer.duration)
            
            
            NSTimer.scheduledTimerWithTimeInterval(0.1,
                target:self,
                selector:"updateAudioSlider:",
                userInfo:nil,
                repeats:true)
                
            audioPlayer.numberOfLoops = 0
            
            audioPlayer.play()
            
            
            playBtn.enabled = false
            playAudioBtn.hidden = true
            pauseAudioBtn.hidden = false
            isStopPlayer = true
            
            
        }
        else
        {
            var alert = UIAlertView(title: "Alert", message: "File not exist", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
//            playBtn.enabled = true
//            playAudioBtn.hidden = false
//            pauseAudioBtn.hidden = true
//            isStopPlayer = false
            
            
        }
        
        
        
    }
    
    func updateAudioSlider(timer:NSTimer)
    {
        
        slider.value = Float(audioPlayer.currentTime)
        println(slider.value)
        
        
    }
    
    
    
    @IBAction func pauseAudioBtn(sender: AnyObject)
    {
        
        playBtn.enabled = true
        playAudioBtn.hidden = false
        pauseAudioBtn.hidden = true
        
        if audioPlayer.play()
        {
            audioPlayer.stop()
        }
        
    }
    
    
    
    
     //MARK:- AVAudio Recorder Delegate
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder!, successfully flag: Bool)
    {
        if(flag)
        {
            //Store in Model
            
            filePathURL = recorder.url
            println(filePathURL)
            audioUrl.append(filePathURL)
            titleStr = recorder.url.lastPathComponent
            //Segway once we've finished processing the audio
        }
        else
        {
            println("recording not successful")
            
        }
    }
    
    
    
    
    
     //MARK:- AVAudio Player Delegate
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer!, successfully flag: Bool)
    {
        playAudioBtn.hidden = false
        pauseAudioBtn.hidden = true
        
        playAudioTimerLbl.text = "00:00"

        //recordTimerLbl.text = "00:00"
        
        //playButton.setTitle(“Play”, forState: .Normal)
    }
    
    
    
    
    
    
    
    
    //MARK:- ADD Data Items Api
    
    
    func addDataItemsApi()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        var takePhotoImage = UIImage()
        
        takePhotoImage = imageView.image!
        
        println(takePhotoImage)
        
        
        
        createMultipart(arrayImageApi, audioFile: audioUrl,barcode: strBagToFurtherScanBarcode as String, Image: takePhotoImage, callback: { success in
            
            if success
            {
                println("success")
            }
            else
            {
                println("fail")
            }
        })
        
         spinningIndicator.hide(true)
        
    }
    
    func createMultipart(image:[UIImage], audioFile:[NSURL] ,barcode: String,Image : UIImage ,callback: Bool -> Void)
    {
    
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let userID = userDefaults.valueForKey("userId") as! Int
        
        println(userID)
        
        let strUserID = toString(userID)
        
        
        
        let strpackageDataId  = toString(packageDataId)
        
        println(packageId)
        println(packageDataId)
        
        
        
        let param = [
            
            "UserId" : strUserID,
            "PackageId" : packageId,
            "PackageDataId" : strpackageDataId,
            "Title" : titleTxtField.text,
            "Description" : descriptionTxtField.text,
            "Value" : valueTxtField.text,
            "Quantity" : quantityShowLbl.text,
            "Color" : colorStr,
            "barcode" : barcode
            
        ]
        
        print(param)
        
        upload(
            .POST,
            URLString: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/adddataitems",
            multipartFormData: { multipartFormData in
                
                
                multipartFormData.appendBodyPart(data: barcode.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"barcode")
                
                
                let imageDataTakeImage = UIImagePNGRepresentation(Image)
                
                multipartFormData.appendBodyPart(data:imageDataTakeImage , name: "Image", fileName: "iosFile.png", mimeType: "Image/*")
                
                var t = Int()
                
                t = 0
               
                for ( var i = 0; i < image.count; i++)
                {
                    println(t)
                    t++
                    let imageData = UIImagePNGRepresentation(image[i])
                    
                    multipartFormData.appendBodyPart(data: imageData, name: "image\(t)", fileName: "iosFile\(i+1).png", mimeType: "image/*")
                   
                     println(t)
                
                }
                
                for ( var j = 0; j < audioFile.count; j++)
                {
                    
                    t++
                    let audiodata = NSData(contentsOfURL: audioFile[j])
                    
                    
                    print(audiodata)
                    
                    multipartFormData.appendBodyPart(data: audiodata!, name: "image\(t)", fileName: "iosFile\(j+1).wav", mimeType: "image/*")
                    
                    println(t)
                }

                  for (key, value) in param
                {
                    multipartFormData.appendBodyPart(data:"\(value)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :key)
                }
                
                
                
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { request, response, data, error in
                        
                        println("json:: \(data)")
                        
                        
                        if data == nil
                            
                        {
                            let alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                            
                            alert.show()
                        }
                        else
                        {
                            
                            var success = data?.valueForKey("success") as! Bool
                            
                            if success
                            {
                                var message = data?.valueForKey("message") as! NSDictionary
                                
                                var itemId = message.valueForKey("ItemId") as! Int
                                
                                println(itemId)
                                
                                
                                self.getPackageDataItems()
                                
                                //spinningIndicator.hide(true)
        
                            }
                                
                            else
                            {
                                
                                var message = data?.valueForKey("message") as! String
                                let alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                
                            }
                        }
                        callback(true)
                    }
                case .Failure(let encodingError):
                    callback(false)
                }
            }
        )
    }
    
    
    //MARK:- Add More Button
    
    @IBAction func addMoreBtn(sender: AnyObject)
    {
        
        if titleTxtField.text == "" || descriptionTxtField.text == ""
        {
            let alert = UIAlertView(title: "Alert", message: "Fill the required fields", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
        }
        else if conditionBoolImage != true
        {
            let alert = UIAlertView(title: "Alert", message: "Select the image", delegate: self, cancelButtonTitle: "OK")
            
            alert.show()
            
            
        }
        else
        {
    
            if imageView.image == UIImage(named: "img_take_photo.png")
            {
                let alert = UIAlertView(title: "Alert", message: "Select the image", delegate: self, cancelButtonTitle: "OK")
                alert.show()
 
            }
            else
            {
                  addDataItemsApi()
                
                 iAttachement = 0
                 attachementLbl.text = "0"
                 arrayImageApi = []
            }
            
          
            
        }
        
    }
    
    
}
