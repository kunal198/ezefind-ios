//
//  FullMenuCategoryViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


var itemId_category = String()


var typeCategoryCheck = String()




class FullMenuCategoryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate
{
    
    
    @IBOutlet weak var categoryTableView: UITableView!
    
    
    var imageArray = NSArray()
    
    @IBOutlet weak var collectionViewCategory: UICollectionView!
    
    @IBOutlet weak var categoryView: UIView!
    
    @IBOutlet weak var filterBtn: UIButton!
    
    @IBOutlet weak var blackView: UIView!
    
    
    var categorySelectFilter = String()
    var locationFilter = ""
   
    @IBOutlet weak var navLAbelTitle: UILabel!
    
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
//        imageArray = ["img_scan_jacket.png","img_scan_jacket.png","img_scan_jacket.png","img_scan_jacket.png","img_scan_jacket.png"]
        
        
        
//        categorySelectFilter = NSUserDefaults.standardUserDefaults().objectForKey("locationName") as! String
//        
        
        
        
        println("location name = \(locationFilter)")

        
        
        
        if locationCheck == "locationFilter"
        {
           fullMenuLocationFilterAPI()
            filterBtn.hidden = true
            
            navLAbelTitle.text = "View:Location"
        }
        else if locationCheck == "category"
        {
            fullMenuCategoryAPI()
            filterBtn.hidden = false
            
            navLAbelTitle.text = "View:Category"
        }
        else
        {
            
        }
    
        
    }
    
    
    
    
    
    //MARK:-Full Menu Location Filter Api
    
    func fullMenuLocationFilterAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&Location_filter=%@",str,locationFilter as! String)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/location-filter")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                            self.imageArray = dicObj?.valueForKey("Data") as! NSArray
                            
                            println("location names = \(self.imageArray)")
                            
                            
                            self.collectionViewCategory.reloadData()
                            
//                            self.locationNames =  self.imageArray.valueForKey("Location") as! NSArray as! [(String)]
//                            
//                            
//                            println("location names = \(self.locationNames)")
//                            
//                            
//                            self.tableViewLocation.reloadData()
                            
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    
    

    
    
    
    
    @IBAction func cancelCategoryTableView(sender: AnyObject)
    {
        blackView.hidden = true
        categoryView.hidden = true
    }
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imageArray.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        var categoryCell = collectionView.dequeueReusableCellWithReuseIdentifier("categoryCell", forIndexPath: indexPath) as! FullMenuCategoryCollectionViewCell
        
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
        var profileUrl: String = imageArray[indexPath.row].valueForKey("Image") as! String
        
        var url: NSURL = NSURL(string: profileUrl)!
        
        categoryCell.categoryImageView.sd_setImageWithURL(url, completed: block)
        
        
        
       categoryCell.jacketLbl.text = imageArray[indexPath.row].valueForKey("Title") as? String
        
        
        categoryCell.FragileLbl.text = imageArray[indexPath.row].valueForKey("Description") as? String
        
        
        
       // itemId_category = (imageArray[indexPath.row].valueForKey("ItemId") as? String)!

        
       println("id = \(itemId_category)")
        
        
        return categoryCell
    }
    
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDelegate.viewInventoryStoryboard() as UIStoryboard
        
        let packGoPackages = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("packGoPackage") as! PackGoPackagesViewController
        
                
     packGoPackages.itemId = (imageArray[indexPath.row].valueForKey("ItemId") as? Int)!
        
    // println("id at didSelect = \(itemId)")

        typeCategoryCheck = "didSelect"
        
        
        self.navigationController?.pushViewController(packGoPackages, animated: true)
        
    }
    
    
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        var sizeHeight = UIScreen.mainScreen().bounds.height
        if sizeHeight == 480 || sizeHeight ==  568
        {
            return CGSizeMake(155, 155)
        }
            
        else if sizeHeight ==  667
        {
            return CGSizeMake(182, 182)
        }
        else if sizeHeight ==  736
        {
            return CGSizeMake(200,200)
        }
            
            
        else
        {
            return CGSizeMake(240, 240)
        }
        
    }

    
    
    
    @IBAction func filterCategoryBtn(sender: AnyObject)
    {
        
        println("filer button clicked")
        
        fullMenuCategoryAPI()
        
        blackView.hidden = false
        categoryView.hidden = false
    }
    
    
    
    
    
    
    
    //MARK: - CategoryTableViewMethods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return imageArray.count
    }
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
                var tableViewCell = tableView.dequeueReusableCellWithIdentifier("newEntryCategoryCell", forIndexPath: indexPath) as! NewEntryCategoryTableViewCell
        
//        
//        var tableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
        
        
    tableViewCell.filterTitleValue.text = imageArray[indexPath.row].valueForKey("Title") as? String
        
        
        
        return tableViewCell
        
    }
    

    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        categorySelectFilter = (imageArray[indexPath.row].valueForKey("Title") as? String)!
        
        println("category id at particular row = \(categorySelectFilter)")
        
        fullMenuCategoryFilterAPI()

    }

    
    
    
    
    
    
    //MARK:-Full Menu Category Filter Api
    
    func fullMenuCategoryFilterAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&Category_filter=%@",str,categorySelectFilter)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/category")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                            self.imageArray = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
                        
                            self.collectionViewCategory.reloadData()
                         
                            self.blackView.hidden = true
                            self.categoryView.hidden = true
                         
                            println("image array from didSelect = \(self.imageArray)")
                           
                            
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    

    
    
    
    
    
    
    
    
    
    

    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- Navigation Barcode Btn
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }
    
    
    
    
    
    
    //MARK:-Full Menu Category Api
    
    func fullMenuCategoryAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i",str)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/category")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })

                    
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                            self.imageArray = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
                            
                            self.collectionViewCategory.reloadData()
                            
                            self.categoryTableView.reloadData()
                            
                            println(self.imageArray)
                            
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    

    
    
    
    
    
    
    

}
