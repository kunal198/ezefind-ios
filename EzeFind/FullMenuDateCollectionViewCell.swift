//
//  FullMenuDateCollectionViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class FullMenuDateCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet var quantityLbl: UILabel!
    @IBOutlet var attachmentlbl: UILabel!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var dateImageView: UIImageView!
}
