//
//  PackGoPackagesViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire



var itemID_toPicklist = Int()

class PackGoPackagesViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVAudioPlayerDelegate,AVAudioRecorderDelegate,UICollectionViewDelegate,UICollectionViewDataSource
{
    var audioUrl = [NSURL]()
    var audioBool = Bool()
    
    var arrayImageUpdateApi = [UIImage]()
    var arryAttachmentImages = [NSDictionary]()
    var packageId = NSString()
    var packageDataId = NSString()
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var scrollView: UIScrollView!
    var greenTitleStr = NSString()
    @IBOutlet var greenTitleLbl: UILabel!
    @IBOutlet var updateBtn: UIButton!
    
   
    
    @IBOutlet weak var colorLbl: UITextField!
    
    
    @IBOutlet weak var titleLbl: UITextField!
    
    
    @IBOutlet var imageView: UIImageView!
    var singleDataItemArray = NSArray()
    var itemId  = Int()
    var itemId_category = String()
    
    @IBOutlet var valueTxtField: UITextField!
    @IBOutlet var locationTxtField: UITextField!
    @IBOutlet var quantityTxtField: UITextField!
    @IBOutlet var attachmentTxtField: UITextField!
    @IBOutlet var descriptionLbl: UILabel!
    var SizeHeight = UIScreen.mainScreen().bounds.height
    
    @IBOutlet var descriptionTxtField: UITextView!
    @IBOutlet var blackView: UIView!
    
    @IBOutlet var navigationTitlelbl: UILabel!
    
    
    var deleteAttachmentAlertView = UIAlertView()
    
     var post = NSString()
    
    
    var attachmentID = Int()
    
    
    @IBOutlet weak var barCodeBlackVIew: UIView!
    
    @IBOutlet weak var barCodeWrapperView: UIView!
    
    @IBOutlet var photoVideoAudioBarcodeView: UIView!
    @IBOutlet var photoVideoView: UIView!
    
    @IBOutlet var audioView: UIView!
    var imagePickerAttachment = UIImagePickerController()
    var iAttachement = Int()
    
    var isPlayAudio = Bool()
    
    var recordingName = String()
    var min = Int()
    var sec = Int()
    var minSecStr = String()
    
    @IBOutlet var audioTimerTitleView: UIView!
    @IBOutlet var audioTitleTxtFiel: UITextField!
    @IBOutlet var slider: UISlider!
    @IBOutlet var playAudioTimerLbl: UILabel!
    @IBOutlet var recordTimerLbl: UILabel!
    @IBOutlet var audioCurrentDateLbl: UILabel!
    
    var isStopPlayer = Bool()
    @IBOutlet var deleteAudioFileBtn: UIButton!
    @IBOutlet var saveFileAudioBtn: UIButton!
    @IBOutlet var audioCrossBtn: UIButton!
    @IBOutlet var pauseAudioBtn: UIButton!
    @IBOutlet var playAudioBtn: UIButton!
    var audioRecord:AVAudioRecorder!
    var audioPlayer:AVAudioPlayer!
    
    var titleStr:String!
    var filePathURL:NSURL!
    var  filePath : NSURL!
    
    @IBOutlet var playBtn: UIButton!
    @IBOutlet var playActiveBtn: UIButton!
    
    
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
         println("id at didSelect in category view inventory = \(itemId_category)")
        
        
        if typeCategoryCheck == "didSelect"
        {
            navigationTitlelbl.text = "View:Category"
        }
        else if typeCategoryCheck == "ValueSectionDidSelect"
        {
            navigationTitlelbl.text = "View:Value"
        }
        else if  typeCategoryCheck == "DateSectionDidSelect"
        {
           navigationTitlelbl.text = "View:Date"
        }
        else if typeCategoryCheck == "PackGOSectionDidSelect"
        {
            navigationTitlelbl.text = newEntryPackGoOpenTitleStr as String
            greenTitleLbl.text = greenTitleStr as String
 
        }
        else
        {
            
        }
        
        
        compatibility()
        
        singleItemdetailApi()
        
        titleLbl.delegate = self
        colorLbl.delegate = self
        valueTxtField.delegate = self
        locationTxtField.delegate = self
        quantityTxtField.delegate = self
        attachmentTxtField.delegate = self
        descriptionTxtField.delegate = self
        
        audioTitleTxtFiel.delegate = self
        iAttachement = 0
        imagePickerAttachment.delegate = self
        
        updateBtn.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        photoVideoAudioBarcodeView.layer.borderColor = UIColor(red: 36.0/255, green: 193.0/255, blue: 13.0/255, alpha: 1.5).CGColor
        
        audioView.layer.borderColor = UIColor(red: 56.0/255, green: 102.0/255, blue: 201.0/255, alpha: 1.5).CGColor
        
        
        audioTimerTitleView.layer.borderColor = UIColor(red: 56.0/255, green: 102.0/255, blue: 201.0/255, alpha: 1.5).CGColor
        
        let date = NSDate()
        var dateFormat = NSDateFormatter()
        
        dateFormat.dateFormat = "dd/MM/yyyy"
        
        playAudioTimerLbl.text = "00:00"
        recordTimerLbl.text = "00:00"
        var dateInFormat:NSString = dateFormat.stringFromDate(date)
        audioCurrentDateLbl.text = dateInFormat as String
        println(dateInFormat)
        
        
        self.collectionView.backgroundColor = UIColor.whiteColor()
        
    }
    
    
    
    
    
    func compatibility()
    {
        if SizeHeight == 736
        {
            descriptionTxtField.frame.origin.y =  descriptionTxtField.frame.origin.y + 5
            
        }
        if SizeHeight == 667 || SizeHeight == 568
        {
            descriptionTxtField.frame.origin.y =  descriptionTxtField.frame.origin.y - 2
            
        }
        
        if SizeHeight == 480
        {
            descriptionTxtField.frame.origin.y =  descriptionTxtField.frame.origin.y - 3
            
        }
        
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad
        {
            attachmentTxtField.frame.origin.x = attachmentTxtField.frame.origin.x - 100
            
            descriptionTxtField.frame.origin.y =  descriptionTxtField.frame.origin.y + 10
            
            descriptionTxtField.frame.origin.x =  descriptionTxtField.frame.origin.x - 100
        }
        
    }
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func BarCodeSelectedBtn(sender: AnyObject)
    {
        
        println("ok button")
        
        self.barCodeBlackVIew.hidden = true
        
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let newEntryScanBarcode = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryScanBarcode") as! NewEntryScanBarcodeViewController
        
        //newEntryScanBarcode.greenTitleStr = singleItemGreenTitleStr
        
        self.navigationController?.pushViewController(newEntryScanBarcode, animated: false)

    }
    
    
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    
    //MARK:- PICKLIST button
    
    @IBAction func pickListBtn(sender: AnyObject)
    {
        blackView.hidden = false
    }
    
    
    
    @IBAction func addToExitingListBtn(sender: AnyObject)
    {
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let pickList = graphStoryboard.instantiateViewControllerWithIdentifier("pickList") as! PickListViewController
        
        pickList.itemID = self.singleDataItemArray[0].valueForKey("ItemId") as! Int
        
        //println(itemID)
        
         picklistView = "pickListView"
        
         editBackCheck = "addtoExistingBtn"
        
        if var itemid = self.singleDataItemArray[0].valueForKey("ItemId") as? Int
        {
            //println("item id = \(itemID)")
            
            itemID_toPicklist = Int(itemid)
            println("item id = \(itemID_toPicklist)")
        }
        
        
        
        
        self.navigationController?.pushViewController(pickList, animated: true)
        
        blackView.hidden = true
    }
    
    
    
    
    
    
    
    @IBAction func startNewPickListBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var settingToFurtherStoryboard = appDelegate.settingStoryboard() as UIStoryboard
        
        let startNewPickList = settingToFurtherStoryboard.instantiateViewControllerWithIdentifier("startNewPickList") as! StartNewPickListViewController
        startNewPickList.itemID = self.singleDataItemArray[0].valueForKey("ItemId") as! Int
        self.navigationController?.pushViewController(startNewPickList, animated: true)
        blackView.hidden = true
    }
    
    
    
    
    
    
    
    @IBAction func crossBtn(sender: AnyObject)
    {
        blackView.hidden = true
    }
    
    
    
    //MARK:- Navigation Barcode Btn
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(GraphViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }
    
    
    
    
    
    //MARK:- Touch Method
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        valueTxtField.resignFirstResponder()
        locationTxtField.resignFirstResponder()
        quantityTxtField.resignFirstResponder()
        attachmentTxtField.resignFirstResponder()
        descriptionTxtField.resignFirstResponder()
    }
    
    
    
    
    //MARK:- TextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        if textField == descriptionTxtField || textField == quantityTxtField || textField == locationTxtField
            || textField == attachmentTxtField || textField == valueTxtField
        {
            
            view.frame.origin.y -= 250
            
        }
        else if textField == colorLbl || textField == titleLbl
        {
            view.frame.origin.y -= 200
        }
        else
        {
            view.frame.origin.y = view.frame.origin.y
        }
        return true
    }
    
    
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if textField == descriptionTxtField || textField == quantityTxtField || textField == locationTxtField
        || textField == attachmentTxtField || textField == valueTxtField
            
        {
            
            view.frame.origin.y += 250
        
        }
        else if textField == colorLbl || textField == titleLbl
        {
            view.frame.origin.y += 200
        }
        else
        {
            
        }
        
        
        return true
        
    }
    
    
    
    
    
    //MARK:- TextView Delegate

    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        
       self.view.frame.origin.y -= 200

        return true
    }
    
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool
    {
        
        self.view.frame.origin.y += 200
        
        return true
    }

    
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    
    

    
    //MARK: - Single Item Detail Api
    func singleItemdetailApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as? Int
        println(str)

        
        var dataModel = NSData()
        
        //var post = NSString(format:"UserId=%i&PackageId=%@&PackageDataId=%@&ItemId=%i",str!,packageId,packageDataId,itemId)
        
        
//        if typeCategoryCheck == "didSelect"
//        {
//            post = NSString(format:"UserId=%i&ItemId=%i",str!,itemId_category)
//            
//            println(post)
//        }
//        else
//        {
            var post = NSString(format:"UserId=%i&ItemId=%i",str!,itemId)
            
            println(post)
  //      }
        
        
      
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/info-item")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })

                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        })
                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            
                            if success
                            {
                                self.singleDataItemArray = dicObj?.valueForKey("message") as! [NSDictionary]
                                
                                println(self.singleDataItemArray)
                                
                                self.collectionView.reloadData()
                                
                                
//                                
//                                if var urlString  =  self.singleDataItemArray[0].valueForKey("Image") as? String, imageurl = NSURL(string: NSString(format: "http://beta.brstdev.com/yiiezefind%@", urlString) as String)
//                                {
//                                    if let data = NSData(contentsOfURL: imageurl)
//                                    {
//                                        println(urlString)
//                                        self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
//                                        self.imageView.image = UIImage(data: data)
//                                    }
//                                }
                                
                                
                                
                                
                                
            var profileUrl: String = self.singleDataItemArray[0].valueForKey("Image") as! String
                                
            var url: NSURL = NSURL(string: profileUrl)!
                                
            if let data : NSData = NSData(contentsOfURL: url)
            {
                self.imageView.image = UIImage(data: data)!
                self.imageView.backgroundColor = UIColor.clearColor()
                self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
                
                //imageView.image = UIImage(data: data)!
            }
                                

                                
    
                                
                                
                                self.titleLbl.text = ""
                                
                                if var strTitle = self.singleDataItemArray[0].valueForKey("Title") as? String
                                {
                                    self.titleLbl.text = strTitle
                                }
                                
                                self.descriptionTxtField.text = ""
                                if var strDesc = self.singleDataItemArray[0].valueForKey("Description") as? String
                                {
                                    self.descriptionTxtField.text = strDesc
                                }
                                
                                self.colorLbl.text = ""
                                if var strColor = self.singleDataItemArray[0].valueForKey("Color") as? String
                                {
                                    self.colorLbl.text = strColor
                                }
                                
                                
                                
                                self.quantityTxtField.text = ""
                                if var strQuantity = self.singleDataItemArray[0].valueForKey("Quantity") as? Int
                                {
                                    self.quantityTxtField.text = toString(strQuantity)
                                }
                                
                                
                                
                                self.attachmentTxtField.text = ""
                                if var strAttachment = self.singleDataItemArray[0].valueForKey("AttachmentCount") as? Int
                                {
                                    self.attachmentTxtField.text = toString(strAttachment)
                                }
                                
                                self.valueTxtField.text = ""
                                if var strValue = self.singleDataItemArray[0].valueForKey("value") as? String
                                {
                                    self.valueTxtField.text = strValue
                                }
                                
                                self.arryAttachmentImages = []
                                if var array = self.singleDataItemArray[0].valueForKey("Attachments")  as? NSArray
                                {
                                    self.arryAttachmentImages = array  as! [NSDictionary]
                                    println(self.arryAttachmentImages)
                                    
                                    
                                }
                                
                                var locationDic =  self.singleDataItemArray[0].valueForKey("Location")  as? NSDictionary
                                
                                println("location dic = \(locationDic)")
                                
                                
                                self.locationTxtField.text = ""
                                if var strlocation = self.singleDataItemArray[0].valueForKey("Address") as? String
                                {
                                    self.locationTxtField.text = strlocation
                                }

                                
                                
                                
                                if var strQuantity = self.singleDataItemArray[0].valueForKey("Quantity") as? Int
                                {
                                    self.quantityTxtField.text = toString(strQuantity)
                                }
                                
                                
                                
                                
                       if var itemid = self.singleDataItemArray[0].valueForKey("ItemId") as? Int
                       {
                            //println("item id = \(itemID)")
                        
                            itemID_toPicklist = Int(itemid)
                            println("item id = \(itemID_toPicklist)")
                       }
                        
                        
                                
                                
                        spinningIndicator.hide(true)
                                
                                
                                
                                
                                
                                
                            }
                                
                            else
                            {
                                var message = dicObj?.valueForKey("message") as! String
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                spinningIndicator.hide(true)
                                
                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        
        
    }
    
    //MARK:- Attachment Button
    
    @IBAction func attachmentBtn(sender: AnyObject)
    {
        photoVideoAudioBarcodeView.hidden = false
        
        
        
        
    }
    
    //MARK:- Attachment Process
    
    @IBAction func crossBtnPack(sender: AnyObject)
    {
        photoVideoAudioBarcodeView.hidden = true
    }
    
    @IBAction func photoVideoBtn(sender: AnyObject)
    {
        photoVideoAudioBarcodeView.hidden = true
        photoVideoView.hidden = false
    }
    
    @IBAction func audioBtn(sender: AnyObject)
    {
        photoVideoAudioBarcodeView.hidden = true
        audioView.hidden = false
        
    }
    
    
    
    
    @IBAction func barcodeBtn(sender: AnyObject)
    {
        self.barCodeBlackVIew.hidden = false
    }
    
    
    
    
    
    
    @IBAction func takePhotoVideo(sender: AnyObject)
    {
        photoVideoView.hidden = true
        
        if (UIImagePickerController.availableCaptureModesForCameraDevice(UIImagePickerControllerCameraDevice.Rear) != nil)
        {
            imagePickerAttachment.allowsEditing = true
            imagePickerAttachment.sourceType = UIImagePickerControllerSourceType.Camera
            imagePickerAttachment.cameraCaptureMode = .Photo
            
            
            presentViewController(imagePickerAttachment, animated: true, completion: nil)
        }
        else
        {
            let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
            alertView.show()
            
        }
        
        
        
    }
    
    @IBAction func chooseExisting(sender: AnyObject)
    {
        photoVideoView.hidden = true
        imagePickerAttachment.allowsEditing = true
        imagePickerAttachment.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        
        
        presentViewController(imagePickerAttachment, animated: true, completion: nil)
        
        
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject])
    {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            
            arrayImageUpdateApi.append(pickedImage)
            
            println(arrayImageUpdateApi)
            
            self.dismissViewControllerAnimated(false, completion: nil)
            
            
            var intVAlue = (attachmentTxtField.text as String).toInt()
            
            attachmentTxtField.text = NSString(format: "%i",intVAlue! + 1) as String
            
        }
        
        
    }
    
    
    @IBAction func photoVideoCrossBtn(sender: AnyObject)
    {
        photoVideoView.hidden = true
    }
    
    //MARK:- Audio Process
    
    
    @IBAction func audioCrossBtn(sender: AnyObject)
    {
        
        audioView.hidden = true
        playAudioBtn.hidden = false
        pauseAudioBtn.hidden = true
        
        if isStopPlayer == true
        {
            audioPlayer.stop()
        }
        
    }
    
    
    @IBAction func saveFileBtn(sender: AnyObject)
    {
        
        audioView.hidden = true
        playAudioBtn.hidden = false
        pauseAudioBtn.hidden = true
        
        if isStopPlayer == true
        {
            audioPlayer.stop()
        }
        
        
    }
    
    @IBAction func playActiveBtn(sender: AnyObject)
    {
        
        playActiveBtn.hidden = true
        playBtn.hidden = false
        playAudioBtn.enabled = true
        saveFileAudioBtn.enabled = true
        deleteAudioFileBtn.enabled = true
        audioCrossBtn.enabled = true
        
        audioRecord.stop()
    }
    
    
    @IBAction func playBtn(sender: AnyObject)
    {
        
        playActiveBtn.hidden = false
        playBtn.hidden = true
        playAudioBtn.enabled = false
        saveFileAudioBtn.enabled = false
        deleteAudioFileBtn.enabled = false
        audioCrossBtn.enabled = false
        let dirPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as! String
        
        let currentDateTime = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "ddMMyyyy-HHmmss"
        recordingName = formatter.stringFromDate(currentDateTime)+".wav"
        println(recordingName)
        
        let pathArray = [dirPath, recordingName]
        filePath = NSURL.fileURLWithPathComponents(pathArray)!
        print(filePath)
        
        let session = AVAudioSession.sharedInstance()
        session.setCategory(AVAudioSessionCategoryPlayAndRecord, error: nil)
        audioRecord = AVAudioRecorder(URL: filePath, settings: nil, error: nil)
        audioRecord.delegate = self
        audioRecord.meteringEnabled = true
        audioRecord.prepareToRecord()
        audioRecord.record()
        isPlayAudio = true
        var meterTimer = NSTimer.scheduledTimerWithTimeInterval(0.1,
            target:self,
            selector:"updateAudioMeter:",
            userInfo:nil,
            repeats:true)
        
        iAttachement++
        attachmentTxtField.text = NSString(format: "%i",iAttachement) as String
        
        
    }
    
    func updateAudioMeter(timer:NSTimer) {
        
        if audioRecord.recording {
            min = Int(audioRecord.currentTime / 60)
            sec = Int(audioRecord.currentTime % 60)
            let minsec = String(format: "%02d:%02d", min, sec)
            recordTimerLbl.text = minsec
            audioRecord.updateMeters()
            // if you want to draw some graphics...
            //var apc0 = recorder.averagePowerForChannel(0)
            //var peak0 = recorder.peakPowerForChannel(0)
        }
    }
    
    
    
    @IBAction func deleteAudioFileBtn(sender: AnyObject)
    {
        
        if isStopPlayer == true
        {
            audioPlayer.stop()
        }
        
        
        var fileManager: NSFileManager = NSFileManager.defaultManager()
        
        var documentsPath: String = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        var filePathxsa: String = documentsPath.stringByAppendingPathComponent(recordingName) as String
        
        var error: NSError?
        
        var success:Bool  = fileManager.removeItemAtPath(filePathxsa, error: &error)
        println(success)
        if success
        {
            var removeSuccessFulAlert: UIAlertView = UIAlertView(title: "Alert", message: "Successfully removed", delegate: self, cancelButtonTitle: "OK")
            removeSuccessFulAlert.show()
            isPlayAudio = false
        }
        else
        {
            NSLog("Could not delete file -:%@ ", error!.localizedDescription)
            
            var alert = UIAlertView(title: "Alert", message: "File not exist", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
        
        playAudioBtn.hidden = false
        pauseAudioBtn.hidden = true
    }
    
    
    
    @IBAction func playAudioBtn(sender: AnyObject)
    {
        
        if isPlayAudio
        {
            var url = NSURL.fileURLWithPath(filePath.path!)
            audioPlayer = AVAudioPlayer(contentsOfURL: url, error: nil)
            audioPlayer.prepareToPlay()
            audioPlayer.volume = 100.0
            
            minSecStr = String(format: "%02d min,%02d sec", min, sec)
            println(minSecStr)
            playAudioTimerLbl.text = minSecStr
            
            slider.maximumValue = Float(audioPlayer.duration)
            
            
            NSTimer.scheduledTimerWithTimeInterval(0.1,
                target:self,
                selector:"updateAudioSlider:",
                userInfo:nil,
                repeats:true)
            
            
            audioPlayer.play()
            playBtn.enabled = false
            playAudioBtn.hidden = true
            pauseAudioBtn.hidden = false
            isStopPlayer = true
            
        }
        else
        {
            var alert = UIAlertView(title: "Alert", message: "File not exist", delegate: self, cancelButtonTitle: "OK")
            
            alert.show()
            
        }
        
        
        
    }
    
    
    
    func updateAudioSlider(timer:NSTimer)
    {
        
        slider.value = Float(audioPlayer.currentTime)
        println(slider.value)
        
        
    }
    
    
    
    @IBAction func pauseAudioBtn(sender: AnyObject)
    {
        
        playBtn.enabled = true
        playAudioBtn.hidden = false
        pauseAudioBtn.hidden = true
        
        if audioPlayer.play()
        {
            audioPlayer.stop()
        }
        
    }
    
    
    
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder!, successfully flag: Bool)
    {
        if(flag)
        {
            //Store in Model
            
            filePathURL = recorder.url
            
            audioUrl.append(filePathURL)
            
            titleStr = recorder.url.lastPathComponent
            //Segway once we've finished processing the audio
        }
        else
        {
            println("recording not successful")
            
        }
    }
    
    //MARK:- updateButton
    
    @IBAction func updateBtn(sender: AnyObject)
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        println("barcode = \(strBagToFurtherScanBarcode)")
       
        createMultipart(arrayImageUpdateApi, audioFile: audioUrl,barcode: strBagToFurtherScanBarcode as String, callback: { success in
            
            if success
            {
                println("success")
                
                self.collectionView.reloadData()
            }
            else
            {
                println("fail")
            }
        })
        
        spinningIndicator.hide(true)
        

    }
    
    
    
    //MARK:- CollectionView Method
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arryAttachmentImages.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        var packGoPackagesCell = collectionView.dequeueReusableCellWithReuseIdentifier("packGoPackagesCell", forIndexPath: indexPath) as! PackGoPackagesCollectionViewCell
        
        
        
    
        if arryAttachmentImages != []
        {
          scrollView.contentSize.height = collectionView.frame.origin.y + collectionView.frame.size.height
        }
        
        
        var fileType = arryAttachmentImages[indexPath.row].valueForKey("FileType") as! NSString
        

                
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                    //			println(self)
                }
                
                
                
        var profileUrl: String = (arryAttachmentImages[indexPath.row].valueForKey("FileName") as? String)!
                
        var url: NSURL = NSURL(string: profileUrl)!
                
                
        packGoPackagesCell.attachmentImageview.sd_setImageWithURL(url, completed: block)

           
//            }
//            else
//            {
//                
//                var imageAudio = UIImageView()
//                imageAudio.frame = CGRectMake(0, 0, packGoPackagesCell.frame.size.width, packGoPackagesCell.frame.size.height)
//                imageAudio.image = UIImage(named: "img_scan_jacket.png")
//                
//                var buttonAudio = UIButton(frame: CGRectMake(packGoPackagesCell.frame.size.width/3, packGoPackagesCell.frame.size.height/3, packGoPackagesCell.frame.size.width/4,packGoPackagesCell.frame.size.height/4 ))
//                buttonAudio.setImage(UIImage(named: "icon_playing.png"), forState: .Normal)
//                buttonAudio.addTarget(self, action: "audioPlayPauseBtn:", forControlEvents: UIControlEvents.TouchUpInside)
//                buttonAudio.tag = indexPath.row
//                
//                
//
//                packGoPackagesCell.addSubview(imageAudio)
//                packGoPackagesCell.addSubview(buttonAudio)
//                
//            
//                
//            }
        
  //      }
        
        packGoPackagesCell.imageWrapperBtn.tag = indexPath.row
        
        packGoPackagesCell.imageWrapperBtn.addTarget(self, action: Selector("deleteWrapperBtn:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        return packGoPackagesCell
    }
    
    
    
    
    func deleteWrapperBtn(sender: UIButton!)
    {
        
//        var point : CGPoint = sender.convertPoint(CGPointZero, toView:collectionView)
//        var indexPath = collectionView!.indexPathForItemAtPoint(point)
//        let cell = collectionView!.cellForItemAtIndexPath(indexPath!)
        
        
        attachmentID = (arryAttachmentImages[sender.tag].valueForKey("AttachmentId") as? Int)!
        println("attachmentID = \(attachmentID)")
        
        deleteAttachmentAlertView = UIAlertView(title: "Alert", message: "Do you want to delete this attachment?", delegate: self,  cancelButtonTitle: "NO", otherButtonTitles: "YES")
        
        deleteAttachmentAlertView.show()

    }
    
    
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        var sizeHeight = UIScreen.mainScreen().bounds.height
        if sizeHeight == 480 || sizeHeight ==  568
        {
            return CGSizeMake(155, 155)
        }
            
        else if sizeHeight ==  667
        {
            return CGSizeMake(182, 182)
        }
        else if sizeHeight ==  736
        {
            return CGSizeMake(200,200)
        }
            
            
        else
        {
            return CGSizeMake(240, 240)
        }
        
    }
    
    
    func audioPlayPauseBtn(sender:UIButton)
    {
        if audioBool == false
        {
            sender.setImage(UIImage(named: "icon_pause.png"), forState: UIControlState.Normal)
            
            if var urlString  =  arryAttachmentImages[sender.tag].valueForKey("FileName") as? String, imageurl = NSURL(string: NSString(format: "http://beta.brstdev.com/yiiezefind%@", urlString) as String)
            {
                
                

                let soundData = NSData(contentsOfURL: imageurl)
                var error: NSError?
                audioPlayer = AVAudioPlayer(data: soundData, error: &error)
                if audioPlayer == nil
                {
                    if let e = error
                    {
                        println(e.localizedDescription)
                    }
                }
                audioPlayer.prepareToPlay()
                audioPlayer.volume = 1.0
                audioPlayer.play()
                
                
            }

            
            audioBool = true
        }
        else
        {
            sender.setImage(UIImage(named: "icon_playing.png"), forState: UIControlState.Normal)
            audioBool = false
            audioPlayer.stop()
        }
        
        
        
    }
    
    func buttonAudioPause(sender:UIButton)
    {
        
    }
    
    
    

    //MARK:- UpdateDataItem
    
    func createMultipart(image: [UIImage], audioFile:[NSURL],barcode: String,callback: Bool -> Void)
    {
        
        if singleDataItemArray != []
        {
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let userID = userDefaults.valueForKey("userId") as! Int
        
        println(userID)
        
        let strUserID = toString(userID)
        
        
        var packageIdUpdateItemInt = singleDataItemArray[0].valueForKey("PackageId") as! Int
        var packageIdUpdateItem = toString(packageIdUpdateItemInt)
        var packageDataIdUpdateItemInt = singleDataItemArray[0].valueForKey("PackageDataId") as! Int
        var packageDataIdUpdateItem = toString(packageDataIdUpdateItemInt)
        var itemIdUpdateItemInt = singleDataItemArray[0].valueForKey("ItemId") as! Int
        var itemIdUpdateItem = toString(itemIdUpdateItemInt)
        
        println(packageIdUpdateItem)
        println(packageDataIdUpdateItem)
        println(itemIdUpdateItem)
       
        
        
        let param = [
            
            "UserId" : strUserID,
            "PackageId" : packageIdUpdateItem,
            "PackageDataId" : packageDataIdUpdateItem,
            "Title" : titleLbl.text,
            "Description" : descriptionTxtField.text,
            "Value" : valueTxtField.text,
            "Quantity" : quantityTxtField.text,
            "Color" : colorLbl.text,
            "ItemId" : itemIdUpdateItem,
            "Address" : locationTxtField.text,
            "barcode" : barcode
            
        ]
        
        print(param)
        
        upload(
            .POST,
            URLString: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/updatedataitem",
            multipartFormData: { multipartFormData in
                
               
                var t = Int()
                t = 0
                
                
                
                
                multipartFormData.appendBodyPart(data: barcode.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"barcode")
                
        
                
                
                for ( var i = 0; i < image.count; i++)
                {
                    let imageData = UIImagePNGRepresentation(image[i])
                    multipartFormData.appendBodyPart(data: imageData, name: "image\(i+1)", fileName: "iosFile\(i+1).png", mimeType: "image/*")
                }
                
                for ( var j = 0; j < audioFile.count; j++)
                {
                    
                    t++
                    
                    let audiodata = NSData(contentsOfURL: audioFile[j])
                    
                    multipartFormData.appendBodyPart(data: audiodata!, name: "image\(t)", fileName: "iosFile\(j+1).wav", mimeType: "image/*")
                    
                    println(t)
                }
                
                            
                
                

                
                for (key, value) in param
                {
                    multipartFormData.appendBodyPart(data:"\(value)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :key)
                }
                
                
                
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { request, response, data, error in
                        
                        println("json:: \(data)")
                        
                        
                        if data == nil
                        {
                            let alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                        }
                        else
                        {
                            
                            var success = data?.valueForKey("success") as! Bool
                            
                            if success
                            {
                                var message = data?.valueForKey("message") as! NSDictionary
                                
                               
                                let alert = UIAlertView(title: "Alert", message: "Successfully Updated", delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                
                                
                            }
                                
                            else
                            {
                                
                                var message = data?.valueForKey("message") as! String
                                let alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                
                            }
                        }
                        callback(true)
                    }
                case .Failure(let encodingError):
                    callback(false)
                }
            }
        )
            
            
        }
        else
        
        {
            let alert = UIAlertView(title: "Alert", message: "Data Not updated", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    

    
    
    
    //MARK:- AlertView Method
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        
//        if alertView == backBtnAlertView
//        {
//            if buttonIndex == 1
//            {
//                
//                deletePackageData()
//            }
//            
//        }
        
        
        if alertView == deleteAttachmentAlertView
        {
            if buttonIndex == 1
            {
                deleteAttachmentPackageData()
            }
        }
        
        
        
    }
    
    
    
    
    //MARK:- Delete Attachment PackageData
    
    func deleteAttachmentPackageData()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        blackView.hidden = true
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        let packageId = userDefaults.valueForKey("packageId") as? String
        let packageDataId = userDefaults.valueForKey("packageDataId") as? Int
        println(packageId)
        println(packageDataId)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&ItemId=%i&AttachmentId=%i",str,itemId,attachmentID)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/deleteattachment")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })
                        
                        
                        
                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Int
                            var message = dicObj?.valueForKey("message") as! String
                            
                            if success == 0
                            {
                                
                                
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                
                                alert.show()
                                
                                
                                spinningIndicator.hide(true)
                                
                            }
                                
                            else
                            {
                                
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                
                                alert.show()
                                
                                self.collectionView.reloadData()
                                
                                spinningIndicator.hide(true)
                                
                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        
        
    }


    
    
}
