//
//  RegisterViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/2/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate ,UIPopoverControllerDelegate
{
    
    @IBOutlet weak var imageWrapperBtn: UIButton!
    var imageBool = Bool()
    
    var picker:UIImagePickerController?=UIImagePickerController()

    var popover:UIPopoverController?=nil
    
     var sizeHeight = UIScreen.mainScreen().bounds.height
    
    var alertViewImage = UIAlertView()
    
    var alertSuccessFull_regis = UIAlertView()

    
    var imagePicker = UIImagePickerController()
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var rePwdTxtField: UITextField!
    @IBOutlet var pwdTxtField: UITextField!
    @IBOutlet var phoneNoTxtField: UITextField!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var firstNameTxtField: UITextField!
    
    var value = Bool()
    
    var dataModel = NSData()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        alertViewImage.delegate = self
        imagePicker.delegate = self
        
        
        value = true
        
        emailTxtField.delegate = self
        pwdTxtField.delegate = self
        firstNameTxtField.delegate = self
        rePwdTxtField.delegate = self
        phoneNoTxtField.delegate = self
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(animated: Bool)
    {
        
    }
    
    
    @IBAction func verifyBtn(sender: AnyObject)
    {
        
        if firstNameTxtField.text == "" || emailTxtField.text == "" || phoneNoTxtField.text == "" || pwdTxtField.text == "" || rePwdTxtField.text == ""
        {
            var alert = UIAlertView()
            alert = UIAlertView(title:"Alert", message: "Please fill all textFields", delegate: self, cancelButtonTitle:"OK")
            alert.show()
        }
        else if count(pwdTxtField.text) <= 6
        {
            var alert = UIAlertView()
            alert = UIAlertView(title:"Alert", message: "Password length too short", delegate: self, cancelButtonTitle:"OK")
            
            alert.show()
            
            
        }
        else if pwdTxtField.text != rePwdTxtField.text
        {
            var alert = UIAlertView()
            alert = UIAlertView(title:"Alert", message: "Re-Type password not same as password", delegate: self, cancelButtonTitle:"OK")
            alert.show()
            
        }
        else if imageBool != true
            
        {
            var alert = UIAlertView()
            alert = UIAlertView(title:"Alert", message: "Select the Profile Image", delegate: self, cancelButtonTitle:"OK")
            alert.show()
        }
            
        else
        {
            
            var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            spinningIndicator.labelText = "Loading"

            var image:UIImage = imageView.image!
            var imageData = UIImagePNGRepresentation(image)
            var base64String = imageData.base64EncodedStringWithOptions(.allZeros)
            var strImage64 = base64String.stringByReplacingOccurrencesOfString("+", withString: "%2B") as String
            
            var number = UInt32()
            
            number = arc4random() % 1000000
            

            
            
           // println(number)
            var uuid = NSUUID().UUIDString
           // println(uuid)
            
            var post = NSString(format:"FirstName=%@&Email=%@&Password=%@&MobileNo=%@&DeviceId=%@&DeviceType=%@&ProfileImage=%@", firstNameTxtField.text,emailTxtField.text,pwdTxtField.text,phoneNoTxtField.text,uuid,"1",strImage64)
            
            
           //println(post)
            
            dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength = String(dataModel.length)
            
            var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/registration")
            
            var urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = dataModel
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                
                println(dicObj)
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })

                }
                    
                else
                {
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success == false
                        {
                            var dict = dicObj?.valueForKey("message") as! NSDictionary
                            var message = dict.valueForKey("Email") as! String
                             var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        else
                            
                        {
                            var messageSuccessfully = dicObj?.valueForKey("message") as! String
                            
                            
                            self.alertSuccessFull_regis = UIAlertView(title:"Alert", message: messageSuccessfully, delegate: self, cancelButtonTitle:"OK")
                            
                            self.alertSuccessFull_regis.show()
                            
                            spinningIndicator.hide(true)
                            
                        }
                        
                        
                        
                        
                    })
                    
                    
                }
                
                
            })
            task.resume()
        }
    }
    
    
    

    
    
    
        func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
        {
            if alertView == alertSuccessFull_regis
            {
                if buttonIndex == 0
                {
                    let registerDone = storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginViewController
                    
                    self.navigationController?.pushViewController(registerDone, animated: true)
                    
                    //self.navigationController?.popToViewController(registerDone, animated: true)
                }
            }
            
        }
    

    
    
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    @IBAction func imageViewBtn(sender: AnyObject)
    {
        
//        alertViewImage = UIAlertView(title:"Alert", message: "Choose the source", delegate: self, cancelButtonTitle: "Camera", otherButtonTitles: "Photo Gallery")
//        alertViewImage.show()
    
        
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default)
            {
                UIAlertAction in
                self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertActionStyle.Default)
            {
                UIAlertAction in
                self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
            {
                UIAlertAction in
        }
        
        
        
        // Add the actions
        //picker?.delegate = self
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        
        
        // Present the controller
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            popover=UIPopoverController(contentViewController: alert)
            popover!.presentPopoverFromRect(imageWrapperBtn.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
        
    }
    
    
    
//    func openCamera()
//    {
//        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
//        {
//            picker!.sourceType = UIImagePickerControllerSourceType.Camera
//            self .presentViewController(picker, animated: true, completion: nil)
//        }
//    }
    
    
    
    
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            
            picker!.delegate = self
            
          //  picker!.sourceType = UIImagePickerControllerSourceType.Camera
            
          //  picker!.mediaTypes = [kUTTypeImage]
            
            picker!.allowsEditing = true
            
            self.presentViewController(picker!, animated: true, completion: nil)
            
    
        }
        else
        {
           var alert = UIAlertView(title: "", message: "This device doesnot contains camera", delegate: self, cancelButtonTitle: "Ok")
            
            alert.show()
            
            
            
        }
    }
    
    
    
    
    func openGallary()
    {
        
        picker!.delegate = self
        
        picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        
        //picker!.mediaTypes = [kUTTypeImage]
        
        picker!.allowsEditing = true
        
        self.presentViewController(picker!, animated: true, completion: nil)
        
//        
//        picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
//        
//        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
//        {
//            self.presentViewController(picker!, animated: true, completion: nil)
//        }

    }
   
    
    
    
    
//    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
//    {
//        if alertView == alertViewImage
//        {
//            if buttonIndex == 0
//            {
//                if (UIImagePickerController.availableCaptureModesForCameraDevice(UIImagePickerControllerCameraDevice.Rear) != nil)
//                {
//                    imagePicker.allowsEditing = true
//                    imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
//                    imagePicker.cameraCaptureMode = .Photo
//                    
//                    
//                    presentViewController(imagePicker, animated: true, completion: nil)
//                }
//                else
//                {
//                    let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
//                    alertView.show()
//                    
//                }
//                
//            }
//            else
//            {
//                
//                imagePicker.allowsEditing = true
//                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
//                
//                
//                presentViewController(imagePicker, animated: true, completion: nil)
//                
//            }
//            
//        }
//        
//    }
    
    
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject])
    {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            
            imageView.contentMode = .ScaleAspectFit
            imageView.image = pickedImage
            imageBool = true
            self.dismissViewControllerAnimated(false, completion: nil)
            value = false
            
        }
        
        
        
    }
    
    //MARK:- Touch Method
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        emailTxtField.resignFirstResponder()
        pwdTxtField.resignFirstResponder()
        rePwdTxtField.resignFirstResponder()
        phoneNoTxtField.resignFirstResponder()
        firstNameTxtField.resignFirstResponder()
    }
    
    
    
    
    //MARK:- TextField Dlegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        if textField == emailTxtField || textField == phoneNoTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 80
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y -= 50
            }
            
            if sizeHeight == 667
            {
                view.frame.origin.y -= 20
            }
            
        }
            
        else if textField == pwdTxtField || textField == rePwdTxtField
            
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 170
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y -= 150
            }
            
            if sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y -= 130
            }
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad
            {
                
                view.frame.origin.y -= 110
            }

           


        }
            
        else
            
        {
            view.frame.origin.y = view.frame.origin.y
        }
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if textField == emailTxtField || textField == phoneNoTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 80
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y += 50
            }
            
            if sizeHeight == 667
            {
                view.frame.origin.y += 20
            }

            
            
        }
       
        else if textField == pwdTxtField || textField == rePwdTxtField
            
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 170
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y += 150
            }
            
            if sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y += 130
            }
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad
            {
                
                view.frame.origin.y += 110
            }

        }
            
        else
            
        {
            view.frame.origin.y = view.frame.origin.y
        }

        return true
        
    }
    

    
}
