//
//  FullMenuLocationViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


var locationCheck = String()


var locationName = NSUserDefaults.standardUserDefaults()


var currentAnnotation : MKAnnotation!

var name = ""

var locationFilter = String()

class FullMenuLocationViewController: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate,UITableViewDelegate
{
    
    var latStr = String()
    var longStr = String()
    
    var lon:Double!
    var lat:Double!
    
    
     var categorySelectFilter = String()
    
    var imageArray = NSArray()

    var locationNames = [String]()
    
    var locationManager = CLLocationManager()
    
    var location = CLLocationCoordinate2D()
    
    @IBOutlet var mapView: MKMapView!
    
    
    
    @IBOutlet weak var tableViewLocation: UITableView!
    
    
    @IBOutlet weak var blackView: UIView!
    
    
    
    @IBOutlet weak var locationView: UIView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        mapView.delegate = self
        
        
        fullMenuLocationAPI()
        
    }

    
    
    
    @IBAction func cancelTableView(sender: AnyObject)
    {
        blackView.hidden = true
        locationView.hidden = true
    }
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    
    
//    //Return CLLocation Coordinate
//    func getLocationObject(latitude:Double, longitude:Double) -> CLLocationCoordinate2D
//    {
//        return CLLocationCoordinate2D(
//            latitude: latitude,
//            longitude: longitude
//        )
//    }
//    
//    
//    
//    
//    
//    //Create annotation object & return
//    func createAnnotation(latitude:Double, longitude:Double, locationName:String, territory:String) -> MKPointAnnotation
//    {
//        let annotation = MKPointAnnotation()
//        annotation.coordinate = self.getLocationObject(latitude, longitude: longitude)
//        annotation.title = locationName
//        annotation.subtitle = territory
//        return annotation
//    }
//    
//    
//    
//    //Create annotaion for current ship position
//    func createAnnotationForCurrentPosition(location:[String: AnyObject]) -> MKPointAnnotation
//    {
//        let latitude = (location["latitude"] as? Double)!
//        let longitude = (location["longitude"] as? Double)!
//        let name = (location["locationName"] as? String)!
//        let territory = (location["territory"] as? String)!
//        
//        return self.createAnnotation(latitude, longitude: longitude, locationName: name, territory: territory)
//    }
//    
//    
//    
//    
//    //Set region on map view
//    func setRegion(location:[String: AnyObject])
//    {
//        let latitude = (location["latitude"] as? Double)!
//        let longitude = (location["longitude"] as? Double)!
//        let location = self.getLocationObject(latitude, longitude: longitude)
//        
//        //Set zoom span
//        let span = MKCoordinateSpanMake(0.05, 0.05)
//        
//        //Create region on map view & show
//        let region = MKCoordinateRegion(center: location, span: span)
//        self.mapView.setRegion(region, animated: true)
//    }
//    
//    
//    
//    
//    //This function will fire from data manager when current location from API has been updated
//    func currentLocationHasBeenUpdated(location:[String: AnyObject])
//    {
//        dispatch_async(dispatch_get_main_queue()){
//            if currentAnnotation != nil {
//                var currentAnnotationView = self.mapView.viewForAnnotation(currentAnnotation)
//                if currentAnnotationView != nil
//                {
//                    currentAnnotationView.image = nil //this line makes app crash [gave nil to show standard red icon]
//                    // currentAnnotationView.image = UIImage(named:"first") - this line works perfectly with different icon. but i don't want any custom icon except the standard red one
//                }
//            }
//            self.mapView.addAnnotation(self.createAnnotationForCurrentPosition(location))
//            
//            self.setRegion(location)
//        }
//    }
//    
//    
    
    

//    //Update map view by scheduling time if current location is changed yet
//    func updateMapView() {
//        self.dataManager.getCurrentLocation()
//    }
    
    

    
//    
//    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer!
//    {
//        if overlay is MKPolyline
//        {
//            var polylineRenderer = MKPolylineRenderer(overlay: overlay)
//            polylineRenderer.strokeColor = UIColor.blueColor()
//            polylineRenderer.lineWidth = 2
//            return polylineRenderer
//        }
//        
//        return nil
//    }
//    
    
    
    
    
    
//    //Make custom annotaion pin if the annotation is of ship's current position
//    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView!{
//        println("Hello")
//        self.currentAnnotation = annotation
//        
//        //Custom annotation view with custom pin icon
//        let reuseId = "custom"
//        var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
//        
//        if annotationView == nil {
//            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
//            annotationView.canShowCallout = true
//        }
//        else {
//            annotationView.annotation = annotation
//        }
//        
//        //Set annotation specific properties after the view is dequeued or created...
//        annotationView.image = UIImage(named:"pinicon")
//        
//        return annotationView
//    }
//
    
    
    
    
    
    
    
    
    
    
    
    
   
    

    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    
    
    class CustomPointAnnotation:MKPointAnnotation
    {
        var imageName: String!
        
    }

    
    
    
    
    @IBAction func barFilterLocation(sender: AnyObject)
    {
        blackView.hidden = false
        locationView.hidden = false
        
    }
    
    
    
    
    
   
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
        var userLocation:CLLocation = locations[0] as! CLLocation
        
         lon = userLocation.coordinate.longitude;
         lat = userLocation.coordinate.latitude;
        
        
        
        
        longStr = NSString(format: "%f", userLocation.coordinate.longitude) as String
        latStr = NSString(format: "%f", userLocation.coordinate.latitude) as String
        
//        println(longStr)
//        println(latStr)
        
        
         location = CLLocationCoordinate2D(
            latitude: lat, longitude: lon)
        
        
        let regionRadius: CLLocationDistance = 1000
        let span = MKCoordinateSpanMake(0.05, 0.05)
        var region = MKCoordinateRegion(center: location, span: span)
        
        region = MKCoordinateRegionMakeWithDistance(location, regionRadius * 0.2, regionRadius * 0.2)
        
        mapView.setRegion(region, animated: true)
        
        
        
    }
    
    
    
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView!
    {
        
        
        if !(annotation is CustomPointAnnotation)
        {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        
        if anView == nil
        {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView.canShowCallout = true
        }
        else
        {
            anView.annotation = annotation
        }
        
        
        
        var cpa = annotation as! CustomPointAnnotation
        anView.image = UIImage(named:cpa.imageName)
    
        return anView
    }
    
    
    
    
    
    
    
    
//    
//    func getLocation(data:[AnyObject])
//    {
//        if locationNames.count == 0
//        {
//            return
//        }
//        
//        
//        //1
//        // Change the Map Region to the first lon/lat in the array of dictionaries
//        let regionLongitude = data[1]["longitude"]
//        let regionLatitude = data[1]["latitude"]
//        let center:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: (regionLatitude as! NSString).doubleValue , longitude: (regionLongitude as! NSString).doubleValue)
//        let region: MKCoordinateRegion = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 10, longitudeDelta: 10))
//        self.mapView.setRegion(region, animated: true)
//        // Loop through all items and display them on the map
//        var lon:Double!
//        var lat:Double!
//        var annotationView:MKPinAnnotationView!
//        
//        
//        var pointAnnoation:CustomPointAnnotation!
//        
//        
//        //2
//       // for item in locationNames
//            
//        for var i = 0; i < self.locationNames.count ; i++
//        {
//            
////            let obj = item as! Dictionary
////            lon = obj["longitude"]!.doubleValue
////            lat = obj["latitude"]!.doubleValue
//            
//            pointAnnoation = CustomPointAnnotation()
////            
////            let incomeLevel:Dictionary = obj["incomeLevel"] as! Dictionary
////            let incomeLevelValue = (incomeLevel["value"] as! String)
////            
////            if incomeLevelValue == "High income: OECD" || incomeLevelValue == "High income: nonOECD"
////            {
////                pointAnnoation.pinCustomImageName = "High income"
////            }
////            else
////            {
////                pointAnnoation.pinCustomImageName = (incomeLevel["value"] as! String)
////            }
//            
//            
//            //3
//            pointAnnoation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
//            
//            
//            pointAnnoation.title = locationNames[i]
//            
//            //pointAnnoation.title = obj["name"] as? String
//            //pointAnnoation.subtitle = obj["capitalCity"] as? String
//            
//            annotationView = MKPinAnnotationView(annotation: pointAnnoation, reuseIdentifier: "pin")
//            
//            self.mapView.addAnnotation(annotationView.annotation!)
//        }
//    }
    
    
    
    
    
    
    
    
    
    //MARK:- Navigation Barcode Btn
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }
    
    
    
    
    
    
    
    
    //MARK: - CategoryTableViewMethods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return imageArray.count
    }
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var tableViewCell = tableView.dequeueReusableCellWithIdentifier("newEntryCategoryCell", forIndexPath: indexPath) as! NewEntryCategoryTableViewCell
    
        
        
        tableViewCell.filterTitleValue.text = self.locationNames[indexPath.row]
        

        println(tableViewCell.filterTitleValue.text)
        
        
        return tableViewCell
        
    }
    
    
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        categorySelectFilter = self.locationNames[indexPath.row]
        
        println("category id at particular row = \(categorySelectFilter)")
        
        
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDeleagte.viewInventoryStoryboard() as UIStoryboard
        
        let category = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("category") as! FullMenuCategoryViewController
        
        
        
        category.locationFilter = self.locationNames[indexPath.row]
        
        locationCheck = "locationFilter"
        
        self.navigationController?.pushViewController(category, animated: true)

        
        
        //fullMenuCategoryFilterAPI()
        
    }
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //MARK:-Full Menu Location Api
    
    func fullMenuLocationAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i",str)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/location")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                    dispatch_async(dispatch_get_main_queue(), {
                        spinningIndicator.hide(true)
                    })
                    
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                          self.imageArray = dicObj?.valueForKey("locationdata") as! NSArray
                            
                           println("location names = \(self.imageArray)")
                            
                           self.locationNames =  self.imageArray.valueForKey("Location") as! NSArray as! [(String)]

                            
                            println("location names = \(self.locationNames)")
                            
                            
                            self.tableViewLocation.reloadData()
                            
                        //self.getLocation(self.locationNames as [AnyObject])
                            
                            
                            for var i = 0; i < self.locationNames.count ; i++
                            {
                                var info = CustomPointAnnotation()
                                
                               // info.coordinate = CLLocationCoordinate2DMake(self.lat, self.lon)
                                
                                info.title = self.locationNames[i]
                                info.imageName = "ic_marker_.png"
                                
                                self.mapView.addAnnotation(info)
                                
                                
                            }
                            

                            
                            
                            
                        spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    }



