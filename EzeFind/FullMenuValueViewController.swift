//
//  FullMenuValueViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


var minimumValueInApi = Int()
var maximumValueInApi = String()

var maximumDouble = NSString()

var maximumInteger = Int()

var FilterValue_Minimum = Int()
var FilterValue_Maximum = Int()


class FullMenuValueViewController: UIViewController,UITableViewDelegate
{
    var imageArray = NSArray()
    
    var rangeSlider = RangeSlider(frame: CGRectZero)
    
    //let rangeSlider = RangeSlider(frame: 0,140,320,60)
    
    
    
    @IBOutlet weak var navBarView: UIView!
    
    

    @IBOutlet weak var ttRangeSlider: TTRangeSlider!
   
       
    
    var categorySelectFilter = String()
    
    @IBOutlet weak var valueCollectionView: UICollectionView!
    
    
    @IBOutlet weak var blackView: UIView!
    
    
    @IBOutlet weak var valueFilterView: UIView!
    
    @IBOutlet weak var valueCategoryTableView: UITableView!
    
    
    var minVal = Int()
    
    var maxVal = Int()
    
     override func viewDidLoad()
     {
        
        super.viewDidLoad()

//        
//        //view.addSubview(rangeSlider)
//        
//        navBarView.addSubview(rangeSlider)
//        
//        
////        rangeSlider.minimumValue = 10.0
////        rangeSlider.maximumValue = 100.0
//        
//        
//        rangeSlider.addTarget(self, action: "rangeSliderValueChanged:", forControlEvents: .ValueChanged)
//        
//        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC))
//        dispatch_after(time, dispatch_get_main_queue()) {
//            self.rangeSlider.trackHighlightTintColor = UIColor.greenColor()
//            self.rangeSlider.curvaceousness = 0.0
//        }
        
     
//        
//        rangeSlider.maximumValue = 1000
//        rangeSlider.minimumValue = 200
        
        
        
        //standard rsnge slider
        //self.rangeSlider.delegate = self;
//        self.ttRangeSlider.minValue = 0;
//        self.ttRangeSlider.maxValue = 200;
//        self.ttRangeSlider.selectedMinimum = 30;
//        self.ttRangeSlider.selectedMaximum = 150;
//
        
        self.ttRangeSlider.hidden = true
        
        fullMenuValueAPI()
        
    }

    
        
//    override func viewDidLayoutSubviews()
//    {
//        let margin: CGFloat = 20.0
//        let width = view.bounds.width - 2.0 * margin
//        rangeSlider.frame = CGRect(x: margin, y: margin + topLayoutGuide.length,
//            width: width, height: 31.0)
//    }
//    
//    
//    
//    
//    func rangeSliderValueChanged(rangeSlider: RangeSlider)
//    {
//        println("Range slider value changed: (\(rangeSlider.lowerValue) \(rangeSlider.upperValue))")
//    }
//  
    
        
    
    
    
    @IBAction func filterBtn(sender: AnyObject)
    {
//        blackView.hidden = false
//        
//        valueFilterView.hidden = false
//        
//        fullMenuValueAPI()
        
        
        
        
        FilterValue_Minimum = Int(self.ttRangeSlider.selectedMinimum)
        
        println("FilterValue_Minimum = \(FilterValue_Minimum)")
        
        FilterValue_Maximum = Int(self.ttRangeSlider.selectedMaximum)
        
        println("FilterValue_Maximum = \(FilterValue_Maximum)")

     
//        self.ttRangeSlider.selectedMinimum = 20
//        self.ttRangeSlider.selectedMaximum = 400
        

        fullMenuValueFilterSliderAPI()
        
    }
    
    
    
    
    //MARK:-Full Menu Value Filter Slider Api
    
    func fullMenuValueFilterSliderAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&minvalue=%i&maxvalue=%i",str,FilterValue_Minimum,FilterValue_Maximum)
        
        
       // println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/value-filter")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        
//                        if let min: AnyObject = dicObj?.valueForKey("min")
//                        {
//                            minimumValueInApi = Int(min as! NSNumber)
//                            
//                            println("minimum value = \(minimumValueInApi)")
//                        }
//                        
//                        
//                        
//                        
//                        if let max: AnyObject = dicObj?.valueForKey("max")
//                        {
//                            maximumValueInApi = max as! String
//                            
//                            println("maximum value = \(maximumValueInApi)")
//                        }
//                        
//                        maximumDouble = NSString(string: maximumValueInApi)
//                        println("maximum value in Integer = \(maximumDouble.integerValue)")
//                        
//                        
//                        
//                        maximumInteger = maximumDouble.integerValue
//                        println("maximum Integer = \(maximumInteger)")
//                        
//                        
//                        
//                        
//                        self.ttRangeSlider.minValue = Float(minimumValueInApi)
//                        self.ttRangeSlider.maxValue = Float(maximumInteger)
                        self.ttRangeSlider.selectedMinimum = Float(FilterValue_Minimum)
                        self.ttRangeSlider.selectedMaximum = Float(FilterValue_Maximum)
                        
                        
                        
                        
                        
                        
                        //                        if let max: AnyObject = dicObj?.valueForKey("max")
                        //                        {
                        //                            maximumValueInApi = max as! String
                        //                                //Double(max as! NSNumber)
                        //
                        //                            println("maximum value = \(maximumValueInApi)")
                        //                        }
                        //
                        //
                        //                        maximumDouble = NSString(string: maximumValueInApi)
                        //                        println("maximum value in double = \(maximumDouble.doubleValue)")
                        
                        
                        if success
                        {
                            
                            self.imageArray = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
                            
                            self.valueCollectionView.reloadData()
                            
                            self.valueCategoryTableView.reloadData()
                            
                            println(self.imageArray)
                            
                            
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func cancelFilterTableView(sender: AnyObject)
    {
        blackView.hidden = true
        
        valueFilterView.hidden = true
    }
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
       
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imageArray.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        var valueCell = collectionView.dequeueReusableCellWithReuseIdentifier("valueCell", forIndexPath: indexPath) as! FullMenuValueCollectionViewCell
        
    
        //valueCell.valueImageView.image = UIImage(named: imageArray[indexPath.row] as! String)
        
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
        var profileUrl: String = imageArray[indexPath.row].valueForKey("Image") as! String
        
        var url: NSURL = NSURL(string: profileUrl)!
        
        valueCell.valueImageView.sd_setImageWithURL(url, completed: block)
        
        
        
        valueCell.memberTitleLbl.text = imageArray[indexPath.row].valueForKey("Title") as? String
        
        
        valueCell.descriptionLbl.text = imageArray[indexPath.row].valueForKey("Description") as? String
        
        
        

        
        return valueCell
    }
    
    
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDelegate.viewInventoryStoryboard() as UIStoryboard
        
        let packGoPackages = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("packGoPackage") as! PackGoPackagesViewController
        
        typeCategoryCheck = "ValueSectionDidSelect"
        
        packGoPackages.itemId = (imageArray[indexPath.row].valueForKey("ItemId") as? Int)!
        
        // println("id at didSelect = \(itemId)")

        
        self.navigationController?.pushViewController(packGoPackages, animated: true)
        
    }
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        var sizeHeight = UIScreen.mainScreen().bounds.height
        if sizeHeight == 480 || sizeHeight ==  568
        {
            return CGSizeMake(155, 155)
        }
            
        else if sizeHeight ==  667
        {
            return CGSizeMake(182, 182)
        }
        else if sizeHeight ==  736
        {
            return CGSizeMake(200,200)
        }
            
            
        else
        {
            return CGSizeMake(240, 240)
        }
        
    }


    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }
    
    
    
    
    //MARK: - CategoryTableViewMethods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return imageArray.count
    }
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var tableViewCell = tableView.dequeueReusableCellWithIdentifier("newEntryCategoryCell", forIndexPath: indexPath) as! NewEntryCategoryTableViewCell
        

        
        tableViewCell.filterTitleValue.text = imageArray[indexPath.row].valueForKey("Title") as? String
        

        
               return tableViewCell
        
    }
    
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        categorySelectFilter = (imageArray[indexPath.row].valueForKey("Title") as? String)!
        
        println("category id at particular row = \(categorySelectFilter)")
        
        fullMenuValueFilterAPI()
    }
    
    
    

    
    
    
    //MARK:-Full Menu Value Filter Api
    
    func fullMenuValueFilterAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&Category_filter=%@",str,categorySelectFilter)
        
        
        //println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/value-filter")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })

                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
//                            self.imageArray = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
//                            
//                            self.valueCollectionView.reloadData()
//                            
//                            self.blackView.hidden = true
//                            self.valueFilterView.hidden = true
//                            
//                            println("image array from didSelect = \(self.imageArray)")
//                            
                            
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    

    
    
    
    
    
    
    
    
    
    
    
    //MARK:-Full Menu Value Api
    
    func fullMenuValueAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i",str)
        
        
       // println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/value")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                self.ttRangeSlider.hidden = true
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    
                    self.ttRangeSlider.hidden = true
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })

                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        
                        self.ttRangeSlider.hidden = false
                        
                        if let min: AnyObject = dicObj?.valueForKey("min")
                        {
                           minimumValueInApi = Int(min as! NSNumber)
                            
                           println("minimum value = \(minimumValueInApi)")
                        }
                        
                    
                        
                        
                        if let max: AnyObject = dicObj?.valueForKey("max")
                        {
                            maximumValueInApi = max as! String
                        
                            println("maximum value = \(maximumValueInApi)")
                        }
                        
                        maximumDouble = NSString(string: maximumValueInApi)
                        println("maximum value in Integer = \(maximumDouble.integerValue)")
                        

                        
                        maximumInteger = maximumDouble.integerValue
                        println("maximum Integer = \(maximumInteger)")
                        
                        
                        
                        
                        self.ttRangeSlider.minValue = Float(minimumValueInApi)
                        self.ttRangeSlider.maxValue = Float(maximumInteger)
                        self.ttRangeSlider.selectedMinimum = Float(minimumValueInApi)
                        self.ttRangeSlider.selectedMaximum = Float(maximumInteger)
                        

                        
                        
                        
                      
//                        if let max: AnyObject = dicObj?.valueForKey("max")
//                        {
//                            maximumValueInApi = max as! String
//                                //Double(max as! NSNumber)
//                            
//                            println("maximum value = \(maximumValueInApi)")
//                        }
//                        
//                        
//                        maximumDouble = NSString(string: maximumValueInApi)
//                        println("maximum value in double = \(maximumDouble.doubleValue)")
                        
                        
                        if success
                        {
                            
                            self.imageArray = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
                            
                            self.valueCollectionView.reloadData()
                            
                            self.valueCategoryTableView.reloadData()
                            
                            println(self.imageArray)
                            
                            
                        spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    

    
    
    
}
