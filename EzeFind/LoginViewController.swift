//
//  ViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/2/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit



//var userId = 0


class LoginViewController: UIViewController,UITextFieldDelegate
{
    
    var dataModel = NSData()
    
    var sizeHeight = UIScreen.mainScreen().bounds.height
    
    
    @IBOutlet var uncheckBtn: UIButton!
    @IBOutlet var checkBtn: UIButton!
    @IBOutlet var navigationView: UIView!
    
    @IBOutlet var forgotPwd: UIButton!
    
    @IBOutlet var pwdTxtField: UITextField!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var rememberMe: UIButton!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad() 
        
        UIApplication.sharedApplication().statusBarHidden = false
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        emailTxtField.delegate = self
        pwdTxtField.delegate = self
        
    }
    
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    
    @IBAction func registerBtn(sender: AnyObject)
    {
        let register = storyboard?.instantiateViewControllerWithIdentifier("register") as! RegisterViewController
        
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    
    
    
    
    @IBAction func findMeBtn(sender: AnyObject)
    {
        
        if emailTxtField.text == "" || pwdTxtField.text == ""
        {
            var alert = UIAlertView()
            
            alert = UIAlertView(title:"Alert", message: "Please fill all textFields", delegate: self, cancelButtonTitle:"OK")
            
            alert.show()
            
            
        }
        else
        {
            var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            spinningIndicator.labelText = "Loading"
            
            var post = NSString(format:"Email=%@&Password=%@",emailTxtField.text,pwdTxtField.text)
            
            
            dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength = String(dataModel.length)
            
            var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/login")
            
        
            
            var urlRequest = NSMutableURLRequest(URL: url!)
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = dataModel
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        spinningIndicator.hide(true)
                    })
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    
                    
                    println(dicObj)
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        
                            dispatch_async(dispatch_get_main_queue(), {
                                
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data Not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
    
                        spinningIndicator.hide(true)
                                
                        })

                        
                    }
                    else
                    {
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            
                            if success
                            {
                                
                                let furtherDataDic = dicObj?.valueForKey("msg") as! NSDictionary
                                
                                userId = furtherDataDic.valueForKey("UserId") as! Int
                                
                                println("userid = \(userId)")
                                
                               userDefaults = NSUserDefaults.standardUserDefaults()
                                
                                
                                //userDefaults.setValue(userId, forKey: "userId")
                                
                                userDefaults.setInteger(userId, forKey: "userId")
                                
                                userDefaults.synchronize()
                                
                                
                                println("user id in local storage = \(userId)")
                                
                                
                                
                                
                                
                                if rememberBool
                                {
                                    userDefaults.setBool(true, forKey: "login")
                                    
//                                     userId = NSUserDefaults.standardUserDefaults().setValue(userId, forKey: "userId")
//                                    
//                                    println(NSUserDefaults.standardUserDefaults().setValue(userId, forKey: "userId"))
                                    
                                }
                               
                                else
                                {
                                    userDefaults.setBool(false, forKey: "login")
                                }
                                
                                
                               
                                
                                
                    var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
                                
                    var graphStoryboard = appDelegate.grStoryboard() as UIStoryboard
                                
                    let graph = graphStoryboard.instantiateViewControllerWithIdentifier("graph") as! GraphViewController
                                
                    self.navigationController?.pushViewController(graph, animated: true)
                                
                    spinningIndicator.hide(true)
                                
                                
                                
                            }
                            else
                                
                            {
                                var dict = dicObj?.valueForKey("message") as! NSDictionary
                                var message = dict.valueForKey("Account") as! String
                                
                                
                              
                                

                                var alert = UIAlertView()
                                alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                                alert.show()
                                
                                spinningIndicator.hide(true)
                                
                            }
                            
                            
                            
                        })
                        
                    }
                }
                
                
            })
            task.resume()
            
            
        }
        
    }
    
    @IBAction func forgotPwd(sender: AnyObject)
    {
        
        
        let forgot = storyboard?.instantiateViewControllerWithIdentifier("forgotPwd") as! ForgotPwdViewController
        
        self.navigationController?.pushViewController(forgot, animated: true)
        
        
    }
    
    @IBAction func checkBtn(sender: AnyObject)
    {
        rememberBool = false
        checkBtn.hidden = true
        uncheckBtn.hidden = false
    }
    
    
    @IBAction func uncheckBtn(sender: AnyObject)
    {
        rememberBool = true
        checkBtn.hidden = false
        uncheckBtn.hidden = true
    }
    
    //MARK:- Touch Method
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        emailTxtField.resignFirstResponder()
        pwdTxtField.resignFirstResponder()
    }
    
    //MARK:- TextField Dlegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        if textField == emailTxtField || textField == pwdTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 70
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y -= 30
            }
            
            
        }
            
        else
        {
            view.frame.origin.y = view.frame.origin.y
        }
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if textField == emailTxtField || textField == pwdTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 70
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y += 30
            }
            
            
            
        }
        return true
        
    }
    
}

