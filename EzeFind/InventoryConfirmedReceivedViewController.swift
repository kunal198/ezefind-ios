//
//  InventoryConfirmedReceivedViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class InventoryConfirmedReceivedViewController: UIViewController,UIAlertViewDelegate,UICollectionViewDelegate {

    
    @IBOutlet weak var totalValuesView: UIView!
    
    @IBOutlet weak var confirmReceivedBtn: UIButton!
    @IBOutlet weak var collectionVIew: UICollectionView!
    
    @IBOutlet var saveBtn: UIButton!
   
    @IBOutlet weak var boxLbl: UIButton!
    
    @IBOutlet weak var blackView: UIView!
    
    
    @IBOutlet weak var entryDateLbl: UILabel!
    @IBOutlet weak var BagBoxImage: UIImageView!
    
    @IBOutlet weak var descriptionTxtView: UITextView!
    
    @IBOutlet weak var memberNameLbl: UILabel!
    
    @IBOutlet weak var wrapperViewInBlackView: UIView!
    
    @IBOutlet var scrollView: UIScrollView!
    
     var collectionViewHeight = Float()
    var itemInfoDict = NSArray()
    var alertViewSavedSuccess = UIAlertView()
    var packageIdInConfirmReceived = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
       scrollView.contentSize.height = saveBtn.frame.origin.y + saveBtn.frame.size.height+10
        
        blackView.hidden = false
        
    }

    
    
    override func viewWillAppear(animated: Bool)
    {
        
        if scanOnViewInventory == "viewInventoryScan"
        {
            println("scan code in viewInventory view = \(strBagToFurtherScanBarcode)")
            
            receivedBarCodeScanFilterAPI()
        }
        else
        {
            
        }
        
        
    }

    
    
    
    
    //MARK:-Confirm Received BarCodeScan Filter Api
    
    func receivedBarCodeScanFilterAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        println("scan code in viewInventory view = \(strBagToFurtherScanBarcode)")
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&ScanBarcode=%@",str,strBagToFurtherScanBarcode)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/confirm-recieved-scan-barcode")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
                
                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                            var resultDictArray = dicObj?.valueForKey("Userinfo") as! NSDictionary
                           
                            println("result array = \(resultDictArray)")
                            
                            
                            if let strTitle = dicObj?.valueForKey("description") as? String
                            {
                                self.descriptionTxtView.text = strTitle
                            }
                            
                            
                    
                            
                               if let StrPackageID = dicObj?.valueForKey("packagedataid") as? Int
                               {
                                    self.packageIdInConfirmReceived = toString(StrPackageID)
                                    println("package id = \(self.packageIdInConfirmReceived)")
                               }
                            
                            
                            if let strTitle = dicObj?.valueForKey("packagetype") as? String
                            {
                                println("str image = \(strTitle)")
                                
                                if strTitle == "bag"
                                {
                                    self.BagBoxImage.image = UIImage(named: "ic_bag.png")
                                    
                                    self.boxLbl.titleLabel?.font = UIFont.systemFontOfSize(12)
                                    
                                    self.boxLbl.titleLabel!.text = "Bag#01"
                                    
                                }
                                else if strTitle == "box"
                                {
                                    self.BagBoxImage.image = UIImage(named: "ic_box.png")
                                     self.boxLbl.titleLabel?.font = UIFont.systemFontOfSize(12)
                                    self.boxLbl.titleLabel!.text = "Box#01"
 
                                }
                                else if strTitle == "single item"
                                {
                                    self.BagBoxImage.image = UIImage(named: "bg_single_item.png")
                                     self.boxLbl.titleLabel?.font = UIFont.systemFontOfSize(12)
                                    self.boxLbl.titleLabel!.text = "SingleItem#01"

                                }
                                else
                                {
                                    self.BagBoxImage.image = UIImage(named: "bg_bin.png")
                                     self.boxLbl.titleLabel?.font = UIFont.systemFontOfSize(12)
                                    self.boxLbl.titleLabel!.text = "Bin#01"
 
                                }
                            }
                            
                            
                            if let strTitle = resultDictArray.valueForKey("FirstName") as? String
                            {
                                self.memberNameLbl.text = strTitle
                            }
                            
                            
                            if let strTitle = resultDictArray.valueForKey("EntryDate") as? String
                            {
                                self.entryDateLbl.text = strTitle
                            }
                            
                            
                            
                            self.itemInfoDict = dicObj?.valueForKey("iteminfo") as! NSArray
                            println("item info dictionary = \(self.itemInfoDict)")
                            
                            
                            self.collectionVIew.reloadData()
                            
                            spinningIndicator.hide(true)
                            
                        }
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    
    
    
    
    //MARK:- CollectionView Methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        println("count = \(itemInfoDict.count)")
        return itemInfoDict.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! ImagesConfirmReceivedCollectionViewCell
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
        var profileUrl: String = (itemInfoDict[indexPath.row].valueForKey("itemimage") as? String)!
        
        var url: NSURL = NSURL(string: profileUrl)!
        
        println("image url = \(url)")
        
        cell.imagesCollection.sd_setImageWithURL(url, completed: block)

        
        
        
        
        
        
//        var collectionViewWidth = self.view.frame.size.width
//        
//        println("collection view width = \(collectionViewWidth)")
//        
//        collectionViewHeight = Float((self.itemInfoDict.count/2)*166)
//        
//        collectionVIew.contentSize.height = CGFloat(collectionViewHeight)
//        
//        println("collection view Height = \(collectionVIew.contentSize.height)")
//        
//        
//        println(Float(self.itemInfoDict.count/2))
//       
//        
//        if self.itemInfoDict.count%2 == 0
//        {
//            
//            var collectionView_y = totalValuesView.frame.origin.y + totalValuesView.frame.size.height
//            
//            collectionVIew.frame = CGRectMake(0,collectionView_y, self.collectionVIew.frame.size.width, collectionVIew.contentSize.height)
//            
//            println("collection view frame = \(collectionVIew.frame)")
//            
//
//            
//            println("bar scroll view y = \(scrollView.frame.origin.y)")
//            
//
//            
//            saveBtn.frame.origin.y =  collectionVIew.frame.origin.y + collectionVIew.frame.size.height + 10
//            
//            confirmReceivedBtn.frame.origin.y =  collectionVIew.frame.origin.y + collectionVIew.frame.size.height + 10
//            
//            scrollView.contentSize.height =  confirmReceivedBtn.frame.origin.y + confirmReceivedBtn.frame.size.height + 10
//            
//            println("location scroll view y = \(scrollView.contentSize.height)")
//            
//        }
//        else
//        {
//            var collectionView_y = totalValuesView.frame.origin.y + totalValuesView.frame.size.height
//            
//            collectionVIew.frame = CGRectMake(0,collectionView_y, self.collectionVIew.frame.size.width, collectionVIew.contentSize.height)
//
//            
//            
//            saveBtn.frame.origin.y =  collectionVIew.frame.origin.y + collectionVIew.frame.size.height + 10
//            
//            confirmReceivedBtn.frame.origin.y =  collectionVIew.frame.origin.y + collectionVIew.frame.size.height + 10
//            
//            scrollView.contentSize.height =  confirmReceivedBtn.frame.origin.y + confirmReceivedBtn.frame.size.height + 10
//            
//            
//        }
//        
//        
        
        
        
        
        
        
        return cell
        
    }
    
    

    
    

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
  
    
    
    @IBAction func saveBtn(sender: AnyObject) {
        
        println("save btn clicked")
        SaveReceivedBarCodeScanFilterAPI()
    }
    
    
    
    
    //MARK:-Save Confirm Received BarCodeScan Filter Api
    
    func SaveReceivedBarCodeScanFilterAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
    
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&packagedataid=%@",str,packageIdInConfirmReceived)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
      
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/save-recieved-status")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
                
                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            self.alertViewSavedSuccess = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            self.alertViewSavedSuccess.show()
                            
                            spinningIndicator.hide(true)
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alertView == alertViewSavedSuccess
        {
            //packGoAndInventoryView.hidden = true
            var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
            
            var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
            
            let viewInventory = graphStoryboard.instantiateViewControllerWithIdentifier("viewInventory") as! ViewInventoryViewController
            
            self.navigationController?.pushViewController(viewInventory, animated: false)
        }
    }
    
    
    @IBAction func confirmReceivedBtn(sender: AnyObject)
    {
        println("confirm receive btn clicked")
        blackView.hidden = false
    }
    
    
    
    
    @IBAction func OkBtnInWrapperView(sender: AnyObject)
    {
        blackView.hidden = true
        
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let newEntryScanBarcode = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryScanBarcode") as! NewEntryScanBarcodeViewController
        
        //newEntryScanBarcode.greenTitleStr = singleItemGreenTitleStr
        
        self.navigationController?.pushViewController(newEntryScanBarcode, animated: false)

    }
    
    
    //MARK:- Navigation Barcode Btn
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }

}
