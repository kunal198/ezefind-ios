//
//  FullMenuValueCollectionViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class FullMenuValueCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet var valueImageView: UIImageView!
    
    
    @IBOutlet weak var memberTitleLbl: UILabel!
    
    
    @IBOutlet weak var descriptionLbl: UILabel!
    
    
}
