//
//  FullMenuMemberViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class FullMenuMemberViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate
{
    
    
    var imageArray = NSArray()
    
    var categorySelectFilter = String()
    
    @IBOutlet weak var blackView: UIView!
    
    
    
    
    
    @IBOutlet weak var filterCategoryView: UIView!
    
    
    @IBOutlet weak var FilterTableView: UITableView!
    
    @IBOutlet weak var navTitle: UILabel!
    
    @IBOutlet weak var memberCollectionView: UICollectionView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        imageArray = ["img_scan_jacket.png","img_scan_jacket.png","img_scan_jacket.png","img_scan_jacket.png","img_scan_jacket.png"]
//
        
        
        
        if FullMenuCheck == "memberView"
        {
            fullMenuMemberAPI()
            self.navTitle.text = "View:Member"
        }
        else
        {
            fullMenuColorAPI()
            
            self.navTitle.text = "View:Color"

        }
        
    }
    
    
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    
    @IBAction func filterBtn(sender: AnyObject)
    {
        
        println("filter btn clicked")
        
        blackView.hidden = false
        
        filterCategoryView.hidden = false
        
//        
//        if FullMenuCheck == "memberView"
//        {
//            fullMenuMemberAPI()
//            
//        }
//        else
//        {
//            fullMenuColorFilterAPI()
//            
//        }

        
        
        if FullMenuCheck == "memberView"
        {
            fullMenuMemberAPI()
           // self.navTitle.text = "View:Category"
        }
        else
        {
            fullMenuColorAPI()
            
            //self.navTitle.text = "View:Color"
            
        }

    }
    
    
    @IBAction func cancelFilterTableView(sender: AnyObject)
    {
        blackView.hidden = true
        
        filterCategoryView.hidden = true
        
        
    }
    
    

    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
   
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imageArray.count
    }
    
    
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        var memberCell = collectionView.dequeueReusableCellWithReuseIdentifier("memberCell", forIndexPath: indexPath) as! FullMenuMenberCollectionViewCell
        
        
        
//        memberCell.memberImageView.image = UIImage(named: imageArray[indexPath.row] as! String)
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
        var profileUrl: String = imageArray[indexPath.row].valueForKey("Image") as! String
        
        var url: NSURL = NSURL(string: profileUrl)!
        
        memberCell.memberImageView.sd_setImageWithURL(url, completed: block)
        
        
        
        memberCell.titleLbl.text = imageArray[indexPath.row].valueForKey("Title") as? String
        
        
        memberCell.fragileLbl.text = imageArray[indexPath.row].valueForKey("Description") as? String
        
        
        
        return memberCell
    }
    
    
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDelegate.viewInventoryStoryboard() as UIStoryboard
        
        let packGoPackages = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("packGoPackage") as! PackGoPackagesViewController
        
        packGoPackages.itemId = imageArray[indexPath.row].valueForKey("ItemId") as! Int
        
        println("item id in colors = \(packGoPackages.itemId)")
        
        
        self.navigationController?.pushViewController(packGoPackages, animated: true)
        
    }
    
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        var sizeHeight = UIScreen.mainScreen().bounds.height
        if sizeHeight == 480 || sizeHeight ==  568
        {
            return CGSizeMake(155, 155)
        }
            
        else if sizeHeight ==  667
        {
            return CGSizeMake(182, 182)
        }
        else if sizeHeight ==  736
        {
            return CGSizeMake(200,200)
        }
            
            
        else
        {
            return CGSizeMake(240, 240)
        }
        
    }

    
    
    
    
    //MARK: - CategoryTableViewMethods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return imageArray.count
    }
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var tableViewCell = tableView.dequeueReusableCellWithIdentifier("newEntryCategoryCell", forIndexPath: indexPath) as! NewEntryCategoryTableViewCell
        
        
        
        tableViewCell.filterTitleValue.text = imageArray[indexPath.row].valueForKey("Color") as? String
        
        
        return tableViewCell
        
    }
    
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        categorySelectFilter = (imageArray[indexPath.row].valueForKey("Title") as? String)!
        
        println("category id at particular row = \(categorySelectFilter)")
        
        if FullMenuCheck == "memberView"
        {
            fullMenuMemberAPI()
            
        }
        else
        {
            categorySelectFilter = (imageArray[indexPath.row].valueForKey("Color") as? String)!
            
            println("category id at particular row = \(categorySelectFilter)")
            
           fullMenuColorFilterAPI()
            
        }
        


    }

    
    
    
    
    
    
    
    //MARK:-Full Menu Color Filter Api
    
    func fullMenuColorFilterAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&Color_filter=%@",str,categorySelectFilter)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/color")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                    dispatch_async(dispatch_get_main_queue(), {
                        spinningIndicator.hide(true)
                    })
                    
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                            self.imageArray = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
                            
                            
                            self.memberCollectionView.reloadData()
                            
                            self.blackView.hidden = true
                            
                            self.filterCategoryView.hidden = true
                         
                            println("image array from didSelect = \(self.imageArray)")
                            
                            
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    
    

    
    
    
    
    
    
    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodebtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }
    
    
    
    
    
    
    
    //MARK:-Full Menu Color Api
    
    func fullMenuColorAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i",str)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/color")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                    dispatch_async(dispatch_get_main_queue(), {
                        spinningIndicator.hide(true)
                    })
                    
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                                self.imageArray = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
                            
                                self.memberCollectionView.reloadData()
                            
                                self.FilterTableView.reloadData()
                            
                                println(self.imageArray)
                            
                               spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    
    

    
    
    
    
    
    
    //MARK:-Full Menu Member Api
    
    func fullMenuMemberAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i",str)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/members")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                    dispatch_async(dispatch_get_main_queue(), {
                        spinningIndicator.hide(true)
                    })
                    
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                            self.imageArray = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
                            
                            self.memberCollectionView.reloadData()
                            
                            
                            self.FilterTableView.reloadData()
                            
                            println(self.imageArray)
                            
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    
    

    
    
  
}
