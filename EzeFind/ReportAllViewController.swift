//
//  ReportViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

var strBothInventoryPackage = NSString()

class ReportAllViewController: UIViewController
{
    
    var reportAlldict = NSDictionary()
    
    
    
    @IBOutlet weak var totalLabel: UILabel!
    
    
    @IBOutlet var packageBlueBtn: UIButton!
    @IBOutlet var inventoryBlueBtn: UIButton!
    @IBOutlet var bothBlueBtn: UIButton!
    
    @IBOutlet var bothBtn: UIButton!
    @IBOutlet var inventoryBtn: UIButton!
    @IBOutlet var packageBtn: UIButton!
    @IBOutlet var blueView: UIView!
    @IBOutlet var itemValueLbl: UILabel!
    @IBOutlet var categoryValueLbl: UILabel!
    @IBOutlet var locationValueLbl: UILabel!
    @IBOutlet var dateValueLbl: UILabel!
    @IBOutlet var valueLbl: UILabel!
    @IBOutlet var memberValueLbl: UILabel!
    @IBOutlet var totalValueLbl: UILabel!
    
    
   
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var dateBtn: UIButton!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        scrollView.contentSize.height  = dateBtn.frame.origin.y + dateBtn.frame.size.height+10
        
        strBothInventoryPackage = "package"
        
        self.categoryValueLbl.text = ""
        self.dateValueLbl.text  = ""
        self.itemValueLbl.text = ""
        self.locationValueLbl.text = ""
        self.memberValueLbl.text  = ""
        self.valueLbl.text = ""
        self.totalValueLbl.text  = ""
        self.totalLabel.hidden = true
       
        allGraphApi()
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    //MARK:- AllGraph Api
    
    
    func allGraphApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"

        
        
        var data = NSData()
       
        var post = NSString(format: "UserId=%i&main_filter=%@",userId, strBothInventoryPackage)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
       
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/all"//&UserId=\(userId)&main_filter=\(strBothInventoryPackage)"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")

        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
          
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
            
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    println(dictObj)
                    
                    if error != nil
                    {
                        
                         println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        
                        self.categoryValueLbl.text = ""
                        self.dateValueLbl.text  = ""
                        self.itemValueLbl.text = ""
                        self.locationValueLbl.text = ""
                        self.memberValueLbl.text  = ""
                        self.valueLbl.text = ""
                        self.totalValueLbl.text  = ""
                        
                        self.totalLabel.hidden = true
                        
                        spinningIndicator.hide(true)
                        

                    }
                    else
                    {
                        
                        
                        var success = dictObj?.valueForKey("success") as! Bool
                        
                        
                        if success == false
                        {
                            //var dict = dictObj?.valueForKey("success") as! NSDictionary
                            var message = dictObj!.valueForKey("message") as! String
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            
                             self.categoryValueLbl.text = ""
                            self.dateValueLbl.text  = ""
                            self.itemValueLbl.text = ""
                             self.locationValueLbl.text = ""
                             self.memberValueLbl.text  = ""
                            self.valueLbl.text = ""
                            self.totalValueLbl.text  = ""
                            
                            self.totalLabel.hidden = true
                            
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            self.reportAlldict = dictObj?.valueForKey("message") as! NSDictionary
                            
                            self.categoryValueLbl.text = ""
                            
                            
                            self.totalLabel.hidden = false
                            
                            println(self.reportAlldict.allKeys)
                            println(self.reportAlldict.allValues)
                            
                            println(self.reportAlldict.valueForKey("Category_count") as? Int)
                            
//                            
//                            if let straCategory = self.reportAlldict.valueForKey("Category_count") as? Int
//                            {
//                               // self.categoryValueLbl.text = NSString(format: "%i", straCategory) as String
                            
                                self.categoryValueLbl.text = self.reportAlldict.valueForKey("Category_count") as? String
                                
                                
                                println("category count is = \(self.categoryValueLbl.text)")
                           // }
                            
                            self.dateValueLbl.text = ""
                            
                            if let strDate = self.reportAlldict.valueForKey("Day_count") as? Int
                            {
                                println(strDate)
                                self.dateValueLbl.text = NSString(format: "%i", strDate) as String
                            }
                            
                            self.itemValueLbl.text = ""
                            
                            if let strItem = self.reportAlldict.valueForKey("Items_count") as? String
                            {
                                self.itemValueLbl.text = strItem
                                
                                println(self.itemValueLbl.text)
                            }
                            
                            self.locationValueLbl.text = ""
                            
                            if let strLocation = self.reportAlldict.valueForKey("Location_count") as? String
                            {
                                self.locationValueLbl.text = strLocation
                                println(self.locationValueLbl.text)
                            }
                            
                            self.memberValueLbl.text = ""
                            
                            if let strMember = self.reportAlldict.valueForKey("Users_count") as? String
                            {
                                self.memberValueLbl.text = strMember
                            }
                            
                            self.valueLbl.text = ""
                            
                            if let strValue = self.reportAlldict.valueForKey("Value_count") as? String
                            {
                                self.valueLbl.text = strValue
                            }
                            
                            
                            //self.totalValueLbl.text = dictObj?.valueForKey("total") as? String
                            
                            if let total_Items
                                = self.reportAlldict.valueForKey("total") as? Int
                            {
                                self.totalValueLbl.text = NSString(format: "%i", total_Items) as String
                                
                                println("total items are = \(self.totalValueLbl.text)")
                            }
                            
                            
                            
                            spinningIndicator.hide(true)

                        }
                        
                        
                        
                      
                    }
                    
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
        
    }
    
    
    
    
    
    //MARK:- Filter Refresh Btn
    
    @IBAction func filterRefreshBtn(sender: AnyObject)
    {
        allGraphApi()
        blueView.hidden = true
    }
    
    
    
    
    
    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
        
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(GraphViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

        
    }
    
    
    
    
    
    //MARK:- Back Button
    
    @IBAction func backBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(GraphTrackViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

    }
    
    
    
    
    //MARK:- Report's Buttons
    
    @IBAction func itemsBtn(sender: AnyObject)
    {
        
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportItem  = reportStoryboard.instantiateViewControllerWithIdentifier("reportItem") as! ReportItemViewController
        
        self.navigationController?.pushViewController(reportItem, animated: false)

        
    }
    
    
    @IBAction func membersBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportMember  = reportStoryboard.instantiateViewControllerWithIdentifier("reportMember") as! ReportMemberViewController
        
        self.navigationController?.pushViewController(reportMember, animated: false)
    }
    
    
    @IBAction func categoryBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportCategory  = reportStoryboard.instantiateViewControllerWithIdentifier("reportCategory") as! ReportCategoryViewController
        
        self.navigationController?.pushViewController(reportCategory, animated: false)

        
        
    }
    
    @IBAction func ValueBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportValue  = reportStoryboard.instantiateViewControllerWithIdentifier("reportValue") as! ReportValueViewController
        
        self.navigationController?.pushViewController(reportValue, animated: false)

        
    }
    
    @IBAction func locationBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportLocation  = reportStoryboard.instantiateViewControllerWithIdentifier("reportLocation") as! ReportLocaionViewController
        
        self.navigationController?.pushViewController(reportLocation, animated: false)

        
    }
    
    @IBAction func dateBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportDate  = reportStoryboard.instantiateViewControllerWithIdentifier("reportDate") as! ReportDateViewController
        
        self.navigationController?.pushViewController(reportDate, animated: false)

    }
    
    
    
    
    //MARK:- Report Value's Buttons
    
    @IBAction func itemValueBtn(sender: AnyObject)
    {
        totalValueLbl.text = itemValueLbl.text
    }
    
    
    
    @IBAction func memberValueBtn(sender: AnyObject)
    {
        totalValueLbl.text = memberValueLbl.text
    }
    
    
    
    @IBAction func categoryValueBtn(sender: AnyObject)
    {
        totalValueLbl.text = categoryValueLbl.text
    }
    
    
    
    @IBAction func valueBtn(sender: AnyObject)
    {
        totalValueLbl.text = valueLbl.text
    }
    
    
    @IBAction func locationValueBtn(sender: AnyObject)
    {
        totalValueLbl.text = locationValueLbl.text
    }
    
    
    @IBAction func dateValueBtn(sender: AnyObject)
    {
        totalValueLbl.text = dateValueLbl.text
    }
    
    
    
    //MARK:- Filter Button
    
    @IBAction func filterBtn(sender: AnyObject)
    {
        blueView.hidden = false
    }
    
    
    @IBAction func crossBtn(sender: AnyObject)
    {
         blueView.hidden = true
        
    }
    
    @IBAction func packageBtn(sender: AnyObject)
    {
        
        strBothInventoryPackage = "package"
        packageBtn.hidden = true
        packageBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        inventoryBtn.hidden = false
         bothBtn.hidden = false
    }
   
    
    @IBAction func packageBlueBtn(sender: AnyObject)
    {
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        
    }
    
    @IBAction func inventoryBtn(sender: AnyObject)
    {
        
        strBothInventoryPackage = "inventory"
        inventoryBlueBtn.hidden = false
        inventoryBtn.hidden = true
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        bothBtn.hidden = false
        
    }
    
    
    
    @IBAction func inventoryBlueBtn(sender: AnyObject)
    {
        inventoryBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
    }
    
    
    
    
    @IBAction func bothBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "both"
        bothBtn.hidden = true
        bothBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBtn.hidden = false
    }
    
    
    
    @IBAction func bothBlueBtn(sender: AnyObject)
    {
        bothBtn.hidden = false
        bothBlueBtn.hidden = true
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true

    }
    
    
    
}


