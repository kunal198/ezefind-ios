//
//  ViewInventorySearchViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ViewInventorySearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{

    @IBOutlet var searchTextField: UITextField!
    
    
    @IBOutlet weak var searchTableView: UITableView!
    
    
    var searchTxt = ""
    //let prods = ["abc", "terra", "ar", "fogo"]
    
    
    var TitleItemArray = [String]()
    
    var searchResults:AnyObject? = [AnyObject]()
    
    
    var isFiltered : Bool!
    
    
     var searchArray = [NSDictionary]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
        let paddingViewSearch = UIView(frame: CGRectMake(0, 0, 5,searchTextField.frame.size.height))
        searchTextField.leftView = paddingViewSearch
        searchTextField.leftViewMode = UITextFieldViewMode.Always
        searchTextField.delegate = self
        
        
        
        println("search text = \(searchTextStr)")
        
        searchViewInventoryApi()

    }

    
    
    
    //MARK:- Search View Inventory Api
    
    func searchViewInventoryApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&searchtitle=%@",str,searchTextStr)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/search")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj!)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })
                    
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                            self.searchArray = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
                            
                            self.searchTableView.reloadData()

                            println(self.searchArray)
                            
                            
                            for var i = 0; i < self.searchArray.count; i++
                            {
                                if let titleName = self.searchArray[i].valueForKey("Title") as? String
                                {
                                    self.TitleItemArray.append(titleName)
                                    //self.TitleItemArray.addObject(titleName)
                                }
                            }
                            
                            println("title array values are = \(self.TitleItemArray)")
                            println("title array count is = \(self.TitleItemArray.count)")

                            
//                            for var i = 0; i < self.dataPackageOrInventoryArray.count; i++
//                            {
//                                self.arryBool.append(false)
//                                
//                            }
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    

    
    
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func bacvkBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
   func numberOfSectionsInTableView(tableView: UITableView) -> Int
   {
    return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return searchArray.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! ViewInventorySearchViewTableViewCell
        

       cell.titleLbl.text = searchArray[indexPath.row].valueForKey("Title") as? String
        
        
        
        cell.descriptionView.text = searchArray[indexPath.row].valueForKey("Description") as? String

        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
        var profileUrl: String = searchArray[indexPath.row].valueForKey("Image") as! String
        
        var url: NSURL = NSURL(string: profileUrl)!
        
        cell.itemImageView.sd_setImageWithURL(url, completed: block)
        
        
        if let title = searchArray[indexPath.row].valueForKey("Quantity") as? String
        {
            cell.quantityLbl.text = title
        }
        
        
        
        return cell
        
    }
    
    
    
    
    
    //MARK:-Navigation Barcode Button
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }
    
    
    
    
    //MARK:- Touch Method
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        searchTextField.resignFirstResponder()
        
    }
    
    
    
    
    
    //MARK:- TextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        searchResults = TitleItemArray.filter({(coisas:String) -> Bool in
                    let stringMatch = coisas.rangeOfString(self.searchTextField.text)
                    return stringMatch != nil
        
                })
        
        println("search results array = \(searchResults)")
        
        
//        for var i = 0; i < searchResults?.count; i++
//        {
//            searchArray.append(searchResults[i]) as [AnyObject]
//        }
        
        searchTableView.reloadData()
        
        textField.resignFirstResponder()
        
        return true
    }
    
    
    
    
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        
        if searchTextField.text != ""
        {
            searchTxt = searchTextField.text
            
            println(searchTxt)
            
            
            searchResults = TitleItemArray.filter({(produtos:String) -> Bool in
            
            let nameMatch = produtos.rangeOfString(self.searchTxt, options: NSStringCompareOptions.CaseInsensitiveSearch)
            
            println(nameMatch)
            
            return nameMatch != nil})
            
        }
    }
    
    
    
    
    
    
    
//    func textFieldDidEndEditing(textField: UITextField)
//    {
//        
//        if searchTextField.text != ""
//        {
//            searchTxt = searchTextField.text
//            
//            println(searchTxt)
//            
//            
//            //            searchResults = namesArray.filter({(produtos:String) -> Bool in
//            //
//            //                let nameMatch = produtos.rangeOfString(self.searchTxt, options: NSStringCompareOptions.CaseInsensitiveSearch)
//            //
//            //                println(nameMatch)
//            //
//            //                return nameMatch != nil})
//            
//            isFiltered = true
//            
//            
//            let resultPredicate = NSPredicate(format: "name contains[c] %@", searchTxt)
//            searchResults = TitleItemArray.filteredArrayUsingPredicate(resultPredicate)
//
//            
//             println("search resultss = \(searchResults)")
//            
//            //searchTableView.reloadData()
//            
//        }
//        else
//        {
//            
//            //searchTableView.reloadData()
//        }
//        
//    }

    

    
}
