//
//  ForgotPwdViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/2/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ForgotPwdViewController: UIViewController,UITextFieldDelegate
{
     var dataModel = NSData()
    
     var sizeHeight = UIScreen.mainScreen().bounds.height
    
    @IBOutlet var emailTxtField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        emailTxtField.delegate = self

       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    

    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
  
    @IBAction func submitBtn(sender: AnyObject)
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"

        var post = NSString(format:"Email=%@",emailTxtField.text)
        
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/password-reset")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            var error:NSError?
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
            
            
            println("dic result = \(dicObj)!")
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
               
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })


                
            }
                
            else
            {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    
                    
//                    {
//                        "success": false,
//                        "message": {
//                            "Email": "There is no user registered with such email."
//                        }
//                    }
                
                    
                    
                    
                    
//                    
//                    {
//                        "status": true,
//                        "message": "An Email has been sent to your email id."
//                    }
//                    
                    
                    
                    
                    
                    
                    var success = dicObj?.valueForKey("success") as! Bool
                    
                    println("succcess = \(success)")
                    
                    if success == false
                    {
                        var dict = dicObj?.valueForKey("message") as! NSDictionary
                        var message = dict.valueForKey("Email") as! String
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        spinningIndicator.hide(true)

                    }
                    else
                        
                    {
                        var messageSuccessfully = dicObj?.valueForKey("message") as! String
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: messageSuccessfully, delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        spinningIndicator.hide(true)

                    }

                    
                    
                    
                })
                
                
            }
            
            
        })
        task.resume()
        

        
    }
    
    
    
    
    
    
    //MARK:- Touch Method
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        emailTxtField.resignFirstResponder()
       
    }
    
    
    
    
    
    
    
    
    
    //MARK:- TextField Dlegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        if textField == emailTxtField 
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 50
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y -= 10
            }
            
            
        }
            
        else
        {
            view.frame.origin.y = view.frame.origin.y
        }
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if textField == emailTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 50
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y += 10
            }
            
            
            
        }
        return true
        
    }

    

    
}
