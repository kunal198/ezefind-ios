//
//  ImemberViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


var iMemberDetailResult = NSDictionary()

class ImemberViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    var iMemberNameArray = NSArray()
    
    
    
    @IBOutlet weak var iMember_tableView: UITableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        iMemberNameArray = []
        IMemberApi()
    }

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    

    
    //MARK:- Imember Api
    
    func IMemberApi()
    {
        
        var spinningIndicator  = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i", str)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/get-members"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
            else
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    println("IMember = \(dictObj)!")
                    
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                        
                    }
                    else
                    {
                        var success = dictObj?.valueForKey("success") as! Bool
                        //var message = dictObj?.valueForKey("MemberId") as! String
                        // var memberId = dictObj?.valueForKey("MemberId") as! Int
                        // var name = dictObj?.objectForKey("Name") as! String
                        
                        if !success
                        {
                            
                            var alert = UIAlertView(title: "", message: "No Data Found", delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            
                            println("success")
                            
                            self.iMemberNameArray = dictObj?.valueForKey("message") as! NSMutableArray
                            self.iMember_tableView.reloadData()
                          
                            println( self.iMemberNameArray)
                            spinningIndicator.hide(true)
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }
    

    
    
    
    
    @IBAction func iMemberBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return iMemberNameArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("imember", forIndexPath: indexPath) as! SettingsTableViewCell
        
        
        cell.iMemberName.text = ""
        if let strName = iMemberNameArray[indexPath.row].valueForKey("Name") as? String
        {
            cell.iMemberName.text = NSString(format: "NAME: %@", strName) as String
        }

        
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
       var profileUrl: String = iMemberNameArray[indexPath.row].valueForKey("ProfileImage") as! String
        
      var url: NSURL = NSURL(string: profileUrl)!
        
      cell.iMemberImage.sd_setImageWithURL(url, completed: block)
        
        

        
        
        
        
        
        
        

        
       println(iMemberNameArray[indexPath.row].valueForKey("Name"))
//
       cell.iMemberName.text = iMemberNameArray[indexPath.row].valueForKey("Name") as? String
        
//        println("\(iMemberNameArray[indexPath.row].valueForKey("Name"))
        
        return cell
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        iMemberDetailResult = iMemberNameArray[indexPath.row] as! NSDictionary
        
        println(iMemberNameArray[indexPath.row])
        
        println(iMemberDetailResult)
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        let iMemberDetail = graphStoryboard.instantiateViewControllerWithIdentifier("iMemberDetail") as! IMemberDetailViewController
        
        self.navigationController?.pushViewController(iMemberDetail, animated: true)
        
    }
    
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
    }
    
  
     //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

    }
    
    
    
}
