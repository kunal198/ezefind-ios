//
//  SettingsTableViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/2/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell
{

    @IBOutlet var iMemberName: UILabel!
    @IBOutlet var iMemberImage: UIImageView!
    @IBOutlet var valueNameLbl: UILabel!
    @IBOutlet var fieldNameLbl: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
