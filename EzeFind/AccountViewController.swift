
//
//  AccountViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {
    
    @IBOutlet var userNameViewLine: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func passwordBtn(sender: AnyObject)
    {
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let changePwd = graphStoryboard.instantiateViewControllerWithIdentifier("changePwd") as! ChangePwdViewController
        
        self.navigationController?.pushViewController(changePwd, animated: true)
        
    }
    
    
    
    @IBAction func usernameBtn(sender: AnyObject)
    {
        
        println("username btn clicked")
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let userName = graphStoryboard.instantiateViewControllerWithIdentifier("userName") as! UserNameViewController
        
        self.navigationController?.pushViewController(userName, animated: true)
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    //MARK:- Navigation Barcode Button
    
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

    }
    
    
}
