//
//  PickListViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class PickListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate
{
    
    
    var itemID = Int()
    var arryBool = [Bool]()
     var indexPath = NSIndexPath()
    var strPickListId = NSString()
    var dataPackageOrInventoryArray = [NSDictionary]()
    @IBOutlet var pickListTableView: UITableView!
    
    
    var title_picklist = String()
    var type_picklist = String()
    var id_picklist = String()
    
    var alertViewPicklist = UIAlertView()
    var alertAddItemToPicklist = UIAlertView()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        if  picklistView == "pickListView"
        {
            fullMenuPickListAPI()
        }
        else if  picklistView == "pickListViewSetting"
        {
            pickListTableView.backgroundColor = UIColor(patternImage: UIImage(named: "bg_2.png")!)
            
            getPickListApi()
            
            println("item id = \(itemID_toPicklist)")
        }
        else
        {
            
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    //MARK:-Full Menu PickList Api
    
    func fullMenuPickListAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i",str)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/get-picklist")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                    })
                    
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                            self.dataPackageOrInventoryArray = dicObj?.valueForKey("message") as! NSArray as! [(NSDictionary)]
                            
                            println("picklist view inventory = \(self.dataPackageOrInventoryArray)")
                            
                            
                            
                            self.pickListTableView.reloadData()
                            
                            println(self.dataPackageOrInventoryArray)
                            
                            
                            for var i = 0; i < self.dataPackageOrInventoryArray.count; i++
                            {
                                self.arryBool.append(false)
                                
                            }

                            
                            
//                            self.locationNames =  self.imageArray.valueForKey("Location") as! NSArray as! [(String)]
//                            
//                            
//                            println("location names = \(self.locationNames)")
//                            
//                            
//                            self.tableViewLocation.reloadData()
                            
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    

    
    
    
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
   
    

    
    @IBAction func logoutBtn(sender: AnyObject)
    {
        
        var loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let login = loginStoryboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        
        self.navigationController?.pushViewController(login, animated: false)
        
        
        
    }
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func startNewBtn(sender: AnyObject)
        
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var settingToFurtherStoryboard = appDelegate.settingStoryboard() as UIStoryboard
        
        let startNewPickList = settingToFurtherStoryboard.instantiateViewControllerWithIdentifier("startNewPickList") as! StartNewPickListViewController
        
        self.navigationController?.pushViewController(startNewPickList, animated: true)
        
        
    }
    
    
    //MARK:- TableView Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dataPackageOrInventoryArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! PickListTableViewCell
        cell.cellbtn.tag = indexPath.row
        cell.deleteBtn.tag = indexPath.row
        cell.shareImage.tag = indexPath.row

        
        if arryBool[indexPath.row] == false
        {
            
            cell.cellbtn.hidden = false
            cell.addImage.image = UIImage(named: "img_shirt.png")
            cell.shareImage.image = UIImage(named: "img_shirt.png")
            cell.deleteImage.image = UIImage(named: "img_shirt.png")
            
            
        }
        

        
        
        
        
        
        
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad
        {
            cell.backgroundColor = UIColor(red: 32/255, green: 73/255, blue: 117/255, alpha: 1.0)
            
        }
        
        cell.titleLbl.text = ""
        
        if let strTitle = dataPackageOrInventoryArray[indexPath.row].valueForKey("Title") as? String
        {
            cell.titleLbl.text = strTitle
        }
        
        cell.countLbl.text = ""
        
        if let strCount = dataPackageOrInventoryArray[indexPath.row].valueForKey("PickListItemsCount") as? Int
        {
            cell.countLbl.text = NSString(format: "%i", strCount) as String
        }
        
        cell.startDateLbl.text = ""
        
        if let strStartDate = dataPackageOrInventoryArray[indexPath.row].valueForKey("StartDate") as? String
        {
            cell.startDateLbl.text = NSString(format: "Start Date:%@", strStartDate) as String
        }
        
        var arrayImages = NSArray()
        
        arrayImages = []
        if var array = dataPackageOrInventoryArray[indexPath.row].valueForKey("PicklistItemImages")  as? NSArray
        {
            arrayImages = array
            println(arrayImages)
        
        
        
        
        for var i = 0 ; i < arrayImages.count ; i++
        {
            if i == 0
            {
//                if var urlString  =  arrayImages[0].valueForKey("Image") as? String, imageurl = NSURL(string: NSString(format: "http://beta.brstdev.com/yiiezefind%@", urlString) as String)
//                {
//                    if let data = NSData(contentsOfURL: imageurl)
//                    {
//                        cell.addImage.contentMode = UIViewContentMode.ScaleAspectFit
//                        cell.addImage.image = UIImage(data: data)
//                    }
//                }
                
                
                
                
               let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                    //			println(self)
                }
                
      
                
                
               var profileUrl: String = arrayImages[0].valueForKey("Image") as! String
        
               var url: NSURL = NSURL(string: profileUrl)!
                
               cell.addImage.sd_setImageWithURL(url, completed: block)

                 cell.addImage.contentMode = UIViewContentMode.ScaleAspectFit

                
            }
                
            else if i == 1
            {
//                if var urlString  =  arrayImages[1].valueForKey("Image") as? String, imageurl = NSURL(string: NSString(format: "http://beta.brstdev.com/yiiezefind%@", urlString) as String)
//                {
//                    if let data = NSData(contentsOfURL: imageurl)
//                    {
//                        cell.shareImage.contentMode = UIViewContentMode.ScaleAspectFit
//                        cell.shareImage.image = UIImage(data: data)
//                    }
//                }
                
                
                
                
                let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                    //			println(self)
                }
                
                
                
                
                var profileUrl: String = arrayImages[1].valueForKey("Image") as! String
                
                var url: NSURL = NSURL(string: profileUrl)!
                
                cell.shareImage.sd_setImageWithURL(url, completed: block)
                
                cell.shareImage.contentMode = UIViewContentMode.ScaleAspectFit
                
                
                
                
                
            }
                
            else
            {
//                if var urlString  =  arrayImages[2].valueForKey("Image") as? String, imageurl = NSURL(string: NSString(format: "http://beta.brstdev.com/yiiezefind%@", urlString) as String)
//                {
//                    if let data = NSData(contentsOfURL: imageurl)
//                    {
//                        cell.deleteImage.contentMode = UIViewContentMode.ScaleAspectFit
//                        cell.deleteImage.image = UIImage(data: data)
//                    }
//                }
//                
                
                
                
                let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                    //			println(self)
                }
                
                
                
                
                var profileUrl: String = arrayImages[2].valueForKey("Image") as! String
                
                var url: NSURL = NSURL(string: profileUrl)!
                
                cell.deleteImage.sd_setImageWithURL(url, completed: block)
                
                cell.deleteImage.contentMode = UIViewContentMode.ScaleAspectFit

                
                
            }
            
        }
        }

        
        if arryBool[indexPath.row] == true
        {
            cell.cellbtn.hidden = true
            cell.addImage.image = UIImage(named: "img_add.png")
            cell.shareImage.image = UIImage(named: "img_share.png")
            cell.deleteImage.image = UIImage(named: "img_delete.png")
            
            
        }
        

        
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var settingToFurtherStoryboard = appDelegate.settingStoryboard() as UIStoryboard
        
        
        let pickListViewTitle = settingToFurtherStoryboard.instantiateViewControllerWithIdentifier("pickListViewTitle") as! PickListViewTitleViewController
        
        pickListViewTitle.pickListID = dataPackageOrInventoryArray[indexPath.row].valueForKey("PicklistId") as! String
        
        
        println("picklist didSelect id  is = \(pickListViewTitle.pickListID)")
        
        self.navigationController?.pushViewController(pickListViewTitle, animated: true)
    }
    
    
    
    //MARK:- Cell Button
    
    @IBAction func cellBtn(sender: AnyObject)
    {
         indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        var cell = pickListTableView.cellForRowAtIndexPath(indexPath) as! PickListTableViewCell
        
        if arryBool[indexPath.row] == false
        {

        
        cell.cellbtn.hidden = true
            
        cell.addImage.image = UIImage(named: "img_add.png")
        
        cell.shareImage.image = UIImage(named: "img_share.png")
        
        cell.deleteImage.image = UIImage(named: "img_delete.png")
            arryBool[indexPath.row] = true
    
        }
    }
    
    
    @IBAction func shareBtn(sender: AnyObject)
    {
        
        
        
 //       let objectsToShare = "Hi, I like to invite you to “\(eventTitle)”. You can respond to this by clicking on this link. \(urlString)"
        
//        let activityVC = UIActivityViewController(activityItems:[objectsToShare] , applicationActivities: nil)
//        
//        self.navigationController!.presentViewController(activityVC,
//            animated: true,
//            completion: nil)
        
        
        
        
//        
//        indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
//        
//        var cell = pickListTableView.cellForRowAtIndexPath(indexPath) as! PickListTableViewCell
        

        title_picklist = self.dataPackageOrInventoryArray[indexPath.row]["Title"] as! String
        println("title at index 0 = \(title_picklist)")
        
        
        
//        type_picklist = self.dataPackageOrInventoryArray[indexPath.row]["Type"] as! String
//        println("type at index 0 = \(type_picklist)")
        
        id_picklist = self.dataPackageOrInventoryArray[indexPath.row]["PicklistId"] as! String
        println("id at index 0 = \(id_picklist)")
        
    
        
        
        
        //let firstActivityItem = "my text"
        
        let activityViewController : UIActivityViewController = UIActivityViewController(activityItems: [title_picklist, id_picklist], applicationActivities: nil)
        
        self.navigationController!.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    
    
    @IBAction func addBtn(sender: AnyObject)
    {
        
         println("item id = \( itemID_toPicklist)")
        
        addAPI()
        
    }
    
    
    
    
    
    func addAPI()
    {
        if  picklistView == "pickListViewSetting"
        {
             alertViewPicklist = UIAlertView(title: "Alert", message: "You cannot add package/inventory directly in Picklist.", delegate: self, cancelButtonTitle: "OK")
        
             alertViewPicklist.show()
        }
     else
    {
        var spinningIndicator  = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        spinningIndicator.labelText = "Loading"
        
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let strUserId = userDefaults.valueForKey("userId") as! Int
        
        strPickListId = ""
        
        if var strpickId = dataPackageOrInventoryArray[indexPath.row].valueForKey("PicklistId") as? String
        {
            strPickListId = NSString(format: "%@", strpickId)
            
            println(strPickListId)
        }
        
        
        println(strUserId)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PicklistId=%@&ItemId=%i&Type=%@",strUserId,strPickListId,itemID_toPicklist,type_ToPicklist)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/picklists/addpicklist-items")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj  = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })
                        
                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                           
                            
                            if success
                            {
                                self.alertAddItemToPicklist = UIAlertView(title: "Alert", message: "Item added successfully", delegate: self, cancelButtonTitle: "OK")
                                
                                self.alertAddItemToPicklist.show()
                                
                                self.dataPackageOrInventoryArray.removeAtIndex(self.indexPath.row)
                                
                                self.pickListTableView.reloadData()
                                
                                spinningIndicator.hide(true)
                            }
                            else
                            {
                                
                                 var message = dicObj?.valueForKey("message") as! String
                                
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                spinningIndicator.hide(true)
                            }
                            
                        })
                        
                    }
                    
                }
                
            }
        })
        task.resume()
        
        
    }

}
    
    
    
    //MARK: - AlertVIew delegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alertView == alertAddItemToPicklist
        {
            if buttonIndex == 0
            {
                println("ok btn pressed")
                
                getPickListApi()
            }
        }
    }
    
    
    
    
    @IBAction func deleteBtn(sender: AnyObject)
    {
        var spinningIndicator  = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"

        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let strUserId = userDefaults.valueForKey("userId") as! Int
        
        strPickListId = ""
        
        if var strpickId = dataPackageOrInventoryArray[sender.tag].valueForKey("PicklistId") as? String
        {
            strPickListId = NSString(format: "%@", strpickId)
            
            println(strPickListId)
        }

        
        println(strUserId)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PicklistId=%@",strUserId,strPickListId)
        
        println(post)
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/picklists/delete-picklist")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {

            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj  = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                println(dicObj)
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                    dispatch_async(dispatch_get_main_queue(), {
                        spinningIndicator.hide(true)
                    })

                    
                }
                else
                {

                
                dispatch_async(dispatch_get_main_queue(), {
                    
               
                var success = dicObj?.valueForKey("success") as! Bool
                var message = dicObj?.valueForKey("message") as! String
                
                if success
                {
                    var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                    self.dataPackageOrInventoryArray.removeAtIndex(sender.tag)
                    self.pickListTableView.reloadData()
                    spinningIndicator.hide(true)
                }
                else
                    
                {
                    var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                    spinningIndicator.hide(true)
                }
                    
                 })
                    
                }
                
            }
            
            }
        })
        task.resume()

        
    }
    
    //MARK:- Navigation Barcode button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

    }
    
    
    
    //MARK:- Get Pick List Api
    func getPickListApi()
    {
        var spinningIndicator  = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        println(strInventoryOrPackageTitle)
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i",str)
        
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/picklists/get-picklist")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
            {
                
                var error:NSError?
                
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                   
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })


                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                        
                        
                        self.dataPackageOrInventoryArray = dicObj?.valueForKey("message") as! [NSDictionary]
                            
                            
                        // var title_picklist = self.dataPackageOrInventoryArray[]
                            
                            
//                        var title_pickList = self.dataPackageOrInventoryArray.valueForKey("Title") as! String
////                          
//                            if var title_picklist = (((self.dataPackageOrInventoryArray as? NSArray)?[0] as? NSDictionary)?["Title"] as? NSDictionary)
//                            {
//                               println("title = \(title_picklist)")
//                            }
                            
                        var title = self.dataPackageOrInventoryArray[0]["Title"] as! String
                         println("title at index 0 = \(title)")
                           
                            
                            
                        var type = self.dataPackageOrInventoryArray[0]["Type"] as! String
                        println("type at index 0 = \(type)")
                            
                        var id = self.dataPackageOrInventoryArray[0]["PicklistId"] as! String
                        println("id at index 0 = \(id)")
   
                            
    
                        self.pickListTableView.reloadData()
                        
                        println(self.dataPackageOrInventoryArray)
                        
                        
                        for var i = 0; i < self.dataPackageOrInventoryArray.count; i++
                        {
                            self.arryBool.append(false)
                            
                        }
                            spinningIndicator.hide(true)
                            
                        }
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                            
                        }
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }

    
    
}
