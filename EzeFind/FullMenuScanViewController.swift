//
//  FullMenuScanViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class FullMenuScanViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate
{

    
    @IBOutlet weak var blackView: UIView!
    
    
    @IBOutlet weak var ezeFindBarCodeView: UIView!
    
    @IBOutlet weak var scanFilterCollectionView: UICollectionView!
    
    var imageArray = NSArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        self.blackView.hidden = false
        
        self.ezeFindBarCodeView.hidden = false
      
        //imageArray = ["img_scan_jacket.png","img_scan_jacket.png","img_scan_jacket.png","img_scan_jacket.png","img_scan_jacket.png"]
        
    }

    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    override func viewWillAppear(animated: Bool)
    {
        
        if scanOnViewInventory == "viewInventoryScan"
        {
            println("scan code in viewInventory view = \(strBagToFurtherScanBarcode)")
            
            
            fullMenuBarCodeScanFilterAPI()
        }

        
    }
    
    
    
    
    
    //MARK:-Full Menu BarCodeScan Filter Api
    
    func fullMenuBarCodeScanFilterAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        println("scan code in viewInventory view = \(strBagToFurtherScanBarcode)")
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&Barcode=%@",str,strBagToFurtherScanBarcode)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/scan-barcode")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                            self.imageArray = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
                           
                           self.scanFilterCollectionView.reloadData()
//                            
//                            self.blackView.hidden = true
//                            self.categoryView.hidden = true
//                            
//                            println("image array from didSelect = \(self.imageArray)")
                            
                            
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            
                            alert.show()
                            
                            self.scanFilterCollectionView.reloadData()
                            
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imageArray.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        var scanCell = collectionView.dequeueReusableCellWithReuseIdentifier("scanCell", forIndexPath: indexPath) as! FullMenuScanCollectionViewCell
        
       
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
        
        
//        var profileUrl: String = self.reportAlldict.valueForKey("ProfileImage") as! String
        
        
        var profileUrl: String = imageArray[indexPath.row].valueForKey("Image") as! String
        
        var url: NSURL = NSURL(string: profileUrl)!
        
        
         scanCell.scanImageView.sd_setImageWithURL(url, completed: block)
        
        scanCell.titleLabel.text = imageArray[indexPath.row].valueForKey("Title") as! String

        
//        scanCell.scanImageView.image = UIImage(named: imageArray[indexPath.row] as! String)
        
        
        
        
        
        return scanCell
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDelegate.viewInventoryStoryboard() as UIStoryboard
        
        let packGoPackages = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("packGoPackage") as! PackGoPackagesViewController
        
        self.navigationController?.pushViewController(packGoPackages, animated: true)
        
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        var sizeHeight = UIScreen.mainScreen().bounds.height
        if sizeHeight == 480 || sizeHeight ==  568
        {
            return CGSizeMake(155, 155)
        }
            
         else if sizeHeight ==  667
        {
            return CGSizeMake(182, 182)
        }
        else if sizeHeight ==  736
        {
            return CGSizeMake(200,200)
        }
            
    
        else
        {
            return CGSizeMake(240, 240)
        }
        
    }

    
    
    
    
    
    
    
    @IBAction func OkBtn_barCodeView(sender: AnyObject)
    {
        blackView.hidden = true
        
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let newEntryScanBarcode = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryScanBarcode") as! NewEntryScanBarcodeViewController
        
        //newEntryScanBarcode.greenTitleStr = singleItemGreenTitleStr
        
        self.navigationController?.pushViewController(newEntryScanBarcode, animated: false)

    }
    
    
    
    
    
    
    
    
    
    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- Navigation Barcode Btn
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }

    
}
