//
//  SaveSettingViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class SaveSettingViewController: UIViewController {
    
    var userDefaultsSaveSetting = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet var deviceAndNetworkImage: UIImageView!
    @IBOutlet var networkImage: UIImageView!
    @IBOutlet var deviceImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if NSUserDefaults.standardUserDefaults().boolForKey("deviceOnly")
        {
            deviceImage.hidden = false
            networkImage.hidden = true
            deviceAndNetworkImage.hidden = true
            
            
        }
        if NSUserDefaults.standardUserDefaults().boolForKey("networkOnly")
        {
            
            deviceImage.hidden = true
            networkImage.hidden = false
            deviceAndNetworkImage.hidden = true
            
            
        }
        if NSUserDefaults.standardUserDefaults().boolForKey("deviceAndNetworkBtn")
        {
            deviceImage.hidden = true
            networkImage.hidden = true
            deviceAndNetworkImage.hidden = false
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func deviceOnlyBtn(sender: AnyObject)
    {
        userDefaultsSaveSetting.setBool(true ,forKey: "deviceOnly")
        userDefaultsSaveSetting.setBool(false ,forKey: "networkOnly")
        userDefaultsSaveSetting.setBool(false ,forKey: "deviceAndNetworkBtn")
        
        deviceImage.hidden = false
        networkImage.hidden = true
        deviceAndNetworkImage.hidden = true
        
    }
    
    @IBAction func networkOnly(sender: AnyObject)
    {
        userDefaultsSaveSetting.setBool(false ,forKey: "deviceOnly")
        userDefaultsSaveSetting.setBool(true ,forKey: "networkOnly")
        userDefaultsSaveSetting.setBool(false ,forKey: "deviceAndNetworkBtn")
        deviceImage.hidden = true
        networkImage.hidden = false
        deviceAndNetworkImage.hidden = true
        
    }
    
    @IBAction func deviceAndNetworkBtn(sender: AnyObject)
    {
        userDefaultsSaveSetting.setBool(false ,forKey: "deviceOnly")
        userDefaultsSaveSetting.setBool(false ,forKey: "networkOnly")
        userDefaultsSaveSetting.setBool(true ,forKey: "deviceAndNetworkBtn")
        deviceImage.hidden = true
        networkImage.hidden = true
        deviceAndNetworkImage.hidden = false
        
        
    }
    
    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
    }
    
}
