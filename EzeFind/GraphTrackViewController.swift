//
//  GraphTrackViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/2/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class GraphTrackViewController: UIViewController
{
  @IBOutlet var packGoAndInventoryView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    

    @IBAction func settingBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let setting = graphStoryboard.instantiateViewControllerWithIdentifier("setting") as! SettingsViewController
        
        self.navigationController?.pushViewController(setting, animated: false)

        
    }
   
    
    
    @IBAction func viewInventoryBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let viewInventory = graphStoryboard.instantiateViewControllerWithIdentifier("viewInventory") as! ViewInventoryViewController
        
        self.navigationController?.pushViewController(viewInventory, animated: false)

    }
    
    
    
    @IBAction func newEntryBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = false
    }
    
    
    
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        packGoAndInventoryView.hidden = true
    }
    
    
    
    
    @IBAction func inventoryBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var newEntryStoryboard = appDeleagte.newEntryStoryboard() as UIStoryboard
        
        let newEntryPackGo = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryPackGo") as! NewEntryPackAndGoViewController
        
        newEntryPackGo.titleStr = "Inventory Log"
        startPackingTitleStr = "START INVENTORY"
        
        newEntryPackGoOpenTitleStr = "View:Inventory"
        fullMenuDateTitleStr = "View:Inventory"
        strInventoryOrPackageTitle = "Inventory"
        self.navigationController?.pushViewController(newEntryPackGo, animated: true)
    }
    
    
    
    @IBAction func packGoBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var newEntryStoryboard = appDeleagte.newEntryStoryboard() as UIStoryboard
        
        let newEntryPackGo = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryPackGo") as! NewEntryPackAndGoViewController
        startPackingTitleStr = "START PACKING"
        newEntryPackGo.titleStr = "Pack&Go Log"
        newEntryPackGoOpenTitleStr = "View:Pack&Go Packages"
        fullMenuDateTitleStr = "View:Pack&Go Packages"

        self.navigationController?.pushViewController(newEntryPackGo, animated: true)

    }
    

    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        
   
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    
    }
    
    
    
    @IBAction func historyBtn(sender: AnyObject)
    {
        
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportDate  = reportStoryboard.instantiateViewControllerWithIdentifier("reportDate") as! ReportDateViewController
        
        self.navigationController?.pushViewController(reportDate, animated: true)

        
    }
    
    
    @IBAction func userBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportMember  = reportStoryboard.instantiateViewControllerWithIdentifier("reportMember") as! ReportMemberViewController
        
        self.navigationController?.pushViewController(reportMember, animated: true)

        
    }
    
    
    @IBAction func fullNameBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportAll  = reportStoryboard.instantiateViewControllerWithIdentifier("reportAll") as! ReportAllViewController
        
        self.navigationController?.pushViewController(reportAll, animated: true)

        
    }
    
    
   
    
}
