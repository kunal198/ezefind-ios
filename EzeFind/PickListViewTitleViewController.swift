//
//  PickListViewTitleViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class PickListViewTitleViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    @IBOutlet var pickListTableView: UITableView!
    var pickListID = NSString()
    
    var picklistDetailArray = NSArray()
    
    
    var indexxPath = NSIndexPath()
    var packageDataId = Int()
    var packageId = String()
    
    var id = Int()
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        pickListDetailApi()
    }

    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
       
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func logoutBtn(sender: AnyObject)
    {
        
        var loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let login = loginStoryboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        
        self.navigationController?.pushViewController(login, animated: false)

    }
    
    
    //MARK:- TableView Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return picklistDetailArray.count
    }
    
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! PickListViewTitleTableViewCell
        
//        if var urlString  =  picklistDetailArray[indexPath.row].valueForKey("Image") as? String, imageurl = NSURL(string: NSString(format: "http://beta.brstdev.com/yiiezefind%@", urlString) as String)
//        {
//            if let data = NSData(contentsOfURL: imageurl)
//            {
//                
//                cell.picListTitleImage.image = UIImage(data: data)
//            }
//        }
        
        
        cell.editListEditBtn.layer.borderColor = UIColor.lightGrayColor().CGColor
        cell.editListDeleteBtn.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        
         cell.selectEditAndDeleteBtn.tag = indexPath.row
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
        var profileUrl: String = picklistDetailArray[indexPath.row].valueForKey("Image") as! String
        
        var url: NSURL = NSURL(string: profileUrl)!
        
        cell.picListTitleImage.sd_setImageWithURL(url, completed: block)
        
        
        cell.picListTitleImage.contentMode = UIViewContentMode.ScaleAspectFit

        
        
        
        

        if let title = picklistDetailArray[indexPath.row].valueForKey("Title") as? String
        {
            cell.titleLbl.text = title
        }
        
        if let description = picklistDetailArray[indexPath.row].valueForKey("Description") as? String
        {
            cell.descriptionTxtView.text = description
        }
        
        
        
        if let attachement  = picklistDetailArray[indexPath.row].valueForKey("AttachmentCount") as? Int
        {
            cell.attachementLbl.text = toString(attachement)
        }
        
        if let quantity = picklistDetailArray[indexPath.row].valueForKey("Quantity") as? String
        
        {
            cell.quantityLbl.text = quantity
        }
        
        
        cell.editListEditBtn.addTarget(self, action: "editListEditBtnAction:", forControlEvents: UIControlEvents.TouchUpInside)
        
        cell.editListDeleteBtn.addTarget(self, action: "editListDeleteBtnAction:", forControlEvents: UIControlEvents.TouchUpInside)
        
        
        cell.editListEditBtn.tag = indexPath.row
        
        cell.editListDeleteBtn.tag = indexPath.row
        
        return cell
    }
    
    
    func editListEditBtnAction(sender: UIButton!)
    {
        println("edit list edit btn clicked")
        
        
        //indexxPath = NSIndexPath(forRow: sender.tag, inSection: 0)

        let cell = pickListTableView.cellForRowAtIndexPath(indexxPath) as! PickListViewTitleTableViewCell!
        
        cell.selectEditAndDeleteBtn.hidden = false
        cell.editListEditBtn.hidden = true
        cell.editListDeleteBtn.hidden = true
        
        
        
        
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDelegate.viewInventoryStoryboard() as UIStoryboard
        
        let packGoPackages = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("packGoPackage") as! PackGoPackagesViewController
        
        
        
        if let itemID = picklistDetailArray[indexxPath.row].valueForKey("ItemId") as? String
        {
            
            
            if itemID == "NULL"
            {
                println("item id is 0")
                
                var alert = UIAlertView()
                alert = UIAlertView(title:"Alert", message: "No Records found as you entered null enteries.", delegate: self, cancelButtonTitle:"OK")
                
                alert.show()

            }
            else
            {
                
                
                println("item id at each cell = \(itemID)")
                
                id = itemID.toInt()!
                
                println("item id at each cell = \(id)")
  
            
                
                //        packGoPackages.itemId = picklistDetailArray[indexxPath.row].valueForKey("ItemId") as! Int
                
                packGoPackages.itemId = id
                
                packGoPackages.packageDataId = toString(packageDataId)
                packGoPackages.packageId = packageId
                //packGoPackages.greenTitleStr = greenTitleStr
                self.navigationController?.pushViewController(packGoPackages, animated: true)
            }
            

            
            
        }
        
    }
    
    
    func editListDeleteBtnAction(sender: UIButton!)
    {
        //indexxPath = NSIndexPath(forRow: sender.tag, inSection: 0)

         println("edit list delete btn clicked")
        
        let cell = pickListTableView.cellForRowAtIndexPath(indexxPath) as! PickListViewTitleTableViewCell!
        
        cell.selectEditAndDeleteBtn.hidden = false
        cell.editListEditBtn.hidden = true
        cell.editListDeleteBtn.hidden = true
        
        
        //deleteItemApi()
       // packageDataItems.removeAtIndex(indexPath.row)
        //editItemTableView.reloadData()

    }

    
    
    
    
    
    //MARK:- TableView Button
    @IBAction func selectEditAndDelteBtn(sender: AnyObject)
    {
        indexxPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        let cell = pickListTableView.cellForRowAtIndexPath(indexxPath) as! PickListViewTitleTableViewCell!
        
        cell.selectEditAndDeleteBtn.hidden = true
        cell.editListEditBtn.hidden = false
        cell.editListDeleteBtn.hidden = false
    }

    
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
//        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
//        
//        var viewInventoryStoryboard = appDelegate.viewInventoryStoryboard() as UIStoryboard
//        
//        
//        let packGoPackage = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("packGoPackage") as! PackGoPackagesViewController
//        packGoPackage.itemId = (picklistDetailArray[indexPath.row].valueForKey("ItemId") as! String).toInt()!
//        
//        self.navigationController?.pushViewController(packGoPackage, animated: true)
        
    }

    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        

    }
    
    
    
    //MARK:- PickListDetail
    
    func pickListDetailApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
       
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PicklistId=%@",str,pickListID)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/picklists/picklist-detail")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj  = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()

                        
                        spinningIndicator.hide(true)
                    })

                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            var message = dicObj?.valueForKey("message") as! NSDictionary
                            
                            if message.valueForKey("PicklistItems") == nil
                            {
                                var alert = UIAlertView(title: "Alert", message: "No Record Found", delegate: self, cancelButtonTitle: "OK")
                                
                                alert.show()
                                
                                spinningIndicator.hide(true)

                            
                            }
                            else
                            {
                                self.picklistDetailArray = message.valueForKey("PicklistItems") as! NSArray
                                
                                
                                
                                
                                self.pickListTableView.reloadData()
                                
                                spinningIndicator.hide(true)
                            }
                            
                            
                            
                        }
                        else
                        {
                        
                            
                            var alert = UIAlertView(title: "Alert", message: "No Record Found", delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                    })
                    
                }
            }
            
            
        })
        task.resume()
        

        
    }
    
}
