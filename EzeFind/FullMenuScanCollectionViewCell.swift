//
//  FullMenuScanCollectionViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class FullMenuScanCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet var scanImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
}
