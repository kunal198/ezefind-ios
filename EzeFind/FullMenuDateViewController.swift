//
//  FullMenuDateViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


var fullMenuDateTitleStr = NSString()

var multipleDateArray : [String]!

class FullMenuDateViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,DSLCalendarViewDelegate
{
    
    
    var startDate_Dragging = NSDateComponents()
    var lastDate_Dragging = NSDateComponents()
    let calendar = NSCalendar.currentCalendar()

    var dateStrArr = [String]()
    
    var datSr_last = ""
    
    var datSr = ""
    
     var joinedStrings = String()
    
    
    let date = NSDate()

    var dateFormat = NSDateFormatter()
    
    var dateInFormat:NSString = NSString()
    
    
    @IBOutlet weak var filterDateBtn: UIButton!
    
    @IBOutlet weak var blackView: UIView!
    
    
    
    @IBOutlet weak var calendarBtn: UIButton!
    
    @IBOutlet weak var filterDateLbl: UIButton!
    
    
    
    @IBOutlet weak var calendarView: DSLCalendarView!
       
    @IBOutlet var greenTitleLbl: UILabel!
    var noOfPackages = NSString()
    var address = NSString()
    @IBOutlet var collectionView: UICollectionView!
    var packageDataItems = NSArray()
    var packageDataID = String()
    var packageId = NSString()
    @IBOutlet var titleLbl: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        dateStrArr = []
        multipleDateArray = []
        calendarView.delegate = self
        
        if DateMenuCheck == "dateMenu"
        {
            titleLbl.text = fullMenuDateTitleStr as String
            
            //dateFormat.dateFormat = "MM-dd-yyyy"
            
            dateFormat.dateFormat = "yyyy/MM/dd"
            
            dateInFormat = dateFormat.stringFromDate(date)
            
            println(dateInFormat)
            
            filterDateBtn.hidden = false
            
            self.titleLbl.text = dateInFormat as String
            
            
            self.calendarBtn.hidden = false
            self.filterDateLbl.hidden = false
            
            fullMenuDateAPI()
            
        }
        else
        {
            
            self.calendarBtn.hidden = true
            self.filterDateLbl.hidden = true
            
            
            titleLbl.text = "View:Pack&Go Packages"
            
            greenTitleLbl.text = NSString(format: "%@/%@",address,noOfPackages) as String
            
            getPackageDataItems()
            

        }
        
    

    }

    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
   
    
    
    
    @IBAction func filterDateBtn(sender: AnyObject)
    {
        //blackView.hidden = false
        calendarView.hidden = false
        
    }
    
    
    
    
    
    
    @IBAction func filterBtn(sender: AnyObject)
    {
        
        
        if joinedStrings != ""
        {
           fullMenuDateWithFilterAPI()
        }
        else
        {
            var alert = UIAlertView()
            alert = UIAlertView(title:"Alert", message: "Please select date(s) from calendar for filtering data.", delegate: self, cancelButtonTitle:"OK")
            alert.show()
 
        }
        
    }
    
    
    
    
    
    
    
    
    
    //MARK:-Full Menu Date With Filter Api
    
    func fullMenuDateWithFilterAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
       var post = NSString(format:"UserId=%i&Date=%@",str,joinedStrings)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/date")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })

                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                            
                            self.calendarView.hidden = true
                            
                            
                        self.packageDataItems = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
                            
                          self.collectionView.reloadData()
                           
                            
                            
                            println("filter date items = \(self.packageDataItems)")
                             //self.blackView.hidden = true
                            //self.valueFilterView.hidden = true
                            
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            
                            
                              //var alert = UIAlertView(title: "Alert", message: "Please Select date(s) for filtering", delegate: self, cancelButtonTitle: "OK")
                            
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            
                            alert.show()
                            
                            spinningIndicator.hide(true)
                            
                            self.calendarView.hidden = true
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func calendarView(calendarView: DSLCalendarView!, didSelectRange range: DSLCalendarRange!)
    {
        if range != nil
        {
            println("User selected \(range.startDay) \n to \n \(range.endDay)")
            
            //reportDateApi()
        }
    }
    
    
    
    
    func calendarView(calendarView: DSLCalendarView!, didDragToDay day: NSDateComponents!, selectingRange range: DSLCalendarRange!) -> DSLCalendarRange!
    {

        startDate_Dragging = range.startDay
        
        println("start date = \(startDate_Dragging)")
        
        
        
        //        //let date = NSDate()
        //        let calendar = NSCalendar.currentCalendar()
        //        let components = calendar.components(.CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: date)
        //
        //        let year =  components.year
        //        let month = components.month
        //        let day = components.day
        //
        //        println("year = \(year)")
        //        println("month = \(month)")
        //        println("date = \(day)")
        //
        
        
        
        var Startcompo = NSDateComponents()
        Startcompo = range.startDay
        
        println("compo = \(Startcompo)")
        
        let year =  Startcompo.year
        let month = Startcompo.month
        let day = Startcompo.day
        
        println("year montha nd date = \(year)-\(month)-\(day)")
        println("month = \(month)")
        println("date = \(day)")
        
        datSr = "\(year)/\(month)/\(day)"
        
        
        
        
        
        dateStrArr.append(datSr)
        
//        let str = datSr.join("','")
        
        
        
        println("array = \(dateStrArr)")
        
        println("datSr = \(datSr)")
       // let joined = dateStrArr.join("','")

        
        joinedStrings = ",".join(dateStrArr)
        
        println("joined string = \(joinedStrings)")
        
        //println(\(year)-\(month)-\(day))
        
        println()
        
        var sDate = NSDateFormatter()
        sDate.dateStyle = NSDateFormatterStyle.ShortStyle
        
        println("sdate = \(sDate)")
        
        lastDate_Dragging = range.endDay
        println("last date = \(lastDate_Dragging)")
        
        
        
        
        var Lastcompo = NSDateComponents()
        Lastcompo = range.endDay
        
        println("compo = \(Lastcompo)")
        
        let year_last =  Lastcompo.year
        let month_last = Lastcompo.month
        let day_last = Lastcompo.day
        
        
        
        datSr_last = "\(year_last)-\(month_last)-\(day_last)"
        println("datSr = \(datSr_last)")
        
        
        
        var lDate = NSDateFormatter()
        
        
        println("year last = \(year_last)")
        println("month last= \(month_last)")
        println("date last = \(day_last)")
        
        //fullMenuDateFilterAPI()
        
        
        return range;
        
    }

  
    
    
    func calendarView(calendarView: DSLCalendarView!, didChangeToVisibleMonth month: NSDateComponents!)
    {
        //NSLog(@"Now showing %@", month);
        
        println("Now showing = \(month)")
    }
    
    
    
    
    func calendarView(calendarView: DSLCalendarView!, willChangeToVisibleMonth month: NSDateComponents!, duration: NSTimeInterval)
    {
        //NSLog(@"Will show %@ in %.3f seconds", month, duration);
        
        println("Will show %@ in %.3f seconds =  \(month), \(duration)")
    }
    
    
   
//    
//    - (BOOL)calendarView:(DSLCalendarView *)calendarView shouldSelectDate:(NSDateComponents *)dateComponents {
//    return YES;
//    
//    }
//    
//    - (BOOL)calendarView:(DSLCalendarView *)calendarView shouldDeselectDate:(NSDateComponents *)dateComponents {
//    return YES;
//    }
    
//    -(void)calendarView:(DSLCalendarView *)calendarView didSelectDate:(NSDateComponents *)date
//    {
//    }
//    
//    -(void)calendarView:(DSLCalendarView *)calendarView didDeselectDate:(NSDateComponents *)date
//    {
//    
//    }

    
    
    
    
    
    
    


    
    
    
    
    
    
    
    
    //MARK:-Full Menu Date Api
    
    func fullMenuDateAPI()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        println(str)
        
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&Date=%@",str,dateInFormat)
        
        
        println("post data = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/view-inventory/date")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            //
            //            if data == nil
            //            {
            //                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
            //                alert.show()
            //
            //            }
            //            else
            //
            //          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })
                    
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        
                        if success
                        {
                            
                            //                            self.imageArray = dicObj?.valueForKey("Data") as! NSArray as! [(NSDictionary)]
                            //
                            //                            self.valueCollectionView.reloadData()
                            //
                            //                            self.blackView.hidden = true
                            //                            self.valueFilterView.hidden = true
                            //
                            //                            println("image array from didSelect = \(self.imageArray)")
                            //
                            
                            self.packageDataItems = dicObj?.valueForKey("Data") as! NSArray
                            
                            self.collectionView.reloadData()
                            
                            spinningIndicator.hide(true)
                            
                        }
                            
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            
                            alert.show()
                            
                            self.collectionView.reloadData()
                            
                            spinningIndicator.hide(true)
                        }
                        
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    

    
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return packageDataItems.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        var dateCell = collectionView.dequeueReusableCellWithReuseIdentifier("dateCell", forIndexPath: indexPath) as! FullMenuDateCollectionViewCell
//        
//        if var urlString  =  packageDataItems[indexPath.row].valueForKey("Image") as? String, imageurl = NSURL(string: NSString(format: "http://beta.brstdev.com/yiiezefind%@", urlString) as String)
//        {
//            if let data = NSData(contentsOfURL: imageurl)
//            {
//                dateCell.dateImageView.contentMode = UIViewContentMode.ScaleAspectFit
//                dateCell.dateImageView.image = UIImage(data: data)
//            }
//        }
//        
        
        
        
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
        var profileUrl: String = (packageDataItems[indexPath.row].valueForKey("Image") as? String)!
        var url: NSURL = NSURL(string: profileUrl)!
        
        dateCell.dateImageView.sd_setImageWithURL(url, completed: block)

        
        
        

        if var quantity = packageDataItems[indexPath.row].valueForKey("Quantity") as? Int

        {
            dateCell.quantityLbl.text = toString(quantity)
        }
        
        
        
        if var attachmentCount = packageDataItems[indexPath.row].valueForKey("AttachmentCount") as? Int
            
        {
            dateCell.attachmentlbl.text = toString(attachmentCount)
        }
        
        
        
        if var title = packageDataItems[indexPath.row].valueForKey("Title") as? String
            
        {
            dateCell.titleLbl.text = title
        }


       
        return dateCell
    }
    
    
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDelegate.viewInventoryStoryboard() as UIStoryboard
        
        let packGoPackages = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("packGoPackage") as! PackGoPackagesViewController
        
        packGoPackages.packageId = packageId
        packGoPackages.packageDataId = packageDataID
        packGoPackages.greenTitleStr = address
        
        
        packGoPackages.itemId = packageDataItems[indexPath.row].valueForKey("ItemId") as! Int
        
        
        if DateMenuCheck == "dateMenu"
        {
           typeCategoryCheck = "DateSectionDidSelect" 
        }
        else
        {
            typeCategoryCheck = "PackGOSectionDidSelect"
        }
        
        
        
        self.navigationController?.pushViewController(packGoPackages, animated: true)
        
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        var sizeHeight = UIScreen.mainScreen().bounds.height
        if sizeHeight == 480 || sizeHeight ==  568
        {
            return CGSizeMake(155, 155)
        }
            
        else if sizeHeight ==  667
        {
            return CGSizeMake(182, 182)
        }
        else if sizeHeight ==  736
        {
            return CGSizeMake(200,200)
        }
            
            
        else
        {
            return CGSizeMake(240, 240)
        }
        
    }

    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    //MARK:- Navigation Barcode Btn
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }

    
    
    //MARK:- Get Package Data Items
    func getPackageDataItems()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        
        println(packageId)
        println(packageDataID)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PackageId=%@&PackageDataId=%@",str,packageId,packageDataID)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/getpackagesdataitems")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })

                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        })

                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            
                            if success
                            {
                                self.packageDataItems = dicObj?.valueForKey("message") as! NSArray
                                self.collectionView.reloadData()
                                spinningIndicator.hide(true)
                                
                            }
                                
                            else
                            {
                                var message = dicObj?.valueForKey("message") as! String
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                spinningIndicator.hide(true)
                                
                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        
        
        
    }

    

}
