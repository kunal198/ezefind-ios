//
//  NetworkSatusViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class NetworkSatusViewController: UIViewController
{

    @IBOutlet var statusNetworkLbl: UILabel!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        if Reachability.isConnectedToNetwork() == true
        {
            statusNetworkLbl.text = "CONNECTED"
            
        }
        else
        {
            statusNetworkLbl.text = "NOT CONNECTED"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

    }
    
}
