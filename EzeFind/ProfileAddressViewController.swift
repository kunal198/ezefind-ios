//
//  ProfileAddressViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


var UserNameProfile = String()

var userNameProfile_Defaults = NSUserDefaults.standardUserDefaults()

class ProfileAddressViewController: UIViewController,UITextFieldDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UIPickerViewDelegate,UINavigationControllerDelegate

{

     var imagePicker = UIImagePickerController()
    
    //var imagePickerUpdate = UIImagePickerController()
    
    
    var reportAlldict = NSDictionary()
    var sizeHeight = UIScreen.mainScreen().bounds.height
    
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var stateTxtfield: UITextField!
    @IBOutlet var cityTxtfield: UITextField!
    @IBOutlet var addressTxtField: UITextField!
    @IBOutlet var lastNameTxtField: UITextField!
    @IBOutlet var firstNameTxtfield: UITextField!
    @IBOutlet var zipcodeTxtField: UITextField!
    @IBOutlet var scrollView: UIScrollView!
    
    
    var alertViewImage = UIAlertView()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
     
        let paddingViewFirstName = UIView(frame: CGRectMake(0, 0, 5, firstNameTxtfield.frame.size.height))
        firstNameTxtfield.leftView = paddingViewFirstName
        firstNameTxtfield.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewLastName = UIView(frame: CGRectMake(0, 0, 5, lastNameTxtField.frame.size.height))
        lastNameTxtField.leftView = paddingViewLastName
        lastNameTxtField.leftViewMode = UITextFieldViewMode.Always

        let paddingViewAddress = UIView(frame: CGRectMake(0, 0, 5, addressTxtField.frame.size.height))
        addressTxtField.leftView = paddingViewAddress
        addressTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewCity = UIView(frame: CGRectMake(0, 0, 5, cityTxtfield.frame.size.height))
        cityTxtfield.leftView = paddingViewCity
        cityTxtfield.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewState = UIView(frame: CGRectMake(0, 0, 5, stateTxtfield.frame.size.height))
        stateTxtfield.leftView = paddingViewState
        stateTxtfield.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewZipcode = UIView(frame: CGRectMake(0, 0, 5, zipcodeTxtField.frame.size.height))
        zipcodeTxtField.leftView = paddingViewZipcode
        zipcodeTxtField.leftViewMode = UITextFieldViewMode.Always

        stateTxtfield.delegate = self
        cityTxtfield.delegate = self
        addressTxtField.delegate = self
        lastNameTxtField.delegate = self
        firstNameTxtfield.delegate = self
        zipcodeTxtField.delegate = self
        
        
      scrollView.contentSize.height = zipcodeTxtField.frame.origin.y + zipcodeTxtField.frame.size.height + 5
        
        profileApi()
    }

    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    //MARK:- Member Add Photo Button
    
    @IBAction func addPhotoBtn(sender: AnyObject)
    {
        alertViewImage = UIAlertView(title: "Alert", message: "Choose the Source", delegate: self, cancelButtonTitle: "Camera", otherButtonTitles: "Photo Gallery")
        
        alertViewImage.show()
        
    }
    
    
    
    
    
    //MARK:- AlertView Method
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alertView == alertViewImage
        {
            if buttonIndex == 0
            {
                
            
                
                if (UIImagePickerController.isSourceTypeAvailable(.Camera))
                {
                    if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil
                    {
                        
                        imagePicker.sourceType = .Camera
                        //imagePicker.mediaTypes = [kUTTypeMovie as String]
                        imagePicker.allowsEditing = false
                        imagePicker.delegate = self
                        
                        presentViewController(imagePicker, animated: true, completion: nil)
                    }
                    else
                    {
                        let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
                        alertView.show()
                        
                    }
                    
                    
                }
                
                
                
                
                
//               if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
//                {
//                    
//        
//                    imagePicker.allowsEditing = true
//                    imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
//                    imagePicker.cameraCaptureMode = .Photo
//                    
//                    
//                    presentViewController(imagePicker, animated: true, completion: nil)
//                }
                
                
            }
            
            
            else
            //
            {
                
               if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)
                    

                {
                    
                    imagePicker.delegate = self
                    
                    imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                    
                    // imagePicker.mediaTypes = [kUTTypeImage]
                    
                    imagePicker.allowsEditing = true
                    
                    self.presentViewController(imagePicker, animated: true, completion: nil)
                }
                
                    
              else
              {
                var alert = UIAlertView(title: "Image not Updated", message: "Please check your Internet Settings", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                
                let okBtn = UIAlertAction(title:"OK", style: .Default)
                    {
                        (action: UIAlertAction!) -> Void in
                }
                
                
                alert.show()
                
                NSLog("failed")
            }
            
            
            
        }
        

        
    }
}
    
    
    
    // MARK: - UIImagePicker Delegate Method
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
    {
        
        profileImageView.image = image
        
        //imageData64Base = convertImageToBase64(image)
        
        picker .dismissViewControllerAnimated(true, completion: nil)
        
        
        
    }
    
    
    
    
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        picker .dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
    
    
    
    

    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    //Mark:-Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

    }
    
    //MARK:- UpdateButton
    
    @IBAction func updatebtn(sender: AnyObject)
    {
        if firstNameTxtfield.text == "" || lastNameTxtField.text == "" || addressTxtField.text == "" || cityTxtfield.text == "" || stateTxtfield.text == "" || zipcodeTxtField.text == ""
        {
            var alert = UIAlertView(title: "Alert", message: "Please fill the required fields", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
        
        else
        {
            
          var spinningIndicator  = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            spinningIndicator.labelText = "Loading"
        
            
            
            var image:UIImage = profileImageView.image!
            var imageData = UIImageJPEGRepresentation(image, 0.6)
            var base64String = imageData.base64EncodedStringWithOptions(.allZeros)
            var strImage64 = base64String.stringByReplacingOccurrencesOfString("+", withString: "%2B") as String
            
//            var number = UInt32()
//            
//            number = arc4random() % 1000000
//            
            
        
            
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&FirstName=%@&LastName=%@&City=%@&State=%@&ZipCode=%@&Address=%@&ProfileImage=%@",str,firstNameTxtfield.text,lastNameTxtField.text,cityTxtfield.text,stateTxtfield.text,zipcodeTxtField.text,addressTxtField.text,strImage64)
        
       // println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/update-profile"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    println("UpdateProfile = \(dictObj)")
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        
                        dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        
                        spinningIndicator.hide(true)
                            
                        })

                    }
                    else
                    {
                        
                        var success = dictObj?.valueForKey("success") as! Bool
                        var message = dictObj?.valueForKey("message") as! String
                        
                        if success
                        {
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        }
                        else
                            
                        {
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }

                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
            
            
        }
    }
    

    
    
    
    //MARK:- Profile Api
    
    func profileApi()
    {
        var spinningIndicator  = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"

        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i", str)
        
        //println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/profile"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as?
                   NSDictionary
                    
                    println(dictObj)
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        })

                    }
                    else
                    {
                        
                        dispatch_async(dispatch_get_main_queue(), {
                        
                        
                        self.reportAlldict = dictObj?.valueForKey("message") as! NSDictionary
                        
                        
                        println(self.reportAlldict)
                        
                    
                        
//                    self.profileImageView.image = self.reportAlldict.valueForKey("ProfileImage") as! UIImage
                        
                        
                      
                        
                        
                        
                       
//                var profileUrl: String = self.reportAlldict.valueForKey("ProfileImage") as! String
//
//                var url: NSURL = NSURL(string: profileUrl)!
//                      
//                if let data : NSData = NSData(contentsOfURL: url)
//                {
//                    self.profileImageView.image = UIImage(data: data)!
//                    self.profileImageView.backgroundColor = UIColor.clearColor()
//                        
//                           //imageView.image = UIImage(data: data)!
//                }

                 
                        
                        
//                        let myImage = UIImage(data: NSData(contentsOfURL: NSURL(string:"https://i.stack.imgur.com/Xs4RX.jpg")!)!)!
//                        NSUserDefaults.standardUserDefaults().setImage(myImage, forKey: "anyKey")
//                        if let myLoadedImage = NSUserDefaults.standardUserDefaults().imageForKey("anyKey") {
//                            print(myLoadedImage.size)  // "(719.0, 808.0)"
//                        }
//                        
//                        let myImagesArray = [myImage, myImage]
//                        NSUserDefaults.standardUserDefaults().setImageArray(myImagesArray, forKey: "imageArrayKey")
//                        if let myLoadedImages = NSUserDefaults.standardUserDefaults().imageArrayForKey("imageArrayKey") {
//                            print(myLoadedImages.count)  // 2
//                        }
                     
                        
                          println(self.reportAlldict.valueForKey("ProfileImage")!)
                        
//                        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
//                            
//                        }
//                        
//                        
//                        var profileUrl: String = self.reportAlldict.valueForKey("ProfileImage") as! String
//                        
//                        var url: NSURL = NSURL(string: profileUrl)!
//
//                        
//                        println("url = \(url)")
//                        
//                        self.profileImageView.sd_setImageWithURL(url, completed: block)
//                        
           
                        
                        
                        
                    var profileUrl: String = self.reportAlldict.valueForKey("ProfileImage") as! String
                     
                    var url: NSURL = NSURL(string: profileUrl)!
                        
                    if let data : NSData = NSData(contentsOfURL: url)
                    {
                            self.profileImageView.image = UIImage(data: data)!
                            self.profileImageView.backgroundColor = UIColor.clearColor()
                                                
                                                   //imageView.image = UIImage(data: data)!
                    }

                        
                        
                        
                        
                        
                        UserNameProfile = self.reportAlldict.valueForKey("FirstName") as! String
                        
                        println("username = \(UserNameProfile)")
                        

                        
                        self.firstNameTxtfield.text = ""
                        
                        if let strFirstName = self.reportAlldict.valueForKey("FirstName") as? String
                        {
                            self.firstNameTxtfield.text = strFirstName
                            
                            
                            userNameProfile_Defaults = NSUserDefaults.standardUserDefaults()
                            
                            //userDefaults.setValue(userId, forKey: "userId")
                            
                            userNameProfile_Defaults.setObject(UserNameProfile, forKey: "FirstName")
                            
                    
                            userNameProfile_Defaults.synchronize()

                        }
                        
                        self.lastNameTxtField.text = ""
                        
                        if let strLastName = self.reportAlldict.valueForKey("LastName") as? String
                        {
                            println(strLastName)
                            self.lastNameTxtField.text = strLastName
                        }
                        
                        self.addressTxtField.text = ""
                        
                        if let strAddress = self.reportAlldict.valueForKey("Address") as? String
                        {
                            self.addressTxtField.text = strAddress
                        }
                        
                        self.cityTxtfield.text = ""
                        
                        if let strCity = self.reportAlldict.valueForKey("City") as? String
                        {
                            self.cityTxtfield.text = strCity
                        }
                        
                        
                        self.stateTxtfield.text = ""
                        
                        if let strState = self.reportAlldict.valueForKey("State") as? String
                        {
                            self.stateTxtfield.text = strState
                        }
                        
                        self.zipcodeTxtField.text = ""
                        
                        if let strZipcode = self.reportAlldict.valueForKey("ZipCode") as? String
                        {
                            self.zipcodeTxtField.text = strZipcode
                        }
                        
                        spinningIndicator.hide(true)
                        
                    })
                    
                    }
                    
                    
                })
                
                
            }
            
           
            
            
            
            
            
            
            
            
            
        })
        task.resume()
    }
    
    
    
    
    
    //MARK:- Touch Method

    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        stateTxtfield.resignFirstResponder()
        cityTxtfield.resignFirstResponder()
        addressTxtField.resignFirstResponder()
        lastNameTxtField.resignFirstResponder()
        firstNameTxtfield.resignFirstResponder()
        zipcodeTxtField.resignFirstResponder()
        
    }
    
    //MARK:- TextField Dlegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        if textField == firstNameTxtfield || textField == lastNameTxtField || textField == addressTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 150
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y -= 130
            }
            
            if sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y -= 110
            }
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad
            {
                
                view.frame.origin.y -= 70
            }

        }
            
        else if textField == cityTxtfield || textField == stateTxtfield  || textField == zipcodeTxtField
            
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 250
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y -= 240
            }
            
            if sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y -= 250
            }
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad
            {
                
                view.frame.origin.y -= 300
            }
            
            
            
            
        }
            
        else
            
        {
            view.frame.origin.y = view.frame.origin.y
        }
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if textField == firstNameTxtfield || textField == lastNameTxtField || textField == addressTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 150
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y += 130
            }
            
            if sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y += 110
            }
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad
            {
                
                view.frame.origin.y += 70
            }

            
            
        }
            
        else if textField == cityTxtfield || textField == stateTxtfield  || textField == zipcodeTxtField
            
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 250
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y += 240
            }
            
            if sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y += 250
            }
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad
            {
                
                view.frame.origin.y += 300
            }
            
        }
            
        else
            
        {
            view.frame.origin.y = view.frame.origin.y
        }
        
        return true
        
    }
    


   
}
