//
//  EntrySettingViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


class EntrySettingViewController: UIViewController {

     var userDefault = NSUserDefaults()
    
    @IBOutlet var textAndAudioLbl: UILabel!
    @IBOutlet var audioOnlyLbl: UILabel!
    @IBOutlet var textLbl: UILabel!
    @IBOutlet var textAndAudioImage: UIImageView!
    @IBOutlet var audioImage: UIImageView!
    @IBOutlet var textImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userDefault = NSUserDefaults.standardUserDefaults()
        
        if NSUserDefaults.standardUserDefaults().boolForKey("textValue")
        {
            textImage.hidden = false
            audioImage.hidden = true
            textAndAudioImage.hidden = true

            
        }
        if NSUserDefaults.standardUserDefaults().boolForKey("audioOnlyValue")
        {
            textImage.hidden = true
            audioImage.hidden = false
            textAndAudioImage.hidden = true

            
        }
         if NSUserDefaults.standardUserDefaults().boolForKey("textAndAudioValue")
        {
            textImage.hidden = true
            audioImage.hidden = true
            textAndAudioImage.hidden = false
 
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
        
    }
  
    @IBAction func textBtn(sender: AnyObject)
    {
        
        userDefault.setBool(true ,forKey: "textValue")
        userDefault.setBool(false ,forKey: "audioOnlyValue")
         userDefault.setBool(false ,forKey: "textAndAudioValue")
        
        textImage.hidden = false
        audioImage.hidden = true
        textAndAudioImage.hidden = true
        
    }
    @IBAction func audioOnly(sender: AnyObject)
    {
        
        userDefault.setBool(true ,forKey: "audioOnlyValue")
        userDefault.setBool(false ,forKey: "textValue")
        userDefault.setBool(false ,forKey: "textAndAudioValue")
        
        textImage.hidden = true
        audioImage.hidden = false
        textAndAudioImage.hidden = true
        
    }
    
    
    @IBAction func textAndAudioBtn(sender: AnyObject)
    {
        userDefault.setBool(true ,forKey: "textAndAudioValue")
        userDefault.setBool(false ,forKey: "audioOnlyValue")
        userDefault.setBool(false ,forKey: "textValue")
       
        textImage.hidden = true
        audioImage.hidden = true
        textAndAudioImage.hidden = false
    }
    
    //MARK:- Navigation Barcode Button
    
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

    }
    
}

