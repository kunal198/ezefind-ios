//
//  InventoryFullMenuViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit



var FullMenuCheck = String()

var DateMenuCheck = String()


var picklistView = String()

class InventoryFullMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
   
    var fullMenuImageArray = NSArray()
     var fullMenuNameArray = NSArray()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        fullMenuNameArray = ["SCAN BARCODE","CATEGORY","DATE","VALUE","MEMBER","LOCATION","COLOR","PICK LIST"]
        
        fullMenuImageArray = ["img_view_barcode.png","ic_category.png","ic_view_date.png","ic_value.png","ic_member.png","ic_location.png","ic_view_search.png","ic_picklist.png"]

        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return fullMenuNameArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let fullMenu = tableView.dequeueReusableCellWithIdentifier("fullMenu", forIndexPath: indexPath) as! InventoryFullMenuTableViewCell
        
        
        fullMenu.fullMenuLbl.text = fullMenuNameArray[indexPath.row] as? String
        fullMenu.fullMenuImage.image = UIImage(named: fullMenuImageArray[indexPath.row] as! String)
        
        println(fullMenuImageArray[indexPath.row] as? UIImage)
        
        return fullMenu
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDeleagte.viewInventoryStoryboard() as UIStoryboard

        var str = fullMenuNameArray[indexPath.row] as! String
        
        
        if str == "SCAN BARCODE"
        {
            
            let scanView = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("scanView") as! FullMenuScanViewController
            receiveNextBtnCheck = "full Menu"
            backBtnCheck = "back"
            scanOnViewInventory = "cancelBtn"
            
            self.navigationController?.pushViewController(scanView, animated: true)
            
        }
        else if str == "CATEGORY"
        {
            let category = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("category") as! FullMenuCategoryViewController
            
            locationCheck = "category"
            
            self.navigationController?.pushViewController(category, animated: true)
            
        }
        else if str == "DATE"
        {
            let date = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("date") as! FullMenuDateViewController
            
            DateMenuCheck = "dateMenu"
            
            self.navigationController?.pushViewController(date, animated: true)
            
        }
        
        else if str == "VALUE"
        {
            let value = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("value") as! FullMenuValueViewController
            
            self.navigationController?.pushViewController(value, animated: true)
            
        }
        else if str == "MEMBER"
        {
            let member = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("member") as! FullMenuMemberViewController
            
            FullMenuCheck = "memberView"
            
            self.navigationController?.pushViewController(member, animated: true)
            
        }
        else if str == "LOCATION"
        {
            let mapView = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("mapView") as! FullMenuLocationViewController
            
            self.navigationController?.pushViewController(mapView, animated: true)
            
        }
        
        else if str == "COLOR"
        {
            let member = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("member") as! FullMenuMemberViewController
            
            FullMenuCheck = "colorView"
            
            self.navigationController?.pushViewController(member, animated: true)
        }
        else
        {
            var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
            
            var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
            let pickList = graphStoryboard.instantiateViewControllerWithIdentifier("pickList") as! PickListViewController
            
            picklistView = "pickListView"
            
            self.navigationController?.pushViewController(pickList, animated: true)

        }

        

        
    }
  

    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    //MARK:- Navigation Barcode Btn
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }

}
