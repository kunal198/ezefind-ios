//
//  ViewInventoryViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/2/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit



var searchTextStr = String()


class ViewInventoryViewController: UIViewController,UITextFieldDelegate
{
    
    @IBOutlet var packGoAndInventoryView: UIView!
    
    var sizeHeight = UIScreen.mainScreen().bounds.height
    
    
    var searchKeywordInTxtField = String()
    
    @IBOutlet var keyWordTxtField: UITextField!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
     
        let paddingViewKeyword = UIView(frame: CGRectMake(0, 0, 5, keyWordTxtField.frame.size.height))
        
        keyWordTxtField.leftView = paddingViewKeyword
        keyWordTxtField.leftViewMode = UITextFieldViewMode.Always
         keyWordTxtField.delegate = self
    }

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func settingBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let setting = graphStoryboard.instantiateViewControllerWithIdentifier("setting") as! SettingsViewController
        
        self.navigationController?.pushViewController(setting, animated: false)

    }
   
    @IBAction func newEntryBtn(sender: AnyObject)
    {
         packGoAndInventoryView.hidden = false
    }
    
    
    
    
    @IBAction func inventoryBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var newEntryStoryboard = appDeleagte.newEntryStoryboard() as UIStoryboard
        
        let newEntryPackGo = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryPackGo") as! NewEntryPackAndGoViewController
        
        newEntryPackGo.titleStr = "Inventory Log"
        startPackingTitleStr = "START INVENTORY"
        
        newEntryPackGoOpenTitleStr = "View:Inventory"
        fullMenuDateTitleStr = "View:Inventory"
        strInventoryOrPackageTitle = "Inventory"

        self.navigationController?.pushViewController(newEntryPackGo, animated: true)

    }
    
    
    
    @IBAction func packGoBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var newEntryStoryboard = appDeleagte.newEntryStoryboard() as UIStoryboard
        
        let newEntryPackGo = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryPackGo") as! NewEntryPackAndGoViewController
        startPackingTitleStr = "START PACKING"
        newEntryPackGo.titleStr = "Pack&Go Log"
        newEntryPackGoOpenTitleStr = "View:Pack&Go Packages"
        fullMenuDateTitleStr = "View:Pack&Go Packages"
        self.navigationController?.pushViewController(newEntryPackGo, animated: true)

    }
    

    @IBAction func reportBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let graphTrack = graphStoryboard.instantiateViewControllerWithIdentifier("graphTrack") as! GraphTrackViewController
        
        self.navigationController?.pushViewController(graphTrack, animated: false)
        

    }
    
    
    @IBAction func quickViewBtn(sender: AnyObject)
    {
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDeleagte.viewInventoryStoryboard() as UIStoryboard
        
        let scanView = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("scanView") as! FullMenuScanViewController
        
        self.navigationController?.pushViewController(scanView, animated: true)
        
    }
    
    
    @IBAction func confirmedReceived(sender: AnyObject)
    {
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDeleagte.viewInventoryStoryboard() as UIStoryboard
//        
//        let confirmedReceived = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("confirmedReceived") as! InventoryConfirmedReceivedViewController
        
        let confirmedReceived = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("ConfirmedReceivedListViewController") as! ConfirmedReceivedListViewController
        
        self.navigationController?.pushViewController(confirmedReceived, animated: true)

        
    }
    
    @IBAction func fullMenuBtn(sender: AnyObject)
    {
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDeleagte.viewInventoryStoryboard() as UIStoryboard
        
        let fullmenu = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("fullMenu") as! InventoryFullMenuViewController
        
        self.navigationController?.pushViewController(fullmenu, animated: true)
        
    }
    
    
    
    
    
    @IBAction func searchGobtn(sender: AnyObject)
    {
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDeleagte.viewInventoryStoryboard() as UIStoryboard
        
        let viewInventorySearch = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("viewInventorySearch") as! ViewInventorySearchViewController
        
        
        
        searchTextStr = self.keyWordTxtField.text
        
        
        self.navigationController?.pushViewController(viewInventorySearch, animated: true)

        
    }
    
    
    
    
    
    
    
    //MARK:- Touch Method
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        
       keyWordTxtField.resignFirstResponder()
        packGoAndInventoryView.hidden = true
    }
    
    
    
    //MARK:- TextField Dlegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        if textField == keyWordTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 120
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y -= 100
            }
            if sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y -= 90
            }
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad
            {
                
                view.frame.origin.y -= 70
            }
        }
            
        else
        {
            view.frame.origin.y = view.frame.origin.y
        }
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if textField == keyWordTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 120
                
            }
            
            if sizeHeight == 568
            {
                view.frame.origin.y += 100
            }
            if sizeHeight == 667 || sizeHeight == 736
            {
                view.frame.origin.y += 90
            }
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad
            {
                
                view.frame.origin.y += 70
            }

            
            
        }
        return true
        
    }

    

    
    
}
