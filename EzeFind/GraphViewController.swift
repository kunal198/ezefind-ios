//
//  GraphViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/2/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


//var packGoBtnCheck = String()
var typeBtnCheck = String()

class GraphViewController: UIViewController
{

    @IBOutlet var packGoAndInventoryView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
        UIApplication.sharedApplication().statusBarHidden = false
        
    }

    
    
    override func viewWillAppear(animated: Bool)
    {
        
       userId = NSUserDefaults.standardUserDefaults().integerForKey("userId")
        
        println("user id in local storage in viewwillapp = \(userId)")
        
        
        if userId == 0
        {
            var alert = UIAlertView()
            
            alert = UIAlertView(title:"Alert", message: "User ID is not found", delegate: self, cancelButtonTitle:"OK")
            
            alert.show()
        }
    }
    
    
    
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func reportBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let graphTrack = graphStoryboard.instantiateViewControllerWithIdentifier("graphTrack") as! GraphTrackViewController
        
        self.navigationController?.pushViewController(graphTrack, animated: false)

    
    }
    
    
    
    @IBAction func logoutBtn(sender: AnyObject)
    {
        
        var loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let login = loginStoryboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        
        self.navigationController?.pushViewController(login, animated: false)
        
    }
    
    
    
    @IBAction func settingBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let setting = graphStoryboard.instantiateViewControllerWithIdentifier("setting") as! SettingsViewController
        
        self.navigationController?.pushViewController(setting, animated: false)
        

        
    }
    
    
    @IBAction func newInventoryBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let viewInventory = graphStoryboard.instantiateViewControllerWithIdentifier("viewInventory") as! ViewInventoryViewController
        
        self.navigationController?.pushViewController(viewInventory, animated: false)
        
    }
    
    
    
    @IBAction func NewEntryBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = false
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
         packGoAndInventoryView.hidden = true
    }
    
    
    @IBAction func inventoryBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
       
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var newEntryStoryboard = appDeleagte.newEntryStoryboard() as UIStoryboard
        
        
        typeBtnCheck = "inventory"
        
        let newEntryPackGo = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryPackGo") as! NewEntryPackAndGoViewController
       
        newEntryPackGo.titleStr = "Inventory Log"
        
        startPackingTitleStr = "START INVENTORY"
        
        newEntryPackGoOpenTitleStr = "View:Inventory"
        fullMenuDateTitleStr = "View:Inventory"
        strInventoryOrPackageTitle = "Inventory"
        self.navigationController?.pushViewController(newEntryPackGo, animated: true)

        
    }
    
    
    
    @IBAction func packGoBtn(sender: AnyObject)
    {
        packGoAndInventoryView.hidden = true
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var newEntryStoryboard = appDeleagte.newEntryStoryboard() as UIStoryboard
        
        
        typeBtnCheck = "package"
        
        let newEntryPackGo = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryPackGo") as! NewEntryPackAndGoViewController
         startPackingTitleStr = "START PACKING"
        newEntryPackGo.titleStr = "Pack&Go Log"
       newEntryPackGoOpenTitleStr = "View:Pack&Go Packages"
        fullMenuDateTitleStr = "View:Pack&Go Packages"
        
        strInventoryOrPackageTitle = "Package"
        self.navigationController?.pushViewController(newEntryPackGo, animated: true)
        

        
    }
    
    
    
    
    
    
}
