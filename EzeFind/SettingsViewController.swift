//
//  SettingsViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/2/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
var entrySettingStr = String()
class SettingsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    
    @IBOutlet var settingTableView: UITableView!
    
    var fieldArray = NSArray()
     var valueArray = [String]()
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        
        
        fieldArray = ["PROFILE","ACCOUNT","CHANGE PASSWORD","ENTRY SETTING","NETWORK STATUS","SAVE SETTING","I_MEMBER","PICKLIST"]
        
        
       // println("entrySettingStr= \(entrySettingStr)")
        
        
    }
    
        
        override func viewWillAppear(animated: Bool)
        {
         //println("entrySettingStr= \(entrySettingStr)")
            
            if NSUserDefaults.standardUserDefaults().boolForKey("textValue")
            {
                entrySettingStr = "TEXT"
                
                
            }
            if NSUserDefaults.standardUserDefaults().boolForKey("audioOnlyValue")
            {
               entrySettingStr = "AUDIO ONLY"
                
                
            }
            if NSUserDefaults.standardUserDefaults().boolForKey("textAndAudioValue")
            {
                entrySettingStr = "TEXT AND AUDIO"
            }

            
            
            
            
            
            
       valueArray = ["","","",entrySettingStr,"","","",""]
        
            if Reachability.isConnectedToNetwork() == true
            {
                valueArray.insert("CONNECTED", atIndex: 4)
            }
            else
            {
                
                valueArray.insert("NOT CONNECTED", atIndex: 4)
            }
            
        settingTableView.reloadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
     return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return fieldArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! SettingsTableViewCell
        
        
        cell.fieldNameLbl.text = fieldArray[indexPath.row] as? String
        
        cell.valueNameLbl.text = ""
        if var value = valueArray[indexPath.row] as? String
        {
        cell.valueNameLbl.text = value
        }
        
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        var str = fieldArray[indexPath.row] as! String
        
        
        if str as String == "PROFILE"
        {
        
        
        
            

            let profile = graphStoryboard.instantiateViewControllerWithIdentifier("profile") as! ProfileAddressViewController
            
            self.navigationController?.pushViewController(profile, animated: true)
            
            
        }
        
        else if str == "ACCOUNT"
        {
            let account = graphStoryboard.instantiateViewControllerWithIdentifier("account") as! AccountViewController
            
            self.navigationController?.pushViewController(account, animated: true)

            
        }
        else if str == "CHANGE PASSWORD"
        {
            let changePwd = graphStoryboard.instantiateViewControllerWithIdentifier("changePwd") as! ChangePwdViewController
            
            self.navigationController?.pushViewController(changePwd, animated: true)
            
        }
        else if str == "ENTRY SETTING"
        {
            let entrySetting = graphStoryboard.instantiateViewControllerWithIdentifier("entrySetting") as! EntrySettingViewController
            
            self.navigationController?.pushViewController(entrySetting, animated: true)
            
            
            
        }
        else if str == "NETWORK STATUS"
            
        {
            let networkStatus = graphStoryboard.instantiateViewControllerWithIdentifier("networkStatus") as! NetworkSatusViewController
            
            self.navigationController?.pushViewController(networkStatus, animated: true)
        }
        
        else if str == "SAVE SETTING"
        {
            let saveSetting = graphStoryboard.instantiateViewControllerWithIdentifier("saveSetting") as! SaveSettingViewController
            
            self.navigationController?.pushViewController(saveSetting, animated: true)
        }
        
        else if str == "I_MEMBER"
        {
            
            let iMember = graphStoryboard.instantiateViewControllerWithIdentifier("iMember") as! ImemberViewController
            
            self.navigationController?.pushViewController(iMember, animated: true)

            
        }
        else
        {
            let pickList = graphStoryboard.instantiateViewControllerWithIdentifier("pickList") as! PickListViewController
            
            picklistView = "pickListViewSetting"
            
            self.navigationController?.pushViewController(pickList, animated: true)
            
            
        }
    }
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

    }
    
   
}
