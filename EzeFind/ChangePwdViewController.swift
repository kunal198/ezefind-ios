//
//  ChangePwdViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ChangePwdViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var currentPwdTxtField: UITextField!
    @IBOutlet var newPwdTxtField: UITextField!
    @IBOutlet var reTupePwdTxtField: UITextField!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        currentPwdTxtField.delegate = self
        newPwdTxtField.delegate = self
        reTupePwdTxtField.delegate = self
    }

    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    
      //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

    }
    
    //MARK:- Done button
    
    @IBAction func doneBtn(sender: AnyObject)
    {
        if currentPwdTxtField.text == "" || newPwdTxtField.text == "" || reTupePwdTxtField.text == ""
        {
            var alert = UIAlertView(title: "Alert", message: "Please fill the required fields", delegate: self, cancelButtonTitle: "OK")
            alert.show()

        }
        else if count(newPwdTxtField.text) < 7
        {
            var alert = UIAlertView(title: "Alert", message: "Password too short", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
        }
        else if newPwdTxtField.text != reTupePwdTxtField.text
        {
            var alert = UIAlertView(title: "Alert", message: "Re-type Password Incorrect", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
        }
            
            
        else
        {
        
         var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            spinningIndicator.labelText = "Loading"
         
            
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&OldPassword=%@&NewPassword=%@",str,currentPwdTxtField.text,newPwdTxtField.text)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/change-password"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                spinningIndicator.hide(true)
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    println("ChangePwd = \(dictObj)")
                    
                    
                    if error != nil
                    {
                        println("\(error?.localizedDescription)")
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        })

                    }
                    else
                    {
                        var success = dictObj?.valueForKey("success") as! Bool
                        var message = dictObj?.valueForKey("message") as! String
                        
                        if success
                        {
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        else
                            
                        {
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }

                        
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()

        }
    }
    
    
    //MARK:- Touch Method
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        currentPwdTxtField.resignFirstResponder()
        newPwdTxtField.resignFirstResponder()
        reTupePwdTxtField.resignFirstResponder()
    }
    
    
    
    
    //MARK:- TextField Dlegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }

    
    
}
