//
//  NewEntryAddGpsLocationViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/11/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

var locationNameInGPS = String()

var locationNameCheck = String()

var alertViewApi = UIAlertView()

class NewEntryAddGpsLocationViewController: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate,UITextFieldDelegate,UIAlertViewDelegate
{
    
    
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var locationFurtherView: UIView!
    @IBOutlet weak var cautionView: UIView!
    @IBOutlet weak var memberView: UIView!
    @IBOutlet weak var ezeBarcodeView: UIView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var blackiew: UIView!
    @IBOutlet var dateTitle: UILabel!
    @IBOutlet var detailsTxtField: UITextField!
    @IBOutlet var locationNameTxtField: UITextField!
    @IBOutlet var longitudeTxtField: UITextField!
    @IBOutlet var latitudeTxtField: UITextField!
    @IBOutlet var distanceTxtField: UITextField!
    
     var binBoxGreenTitleStr = NSString()
    var newEntryCategoryArray = NSMutableArray()
    
    
    
    var title_picklist = String()
    var type_picklist = String()
    var id_picklist = String()
    var distance_share = String()
    
    
    var locationName = String()
    var locationDescription = String()
    
    var bagCount = Int()
    var binBoxCountBool = Bool()
    var binBoxCount = Int()
    
    var value = Bool()
    var i = Int()
    var longStr = NSString()
    var latStr = NSString()
    
    @IBOutlet var mapView: MKMapView!
    var locationManager = CLLocationManager()
    var location = CLLocationCoordinate2D()
    
    
    var sizeHeight = UIScreen.mainScreen().bounds.height

    
    var alertViewImage = UIAlertView()
    
    
    @IBOutlet weak var typeBagBoxItemLbl: UILabel!
    
    @IBOutlet var gpsLocationView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let date = NSDate()
        
        var dateFormat = NSDateFormatter()
        
        dateFormat.dateFormat = "dd-MM-yyyy"
        
        var dateInFormat:NSString = dateFormat.stringFromDate(date)
        
        println(dateInFormat)
        
        dateTitle.text = dateInFormat as String
        
        locationNameTxtField.delegate = self
        detailsTxtField.delegate = self
        
        newCategoryTxtField.delegate = self
        locationFurtherTxtField.delegate = self
        alertViewImage.delegate  = self
        //imagePicker.delegate = self

        
        
        let paddingViewLocationName = UIView(frame: CGRectMake(0, 0, 5, locationNameTxtField.frame.size.height))
        locationNameTxtField.leftView = paddingViewLocationName
        locationNameTxtField.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewDetail = UIView(frame: CGRectMake(0, 0, 5, detailsTxtField.frame.size.height))
        detailsTxtField.leftView = paddingViewDetail
        detailsTxtField.leftViewMode = UITextFieldViewMode.Always

        let paddingViewLong = UIView(frame: CGRectMake(0, 0, 5, longitudeTxtField.frame.size.height))
        longitudeTxtField.leftView = paddingViewLong
        longitudeTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewLat = UIView(frame: CGRectMake(0, 0, 5, latitudeTxtField.frame.size.height))
        latitudeTxtField.leftView = paddingViewLat
        latitudeTxtField.leftViewMode = UITextFieldViewMode.Always

        let paddingViewDistance = UIView(frame: CGRectMake(0, 0, 5, distanceTxtField.frame.size.height))
        distanceTxtField.leftView = paddingViewDistance
        distanceTxtField.leftViewMode = UITextFieldViewMode.Always

        
        
        i = 0
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        mapView.delegate = self
        
        
        
        
        
//        
//        let paddingViewEzeBarcode = UIView(frame: CGRectMake(0, 0, 5, ezeBarcodeTxtField.frame.size.height))
//        ezeBarcodeTxtField.leftView = paddingViewEzeBarcode
//        ezeBarcodeTxtField.leftViewMode = UITextFieldViewMode.Always
//        
//        let paddingViewOtherBarcode = UIView(frame: CGRectMake(0, 0, 5, otherBarcodeTxtField.frame.size.height))
//        otherBarcodeTxtField.leftView = paddingViewOtherBarcode
//        otherBarcodeTxtField.leftViewMode = UITextFieldViewMode.Always
//        
//        let paddingViewDescription = UIView(frame: CGRectMake(0, 0, 5, descripitionTxtField.frame.size.height))
//        descripitionTxtField.leftView = paddingViewDescription
//        descripitionTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewMemberInfo = UIView(frame: CGRectMake(0, 0, 5, memberInfoTxtField.frame.size.height))
        memberInfoTxtField.leftView = paddingViewMemberInfo
        memberInfoTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewNewCategory = UIView(frame: CGRectMake(0, 0, 5, newCategoryTxtField.frame.size.height))
        newCategoryTxtField.leftView = paddingViewNewCategory
        newCategoryTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewLocationFurtherTitle = UIView(frame: CGRectMake(0, 0, 5, locationFurtherTitle.frame.size.height))
        locationFurtherTitle.leftView = paddingViewLocationFurtherTitle
        locationFurtherTitle.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewLocationFurther = UIView(frame: CGRectMake(0, 0, 5, locationFurtherTxtField.frame.size.height))
        locationFurtherTxtField.leftView = paddingViewLocationFurther
        locationFurtherTxtField.leftViewMode = UITextFieldViewMode.Always
        
        
        newCategoryTxtField.layer.borderColor = UIColor.grayColor().CGColor
        newCategoryTxtField.layer.borderWidth = 1.0
        
        locationFurtherTitle.layer.borderColor = UIColor.grayColor().CGColor
        locationFurtherSaveBtn.layer.borderColor = UIColor.whiteColor().CGColor
        locationFurtherTxtField.layer.borderColor = UIColor.grayColor().CGColor
        
        
        
        
//        
//        scrollView.contentSize.height = otherBarcodeView.frame.origin.y + otherBarcodeView.frame.size.height
//        bagTxtField.layer.borderColor = UIColor.grayColor().CGColor
//        ezeBarcodeTxtField.layer.borderColor = UIColor.grayColor().CGColor
//        
//        memberTxtField.layer.borderColor = UIColor.grayColor().CGColor
//        categoryTxtField.layer.borderColor = UIColor.grayColor().CGColor
//        descripitionTxtField.layer.borderColor = UIColor.grayColor().CGColor
//        cautionTxtField.layer.borderColor = UIColor.grayColor().CGColor
//        locationTxtField.layer.borderColor = UIColor.grayColor().CGColor
//        otherBarcodeTxtField.layer.borderColor = UIColor.grayColor().CGColor
        
        
        addPhotoBtn.layer.borderColor = UIColor.whiteColor().CGColor
        saveNowMemberInfo.layer.borderColor = UIColor.whiteColor().CGColor
        memberInfoTxtField.layer.borderColor = UIColor.blueColor().CGColor
        imageViewMemberInfo.layer.cornerRadius = imageViewMemberInfo.frame.size.width/2
        imageViewMemberInfo.layer.masksToBounds = true
        
        
        
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(NewEntryGraphToFurtherBagViewController)
            {
                
//                 var savedBagCount: Int = NSUserDefaults.standardUserDefaults().valueForKey("bagCount") as! Int
//                
//                var incremntedValue:Int = savedBagCount + bagCount
//                println(incremntedValue)
//                NSUserDefaults.standardUserDefaults().setInteger(incremntedValue, forKey: "bagCount")
//                
//                self.typeBagBoxItemLbl.text = NSString(format: "Bag %@", toString(incremntedValue)) as String
                
                self.typeBagBoxItemLbl.text = "Bag"
                
                break
            }
            else if controller.isKindOfClass(NewEntryGraphBinViewController)
            {
                
                
                  if typeValue == "box"
                  {
                    self.typeBagBoxItemLbl.text = "Box"
                  }
                  else
                  {
                    self.typeBagBoxItemLbl.text = "Bin"
                  }

                
                break
            }
            else if controller.isKindOfClass(NewEntryGraphSingleItemViewController)
            {
                self.typeBagBoxItemLbl.text = "Single Item"
                break
            }
            else
            {
                
            }
            
        }

        
        
    }
    
    
    
    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    class CustomPointAnnotation:MKPointAnnotation
    {
        var imageName: String!
        
    }
    
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
        
        
        var userLocation:CLLocation = locations[0] as! CLLocation
        let long = userLocation.coordinate.longitude;
        let lat = userLocation.coordinate.latitude;
        
        longStr = NSString(format: "%f", userLocation.coordinate.longitude)
        latStr = NSString(format: "%f", userLocation.coordinate.latitude)
        println(longStr)
        println(latStr)
        location = CLLocationCoordinate2D(
            latitude: lat, longitude: long)
        
        
        let regionRadius: CLLocationDistance = 1000
        let span = MKCoordinateSpanMake(0.05, 0.05)
        var region = MKCoordinateRegion(center: location, span: span)
        
        region = MKCoordinateRegionMakeWithDistance(location, regionRadius * 0.2, regionRadius * 0.2)
        mapView.setRegion(region, animated: true)
        
        var info = CustomPointAnnotation()
        info.coordinate = CLLocationCoordinate2DMake(lat, long)
        
        info.imageName = "ic_marker_.png"
        
        mapView.addAnnotation(info)
        
        
        locationManager.stopUpdatingLocation()
        
        if i == 0
        {
            
            longAndLatiApi()
        }
        
        i++
        
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView!
    {
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if anView == nil
        {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView.canShowCallout = true
        }
        else
        {
            anView.annotation = annotation
        }
        
        
        
        var cpa = annotation as! CustomPointAnnotation
        anView.image = UIImage(named:cpa.imageName)
        
        return anView
    }
    
    
    
    
    
    
    //MARK:- Back Button
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
        barButtonCheck = "1"
        
        
       // self.navigationController?.popViewControllerAnimated(true)
        
//        self.blackiew.hidden = false
//        self.locationView.hidden = false
//        
//        
//        self.categoryView.hidden = true
//        self.confirmView.hidden = true
//        self.locationFurtherView.hidden = true
//        self.ezeBarcodeView.hidden = true
//        
//        self.memberView.hidden = true
//        self.cautionView.hidden = true
        
    }
    
    
    
    
    
    @IBAction func otherEzeBarcodeBtn(sender: AnyObject)
    {
        value = false
        blackiew.hidden = false
        ezeBarcodeView.hidden = false
        memberView.hidden = true
        categoryView.hidden = true
        cautionView.hidden = true
        locationView.hidden = true
        locationFurtherView.hidden = true
        confirmView.hidden = true
    }
    
    
    @IBAction func ezeBacodeBtn(sender: AnyObject)
    {
        value = true
        blackiew.hidden = false
        ezeBarcodeView.hidden = false
        memberView.hidden = true
        categoryView.hidden = true
        cautionView.hidden = true
        locationView.hidden = true
        locationFurtherView.hidden = true
        confirmView.hidden = true
        
    }
    
    @IBAction func okBtn(sender: AnyObject)
    {
        blackiew.hidden = true
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let newEntryScanBarcode = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryScanBarcode") as! NewEntryScanBarcodeViewController
        newEntryScanBarcode.greenTitleStr = binBoxGreenTitleStr
        self.navigationController?.pushViewController(newEntryScanBarcode, animated: false)
        
    }
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
    }
    
    
    
    
    
    @IBAction func shareMapDetails(sender: AnyObject)
    {
        
        
        title_picklist = latStr as! String
        println("title at index 0 = \(title_picklist)")
        
        
        
        type_picklist = longStr as! String
        println("type at index 0 = \(type_picklist)")
        
        
        distance_share = self.distanceTxtField.text as String
        
        
        locationName = self.locationNameTxtField.text as String
        
        locationDescription = self.detailsTxtField.text as String
        
        
//        id_picklist = self.dataPackageOrInventoryArray[indexPath.row]["PicklistId"] as! String
//        println("id at index 0 = \(id_picklist)")
//
        
        
        //let firstActivityItem = "my text"
        
        let activityViewController : UIActivityViewController = UIActivityViewController(activityItems: [title_picklist,type_picklist,distance_share,locationName,locationDescription], applicationActivities: nil)
        
        self.navigationController!.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    
    
    
    
    
    
//    @IBAction func colorBtn(sender: AnyObject)
//    {
//        locationFurtherView.hidden = true
//        memberInfoview.hidden = true
//        blackiew.hidden = false
//        ezeBarcodeView.hidden = true
//        colorView.hidden = false
//        cautionView.hidden = true
//        locationView.hidden = true
//        categoryView.hidden = true
//        confirmView.hidden = true
//    }
    
    
    
    
    
    @IBAction func cautionBtn(sender: AnyObject)
    {
        blackiew.hidden = false
        ezeBarcodeView.hidden = true
        memberView.hidden = true
        categoryView.hidden = true
        cautionView.hidden = false
        locationView.hidden = true
        locationFurtherView.hidden = true
        confirmView.hidden = true
    }
    
//    @IBAction func locationBtn(sender: AnyObject)
//        
//    {
//        locationFurtherView.hidden = true
//        blackiew.hidden = false
//        memberInfoview.hidden = true
//        ezeBarcodeView.hidden = true
//        colorView.hidden = true
//        cautionView.hidden = true
//        locationView.hidden = false
//        categoryView.hidden = true
//        confirmView.hidden = true
//    }
    
    
    
    @IBAction func categoryBtn(sender: AnyObject)
    {
        blackiew.hidden = false
        ezeBarcodeView.hidden = true
        memberView.hidden = true
        categoryView.hidden = false
        cautionView.hidden = true
        locationView.hidden = true
        locationFurtherView.hidden = true
        confirmView.hidden = true
       // getCategoryApi()
        
    }
    
    
    @IBAction func cancelCrossbtn(sender: AnyObject)
    {
        blackiew.hidden = true
    }
    
//    @IBAction func redBtn(sender: AnyObject)
//    {
//        blackiew.hidden = true
//        colorTxtField.text = redLbl.text
//    }
//    
//    @IBAction func yellowBtn(sender: AnyObject)
//    {
//        blackiew.hidden = true
//        colorTxtField.text = yellowLbl.text
//    }
//    
//    @IBAction func blueBtn(sender: AnyObject)
//    {
//        blackiew.hidden = true
//        colorTxtField.text = blueLbl.text
//    }
//    
//    
//    @IBAction func purpleBtn(sender: AnyObject)
//    {
//        blackiew.hidden = true
//        colorTxtField.text = purpleLbl.text
//    }
//    
//    
//    
//    @IBAction func greenBtn(sender: AnyObject)
//    {
//        blackiew.hidden = true
//        colorTxtField.text = grrenLbl.text
//    }
//    
//    
    
    @IBAction func cautionFragileBtn(sender: AnyObject)
    {
        blackiew.hidden = true
        //cautionTxtField.text = cautionFragileLbl.text
    }
    
    
    
    @IBAction func cautionSharpBtn(sender: AnyObject)
    {
        blackiew.hidden = true
        //cautionTxtField.text = cautionSharpLbl.text
    }
    
    
    @IBAction func cautionHeavyBtn(sender: AnyObject)
    {
        blackiew.hidden = true
        //cautionTxtField.text = cautionHeavyLbl.text
        
    }
    
    @IBOutlet weak var cautionLiquidLbl: UILabel!
    
    
    
    @IBOutlet weak var cautionHeavyLbl: UILabel!
    
    
    
    @IBOutlet weak var cautionSharpLbl: UILabel!
    
    
    
    @IBAction func cautionLiquidBtn(sender: AnyObject)
    {
        blackiew.hidden = true
       // cautionTxtField.text = cautionLiquidLbl.text
        
    }
    

    
    
    @IBOutlet weak var imageViewMemberInfo: UIImageView!
    
    @IBOutlet weak var locationPlaceLbl: UILabel!
    
    
    
    
    //MARK:- Member Button
    
    @IBAction func memberBtn(sender: AnyObject)
    {
        blackiew.hidden = false
        ezeBarcodeView.hidden = true
        memberView.hidden = false
        categoryView.hidden = true
        cautionView.hidden = true
        locationView.hidden = true
        locationFurtherView.hidden = true
        confirmView.hidden = true
    }
    
    
    
    @IBAction func memberInfoBackbtn(sender: AnyObject)
    {
        
        blackiew.hidden = true
    }
    
    
    
    @IBAction func memberInfoSaveNowBtn(sender: AnyObject)
    {
        blackiew.hidden = true
        //memberTxtField.text = memberInfoTxtField.text
        //self.navigationController?.popToViewController(NewEntryGraphBinViewController(), animated: false)
    }
    

    
    @IBOutlet weak var cautionFragileLbl: UILabel!
    
    @IBOutlet weak var locationFurtherTitle: UITextField!
    
    
    @IBOutlet weak var newCategoryView: UIView!
    
    
    @IBOutlet weak var newCategoryTxtField: UITextField!
    
    
    //MARK:- Location Button
    @IBAction func locationBtn(sender: AnyObject)
    {
        gpsLocationView.hidden = true
    }
    
    
    
    
    @IBOutlet weak var saveNowMemberInfo: UIButton!
    
    
    @IBOutlet weak var memberInfoTxtField: UITextField!
    
    @IBOutlet weak var locationShelfLbl: UILabel!
    
    @IBOutlet weak var locationNearlbl: UILabel!
    
    
    
    
    @IBAction func locationPlaceBtn(sender: AnyObject)
    {
        locationFurtherView.hidden = false
        locationFurtherTitle.text = locationPlaceLbl.text
        
    }
    
    @IBAction func locationShelfBtn(sender: AnyObject)
    {
        locationFurtherView.hidden = false
        locationFurtherTitle.text = locationShelfLbl.text
    }
    
    @IBAction func locationDrawBtn(sender: AnyObject)
    {
        locationFurtherView.hidden = false
        locationFurtherTitle.text = locationDrawLbl.text
    }
    
    @IBOutlet weak var locationDrawLbl: UILabel!
    
    @IBOutlet weak var locationNearLbl: UILabel!
      
    @IBAction func locatiojnNearBtn(sender: AnyObject)
    {
        locationFurtherView.hidden = false
        locationFurtherTitle.text = locationNearLbl.text
    }
    
    @IBAction func locationGpsBtn(sender: AnyObject)
    {
        self.blackiew.hidden = true
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let newEntryGpsLocation = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryGpsLocation") as! NewEntryAddGpsLocationViewController
        
        self.navigationController?.pushViewController(newEntryGpsLocation, animated: false)
        
        
        
    }
    
    
    @IBOutlet weak var addPhotoBtn: UIButton!
    
    //MARK:- Member Add Photo Button
    
    
    @IBAction func addPhotoBtn(sender: AnyObject)
    {
        alertViewImage = UIAlertView(title: "Alert", message: "Choose the Source", delegate: self, cancelButtonTitle: "Camera", otherButtonTitles: "Photo Gallery")
        alertViewImage.show()
        
        
    }
    
    

    
    
    
    
    @IBOutlet weak var locationAddNewLbl: UILabel!
    
    
    
    @IBAction func locationAddBtn(sender: AnyObject)
    {
        locationFurtherView.hidden = false
        locationFurtherTitle.text = locationAddNewLbl.text
    }
    
    
    
    @IBAction func locationFurtherSaveBtn(sender: AnyObject)
    {
       // blackiew.hidden = true
       // locationTxtField.text = locationFurtherTxtField.text
       // locationSaveForOtherCaseApi()
        
        
        //self.navigationController?.popToRootViewControllerAnimated(false)
        
        //self.navigationController?.popToViewController(NewEntryGraphBinViewController(), animated: true)
        
        
        let backView = storyboard?.instantiateViewControllerWithIdentifier("graphToFurtherBin") as! NewEntryGraphBinViewController
        
       // self.navigationController?.popToViewController(backView, animated: true)
         self.navigationController?.pushViewController(backView, animated: true)
        
       // blackiew.hidden = true
    }
    

    @IBOutlet weak var locationFurtherSaveBtn: UIButton!
    
    
    @IBOutlet weak var newEntryCategoryTableView: UITableView!
    
    @IBOutlet weak var locationFurtherTxtField: UITextField!
    
    
    
    //MARK: - CategoryTableViewMethods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return newEntryCategoryArray.count
    }
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var newEntryCategoryCell = tableView.dequeueReusableCellWithIdentifier("newEntryCategoryCell", forIndexPath: indexPath) as! NewEntryCategoryTableViewCell
        
        newEntryCategoryCell.categoryLbl.text = newEntryCategoryArray[indexPath.row].valueForKey("name") as? String
        
        return newEntryCategoryCell
        
    }
    
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        //categoryTxtField.text = newEntryCategoryArray[indexPath.row].valueForKey("name") as! String
        blackiew.hidden = true
    }
    
    
    
    
    //MARK:- CategoryAddNewBtn
    
    @IBAction func categoryAddNewBtn(sender: AnyObject)
    {
        blackiew.hidden = true
        //newCategoryView.hidden = false
        
    }
    
    
    //MARK:- NewCategorySaveBtn
    
    @IBAction func newCategorySaveBtn(sender: AnyObject)
    {
        //createCategory()
        //categoryTxtField.text = newCategoryTextField.text
        newCategoryView.hidden = true
    }
    
    

    @IBAction func cancelBtn_tableView(sender: AnyObject)
    {
        self.blackiew.hidden = true
        self.categoryView.hidden = true
    }
    
    
    
    
    //MARK:- Touch Method
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        //descripitionTxtField.resignFirstResponder()
        memberInfoTxtField.resignFirstResponder()
        newCategoryTxtField.resignFirstResponder()
        locationFurtherTxtField.resignFirstResponder()
        
    }

    
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
//        if textField == descripitionTxtField
//        {
//            if sizeHeight == 480
//            {
//                
//                view.frame.origin.y -= 150
//                
//            }
//            
//            if sizeHeight == 568
//            {
//                view.frame.origin.y -= 100
//            }
//            
//            
//        }
        
        if textField == memberInfoTxtField || textField == newCategoryTxtField || textField == locationFurtherTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y -= 80
                
            }
            
            if textField == memberInfoTxtField
            {
                if sizeHeight == 568
                {
                    view.frame.origin.y -= 100
                }
            }
            
            
        }
            
        else
        {
            view.frame.origin.y = view.frame.origin.y
        }
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
//        if textField == descripitionTxtField
//        {
//            if sizeHeight == 480
//            {
//                
//                view.frame.origin.y += 150
//                
//            }
//            
//            if sizeHeight == 568
//            {
//                view.frame.origin.y += 100
//            }
//            
//            
//            
//        }
        
            
        if textField == memberInfoTxtField || textField == newCategoryTxtField || textField == locationFurtherTxtField
        {
            if sizeHeight == 480
            {
                
                view.frame.origin.y += 80
                
            }
            
            
            if textField == memberInfoTxtField
            {
                if sizeHeight == 568
                {
                    view.frame.origin.y += 100
                }
            }
            
            
            
            
        }
            
        else
        {
            view.frame.origin.y = view.frame.origin.y
        }
        return true
        
        
        
    }
    
    
    
    
    
    
    //MARK:- Save Button
    
    @IBAction func saveBtn(sender: AnyObject)
    {
        var alert = UIAlertView(title: "Alert", message: "Are you sure for save", delegate: self, cancelButtonTitle: "NO", otherButtonTitles: "YES")
        alert.show()
    }
    
    
    //MARK:- Alert Methods
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            
            if (self.longitudeTxtField.text != nil) && (self.latitudeTxtField.text != nil)
            {
                saveLocationApi()
                
                locationNameCheck = "1"
                println("long value = \(self.longitudeTxtField.text)")
                println("lat value = \(self.latitudeTxtField.text)")
            }
            else
            {
                println("nil values")
            }
            
        }
        
        
        if alertViewApi.tag == 5
        {
            for controller in self.navigationController!.viewControllers as Array
            {
                if controller.isKindOfClass(NewEntryGraphToFurtherBagViewController)
                {
                    self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                    break
                }
                else if controller.isKindOfClass(NewEntryGraphBinViewController)
                {
                    self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                    break
                }
                else if controller.isKindOfClass(NewEntryGraphSingleItemViewController)
                {
                    self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                    break
                }
                else if controller.isKindOfClass(NewEntryGraphBinViewController)
                {
                    self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                    break
                }
                else
                {
                    
                }
                
            }
            

        }
    
        
    }
    
    
    
    
    //MARK:- Cancel Button
    
    @IBAction func cancelBtn(sender: AnyObject)
    {
       // self.navigationController?.popToRootViewControllerAnimated(false)
        
        
        barButtonCheck = "5"
        
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(NewEntryGraphToFurtherBagViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                break
            }
            else if controller.isKindOfClass(NewEntryGraphBinViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                break
            }
            else if controller.isKindOfClass(NewEntryGraphSingleItemViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                break
            }
            else if controller.isKindOfClass(NewEntryGraphBinViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: true)
                break
            }
            else
            {
                
            }
            
        }
        
        
        
        
//        if  typeValue == "bag"
//        {
//            
//            var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
//            var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
//            
//            let graphToFurtherBag = newEntryStoryboard.instantiateViewControllerWithIdentifier("graphToFurtherBag") as! NewEntryGraphToFurtherBagViewController
//            
//            //self.navigationController?.pushViewController(graphToFurtherBag, animated: true)
//            
//            self.navigationController?.popToViewController(graphToFurtherBag, animated: true)
//            
//        }
//        else if  typeValue == "box"
//        {
//            var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
//            var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
//            
//            
//            let graphToFurtherBin = newEntryStoryboard.instantiateViewControllerWithIdentifier("graphToFurtherBin") as! NewEntryGraphBinViewController
//            
//            
//            self.navigationController?.popToViewController(graphToFurtherBin, animated: true)
//            
//        }
//        else if  typeValue == "singleItem"
//        {
//            var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
//            var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
//            
//            let graphToFurtherSingleItem = newEntryStoryboard.instantiateViewControllerWithIdentifier("graphToFurtherSingleItem") as! NewEntryGraphSingleItemViewController
//            
//            self.navigationController?.popToViewController(graphToFurtherSingleItem, animated: true)
//            
//        }
//        else if  typeValue == "bin"
//        {
//            var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
//            var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
//            
//            let graphToFurtherBin = newEntryStoryboard.instantiateViewControllerWithIdentifier("graphToFurtherBin") as! NewEntryGraphBinViewController
//            
//            self.navigationController?.popToViewController(graphToFurtherBin, animated: true)
//            
//        }
//        else
//        {
//            
//        }
//        

    }
    
    
    
    
    //MARK:- Long&LatiApi
    
    func longAndLatiApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"

        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        let packageId = userDefaults.valueForKey("packageId") as? String
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PackageId=%@&Lat=%@&Long=%@",str,packageId!,latStr,longStr)
        
        println(post)
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/distance")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                            alert.show()

                            spinningIndicator.hide(true)
                        })


                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            
                            if success
                            {
                                var distanceStr = dicObj?.valueForKey("distance") as! Float
                                println(distanceStr)
                                self.distanceTxtField.text = NSString(format: "%f", distanceStr) as String
                                self.longitudeTxtField.text = self.longStr as String
                                self.latitudeTxtField.text = self.latStr as String
                                spinningIndicator.hide(true)
                                
                            }
                            
                            else
                            {
                                var message = dicObj?.valueForKey("message") as? String
                                
                                alertViewApi = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                
                                alertViewApi.show()
                                
                                alertViewApi.tag = 5
                                
                                spinningIndicator.hide(true)
                                
                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        
        
    }
    
    //MARK:- Save Location Api
    
    func saveLocationApi()
    {
        if locationNameTxtField.text == "" || detailsTxtField.text == ""
        {
            var alert = UIAlertView(title: "Alert", message: "Please enter location name and detail", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
        else
        {
            
            var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            spinningIndicator.labelText = "Loading"

            
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        let packageId = userDefaults.valueForKey("packageId") as? String
        let packageDataId = userDefaults.valueForKey("packageDataId") as! Int
        println(packageId)
        println(packageDataId)
        
            
        locationNameInGPS = self.locationNameTxtField.text
            
        //var myValue:NSString = usernameTextfield.text
            
        NSUserDefaults.standardUserDefaults().setObject(locationNameInGPS, forKey:"LocationNameInGPS")
        NSUserDefaults.standardUserDefaults().synchronize()
            
        println("location name in gps = \(locationNameInGPS)")
            
            
            
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PackageId=%@&PackageDataId=%i&Type=%@&LocationName=%@&Details=%@&Lat=%@&Long=%@&DistanceFromCurrentLocation=%@",str,packageId!,packageDataId,"gps",locationNameTxtField.text,detailsTxtField.text,latitudeTxtField.text,longitudeTxtField.text,distanceTxtField.text)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/packagedata-location")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })

                        

                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            
                            if success
                            {
                                locationNameStrGps = self.locationNameTxtField.text
                                
                                
                                locationNameInGPS = self.locationNameTxtField.text
                                
                                

                                self.navigationController?.popViewControllerAnimated(false)
                                 spinningIndicator.hide(true)
                                                
                            }
                                
                            else
                            {
                               
                                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                 spinningIndicator.hide(true)

                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()
        
        }
        
    }
    
    
}
