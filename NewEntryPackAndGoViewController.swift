//
//  NewEntryPackGoViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/9/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit




var packageType = String()
var type_ToPicklist = String()

class NewEntryPackAndGoViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    var refreshControl:UIRefreshControl!
    var hitWebService:Bool = true
    var page = 0
    var limit = 50

    
    
    var imageCache = [String:UIImage]()
    
    var dataPackageOrInventoryArray = [NSDictionary]()
    
    var arryBool = [Bool]()
    
    @IBOutlet var packTableView: UITableView!
    var indexPath = NSIndexPath()
    
    var titleStr = NSString()
    @IBOutlet var titleLbl: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        titleLbl.text = titleStr as? String
        
        
       // self.packTableView.reloadData()
        
        
//        addPullToRefreshTableView()
//        getPackageOrInventoryApi(0, limit: limit, reloadFromStart:false)
        
        
        getPackageOrInventoryApi()
        
        packTableView.backgroundColor = UIColor(patternImage: UIImage(named: "bg_2.png")!)
        
       // println("type = \(packGoBtnCheck)")
        
        if typeBtnCheck == "package"
        {
            type_ToPicklist = "package"
            println("type = \(type_ToPicklist)")
        }
        
        else
        {
            type_ToPicklist = "inventory"
            println("type = \(type_ToPicklist)")
        }
        
    }
    
    
    
    override  func viewWillAppear(animated: Bool)
    {
        if typeBtnCheck == "package"
        {
            type_ToPicklist = "package"
            println("type = \(type_ToPicklist)")
        }
        
        else
        {
            type_ToPicklist = "inventory"
             println("type = \(type_ToPicklist)")
        }
        
        
       // self.packTableView.reloadData()
    }
    
    
    
    
   
//    //MARK: - refresh table function
//    func addPullToRefreshTableView()
//    {
//        self.refreshControl = UIRefreshControl()
//        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
//        self.refreshControl.addTarget(self, action: Selector("refresh"), forControlEvents: UIControlEvents.ValueChanged)
//        
//        self.packTableView.addSubview(refreshControl)
//    }
//    
//    
//    
//    func refresh()
//    {
//        println("after refresh")
//        
//        //fetchData(0, limit: invitationsArray.count, reloadFromStart:true)
//        
//        
//        //getPackageOrInventoryApi(0, limit: dataPackageOrInventoryArray.count, reloadFromStart: true)
//    }
//
//    
//
//    
//    //MARK: - scrollViewDidScroll function
//    func scrollViewDidScroll(_scrollView: UIScrollView)
//    {
//        var newScroll = Int(_scrollView.contentOffset.y)
//        
//        var maxScroll = (dataPackageOrInventoryArray.count*100)-Int(self.packTableView.frame.height)
//        
//        if newScroll >= maxScroll && hitWebService == true
//        {
//            hitWebService = false
//            
//            //getPackageOrInventoryApi(dataPackageOrInventoryArray.count, limit: limit, reloadFromStart:false)
//        }
//        
//        println((6*100)-self.packTableView.frame.height)
//    }
  

    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
   
    
    @IBAction func logOutBtn(sender: AnyObject)
    {
        var loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let login = loginStoryboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        
        self.navigationController?.pushViewController(login, animated: false)
        
    }
    
   
    
    @IBAction func startBtn(sender: AnyObject)
    {
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var newEntryStoryboard = appDeleagte.newEntryStoryboard() as UIStoryboard
        
        let startPacking = newEntryStoryboard.instantiateViewControllerWithIdentifier("startPacking") as! StartPackingViewController
        
        self.navigationController?.pushViewController(startPacking, animated: true)
        
        
        
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        println("data array count = \(dataPackageOrInventoryArray)")
        return dataPackageOrInventoryArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! NewEntryPackGoTableViewCell
        
        cell.cellShirtBtn.tag = indexPath.row
        cell.deleteBtn.tag = indexPath.row
        
        
        if arryBool[indexPath.row] == false
        {
            
            cell.cellShirtBtn.hidden = false
            cell.openImagevIew.image = UIImage(named: "img_shirt.png")
            cell.inviteImageView.image = UIImage(named: "img_shirt.png")
            cell.deleteImageview.image = UIImage(named: "img_shirt.png")
            
            
        }
        
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad
        {
            cell.backgroundColor = UIColor(red: 32/255, green: 73/255, blue: 117/255, alpha: 1.0)
            
        }
      
        //cell.titleLbl.text = ""
        
        if let strTitle = dataPackageOrInventoryArray[indexPath.row].valueForKey("Title") as? String
        {
            cell.titleLbl.text = strTitle
        }
        
        //cell.countLbl.text = ""
        
        if let strCount = dataPackageOrInventoryArray[indexPath.row].valueForKey("PackageDataCount") as? Int
        {
            cell.countLbl.text = NSString(format: "%i", strCount) as String
        }
        
       // cell.startDateLbl.text = ""
        
        if let strStartDate = dataPackageOrInventoryArray[indexPath.row].valueForKey("StartDate") as? String
        {
            cell.startDateLbl.text = NSString(format: "Start Date:%@", strStartDate) as String
        }
        
        var arrayImages = NSArray()
        
        arrayImages = []
        
        if var array = dataPackageOrInventoryArray[indexPath.row].valueForKey("PackageDataItemImages")  as? NSArray
        {
            arrayImages = array
            
            println(arrayImages)
        
        
        
        
        for var i = 0 ; i < arrayImages.count ; i++
        {
            if i == 0
            {
                
                
                
                let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                    //			println(self)
                }
                
                
                var profileUrl: String = arrayImages[0].valueForKey("Image") as! String
                
                var url: NSURL = NSURL(string: profileUrl)!
                
                cell.openImagevIew.sd_setImageWithURL(url, completed: block)
     
                
                cell.openImagevIew.contentMode = UIViewContentMode.ScaleAspectFit
                
                
            
                
                
//                var profileUrl: String = arrayImages[0].valueForKey("Image") as! String
//                
//                if let url = NSURL(string: profileUrl)
//                {
//                    let request = NSURLRequest(URL: url)
//                    NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
//                        (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
//                        if let imageData = data as NSData?
//                        {
//                            cell.openImagevIew.image = UIImage(data: imageData)
//                            cell.openImagevIew.backgroundColor = UIColor.clearColor()
//                            cell.openImagevIew.contentMode = UIViewContentMode.ScaleAspectFit
//                            //imageView.image = UIImage(data: data)!
//
//                        }
//                    }
//                }
//           
                

                
            }
            
            else if i == 1
            {
                
                
                let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                    //			println(self)
                }
                
                
                
                var profileUrl: String = arrayImages[1].valueForKey("Image") as! String
                
                var url: NSURL = NSURL(string: profileUrl)!

                
                cell.inviteImageView.sd_setImageWithURL(url, completed: block)

                cell.inviteImageView.contentMode = UIViewContentMode.ScaleAspectFit

            }
            
            else
            {
                
                
                let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                    //			println(self)
                }
                
                
                
                var profileUrl: String = arrayImages[2].valueForKey("Image") as! String
                
                var url: NSURL = NSURL(string: profileUrl)!
                
                
                cell.deleteImageview.sd_setImageWithURL(url, completed: block)

                cell.deleteImageview.contentMode = UIViewContentMode.ScaleAspectFit
            
                
            }
            
        }
            
    }
        
        if arryBool[indexPath.row] == true
        {
            cell.cellShirtBtn.hidden = true
            cell.openImagevIew.image = UIImage(named: "img_open.png")
            cell.inviteImageView.image = UIImage(named: "img_invite.png")
            cell.deleteImageview.image = UIImage(named: "img_delete.png")

        }
        

        
//        cell.wrapperBtnCell.tag = indexPath.row
//        cell.wrapperBtnCell.addTarget(self, action: Selector("wrapperBtn:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        
        return cell
    }
    
    
    
//    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath)
//    {
//        println("did select")
//        
////        var cell = tableView.cellForRowAtIndexPath(indexPath)
////        cell?.contentView.backgroundColor = UIColor.redColor()
////        cell?.backgroundColor = UIColor.redColor()
//        
//        let backgroundView = UIView()
//        backgroundView.backgroundColor = UIColor.redColor()
//        
//        var cell = tableView.cellForRowAtIndexPath(indexPath)
//        cell?.backgroundColor = UIColor.redColor()
//        
//    }
    
    
    
    
    
//    func wrapperBtn(sender: UIButton!)
//    {
//        if sender.backgroundColor == UIColor.redColor()
//        {
//            
//        }
//        else
//        {
//            sender.backgroundColor = UIColor.redColor()
//        }
//        
//        
//    }
    
    
    
//    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath)
//    {
//        var cell = tableView.cellForRowAtIndexPath(indexPath)
//        cell?.contentView.backgroundColor = UIColor.redColor()
//        cell?.backgroundColor = UIColor.redColor()
//    }
//    
//    
//     func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath)
//     {
//        var cell = tableView.cellForRowAtIndexPath(indexPath)
//        cell?.contentView.backgroundColor =  UIColor(red: 32.0, green: 73.0, blue: 117.0, alpha: 0.5)
//        cell?.backgroundColor =  UIColor(red: 32.0, green: 73.0, blue: 117.0, alpha: 0.5)
//    }
//    
//    
    
    
    @IBAction func cellShirtBtn(sender: AnyObject)
    {
        
        indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
    
        
        let cell = packTableView.cellForRowAtIndexPath(indexPath) as! NewEntryPackGoTableViewCell!
        
        if arryBool[indexPath.row] == false
        {
        
        cell.cellShirtBtn.hidden = true
        cell.openImagevIew.image = UIImage(named: "img_open.png")
        cell.inviteImageView.image = UIImage(named: "img_invite.png")
        cell.deleteImageview.image = UIImage(named: "img_delete.png")
         arryBool[indexPath.row] = true
            
        }
        
    }
    
    
    
    @IBAction func deleteBtn(sender: AnyObject)
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let strUserId = userDefaults.valueForKey("userId") as! Int
        
        var strPackageId = ""
        
        if var strpackId = dataPackageOrInventoryArray[sender.tag].valueForKey("PackageId") as? Int
        {
            strPackageId = NSString(format: "%i", strpackId) as String
            
            println(strPackageId)
        }

        
        
        println(strUserId)
     
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PackageId=%@",strUserId,strPackageId)
        
        println(post)
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/delete-package")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj  = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                println(dicObj)
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                    dispatch_async(dispatch_get_main_queue(), {
                        spinningIndicator.hide(true)
                    })

                    
                }
                else
                {

                
                dispatch_async(dispatch_get_main_queue(), {
                    
                
                var success = dicObj?.valueForKey("success") as! Bool
                var message = dicObj?.valueForKey("message") as! String
                
                if success
                {
                    var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                
                    alert.show()
                    
                    self.dataPackageOrInventoryArray.removeAtIndex(sender.tag)
                    
                    self.packTableView.reloadData()
                    
                    spinningIndicator.hide(true)
                }
                else
                {
                    var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                    
                    alert.show()
                    
                    spinningIndicator.hide(true)
                    
        
                }
                    
                    
                    })
                }
                
        }
            
            
        })
        task.resume()
        
    }
    
    
    
    
    
    @IBAction func openBtn(sender: AnyObject)
    {
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var newEntryStoryboard = appDeleagte.newEntryStoryboard() as UIStoryboard
        
        let openPackage = newEntryStoryboard.instantiateViewControllerWithIdentifier("openPackage") as! NewEntryPackGoOpenViewController
        
        openPackage.packGoOpenPackageId = dataPackageOrInventoryArray[indexPath.row].valueForKey("PackageId") as! Int
        
        openPackage.greenTitleStr = dataPackageOrInventoryArray[indexPath.row].valueForKey("Address") as! String
        println(openPackage.packGoOpenPackageId)
        
        self.navigationController?.pushViewController(openPackage, animated: true)
        
    }
    
    
    @IBAction func inviteBtn(sender: AnyObject)
    {
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var newEntryStoryboard = appDeleagte.newEntryStoryboard() as UIStoryboard
        
        
        let newEntryInvite = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryInvite") as! NewEntryInviteViewController
        newEntryInvite.packageId = dataPackageOrInventoryArray[indexPath.row].valueForKey("PackageId") as! Int
        
        println(newEntryInvite.packageId)
        self.navigationController?.pushViewController(newEntryInvite, animated: true)
        
    }
    
    
    
    
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(GraphViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
    }
    
    
    
    //MARK:- Get Packages Or Inventory Api
    //func getPackageOrInventoryApi(page: Int, limit: Int, reloadFromStart:Bool)
    func getPackageOrInventoryApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    
        spinningIndicator.labelText = "Loading"

        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        println(strInventoryOrPackageTitle)
        var dataModel = NSData()
        
        
        
        
       // &page=\(page)&limit=\(limit)
        var post = NSString(format:"UserId=%i&Typed=%@",str,strInventoryOrPackageTitle)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/get-packages")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
//            
//            if data == nil
//            {
//                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
//                alert.show()
//                
//            }
//            else
//                
//          {
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
                
            else
            {
                
                var error:NSError?
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                
                println(dicObj)
                
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                    })


                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var success = dicObj?.valueForKey("success") as! Bool
                        if success
                        {
                            
                            
                            
                           // if var posts = InvitationsResult.valueForKey("post") as? NSMutableArray
                                
                                
//                             self.dataPackageOrInventoryArray = dicObj?.valueForKey("message") as! [NSDictionary]
//                            
//                            if var posts = self.dataPackageOrInventoryArray as? [NSDictionary]
//                            {
//                                
//                                println("posts dictionary = \(posts)")
//                                
//                                
//                                 //self.arryBool.append(false)
//                                
//                                if reloadFromStart
//                                {
//                                    self.dataPackageOrInventoryArray = []
//                                }
//                                
//                                for post in posts
//                                {
//                                    self.dataPackageOrInventoryArray.append(post)
//                                }
//                                
//                                if posts.count < limit
//                                {
//                                    self.hitWebService = false
//                                }
//                                else
//                                {
//                                    self.hitWebService = true
//                                }
//                                
//                                self.packTableView.reloadData()
//                                
//                                if reloadFromStart
//                                {
//                                    self.refreshControl?.endRefreshing()
//                                }
//                                
//                            }

                            
                            
                            
                        self.dataPackageOrInventoryArray = dicObj?.valueForKey("message") as! [NSDictionary]
                            
                        self.packTableView.reloadData()
                        
                        println(self.dataPackageOrInventoryArray)
                        
                        for var i = 0; i < self.dataPackageOrInventoryArray.count; i++
                        {
                            self.arryBool.append(false)
                            
                        }
                            spinningIndicator.hide(true)
                        
                        }
                        
                        else
                        {
                            var message = dicObj?.valueForKey("message") as! String
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        
                        
//                        for var i = 0; i < self.dataPackageOrInventoryArray.count; i++
//                        {
//                           self.arryBool.append(false)
//                        
//                        }
//                        
//                        self.packTableView.reloadData()
//                        spinningIndicator.hide(true)
                        
                    })
                    
                }
            }
            
  //          }
        })
        task.resume()
        
        
    }
    
    
    
}





