//
//  NewEntryGraphViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/9/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

var typeValue = String()

var bagBoxBinSingleItemStr = NSString()



class NewEntryGraphViewController: UIViewController
{
    var greenTitleStr = NSString()
    var bagBoxSingitemBin = Int()
    var packageId = Int()
    
    
   
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        println("PackageOrInventory= \(strInventoryOrPackageTitle)")
        bagBoxSingitemBin = 1
    }
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func logoutBtn(sender: AnyObject)
    {
        var loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let login = loginStoryboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        
        self.navigationController?.pushViewController(login, animated: false)
    }
    
    @IBAction func reportBtn(sender: AnyObject)
    {
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let graphTrack = graphStoryboard.instantiateViewControllerWithIdentifier("graphTrack") as! GraphTrackViewController
        
        self.navigationController?.pushViewController(graphTrack, animated: false)
        
        
        
        
    }
    
    @IBAction func newInventoryBtn(sender: AnyObject)
    {
        
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var graphStoryboard = appDeleagte.grStoryboard() as UIStoryboard
        
        let viewInventory = graphStoryboard.instantiateViewControllerWithIdentifier("viewInventory") as! ViewInventoryViewController
        
        self.navigationController?.pushViewController(viewInventory, animated: false)
        
        
    }
    
    
    @IBAction func settingBtn(sender: AnyObject)
    {
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var graphStoryboard = appDelegate.grStoryboard() as UIStoryboard
        
        let setting = graphStoryboard.instantiateViewControllerWithIdentifier("setting") as! SettingsViewController
        
        self.navigationController?.pushViewController(setting, animated: true)
        
    }
    
    
    
    
    
    @IBAction func bagBtn(sender: AnyObject)
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
            
        
        typeValue = "bag"
        
        
         barButtonCheck = "2"
         locationNameCheck = "2"
        
        bagBoxBinSingleItemStr = "Bag"
        
        self.BagBoxBinSingleItemApi()
        
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let graphToFurtherBag = newEntryStoryboard.instantiateViewControllerWithIdentifier("graphToFurtherBag") as! NewEntryGraphToFurtherBagViewController
        
        
        graphToFurtherBag.bagCount = 1
        graphToFurtherBag.bagGreenTitleStr = greenTitleStr as String
        
        self.navigationController?.pushViewController(graphToFurtherBag, animated: true)
        spinningIndicator.hide(true)
        
    }
    
    
    
    
    
    @IBAction func boxBtn(sender: AnyObject)
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"

        
         typeValue = "box"
        
        
         barButtonCheck = "2"
         locationNameCheck = "2"
        
        bagBoxBinSingleItemStr = "Box"
        BagBoxBinSingleItemApi()
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        
        
        let graphToFurtherBin = newEntryStoryboard.instantiateViewControllerWithIdentifier("graphToFurtherBin") as! NewEntryGraphBinViewController
      
        
        graphToFurtherBin.binBoxCountBool = true
         graphToFurtherBin.binBoxCount = 1
        graphToFurtherBin.binBoxGreenTitleStr = greenTitleStr
        
        self.navigationController?.pushViewController(graphToFurtherBin, animated: true)
        spinningIndicator.hide(true)
    }
    
    @IBAction func singleItemBtnn(sender: AnyObject)
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        
         typeValue = "singleItem"
        
         barButtonCheck = "2"
         locationNameCheck = "2"
        
        
        bagBoxBinSingleItemStr = "Single Item"
        BagBoxBinSingleItemApi()
        
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let graphToFurtherSingleItem = newEntryStoryboard.instantiateViewControllerWithIdentifier("graphToFurtherSingleItem") as! NewEntryGraphSingleItemViewController
        graphToFurtherSingleItem.singleItemStr = NSString(format: "Single Item %i", bagBoxSingitemBin)
        
        graphToFurtherSingleItem.singleItemGreenTitleStr = greenTitleStr
        self.navigationController?.pushViewController(graphToFurtherSingleItem, animated: true)
        
        spinningIndicator.hide(true)
    }
    
    
    @IBAction func binBtn(sender: AnyObject)
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"

        
         typeValue = "bin"
        
        
         barButtonCheck = "2"
         locationNameCheck = "2"
        
        
        bagBoxBinSingleItemStr = "Bin"
        BagBoxBinSingleItemApi()
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        let graphToFurtherBin = newEntryStoryboard.instantiateViewControllerWithIdentifier("graphToFurtherBin") as! NewEntryGraphBinViewController
        graphToFurtherBin.binBoxCountBool = false
        graphToFurtherBin.binBoxCount = 1
        graphToFurtherBin.binBoxGreenTitleStr = greenTitleStr
        self.navigationController?.pushViewController(graphToFurtherBin, animated: true)
        spinningIndicator.hide(true)
    }
    
    
    
    //Mark:- BagBoxBinSingleItem Api
    
    func BagBoxBinSingleItemApi()
    {
       

        
        println("bagBoxSingleBin= \(bagBoxBinSingleItemStr)")
         println(packageId)
        var userDefaults = NSUserDefaults.standardUserDefaults()
        let userId = userDefaults.valueForKey("userId") as! Int
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&PackageId=%i&Type=%@&Typed=%@",userId,packageId,bagBoxBinSingleItemStr,strInventoryOrPackageTitle)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/create-packagedata"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    println("CreatePackageData= \(dictObj)")
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        

                    }
                    else
                    {
                        var dictFurther = dictObj?.valueForKey("message") as? NSDictionary
                        
                        
                        let packageId = dictFurther?.valueForKey("PackageId") as! String
                        let packageeDataId = dictFurther?.valueForKey("PackageDataId") as! Int
                        
                        println(packageId)
                        println(packageeDataId)
                        
                        var userDefaultPackageId = NSUserDefaults.standardUserDefaults()
                        userDefaultPackageId.setValue(packageId, forKey: "packageId")
                        userDefaultPackageId.setValue(packageeDataId, forKey: "packageDataId")
                        
                        
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
        
        
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        
        
    }
    
    //MARK:- TextField Dlegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
