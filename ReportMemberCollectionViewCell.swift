//
//  ReportMemberCollectionViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/21/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ReportMemberCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var filterBtnCell: UIButton!
    @IBOutlet var bagItemLbl: UILabel!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var itemLbl: UILabel!
    @IBOutlet var imageView: UIImageView!
    
}
