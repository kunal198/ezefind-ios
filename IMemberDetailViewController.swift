//
//  IMemberDetailViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/18/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class IMemberDetailViewController: UIViewController
{
    
    @IBOutlet weak var memberImageView: UIImageView!
    
    @IBOutlet weak var memberName: UILabel!
    @IBOutlet var viewOnlyWorkImage: UIImageView!
    @IBOutlet var viewAllMemberImage: UIImageView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        println(iMemberDetailResult)
        
        println(iMemberDetailResult.valueForKey("Name"))
        
        self.memberName.text = iMemberDetailResult.valueForKey("Name") as! String
        
        
        
        
        
        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //			println(self)
        }
        
        
        var profileUrl: String = iMemberDetailResult.valueForKey("ProfileImage") as! String
        
        var url: NSURL = NSURL(string: profileUrl)!
        
        self.memberImageView.sd_setImageWithURL(url, completed: block)

        
        
        
        IMemberDetailApi()

        
    }
    
    
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
       
    }
    
    
    
    //MARK:- Back Button
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    

    
    //Mark:- CheckMarkBtn
    
    
    @IBAction func viewOnlyThereWork(sender: AnyObject)
    {
        viewOnlyWorkImage.hidden = false
        viewAllMemberImage.hidden = true
    }

    @IBAction func viewAllMembersAndWork(sender: AnyObject)
    {
        viewOnlyWorkImage.hidden = true
        viewAllMemberImage.hidden = false

    }
    
    
    //MARK:- Delete IMember Button
    
    
    @IBAction func deleteImemberBtn(sender: AnyObject)
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        let memberId = "37"
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&MemberId=%@", str,memberId)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/delete-member-detail"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert = UIAlertView()
                    alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })

                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    
                    println("IMemberDetail\(dictObj)")
                    
                    
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        })

                        

                    }
                    else
                    {
                        var success = dictObj?.valueForKey("success") as! Int
                        var message = dictObj?.valueForKey("message") as! String
                        
                        println(message)
                        
                        if success == 1
                        {
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()

    }
    
    
    //MARK:-Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }

    }
    //MARK:- Imember Detail Api
    
    
    func IMemberDetailApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        let memberId = "50"
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&MemberId=%i", str,memberId)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/users/member-detail"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error)
                    println("IMemberDetail\(dictObj)")
                    
                    if error != nil
                    {
                        var alert = UIAlertView(title: "Alert", message: "Something Wrong", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                        
                    }
                    else
                    {
                        var success = dictObj?.valueForKey("success") as! Int
                        var message = dictObj?.valueForKey("message") as! String
                        
                        println(message)
                        
                        if success == 1
                        {
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            spinningIndicator.hide(true)
                        }

                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
    }
    

    
}
