//
//  ReportItemViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/16/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import Charts


var strYearMonthDay = NSString()


 class ReportItemViewController: UIViewController
 {
     var reportItemdict = NSDictionary()
    
    
    @IBOutlet weak var totalWeeks_lbl: UILabel!
    @IBOutlet weak var totalDays_lbl: UILabel!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var bagsBoxesFilterView: UIView!
    @IBOutlet var viewFilter: UIView!
    @IBOutlet var barScrollView: UIScrollView!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var barChartView: BarChartView!
    var bagBoxBinSingleitem:[String]!
    var yearMonthWeek:[String]!
    
    @IBOutlet var totalDaysLbl: UILabel!
    @IBOutlet var itemScrollView: UIScrollView!
   
    @IBOutlet var dateBtn: UIButton!
    
    @IBOutlet var totalLbl: UILabel!
    
    @IBOutlet var packageBlueBtn: UIButton!
    @IBOutlet var inventoryBlueBtn: UIButton!
    @IBOutlet var bothBlueBtn: UIButton!
    @IBOutlet var bothBtn: UIButton!
    @IBOutlet var inventoryBtn: UIButton!
    @IBOutlet var packageBtn: UIButton!
    @IBOutlet var blueView: UIView!
    
    @IBOutlet var yearBtn: UIButton!
    @IBOutlet var yearBlueBtn: UIButton!
    @IBOutlet var monthBtn: UIButton!
    @IBOutlet var monthBlueBtn: UIButton!
    @IBOutlet var weekBtn: UIButton!
    @IBOutlet var weekBlueBtn: UIButton!
    
    
    @IBOutlet weak var itemsLbl: UILabel!
    
    @IBOutlet weak var weeksLbl: UILabel!
    
    var strBothInventoryPackage = NSString()
     var strBagBinBoxSingleitem = NSString()
    
    @IBOutlet var bagLbl: UILabel!

    @IBOutlet var boxLbl: UILabel!
    
    @IBOutlet weak var separatorLineView: UIView!
    @IBOutlet var binsLbl: UILabel!
    @IBOutlet var singleItemLbl: UILabel!
    
    @IBOutlet var dateLbl: UILabel!
    var unitsSold:[Double] = []
    
    var yearWeekMonthUnitsSold:[Double] = []
    
    var bagStr = NSString()
    var bagDouble = Double()
    
    var boxStr = NSString()
    var boxDouble = Double()
    
    var binStr = NSString()
    var binDouble = Double()
    
    var singleItemStr = NSString()
    var singleItemDouble = Double()
    
    
    
    @IBOutlet var bagM: UILabel!
    @IBOutlet var binM: UILabel!
    @IBOutlet var boxM: UILabel!
    @IBOutlet var singleItemM: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        scrollView.contentSize.height  = dateBtn.frame.origin.y + dateBtn.frame.size.height+10
        itemScrollView.contentSize.height  = totalLbl.frame.origin.y + totalLbl.frame.size.height+10
        barScrollView.contentSize.width = barChartView.frame.origin.x + barChartView.frame.size.width
        
        let date = NSDate()
        var dateFormat = NSDateFormatter()
        
        dateFormat.dateFormat = "EE dd/MM/yyyy"
        
        var dateInFormat:NSString = dateFormat.stringFromDate(date)
        dateLbl.text = dateInFormat as String
        println(dateInFormat)
        
        strBothInventoryPackage = "package"
        
        
        self.bagLbl.text = ""
        self.totalDays_lbl.text = ""
        self.boxLbl.text = ""
        self.binsLbl.text = ""
        self.singleItemLbl.text = ""
        

        
        
        self.separatorLineView.hidden = true
        self.weeksLbl.hidden = true
        self.itemsLbl.hidden = true
        self.totalWeeks_lbl.hidden = true
        self.totalDays_lbl.hidden = true
         self.totalLbl.hidden = true
        
        
        
        reportItemApi()
        
        BarChart()
        
    }
    
    
    
    //MARK:- BarChart
    
    
    func BarChart()
    {
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.noDataTextDescription = "GIVE REASON"
        barChartView.descriptionText = ""
        
        
        barChartView.drawBarShadowEnabled = false
        barChartView.drawValueAboveBarEnabled = true
        barChartView.maxVisibleValueCount = 60
        barChartView.pinchZoomEnabled = false
        barChartView.drawGridBackgroundEnabled = false
        
        var xaxis = barChartView.xAxis as ChartXAxis
        xaxis.labelPosition = .Bottom
        xaxis.labelFont = UIFont.systemFontOfSize(10)
        
        xaxis.spaceBetweenLabels = 2
        
        
        
        
        var leftAxis = barChartView.leftAxis as ChartYAxis
        
        
        leftAxis.labelFont = UIFont.systemFontOfSize(10)
        leftAxis.labelCount = 4
        leftAxis.valueFormatter = NSNumberFormatter.alloc()
        leftAxis.valueFormatter!.maximumFractionDigits = 1
        leftAxis.valueFormatter!.negativeSuffix = " $"
        leftAxis.valueFormatter!.positiveSuffix = " $"
        leftAxis.labelPosition = .OutsideChart
        leftAxis.spaceTop = 5
        
        
        var rightAxis = barChartView.rightAxis as ChartYAxis
        rightAxis.drawGridLinesEnabled = false
        rightAxis.labelFont = UIFont.systemFontOfSize(10)
        rightAxis.labelCount = 4
        rightAxis.valueFormatter = leftAxis.valueFormatter
        rightAxis.spaceTop = 5
        
        barChartView.legend.position = .BelowChartLeft
        barChartView.legend.form = .Square
        barChartView.legend.formSize = 9.0
        barChartView.legend.font = UIFont(name: "HelveticaNeue-Light", size: 11.0)!
        barChartView.legend.xEntrySpace = 4.0

        
    }
    
    
    
    
    
    func setChart(dataPoints:[String], values:[Double])
    {
        
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for  i in 0..<dataPoints.count
            
        {
            let dataEntry = BarChartDataEntry(value:values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
        let chartData = BarChartData(xVals: bagBoxBinSingleitem, dataSet: chartDataSet)
        barChartView.data = chartData
        
    }
    
    
    
    
    
    func setChartYearMonthWeek(dataPoints:[String], values:[Double])
    {
        
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for  i in 0..<dataPoints.count
            
        {
            let dataEntry = BarChartDataEntry(value:values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
        let chartData = BarChartData(xVals: yearMonthWeek, dataSet: chartDataSet)
        barChartView.data = chartData
    
    }

    
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
       
    }
    
    
    
    
    //MARK:- ReportItemFilterApi
    
    func ReportItemFilterApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&main_filter=%@&Filter_by=%@&Type=%@",userId, strBothInventoryPackage,strYearMonthDay,strBagBinBoxSingleitem)
        
        // var post = NSString(format: "UserId=%i&main_filter=%@",userId, strBothInventoryPackage)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/items-filter"//&UserId=\(userId)&main_filter=\()"
        
       // var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/items"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    println(dictObj)
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        spinningIndicator.hide(true)
                        

                    }
                    else
                    {
                        
                        var  dataDic = dictObj?.valueForKey("Data") as! NSDictionary
                        
                        
                        println(dataDic.allKeys)
                        
                        if strYearMonthDay == "year"
                            
                        {
                        self.yearMonthWeek = dataDic.allKeys as NSArray as! [String]
                        self.yearWeekMonthUnitsSold = dataDic.allValues as NSArray as! [Double]
                        println(self.yearMonthWeek)
                        }
                        if strYearMonthDay == "month"
                        {
                            
                            
                            if self.yearMonthWeek != nil
                            {
                            self.yearMonthWeek.removeAll()
                            }
                            
                            if self.yearWeekMonthUnitsSold != []
                            {
                                self.yearWeekMonthUnitsSold.removeAll()
                            }
                            

                            self.yearMonthWeek = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
                            println(self.yearMonthWeek)
                            
                            var jan = dataDic.valueForKey("01") as! Int
                            println(jan)
                            
                            var janDouble = Double(jan)
                            
                            
                            var feb = dataDic.valueForKey("02") as! Int
                            println(feb)
                            
                            var febDouble = Double(feb)

                            var mar = dataDic.valueForKey("03") as! Int
                            println(mar)
                            
                            var marDouble = Double(mar)
                            
                            
                            var apr = dataDic.valueForKey("04") as! Int
                            println(apr)
                            
                            var aprDouble = Double(apr)

                            
                            var may = dataDic.valueForKey("05") as! Int
                            println(may)
                            
                            var mayDouble = Double(may)
                            
                            var june = dataDic.valueForKey("06") as! Int
                            println(june)
                            
                            var juneDouble = Double(june)

                            var july = dataDic.valueForKey("07") as! Int
                            println(july)
                            
                            var julyDouble = Double(july)
                            

                            var aug = dataDic.valueForKey("08") as! Int
                            println(aug)
                            
                            var augDouble = Double(aug)
                            
                            var sep = dataDic.valueForKey("09") as! Int
                            println(sep)
                            
                            var sepDouble = Double(sep)
                            
                            
                            var oct = dataDic.valueForKey("10") as! Int
                            println(oct)
                            
                            var octDouble = Double(oct)
                            
                            var nov = dataDic.valueForKey("11") as! Int
                            println(nov)
                            
                            var novDouble = Double(nov)
                            
                            var dec = dataDic.valueForKey("12") as! Int
                            println(dec)
                            
                            var decDouble = Double(dec)



                            
                            self.yearWeekMonthUnitsSold = [janDouble,febDouble,marDouble,aprDouble,mayDouble,juneDouble,julyDouble,augDouble,sepDouble,octDouble,novDouble,decDouble]
                            
                        }
                        
                        
                        if strYearMonthDay == "week"
                        {
                            if self.yearMonthWeek != nil
                            {
                                self.yearMonthWeek.removeAll()
                            }
                            
                            if self.yearWeekMonthUnitsSold != []
                            {
                                self.yearWeekMonthUnitsSold.removeAll()
                            }

                            self.yearMonthWeek = ["Mon","Tue","Wed","Thur","Fri","Sat","Sun"]
                            println(self.yearMonthWeek)
                            
                            
                            var mon = dataDic.valueForKey("Mon") as! Int
                            println(mon)
                            
                            var monDouble = Double(mon)
                            
                            
                            var tue = dataDic.valueForKey("Tue") as! Int
                            println(tue)
                            
                            var tueDouble = Double(tue)
                            
                            var wed = dataDic.valueForKey("Wed") as! Int
                            println(wed)
                            
                            var wedDouble = Double(wed)
                            
                            
                            var thur = dataDic.valueForKey("Thu") as! Int
                            println(thur)
                            
                            var thurDouble = Double(thur)
                            
                            
                            var fri = dataDic.valueForKey("Fri") as! Int
                            println(fri)
                            
                            var friDouble = Double(fri)
                            
                            var sat = dataDic.valueForKey("Sat") as! Int
                            println(sat)
                            
                            var satDouble = Double(sat)
                            
                            var sun = dataDic.valueForKey("Sun") as! Int
                            println(sun)
                            
                            var sunDouble = Double(sun)
                            
                             self.yearWeekMonthUnitsSold = [monDouble,tueDouble,wedDouble,thurDouble,friDouble,satDouble,sunDouble]
                            
                        }
                        
                        
                        
                        
                        println(self.yearWeekMonthUnitsSold)
                        
                        self.setChartYearMonthWeek(self.yearMonthWeek, values: self.yearWeekMonthUnitsSold)
                        
                         spinningIndicator.hide(true)
                    }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
        
    }
    

    
    
    
    
    
    //MARK:- reportItemApi Api
    
    func reportItemApi()
    {
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        var data = NSData()
        
        var post = NSString(format: "UserId=%i&main_filter=%@",userId, strBothInventoryPackage)
        
        println(post)
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)

        
        var path = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/items"
        
        var url = NSURL(string: path)
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        var session  = NSURLSession.sharedSession()
        
        var task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            
            
            
            
            
            if (error != nil)
            {
                println("\(error?.localizedDescription)")
                
            }
            else
                
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    
                    var error:NSError?
                    var dictObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSDictionary
                    println(dictObj)
                    if error != nil
                    {
                        
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        
                        self.bagLbl.text = ""
                        self.totalDays_lbl.text = ""
                        self.boxLbl.text = ""
                        self.binsLbl.text = ""
                        self.singleItemLbl.text = ""
                     
                        
                        
                        self.separatorLineView.hidden = true
                        self.weeksLbl.hidden = true
                        self.itemsLbl.hidden = true
                        self.totalWeeks_lbl.hidden = true
                        self.totalDays_lbl.hidden = true
                        self.totalLbl.hidden = true
                        spinningIndicator.hide(true)
                    }
                    else
                    {
                        
                        
                        var success = dictObj?.valueForKey("success") as! Bool
                        
                        if success == false
                        {
                            //var dict = dictObj?.valueForKey("success") as! NSDictionary
                            var message = dictObj!.valueForKey("message") as! String
                            var alert = UIAlertView()
                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                            
                            self.bagLbl.text = ""
                            self.totalDays_lbl.text = ""
                            self.boxLbl.text = ""
                            self.binsLbl.text = ""
                             self.singleItemLbl.text = ""
                            
            
                            
                            self.separatorLineView.hidden = true
                            self.weeksLbl.hidden = true
                            self.itemsLbl.hidden = true
                            self.totalWeeks_lbl.hidden = true
                            self.totalDays_lbl.hidden = true
                            self.totalLbl.hidden = true
                            
                            
                            
                            
                            alert.show()
                            spinningIndicator.hide(true)
                        }
                        else
                        {
                            
                            self.reportItemdict = dictObj?.valueForKey("message") as! NSDictionary
                       
                            
                            
                            
                            self.separatorLineView.hidden = false
                            self.weeksLbl.hidden = false
                            self.itemsLbl.hidden = false
                            self.totalWeeks_lbl.hidden = false
                            self.totalDays_lbl.hidden = false
                            self.totalLbl.hidden = false
                            
                            println("report dictionary = \(self.reportItemdict)")
                            
                            
                            self.bagLbl.text = ""
                            
                            if let straBag = self.reportItemdict.valueForKey("bag") as? Int
                            {
                                self.bagLbl.text = NSString(format: "%i", straBag) as String
                                
                                self.bagStr = NSString(format: "%@", self.bagLbl.text!)
                                
                                
                                self.bagDouble = (self.bagStr).doubleValue
                                
                                println(self.bagDouble)
                                
                                
                            }
                            
                            
                            
                            self.totalDays_lbl.text = ""
                            
                            if let totalValue = dictObj?.valueForKey("Total") as? Int
                            {
                                self.totalDays_lbl.text = NSString(format: "%i", totalValue) as String
                                
                                println(self.totalDays_lbl.text)
                                
                            }
                            
                            
                            
                            
                            
                            
                            self.boxLbl.text = ""
                            
                            if let strBox = self.reportItemdict.valueForKey("box") as? Int
                            {
                                println(strBox)
                                
                                
                                self.boxLbl.text = NSString(format: "%i", strBox) as String
                                
                                self.boxStr = NSString(format: "%@", self.boxLbl.text!)
                                self.boxDouble = (self.boxStr).doubleValue
                                println(self.boxDouble)
                                
                            }
                            
                            self.binsLbl.text = ""
                            
                            if let strBin = self.reportItemdict.valueForKey("bin") as? Int
                            {
                                self.binsLbl.text = NSString(format: "%i", strBin) as String
                                
                                
                                self.binStr = NSString(format: "%@", self.binsLbl.text!)
                                self.binDouble = (self.binStr).doubleValue
                                println(self.binDouble)
                            }
                            
                            self.singleItemLbl.text = ""
                            
                            if let strSingleItem = self.reportItemdict.valueForKey("single item") as? Int
                            {
                                self.singleItemLbl.text = NSString(format: "%i", strSingleItem) as String
                                
                                self.singleItemStr = NSString(format: "%@", self.singleItemLbl.text!)
                                
                                self.singleItemDouble = (self.singleItemStr).doubleValue
                                println(self.singleItemDouble)
                                
                            }
                            
                            
                            
                            self.unitsSold = [self.bagDouble,self.boxDouble,self.binDouble,self.singleItemDouble]
                            self.bagBoxBinSingleitem = ["Bag", "Box", "Single Item", "Bin"]
                            self.setChart(self.bagBoxBinSingleitem, values: self.unitsSold)
                            spinningIndicator.hide(true)

                        }

                        
                        
                  }
                    
                    
                })
                
                
            }
            
            
            
            
        })
        task.resume()
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


    //MARK:- Navigation Barcode Button
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
        
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
        
    }
    
    //MARK:- Back Button
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(GraphTrackViewController) {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        
    }
    
    //MARK:- Report's Buttons
    
    @IBAction func allBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportAll  = reportStoryboard.instantiateViewControllerWithIdentifier("reportAll") as! ReportAllViewController
        
        self.navigationController?.pushViewController(reportAll, animated: false)
        

        
        
    }
    
    
    
    @IBAction func membersBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportMember  = reportStoryboard.instantiateViewControllerWithIdentifier("reportMember") as! ReportMemberViewController
        
        self.navigationController?.pushViewController(reportMember, animated: false)
    }
    
    
    @IBAction func categoryBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportCategory  = reportStoryboard.instantiateViewControllerWithIdentifier("reportCategory") as! ReportCategoryViewController
        
        self.navigationController?.pushViewController(reportCategory, animated: false)
        
        
        
    }
    
    @IBAction func ValueBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportValue  = reportStoryboard.instantiateViewControllerWithIdentifier("reportValue") as! ReportValueViewController
        
        self.navigationController?.pushViewController(reportValue, animated: false)
        
        
    }
    
    @IBAction func locationBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportLocation  = reportStoryboard.instantiateViewControllerWithIdentifier("reportLocation") as! ReportLocaionViewController
        
        self.navigationController?.pushViewController(reportLocation, animated: false)
        
        
    }
    
    @IBAction func dateBtn(sender: AnyObject)
    {
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var reportStoryboard = appDelegate.reportStoryboard() as UIStoryboard
        
        let reportDate  = reportStoryboard.instantiateViewControllerWithIdentifier("reportDate") as! ReportDateViewController
        
        self.navigationController?.pushViewController(reportDate, animated: false)
        
    }

    //MARK:- Filter Button
    
    @IBAction func filterBtn(sender: AnyObject)
    {
        blueView.hidden = false
        bagsBoxesFilterView.hidden = true
        viewFilter.hidden = false
    }
    
    
    @IBAction func crossBtn(sender: AnyObject)
    {
        blueView.hidden = true
        
    }
    
    @IBAction func packageBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "package"
        packageBtn.hidden = true
        packageBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBtn.hidden = false
        
        
//        strBothInventoryPackage = "package"
//        packageBtn.hidden = true
//        packageBlueBtn.hidden = false
//        inventoryBlueBtn.hidden = true
//        bothBlueBtn.hidden = true
//        inventoryBtn.hidden = false
//        bothBtn.hidden = false
    }
    
    
    
    
    @IBAction func packageBlueBtn(sender: AnyObject)
    {
        // strBothInventoryPackage = ""
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBlueBtn.hidden = true
        bothBlueBtn.hidden = true
        
    }
    
    
    
    @IBAction func inventoryBtn(sender: AnyObject)
    {
        strBothInventoryPackage = "inventory"
        inventoryBlueBtn.hidden = false
        inventoryBtn.hidden = true
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        bothBtn.hidden = false
        
    }
    
    @IBAction func inventoryBlueBtn(sender: AnyObject)
    {
        // strBothInventoryPackage = ""
        inventoryBlueBtn.hidden = true
        inventoryBtn.hidden = false
        bothBlueBtn.hidden = true
        packageBlueBtn.hidden = true
    }
    
    
    @IBAction func bothBtn(sender: AnyObject)
    {
         strBothInventoryPackage = "both"
        bothBtn.hidden = true
        bothBlueBtn.hidden = false
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        packageBtn.hidden = false
        inventoryBtn.hidden = false
    }
    
    @IBAction func bothBlueBtn(sender: AnyObject)
    {
        //strBothInventoryPackage = ""
        bothBtn.hidden = false
        bothBlueBtn.hidden = true
        inventoryBlueBtn.hidden = true
        packageBlueBtn.hidden = true
        
    }
    
    
    
    //MARK:- Bags,Bins,Item,Boxes Filter Button
    
    
    @IBAction func yearbtn(sender: AnyObject)
    {
        
        strYearMonthDay = "year"
        yearBtn.hidden = true
        yearBlueBtn.hidden = false
        monthBlueBtn.hidden = true
        weekBlueBtn.hidden = true
        monthBtn.hidden = false
        weekBtn.hidden = false
        
        
        
    }
    
    @IBAction func yearBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        monthBlueBtn.hidden = true
        weekBlueBtn.hidden = true
        

    }
    
    
   
    @IBAction func monthBtn(sender: AnyObject)
    {
        strYearMonthDay = "month"
        monthBlueBtn.hidden = false
        monthBtn.hidden = true
        weekBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        weekBtn.hidden = false
    }
    
    
    @IBAction func monthBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        monthBlueBtn.hidden = true
        monthBtn.hidden = false
        weekBlueBtn.hidden = true
        yearBlueBtn.hidden = true

    }
    
    
    @IBAction func weekBtn(sender: AnyObject)
    {
        strYearMonthDay = "week"
        weekBtn.hidden = true
        weekBlueBtn.hidden = false
        monthBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        yearBtn.hidden = false
        monthBtn.hidden = false
    }
    
    
    @IBAction func weekBlueBtn(sender: AnyObject)
    {
        strYearMonthDay = ""
        weekBtn.hidden = false
        weekBlueBtn.hidden = true
        monthBlueBtn.hidden = true
        yearBlueBtn.hidden = true
        

    }
    
   @IBAction func bagsFilter(sender: AnyObject)
    {
        strBagBinBoxSingleitem = "bag"
        blueView.hidden = false
        viewFilter.hidden = true
        bagsBoxesFilterView.hidden = false
    }
    
    @IBAction func boxesFilter(sender: AnyObject)
    {
        strBagBinBoxSingleitem = "box"
        blueView.hidden = false
        viewFilter.hidden = true
        bagsBoxesFilterView.hidden = false

        
    }
    
    @IBAction func binsFilter(sender: AnyObject)
    {
        strBagBinBoxSingleitem = "bin"
        blueView.hidden = false
        viewFilter.hidden = true
        bagsBoxesFilterView.hidden = false

        
    }
    
    @IBAction func  singleItemFilter(sender: AnyObject)
    {
        strBagBinBoxSingleitem = "single item"
        blueView.hidden = false
        viewFilter.hidden = true
        bagsBoxesFilterView.hidden = false

        
    }
    
    //MARK:- Filter Refresh Button
    
    
    @IBAction func filterRefrshtn(sender: AnyObject)
    {
        blueView.hidden = true
        reportItemApi()
    
    }
    
    //MARK:- Filter Refresh YearMonthWeek Button
    
    
    @IBAction func yearMonthBtn(sender: AnyObject)
    
    
    {
        if strYearMonthDay != ""
        {
        
        blueView.hidden = true
        ReportItemFilterApi()
        }
        
         blueView.hidden = true
        
        
        
        if strBagBinBoxSingleitem == "bag"
        {
            if strYearMonthDay == "year"
            {
                bagM.text = "Y"
                
            }
            
            else if strYearMonthDay == "month"
            {
               bagM.text = "M"
            }
            
            else
            {
                bagM.text = "W"
            }
            
        }
        
        else if strBagBinBoxSingleitem == "box"
        {
            if strYearMonthDay == "year"
            {
                boxM.text = "Y"
                
            }
                
            else if strYearMonthDay == "month"
            {
                boxM.text = "M"
            }
                
            else
            {
                boxM.text = "W"
            }
            
        }

        
        else if strBagBinBoxSingleitem == "bin"
        {
            if strYearMonthDay == "year"
            {
                binM.text = "Y"
                
            }
                
            else if strYearMonthDay == "month"
            {
                binM.text = "M"
            }
                
            else
            {
                binM.text = "W"
            }
            
        }
        

        else
        {
            if strYearMonthDay == "year"
            {
                singleItemM.text = "Y"
                
            }
                
            else if strYearMonthDay == "month"
            {
                singleItemM.text = "M"
            }
                
            else
            {
                singleItemM.text = "W"
            }

        }
        
        
    }
    

    //MARK:- BadBoxBinButton
    
    
    @IBAction func bagWholeBtn(sender: AnyObject)
    {
        totalDaysLbl.text = bagLbl.text
    }
    
    @IBAction func boxesWholeBtn(sender: AnyObject)
    {
        totalDaysLbl.text = boxLbl.text
        
    }
    
    
    @IBAction func binWholeBtn(sender: AnyObject)
    {
        totalDaysLbl.text = binsLbl.text
    }
    
    
    @IBAction func singleItemWholeBtn(sender: AnyObject)
    {
        totalDaysLbl.text = singleItemLbl.text
    }
    
    
}
