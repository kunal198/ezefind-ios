//
//  AddListTableViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 10/19/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class AddListTableViewCell: UITableViewCell {

    @IBOutlet var valueTxtfield: UITextField!
    @IBOutlet var quantityLbl: UILabel!
    @IBOutlet var attachmentLbl: UILabel!
    @IBOutlet var descriptionTxtField: UITextView!
    @IBOutlet var titleTxtField: UILabel!
    @IBOutlet var cellImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
