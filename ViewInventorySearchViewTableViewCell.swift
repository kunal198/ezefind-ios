//
//  ViewInventorySearchViewTableViewCell.swift
//  EzeFind
//
//  Created by mrinal khullar on 1/15/16.
//  Copyright (c) 2016 mrinal khullar. All rights reserved.
//

import UIKit

class ViewInventorySearchViewTableViewCell: UITableViewCell
{

    
   
    @IBOutlet weak var descriptionView: UITextView!

    @IBOutlet weak var attachmentLbl: UILabel!

    @IBOutlet weak var quantityLbl: UILabel!
    
    @IBOutlet weak var itemImageView: UIImageView!
    
    
       @IBOutlet weak var titleLbl: UILabel!
    
      override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
