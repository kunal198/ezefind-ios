//
//  NewEntryPackGoOpenViewController.swift
//  EzeFind
//
//  Created by mrinal khullar on 9/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


var newEntryPackGoOpenTitleStr = NSString()
var packageId = Int()

class NewEntryPackGoOpenViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate
{
    var message = NSDictionary()
    @IBOutlet var greenTitleLbl: UILabel!
    @IBOutlet var packGoOpenCollectionView: UICollectionView!
    var packGoOpenPackageId = Int()
    var bagBoxBinSingleitemStr = NSString()
    var greenTitleStr = NSString()
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var bagBtn: UIButton!
    @IBOutlet var bagBlueBtn: UIButton!
    @IBOutlet var binBtn: UIButton!
    @IBOutlet var binBlueBtn: UIButton!
    @IBOutlet var boxBtn: UIButton!
    @IBOutlet var boxBlueBtn: UIButton!
    @IBOutlet var singleItemBtn: UIButton!
    @IBOutlet var singleItemBlueBtn: UIButton!
    
    @IBOutlet var blackview: UIView!
    
    var packageDetailArray = NSArray()
    
    
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        openPAckageApi()
        
          println("......\(newEntryPackGoOpenTitleStr)")
        
        titleLbl.text = newEntryPackGoOpenTitleStr as String
        greenTitleLbl.text = greenTitleStr as String
        
    }

    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return packageDetailArray.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        var openCell = collectionView.dequeueReusableCellWithReuseIdentifier("open", forIndexPath: indexPath) as! NewEntryPackGoOpenCollectionViewCell
        
        openCell.cellAddBtn.tag = indexPath.row
        
        if let itemCount = packageDetailArray[indexPath.row].valueForKey("ItemsCount") as? String
        {
            openCell.itemCountLbl.text = itemCount
            
        }
        
        if let entryDate = packageDetailArray[indexPath.row].valueForKey("EntryDate") as? String
        {
            openCell.dateLbl.text = entryDate
        }
        
        var type = ""
        
        if let typed = packageDetailArray[indexPath.row].valueForKey("Type") as? String
        {
            type = typed
        }
        
        var numberCount = ""
    
        if let number = packageDetailArray[indexPath.row].valueForKey("Number") as? String
        {
            numberCount = number
        }
        
        
        
        openCell.typeLbl.text = NSString(format: "%@ %@", type,numberCount) as String
        
        
        
//        if var urlString  =  packageDetailArray[indexPath.row].valueForKey("ItemImage") as? String, imageurl = NSURL(string: NSString(format: "http://beta.brstdev.com/yiiezefind/%@", urlString) as String)
//        {
//            if let data = NSData(contentsOfURL: imageurl)
//            {
//                
//                openCell.imageView.image = UIImage(data: data)
//            }
//        }
//
        

        
        
        
    var profileUrl: String = packageDetailArray[indexPath.row].valueForKey("ItemImage") as! String
        
        var url: NSURL = NSURL(string: profileUrl)!
        
        if let data : NSData = NSData(contentsOfURL: url)
        {
            openCell.imageView.image = UIImage(data: data)!
            openCell.imageView.backgroundColor = UIColor.clearColor()
            
            //imageView.image = UIImage(data: data)!
        }
        
        

        
        
        
        return openCell
    }
    
    
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        var type = ""
        
        if let typed = packageDetailArray[indexPath.row].valueForKey("Type") as? String
        {
            type = typed
        }
        
        var numberCount = ""
        
        if let number = packageDetailArray[indexPath.row].valueForKey("Number") as? String
        {
            numberCount = number
        }
        
        
        
        

        
        
        var appDelegate = AppDelegate.sharedDelegate() as AppDelegate
        
        var viewInventoryStoryboard = appDelegate.viewInventoryStoryboard() as UIStoryboard
        
        let date = viewInventoryStoryboard.instantiateViewControllerWithIdentifier("date") as! FullMenuDateViewController
        date.packageDataID = packageDetailArray[indexPath.row].valueForKey("PackageDataId") as! String
        
        DateMenuCheck = "didSelectCall"
        
        typeCategoryCheck = "newEntry"
        
        date.packageId = packageDetailArray[indexPath.row].valueForKey("PackageId_fk") as! String
        date.noOfPackages =  NSString(format: "%@ %@", type,numberCount) as String
        date.address = greenTitleStr
        self.navigationController?.pushViewController(date, animated: true)
        
    }
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        var sizeHeight = UIScreen.mainScreen().bounds.height
        if sizeHeight == 480 || sizeHeight ==  568
        {
            return CGSizeMake(155, 155)
        }
            
        else if sizeHeight ==  667
        {
            return CGSizeMake(182, 182)
        }
        else if sizeHeight ==  736
        {
            return CGSizeMake(200,200)
        }
            
            
        else
        {
            return CGSizeMake(240, 240)
        }
        
    }


    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func cellAddBtn(sender: AnyObject)
    {
        var appDelegate =  AppDelegate.sharedDelegate() as AppDelegate
        var newEntryStoryboard = appDelegate.newEntryStoryboard() as UIStoryboard
        
        editBackCheck = "cellAddBtn"
        
        let newEntryAddList = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryAddList") as! AddListViewController
        
        var str =  packageDetailArray[sender.tag].valueForKey("PackageDataId") as! String
        var packageDataIdInt = (str).toInt()
        
        newEntryAddList.packageDataId = packageDataIdInt!
        
        newEntryAddList.packageId = packageDetailArray[sender.tag].valueForKey("PackageId_fk") as! String
        
        newEntryAddList.greenTitleStr = message.valueForKey("Address") as! String
        newEntryAddList.backBtnBool = true
        println(newEntryAddList.packageId)
        println(newEntryAddList.packageDataId)
        self.navigationController?.pushViewController(newEntryAddList, animated: true)
        
    }
    
    
    
    
    //MARK:- Navigation Barcode Btn
    
    @IBAction func navigationBarcodeBtn(sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKindOfClass(GraphViewController)
            {
                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
    }
    
    
    
    
    
    //MARK:- Filter Button

    @IBAction func FilterBtn(sender: AnyObject)
    {
        blackview.hidden = false
    }
    
    
    @IBAction func crossBtn(sender: AnyObject)
    {
        blackview.hidden = true
        
    }
    
    
    @IBAction func bagBtn(sender: AnyObject)
    {
        bagBoxBinSingleitemStr = "bag"
        bagBtn.hidden = true
        bagBlueBtn.hidden = false
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        binBtn.hidden = false
        boxBtn.hidden = false
        singleItemBtn.hidden = false
        
    }
    
    
    @IBAction func bagBlueBtn(sender: AnyObject)
    {
        bagBtn.hidden = false
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        
    }
    
    @IBAction func binBtn(sender: AnyObject)
    {
        bagBoxBinSingleitemStr = "bin"
        binBtn.hidden = true
        binBlueBtn.hidden = false
        bagBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        bagBtn.hidden = false
        boxBtn.hidden = false
        singleItemBtn.hidden = false
        
    }
    
    
    
    
    
    @IBAction func binBlueBtn(sender: AnyObject)
    {
        binBtn.hidden = false
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        
    }
    
    
    
    
    
    
    @IBAction func boxBtn(sender: AnyObject)
    {
        bagBoxBinSingleitemStr = "box"
        boxBtn.hidden = true
        boxBlueBtn.hidden = false
        binBlueBtn.hidden = true
        bagBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        binBtn.hidden = false
        bagBtn.hidden = false
        singleItemBtn.hidden = false
        
        
    }
    
    
    
    @IBAction func boxBlueBtn(sender: AnyObject)
    {
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        
        boxBtn.hidden = false
        
    }
    
    
    
    
    @IBAction func singleItemBtn(sender: AnyObject)
    {
        bagBoxBinSingleitemStr = "single item"
        singleItemBtn.hidden = true
        singleItemBlueBtn.hidden = false
        boxBlueBtn.hidden = true
        binBlueBtn.hidden = true
        bagBlueBtn.hidden = true
        boxBtn.hidden = false
        binBtn.hidden = false
        bagBtn.hidden = false
        
    }
    
    
    
    @IBAction func singleItemBlueBtn(sender: AnyObject)
    {
        bagBlueBtn.hidden = true
        binBlueBtn.hidden = true
        boxBlueBtn.hidden = true
        singleItemBlueBtn.hidden = true
        
        singleItemBtn.hidden = false
        
    }
    
    
    
    
    
    @IBAction func newEntryBtn(sender: AnyObject)
    {
        blackview.hidden = true
        var appDeleagte = AppDelegate.sharedDelegate() as AppDelegate
        
        var newEntryStoryboard = appDeleagte.newEntryStoryboard() as UIStoryboard
        
        let newEntryGraph = newEntryStoryboard.instantiateViewControllerWithIdentifier("newEntryGraph") as! NewEntryGraphViewController
        
        newEntryGraph.packageId = packGoOpenPackageId
        println("new entry graph id = \(newEntryGraph.packageId)")
       
        self.navigationController?.pushViewController(newEntryGraph, animated: true)

    }
    
    
    
    @IBAction func refreshBtn(sender: AnyObject)
    {
        blackview.hidden = true
        openPAckageApi()
    }
    
    
    
    
    

    //MARK:- To Open the Package
    
    func openPAckageApi()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"

        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let str = userDefaults.valueForKey("userId") as! Int
        println(str)
        
        var dataModel = NSData()
        
        var post = NSString(format:"UserId=%i&PackageId=%i&Type=%@",str,packGoOpenPackageId,bagBoxBinSingleitemStr)
        
        
         // var post = NSString(format:"UserId=%i&PackageId=%i",str,packGoOpenPackageId)
        
        println(post)
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://beta.brstdev.com/yiiezefind/frontend/index.php/package/getpackages-details")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if data == nil
            {
                var alert = UIAlertView(title: "Alert", message: "Data Not Found", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
            }
            else
                
            {
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    println(dicObj)
                    
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data not found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        dispatch_async(dispatch_get_main_queue(), {
                            spinningIndicator.hide(true)
                        })

                        
                    }
                    else
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var success = dicObj?.valueForKey("success") as! Bool
                            
                            if success
                            {
                                self.message = dicObj?.valueForKey("message") as! NSDictionary
                                
                                self.packageDetailArray = self.message.valueForKey("packageDatas") as! NSArray
                                
                               self.packGoOpenCollectionView.reloadData()
                                
                                println(self.packageDetailArray)
                                
                                println(self.message)
                                
                                spinningIndicator.hide(true)
                            }
                                
                            else
                            {
                                
                                var alert = UIAlertView(title: "Alert", message: "No Data Found", delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                
                                self.message = dicObj?.valueForKey("message") as! NSDictionary
                                println("failure message = \(self.message)")
                                
                                spinningIndicator.hide(true)
                                
                                
                            }
                            
                        })
                        
                    }
                }
                
            }
        })
        task.resume()

        
        
        
        
        
    }

}
